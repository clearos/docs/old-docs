This repo contains the old documentation project using Dokuwiki. For the new document site, use the docs-clearos-com repo.

To process documents, 

1.  create a new folder on the new document repo for the doc
2.  update any parent index associated with the folder
3.  copy the old document from the old-docs repo onto new document repo (this repo).
2.  update the document's dokuwiki code to use markdown format on this repo.
3.  copy any images associated in the document to this repo.
4.  move old document in the old-docs repo to the 'redir-pending' folder in the same old-docs repo.
5.  make a comment in the text of the old document to where the new document (ie. URL) resides using dokuwiki link tags on the last line
5.  move any associated images into the 'completed' folder.