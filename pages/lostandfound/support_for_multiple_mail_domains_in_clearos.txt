===== Does ClearOS Support Multiple Virtual Mail Domains? =====
Virtual mail domains are not supported.  

Just one point of clarification.  If you want to map multiple destination domains to your system, that is certainly supported.  For example, if you have two registered domains for your organization:

  * example.com
  * example.net

You can have both stanley@example.com and stanley@example.net go to the same stanley mailbox.

===== Why are Virtual Mail Domains Not Supported? =====
The mail system and all the various underlying software components would require significant changes.  With virtual mail domains, you would also need virtual domain support for:

  * Mail disclaimers
  * Antivirus policies
  * Antispam policies
  * Greylisting policies
  * Mail filter policies
  * Mail archive policies
  * Mail quarantine policies
  * Directory/User changes (ouch)
  * Global address book
  * Groupware

===== Now What? =====
In the age of virtualization, you might want to consider implementing a ClearOS system per virtual mail domain.  You will not only be able to  cleanly separate the mail features per domain, but also configure other policies as well:

  * Content filter policies
  * VPN policies
  * etc.
