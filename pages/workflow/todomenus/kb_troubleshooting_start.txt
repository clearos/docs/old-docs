===== Troubleshooting and Error Correction =====
This section deals with issues that you may experience under ClearOS. Fixing common problems and other useful information. This section is comprised of several past troubleshooting mechanisms and should be comprehensive.

===== Installation - Recovery - Support =====
  * [[Getting NICs and hardware detected in ClearOS]]
  * [[Installation Fails Asking to Insert the CD or other media|Installation Fails Asking to 'Insert the CD or other media']]
  * [[KeyError During Install]]
  * [[Recovery Scenarios Using the Rescue CD|Recovery Scenarios Using the Rescue Image]]
  * [[Remote Login Support Options for ClearCenter Technicians]]
  * [[Trouble Booting, Hard Disk Errors]]
  * [[Troubleshooting a Crashing Server]]
  * [[Moving a ClearOS System to New Hardware]]


===== Settings =====
  * [[Checking Your Disk Health with SmartMon]]
  * [[Running Manual Updates on ClearOS]]
  * [[Unable to Contact Remote Server - Account Synchronization]]
  * [[Understanding Performance Using System Load]]

===== Marketplace =====

===== Server =====
  * [[alock Package is Unstable]]
  * [[FTP Issues]]
  * [[OpenLDAP fails to start]]
  * [[You are accessing the server from an untrusted domain|Owncloud gives error - You are accessing the server from an untrusted domain]]
  * [[Re-initialize OpenLDAP directory]]
  * [[Resetting the MySQL Root Password]]
  * [[Trouble Initializing Directory]]

==== Windows Networking (Samba) ====
  * [[Desktop.ini Pops Up when Using Profiles|Desktop.ini Pops Up when Using Roaming Profiles]]
  * [[Windows Domain Errors]]

==== Zarafa ==== 
  * [[Date and Time are weird in Zarafa Webaccess]]
===== Gateway =====

==== Proxy and Content Filter ====
  * [[Checking Up on Squid]]
  * [[Diagnosing Broken Internet Sites when Using Content Filter or Proxy]]
  * [[Sites that have Trouble with the Proxy and or Content Filter|Sites that have Trouble with the Proxy and/or Content Filter]]

===== Network =====
  * [[Kernel- HTB- quantum of class 10001 is big. Consider r2q change.]]
  * [[OpenVPN works but Cannot Access Workstations-Servers Behind ClearOS]]
  * [[Troubleshooting Connectivity|Troubleshooting Connectivity (Advanced)]]
  * [[Troubleshooting Internet Connectivity|Troubleshooting Internet Connectivity (Basic)]]
  * [[Troubleshooting SIP Connectivity Across a ClearOS Gateway]]
  * [[Undetected or Misconfigured NICs]]

===== Cloud / Service Delivery Network (ClearSDN) =====
  * [[Troubleshooting Remote Backup Service]]

===== 3rd party =====
  * [[Security Audit Failures|Security Audit Failures / Penetration Test Fails]]

===== Help =====
==== Links ====
  * [[http://www.clearcenter.com/support/documentation/clearos_guides/start|Old ClearCenter Guides]]
  * [[http://w3w.clearfoundation.com/docs/|Old ClearFoundation Guides]]
  * [[http://www.clearos.com/clearfoundation/social/community-forums|ClearOS Forums]]
  * [[http://tracker.clearfoundation.com|ClearFoundation Bug Tracker]]

==== Navigation ====

[[:|ClearOS Documentation]] ... [[..:|Knowledgebase]]
{{keywords>clearos, clearos content, AppName, app_name, versionx, xcategory, maintainer_x, maintainerreview_x, titlefix, keywordfix}}
