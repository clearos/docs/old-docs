===== App Development =====
==== The Basics ====
  * [[Hello World]]
  * [[Packaging an App]]
  * [[Managing Daemons]]
  * [[Adding User or Device Limits]]
  * [[Creating Scripts]]

==== Source Code Management ====
ClearOS code is managed on GitHub.  The **[[Getting Started with ClearOS on GitHub]]** guide is the best place to start.

  * [[Getting Started with ClearOS on GitHub]]
  * [[Adding ClearOS Apps In GitHub]]
  * [[Contributing to ClearOS Apps In GitHub]]
  * [[Building ClearOS Apps from GitHub]]

==== Tips and Tricks ====
  * [[Performing a Webconfig Restart on Install]]

===== Core Development =====
The following documents are intended for the core ClearOS development team. 

  * [[Comprehensive Development Environment and Source]]
  * [[Framework Development]]
{{keywords>clearos, clearos content, dev, framework, tutorial, menu, maintainer_dloper}}
