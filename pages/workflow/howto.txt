===== Howtos =====
/**
 * This guide is dynamically updated. the following tags are used in the following sections:
 * howto clearosN categoryX
 */
This section contains helpful howtos for ClearOS and other topics related to ClearOS. It is organized according to ClearOS version since some guides are useful for multiple versions, those guides will appear as links in multiple sections. Articles specific to versions will appear only in the version section to which they are relevant.

===== Guides relevant to ClearOS 7 =====
==== System ====
  * [[:content:en_us:kb_howtos_accessing_the_command_line_from_a_remote_system|Accessing the Command Line from a Remote System]]
  * [[:content:en_us:kb_howtos_adding_server_certificate_to_client_workstations|Adding Server Certificate to Client Workstations]]
  * [[:content:en_us:kb_howtos_download_packages_from_yum_repository|Download Packages from YUM Repository]]
  * [[:content:en_us:kb_howtos_phpldapadmin|Setup PHP LDAP Admin]]
==== Server ====
  * [[:content:en_us:kb_howtos_add_linux_workstation_to_the_samba_domain|Add Linux Workstation to the Samba Domain]]
  * [[:content:en_us:kb_howtos_connect_jitsi_to_clearos_directory|Connect Jitsi to ClearOS]]
  * [[:content:en_us:kb_howtos_ejabberd|Setup eJabber]]
  * [[:content:en_us:kb_howtos_elasticsearch|ElasticSearch]]
  * [[:content:en_us:kb_howtos_installing_teamspeak_2_on_clearos|Team Speak 2]]
  * [[:content:en_us:kb_howtos_login_scripts_in_windows_networking|Login Scripts in Windows Networking]]
  * [[:content:en_us:kb_howtos_openemr|OpenEMR]]
  * [[:content:en_us:kb_howtos_piwik|Piwik Howto]]
  * [[:content:en_us:kb_howtos_smtp_authentication_to_isp|SMTP Authentication to ISP]]
==== Gateway ====
  * [[:content:en_us:kb_howtos_antimalware_and_antispam_gateway|Antimalware and Antispam Gateway]]
  * [[:content:en_us:kb_howtos_proxy_server_auto-detection|Proxy Server Auto-Detection]]
==== Network ====
  * [[:content:en_us:kb_howtos_network_bonding|Network Bonding]]
==== ClearBOX ====
  * [[:content:en_us:kb_howtos_configuring_high_availability|Configuring High Availability]]
==== Cloud and Service Delivery Network ====
  * [[:content:en_us:kb_howtos_creating_a_clearfoundation_mirror|Creating a Clearfoundation Mirror]]
==== Other ====
  * [[:content:en_us:kb_howtos_building_custom_kernel_modules|Building Custom Kernel Modules]]
  * [[:content:en_us:kb_howtos_getting_started_with_vi|Getting Started with vi]]

===== Guides relevant to ClearOS 6 =====
==== System ====
  * [[:content:en_us:kb_howtos_accessing_the_command_line_from_a_remote_system|Accessing the Command Line from a Remote System]]
  * [[:content:en_us:kb_howtos_adding_a_user_account_from_the_command_line|Adding a User Account from the Command Line]]
  * [[:content:en_us:kb_howtos_adding_server_certificate_to_client_workstations|Adding Server Certificate to Client Workstations]]
  * [[:content:en_us:kb_howtos_change_standalone_mode_to_master_without_losing_directory|Change Standalone Mode to Master Without Losing Directory]]
  * [[:content:en_us:kb_howtos_download_packages_from_yum_repository|Download Packages from YUM Repository]]
  * [[:content:en_us:kb_howtos_phpldapadmin|Setup PHP LDAP Admin]]
  * [[:content:en_us:kb_howtos_promoting_your_replicate_to_a_master|Promoting your Replicate to a Master]]
==== Server ====
  * [[:content:en_us:kb_howtos_add_linux_workstation_to_the_samba_domain|Add Linux Workstation to the Samba Domain]]
  * [[:content:en_us:kb_howtos_connect_jitsi_to_clearos_directory|Connect Jitsi to ClearOS]]
  * [[:content:en_us:kb_howtos_egroupware|Setup eGroupware]]
  * [[:content:en_us:kb_howtos_ejabberd|Setup eJabber]]
  * [[:content:en_us:kb_howtos_elasticsearch|ElasticSearch]]
  * [[:content:en_us:kb_howtos_fail2ban|Setup Fail2Ban]]
  * [[:content:en_us:kb_howtos_flexshare_setup_to_list_a_subdomain|Flexshare Setup to List a SubDomain]]
  * [[:content:en_us:kb_howtos_freeswitch_with_sipxecs|Freeswitch with sipXecs]]
  * [[:content:en_us:kb_howtos_installing_printers|Installing Pinters on the ClearOS server]]
  * [[:content:en_us:kb_howtos_installing_teamspeak_2_on_clearos|Team Speak 2]]
  * [[:content:en_us:kb_howtos_login_scripts_in_windows_networking|Login Scripts in Windows Networking]]
  * [[:content:en_us:kb_howtos_openemr|OpenEMR]]
  * [[:content:en_us:kb_howtos_piwik|Piwik Howto]]
  * [[:content:en_us:kb_howtos_setting_up_a_user_named_administrator|Setting up a User Named Administrator]]
  * [[:content:en_us:kb_howtos_smtp_authentication_to_isp|SMTP Authentication to ISP]]
  * [[:content:en_us:kb_howtos_teamspeak_2_and_teamspeak_3_to_clearos|Teamspeak 2 and Teamspeak 3 on ClearOS]]
==== Gateway ====
  * [[:content:en_us:kb_howtos_antimalware_and_antispam_gateway|Antimalware and Antispam Gateway]]
  * [[:content:en_us:kb_howtos_captive_portal_with_dansguardian|Captive Portal with Dansguardian]]
  * [[:content:en_us:kb_howtos_non-transparent_proxy_and_content_filter_bypass|Non-Transparent Proxy and Content Filter Bypass]]
  * [[:content:en_us:kb_howtos_proxy_server_auto-detection|Proxy Server Auto-Detection]]
==== Network ====
  * [[:content:en_us:kb_howtos_bandwidth_reporting_with_ntop|Install NTOP for Network Traffic Monitoring]]
  * [[:content:en_us:kb_howtos_network_bonding|Network Bonding]]
  * [[:content:en_us:kb_howtos_network_bridging|Network Bridging]]
  * [[:content:en_us:kb_howtos_pppoe_server|PPPoE Server]]
==== ClearBOX ====
  * [[:content:en_us:kb_howtos_configuring_high_availability|Configuring High Availability]]
==== Cloud and Service Delivery Network ====
  * [[:content:en_us:kb_howtos_creating_a_clearfoundation_mirror|Creating a Clearfoundation Mirror]]
==== Other ====
  * [[:content:en_us:kb_howtos_building_custom_kernel_modules|Building Custom Kernel Modules]]
  * [[:content:en_us:kb_howtos_clearos_on_a_laptop|ClearOS on a Laptop]]
  * [[:content:en_us:kb_howtos_connect_thunderbird_to_clearos_directory|Connect Thunderbird addressbook to ClearOS Directory]]
  * [[:content:en_us:kb_howtos_drobo|Drobo]]
  * [[:content:en_us:kb_howtos_getting_started_with_vi|Getting Started with vi]]
  * [[:content:en_us:kb_howtos_gnome|Gnome]]
  * [[:content:en_us:kb_howtos_installing_via_pxe_enabled_network_card|PXE Install and Rescue Server]]

===== Guides relevant to ClearOS 5 =====
==== System ====
  * [[:content:en_us:kb_howtos_accessing_the_command_line_from_a_remote_system|Accessing the Command Line from a Remote System]]
  * [[:content:en_us:kb_howtos_adding_a_user_account_from_the_command_line|Adding a User Account from the Command Line]]
  * [[:content:en_us:kb_howtos_adding_server_certificate_to_client_workstations|Adding Server Certificate to Client Workstations]]
  * [[:content:en_us:kb_howtos_adding_shell_access_for_a_user|Adding Shell Access for a User]]
  * [[:content:en_us:kb_howtos_adding_tim_s_repo|Tim's Repo]]
  * [[:content:en_us:kb_howtos_configuring_vmware_on_clearos_5.2|Configuring VMware on ClearOS 5.2]]
  * [[:content:en_us:kb_howtos_installing_clearos_with_lvm_and_raid|Installing ClearOS with LVM and Raid]]
==== Server ====
  * [[:content:en_us:kb_howtos_add_linux_workstation_to_the_samba_domain|Add Linux Workstation to the Samba Domain]]
  * [[:content:en_us:kb_howtos_installing_printers|Installing Pinters on the ClearOS server]]
  * [[:content:en_us:kb_howtos_login_scripts_in_windows_networking|Login Scripts in Windows Networking]]
  * [[:content:en_us:kb_howtos_setting_up_a_user_named_administrator|Setting up a User Named Administrator]]
  * [[:content:en_us:kb_howtos_setting_up_radius_to_use_ldap|Setting up Radius to Use LDAP]]
  * [[:content:en_us:kb_howtos_smtp_authentication_to_isp|SMTP Authentication to ISP]]
  * [[:content:en_us:kb_howtos_time_machine|Time Machine]]
==== Gateway ====
  * [[:content:en_us:kb_howtos_antimalware_and_antispam_gateway|Antimalware and Antispam Gateway]]
  * [[:content:en_us:kb_howtos_captive_portal|Captive Portal with CoovaChilli]]
  * [[:content:en_us:kb_howtos_captive_portal_with_dansguardian|Captive Portal with Dansguardian]]
==== Network ====
  * [[:content:en_us:kb_howtos_bandwidth_reporting_with_ntop|Install NTOP for Network Traffic Monitoring]]
  * [[:content:en_us:kb_howtos_manually_editing_firewall_rules|Manually Editing Firewall Rules in ClearOS 5.x]]
  * [[:content:en_us:kb_howtos_network_bonding|Network Bonding]]
  * [[:content:en_us:kb_howtos_network_bridging|Network Bridging]]
==== ClearBOX ====
  * [[:content:en_us:kb_howtos_configuring_high_availability|Configuring High Availability]]
==== Cloud and Service Delivery Network ====
  * [[:content:en_us:kb_howtos_creating_a_local_mirror|Creating a Local Mirror]]
==== Other ====
  * [[:content:en_us:kb_howtos_building_custom_kernel_modules|Building Custom Kernel Modules]]
  * [[:content:en_us:kb_howtos_clearos_on_a_laptop|ClearOS on a Laptop]]
  * [[:content:en_us:kb_howtos_getting_started_with_vi|Getting Started with vi]]
  * [[:content:en_us:kb_howtos_installing_via_pxe_enabled_network_card|PXE Install and Rescue Server]]

