===== Development Framework =====
/**
 * This guide is dynamically updated. the following tags are used in the following sections:
 * dev framework bigpicture
 * dev framework mvc
 */
The ClearOS App development framework not only simplifies the development process, but also provides the foundation for the following important design attributes:

  * Rapid development 
  * Secure architecture
  * Extensibility
  * REST-like APIs
  * Localization

The following are reference documents.  If you are new to ClearOS development, you may want to start with the [[..:Tutorials:start|tutorials]].

===== Big Picture =====
  * [[:content:en_us:dev_framework_reference_guide_app_development_components|App Development Components]]
  * [[:content:en_us:dev_framework_reference_guide_file_system_layout|File System Layout]]

===== MVC =====
==== Libraries ====
  * [[:content:en_us:dev_framework_reference_guide_libraries|Libraries]]

==== Views ====
  * [[:content:en_us:dev_framework_reference_guide_crud_view|Crud View]]
  * [[:content:en_us:dev_framework_reference_guide_form_view|Form View]]
  * [[:content:en_us:dev_framework_reference_guide_views|Views]]
  * [[Advanced View]] TBD

==== Controllers ====
  * [[:content:en_us:dev_framework_reference_guide_controllers|Controllers]]
  * [[:content:en_us:dev_framework_reference_guide_crud_controller|Crud Controller]]
  * [[:content:en_us:dev_framework_reference_guide_form_controller|Form Controller=====]]
  * [[Advanced Controller]] TBD 

===== Helpers =====
  * [[:content:en_us:dev_framework_reference_guide_language|Language]]
  * [[Htdocs]] (Images and Ajax) TBD
  * [[Tests]] TBD

===== Widgets =====
  * [[:content:en_us:dev_framework_reference_guide_button_sets|Button Sets]]
  * [[:content:en_us:dev_framework_reference_guide_buttons|Buttons]]
  * [[:content:en_us:dev_framework_reference_guide_forms|Forms]]
  * [[Page]] TBD
  * [[Input Fields]] TBD
  * [[Select Boxes]] TBD
  * [[List Table]] TBD
  * [[Summary Table]] TBD

===== Marketplace =====
  * [[Packaging]] TBD
  * [[Metadata]] TBD

===== Other Topics =====
  * [[:content:en_us:dev_framework_reference_guide_field_class_-_why|Field Class - Why]]
  * [[:content:en_us:dev_framework_reference_guide_id_handling|ID Handling]]
  * [[:content:en_us:dev_framework_reference_guide_input_validation|Input Validation]]
  * [[:content:en_us:dev_framework_reference_guide_stateless_libraries|Stateless Libraries]]
  * [[REST-like API]] TBD

{{keywords>clearos, clearos content, dev, framework, menu, index}}
