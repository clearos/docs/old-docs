===== ClearOS 6 User Guide =====
/**
 * This guide is dynamically updated. the following tags are used in the following sections:
 * userguide clearos6 categoryinstallation subcategorypreparation
 * userguide clearos6 categoryinstallation subcategoryinstaller subsubcategoryfullinstalliso
 * userguide clearos6 categoryinstallation subcategoryinstaller subsubcategoryvirtualmachines
 * userguide clearos6 categoryinstallation subcategoryinstaller subsubcategorycloud
 * userguide clearos6 categoryinstallation subcategoryfirstboot
 * userguide clearos6 categoryinstallation subcategorymarketplace
 * userguide clearos6 categoryinstallation subcategoryinstallationappendix
 * userguide clearos6 categoryserver subcategorydatabase
 * userguide clearos6 categoryserver subcategorydirectory
 * userguide clearos6 categoryserver subcategoryfileandprint
 * userguide clearos6 categoryserver subcategorymail
 * userguide clearos6 categoryserver subcategorymessagingandcollaboration
 * userguide clearos6 categoryserver subcategoryweb
 * userguide clearos6 categorynetwork subcategorybandwidthandqos
 * userguide clearos6 categorynetwork subcategoryfirewall
 * userguide clearos6 categorynetwork subcategoryinfrastructure
 * userguide clearos6 categorynetwork subcategoryvpn
 * userguide clearos6 categorynetwork subcategorysettings
 * userguide clearos6 categorygateway subcategoryantimalware
 * userguide clearos6 categorygateway subcategorycontentfilterandproxy
 * userguide clearos6 categorygateway subcategoryintrusionprotection
 * userguide clearos6 categorygateway subcategoryprotocolfilter
 * userguide clearos6 categorysystem subcategoryaccounts
 * userguide clearos6 categorysystem subcategoryaccountmanager
 * userguide clearos6 categorysystem subcategorybackup
 * userguide clearos6 categorysystem subcategorydeveloper
 * userguide clearos6 categorysystem subcategoryoperatingsystem
 * userguide clearos6 categorysystem subcategoryresources
 * userguide clearos6 categorysystem subcategorysecurity
 * userguide clearos6 categorysystem subcategorystorage
 * userguide clearos6 categorysystem subcategorysettings
 * userguide clearos6 categoryreports subcategorygateway
 * userguide clearos6 categoryreports subcategorynetwork
 * userguide clearos6 categoryreports subcategorysystem
 */
This guide will help you install and configure ClearOS 6.

===== Installation =====
==== Preparation ====
  * [[:content:en_us:6_a_what_is_clearos|What is ClearOS?]]
  * [[:content:en_us:6_b_system_requirements|System Requirements]]
  * [[:content:en_us:6_c_raid_support|RAID Support Overview]]
  * [[:content:en_us:6_d_compatibility|Compatibility]]
  * [[:content:en_us:6_e_downloading|Downloading ClearOS]]
==== Installer ====
=== Full Install - ISO ===
  * [[:content:en_us:6_a_starting_the_install|Starting the Install]]
  * [[:content:en_us:6_b_installation_wizard|Installation Wizard]]
  * [[:content:en_us:6_boot_loader|Manual GRUB install of Bootloader]]
  * [[:content:en_us:6_c_configuring_partitions_and_raid|Configuring Partitions and RAID]]
  * [[:content:en_us:6_deleting_partitions|Deleting Partitions]]
  * [[:content:en_us:6_d_raid_0_-_striping|RAID 0 - Striping]]
  * [[:content:en_us:6_d_raid_1_-_mirroring|RAID 1 - Mirroring]]
  * [[:content:en_us:6_d_raid_5_-_redundancy|RAID 5 - Striping with Parity]]
  * [[:content:en_us:6_d_raid_6_-_extra_redundancy|RAID 6 - Striping with two parity drives]]
  * [[:content:en_us:6_e_installer_troubleshooting|Installer Troubleshooting]]
  * [[:content:en_us:6_vmware_enterprise|ClearOS on VMWare Enterprise]]
=== Virtual Machines ===
  * [[:content:en_us:6_virtualpc|ClearOS on Virtual PC]]
  * [[:content:en_us:6_vmware_basic|ClearOS on VMWare Basic]]
=== Cloud ===
  * [[:content:en_us:6_amazon_ec2|Amazon EC2]]
  * [[:content:en_us:6_linode|Linode]]
==== First Boot ====
  * [[:content:en_us:6_first_boot_wizard|First Boot Configuration Wizard]]
  * [[:content:en_us:6_network_console|Network Console]]
  * [[:content:en_us:6_webconfig|Webconfig]]
==== Marketplace ====
  * [[:content:en_us:6_marketplace|Marketplace]]
==== Installation Appendix ====
  * [[:content:en_us:6_command_line|Connecting to the Secure Shell]]
  * [[:content:en_us:6_network_notation|Network Notation]]

===== Server =====
==== Backup ====
  * [[:content:en_us:6_backuppc|BackupPC]]
  * [[:content:en_us:6_bmbackup|BareMetal Backup]]
==== Database ====
==== Directory ====
  * [[:content:en_us:6_active_directory_connector|Active Directory Connector]]
==== File and Print ====
  * [[:content:en_us:6_advanced_print_server|Advanced Print Server]]
==== Mail ====
  * [[:content:en_us:6_antispam_updates|Antispam Updates]]
  * [[:content:en_us:6_greylisting|Greylisting]]
==== Messaging and Collaboration ====
  * [[:content:en_us:6_google_apps_synchronization|Google Apps Synchronization]]
==== Web ====

===== Network =====
==== Bandwidth and QoS ====
  * [[:content:en_us:6_bandwidth_manager|Bandwidth Manager]]
==== Firewall ====
  * [[:content:en_us:6_1_to_1_nat|1-to-1 NAT]]
  * [[:content:en_us:6_custom_firewall|Custom Firewall]]
  * [[:content:en_us:6_dmz_firewall|DMZ Firewall]]
  * [[:content:en_us:6_egress_firewall|Egress Firewall]]
  * [[:content:en_us:6_firewall_groups|Firewall Groups]]
  * [[:content:en_us:6_incoming_firewall|Incoming Firewall]]
==== Infrastructure ====
  * [[:content:en_us:6_dhcp_server|DHCP Server]]
  * [[:content:en_us:6_dns_server|DNS Server]]
==== VPN ====
  * [[:content:en_us:6_dynamic_vpn|Dynamic VPN]]
  * [[:content:en_us:6_ibvpn|ibVPN]]
==== Settings ====
  * [[:content:en_us:6_dynamic_dns|Dynamic DNS]]
  * [[:content:en_us:6_ether_wake|Ether Wake]]

===== Gateway =====
==== Anti-Malware ====
  * [[:content:en_us:6_antimalware_updates|Antimalware Updates]]
  * [[:content:en_us:6_antiphishing|Antiphishing]]
  * [[:content:en_us:6_gateway_antimalware_premium_powered_by_kaspersky|Gateway Antimalware Premium Powered by Kaspersky]]
  * [[:content:en_us:6_gateway_antiphishing|Gateway Antiphishing]]
  * [[:content:en_us:6_gateway_antivirus|Gateway Antivirus]]
==== Content Filter and Proxy ====
  * [[:content:en_us:6_content_filter|Content Filter]]
  * [[:content:en_us:6_content_filter_updates|Content Filter Updates]]
==== Intrusion Protection ====
  * [[:content:en_us:6_intrusion_detection|Intrusion Detection]]
==== Protocol Filter ====

===== System =====
==== Accounts ====
  * [[:content:en_us:6_administrators|Administrators]]
  * [[:content:en_us:6_groups|Groups]]
==== Account Manager ====
  * [[:content:en_us:6_account_import|Account Import]]
  * [[:content:en_us:6_account_manager|Account Manager]]
==== Backup ====
  * [[:content:en_us:6_configuration_backup|Configuration Backup]]
==== Developer ====
  * [[:content:en_us:6_developer_tools|Developer Tools]]
==== Operating System ====
  * [[:content:en_us:6_amazon_ec2_support|Amazon EC2 Support]]
==== Resources ====
==== Security ====
  * [[:content:en_us:6_certificate_manager|Certificate Manager]]
==== Storage ====
  * [[:content:en_us:6_disk_usage|Disk Usage]]
  * [[:content:en_us:6_encrypted_file_system|Encrypted File System]]
==== Settings ====
  * [[:content:en_us:6_account_synchronization|Account Synchronization]]
  * [[:content:en_us:6_date_and_time|Date and Time]]

===== Reports =====
==== Gateway ====
==== Network ====
  * [[:content:en_us:6_bandwidth_viewer|Bandwidth Viewer]]
==== System ====

===== Cloud =====
  * [[:content:en_us:6_amazon_ec2|Amazon EC2]]
  * [[:content:en_us:6_appfirst|AppFirst]]
  * [[:content:en_us:6_linode|Linode]]

===== My Account =====

===== Spotlight =====
  * [[:content:en_us:6_dashboard|Dashboard]]

