===== Setup PHP LDAP Admin =====
According to http://phpldapadmin.sourceforge.net: "phpLDAPadmin (also known as PLA) is a web-based LDAP client. It provides easy, anywhere-accessible, multi-language administration for your LDAP server."


==== How to install ====
<code>
yum install --enablerepo=clearos-core phpldapadmin
</code>

   * Todo: Make sure to keep security very high (ex.: only access from a certain IP or if VPNing in)


==== Related links ====
   * http://tracker.clearfoundation.com/view.php?id=1874

{{keywords>clearos, clearos content, howtos, skunkworks, app-directory, clearos6, clearos7, categorysystem, maintainer_dloper}}
