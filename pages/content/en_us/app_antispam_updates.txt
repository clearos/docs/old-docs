{{ :userguides:antispam_updates.svg?80}}
<WRAP clear></WRAP>

===== Antispam Rules =====
A typical organization with 20 employees could lose upwards of $4,000 / year in lost productivity sifting through and filtering out spam that made it to their inboxes.  This app ensures the antispam engine is kept up-to-date with the latest techniques used by spammers, reducing the amount of junk mail and providing a return on investment many times over.

The ClearCenter Antispam Updates service provides daily signature updates in addition to those that are freely available from the community.  These signatures are compiled from third party organizations as well as internal engineering resources from ClearCenter.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/cloud/antispam_updates|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_antispam_updates|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_antispam_updates|here]].
==== Additional Notes ====
