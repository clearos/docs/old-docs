{{ :userguides:ether_wake.svg?80}}
<WRAP clear></WRAP>

===== Wake on LAN =====
A tool to send a Wake-On-LAN Magic Packet with the additional ability to program/set scheduled wake-ups.  This app is a wrapper for the ether-wake command line tool.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/network/ether_wake|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_ether_wake|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_ether_wake|here]].
==== Additional Notes ====
