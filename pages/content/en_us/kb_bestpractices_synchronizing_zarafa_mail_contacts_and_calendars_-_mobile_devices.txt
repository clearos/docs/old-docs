===== Synchronizing Zarafa Mail Contacts and Calenders - Mobile Devices =====
Zarafa provides support for synchronizing mail, contacts and calendars right out of the box for both iPhone/iOS and Android devices.  

===== Configuration on ClearOS =====
The [[http://z-push.sourceforge.net/soswp/|Z-push]] technology used for synchronizing mobile devices is included on every ClearOS Zarafa install.  In order to use it, please make sure access to TCP port 80 is available on [[:content:en_us:6_incoming_firewall|Incoming Firewall]] page.  To verify that it is working, use your web browser to test it.  In your web browser, type the following web address:

  http://[your-server-ip]/Microsoft-Server-ActiveSync
  
You should see an authentication dialog and your username and password should allow you access to a simple web page. 

===== Configuring Mobile Devices =====
The following videos provide a walkthrough for configuring iPhone/iOS and Android devices:

  * [[http://www.youtube.com/watch?v=jv42BawRTLY|Video Walkthrough for Android]]
  * [[http://www.youtube.com/watch?v=MLaISaJ8kiQ|Video Walkthrough for iPhone/iOS]]

===== Links =====
  * [[:content:en_us:6_zarafa_small_business_for_clearos|Zarafa Small Business For ClearOS]]
  * [[:content:en_us:6_zarafa_professional_for_clearos|Zarafa Professional For ClearOS]]
  * [[:content:en_us:6_zarafa_community_for_clearos|Zarafa Community For ClearOS]]
  * [[http://www.zarafa.com/content/mobility#zpush|Zarafa for ClearOS Mobility]]
{{keywords>clearos, clearos content, kb, bestpractices, maintainer_dloper, maintainerreview_x, keywordfix}}
