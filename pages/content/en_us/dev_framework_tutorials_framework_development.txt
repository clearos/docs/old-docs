===== Tutorials Framework Development =====
By default, the ClearOS development environment uses the latest stable release of the framework and theme.  From time to time, you may find that you need to use a bleeding edge feature that requires the development version.  This document describes how to configure such an environment.

===== Webconfig Configuration =====
In order to enable the development version of the framework, you need to change the some lines in the Apache configlet file  /usr/clearos/sandbox/etc/httpd/conf.d/devel.conf:

  * Change the DocumentRoot to point to the SVN/git version of the framework
  * Set the CLEAROS_BOOTSTRAP environment variable

Here is an example to use as a guideline:

  <VirtualHost _default_:1501>
    # DocumentRoot /usr/clearos/framework/htdocs   <-- comment out
    SetEnv CLEAROS_CONFIG /home/devuser/.clearos
    
    # For framework development only
    DocumentRoot /home/devuser/framework/htdocs
    SetEnv CLEAROS_BOOTSTRAP /home/devuser/framework/shared
    
    ... snip ...


Please don't forget to change **devuser** to your own username, and to change the path to the location of the git/SVN version of the framework.  Restart **webconfig** and you should see the framework status at the top of a webconfig page change to **Development**.  

{{:developer:framework:tutorials:devel_status_in_theme_framework.png|ClearOS Development Status}}

===== CLI Configuration ======
For command line scripts, the CLEAROS_BOOTSTRAP environment variable is used.  In your ~/.bash_profile, add the following:

   CLEAROS_BOOTSTRAP=/home/devuser/clearos/webconfig/framework/trunk/shared
   export CLEAROS_BOOTSTRAP

Again, please don't forget to change **devuser** to your own username, and to change the path to the location of the git/SVN version of the framework!

===== Links =====
  * [[:content:en_us:dev_framework_tutorials_contributing_to_clearos_apps_in_github|Contributing To ClearOS Apps in Github]]
{{keywords>clearos, clearos content, dev, framework, maintainer_dloper, maintainerreview_x, keywordfix}}
