===== Facilitating Fast Takeovers with Failover Configuration =====
This guide will help you to facilitate quick takeovers by your backup server when using ClearBOX's bypass segment technology in a mission critical environment.

===== Requirements =====
This guide requires ClearBOX 100 or 300. Your backup server should be configured in bypass mode and should have the same addresses assigned for the NICS on the server.

===== MAC Address identity =====
The primary server's MAC address is should be identified and then mimicked on the backup. Find out your primary server's MAC address by performing the following:

  ifconfig eth0

For example, your results may indicate the following similar output:

  eth0      Link encap:Ethernet  HWaddr 00:90:FB:12:34:56

===== Spoofing the address on the backup =====
To allocate this address to the eth0 NIC on your backup server, modify the following file:

  vi /etc/sysconfig/network-scripts/ifcfg-eth0

Make changes by commenting out the HWADDR line and adding the other MAC address as a MACADDR value. For example:

<code>
DEVICE=eth0
TYPE="Ethernet"
ONBOOT="yes"
USERCTL="no"
#HWADDR="00:90:fb:00:11:22"
MACADDR="00:90:FB:12:34:56"
BOOTPROTO="static"
IPADDR="10.1.1.10"
NETMASK="255.255.255.0"
GATEWAY="10.1.1.1"
{{keywords>clearos, clearos content, AppName, app_name, kb, howto, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
