===== Yum-Utils =====
A collection of utilities for the yum software installer.

===== Changes =====
  * The flag (keep_non_rhn) that disables a number of utilities incompatible with the Red Hat Network is disabled.
{{keywords>clearos, clearos content, AppName, app_name, version6, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
