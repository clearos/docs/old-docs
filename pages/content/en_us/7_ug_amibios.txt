===== AMIBIOS =====
The AMIBIOS app for ClearOS gives you BIOS management access for your HPE Gen 10 Microserver on which it is installed. This app is supported only for HPE Gen 10 Microserver.

Once installed, it will allow you to backup the BIOS, upgrade the BIOS, and curate BIOS ROMs that used for your system. This app is particularly useful to individuals and businesses that support many HPE Microserver Gen10 servers.

<note warning>Modifying your BIOS firmware must be done in careful ways. Power failures, or attempts to update the BIOS while a previous BIOS update is running or other conditions can cause your system to become inoperable. Please use with caution.</note>

===== Features =====
The main features are:
  * View and manage the BIOS versions on your HPE Microserver Gen10
  * Backup current running BIOS
  * Install new BIOS remotely
  * Program is Free and Open Source
  * Runs on ClearOS, downloadable from Marketplace
  * Works with other ClearOS apps to provide additional security layers to your BIOS Management Environment
  * Works with ClearAPI driver to display information in ClearGLASS
  * Manage/update/backup BIOS of multiple servers via ClearGLASS

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Server | Management | AMIBIOS</navigation>

===== Configuration =====
{{:content:en_us:amibios1.png?550|AMIBIOS Firmware Information}}

After your app is installed and any time you use the app, you should 'Scan' or 'Rescan' the system to validate the Firmware.

{{:content:en_us:amibios3.png?550|AMIBIOS Scanning}}

From this main page, you can Install Firmware located on your system. If you don't have any listed, upload your first one.

{{:content:en_us:amibios2.png?550|AMIBIOS Uploaded Firmware}}

Only official ROMs from HPE are supported. Click 'Add Zip Package' to upload the HPE package. Once added, it will allow you to apply the ROM inside.

{{:content:en_us:amibios4.png?550|AMIBIOS}}

<note>The code that actually updates the BIOS is contained in the zip file. You will not have any options to upload ROMs or apply them until you extract your first one.</note>

===== Related Links =====
  * [[HPE Firmware | https://support.hpe.com/hpsc/doc/public/display?docId=c04044353]]

{{keywords>clearos, clearos content, HPE, Microserver Gen10, AMIBIOS, app-amibios, clearos7, userguide, categoryserver, subcategorymanagement, maintainer_dloper}}