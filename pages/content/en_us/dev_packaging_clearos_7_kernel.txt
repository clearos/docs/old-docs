===== Kernel =====
The heart and soul of the Linux system.

===== Patches =====
  * IMQ patch (bandwidth shaping)
  * Multi-WAN routing enhancement patch
{{keywords>clearos, clearos content, dev, packaging, version7, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
