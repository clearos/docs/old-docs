====== Using Let's Encrypt Certificates for Mail ======
===== Background =====

There is a Let's Encrypt app which enables you to get free SSL certificates for use on your Web Sites or for use in the Webconfig. With very minor changes it is possible to use a [[content:en_us:7_ug_lets_encrypt|Let's Encrypt certificate]] in the mail apps - SMTP server (postfix), POP and IMAP server (cyrus-imapd), Zarafa and Kopano.

Let's Encrypt maintains four symlinks for each certificate requested:
=== /etc/letsencrypt/live/your_FQDN/cert.pem ===
This is the basic certificate
=== /etc/letsencrypt/live/your_FQDN/chain.pem ===
The chain file, also known as the intermediate certificate links the cert.pem back to the Let's Encrypt root CA (which is not held in the Let's Encrypt app)
=== /etc/letsencrypt/live/your_FQDN/fullchain.pem ===
This is a combination of the chain.pem file and cert.pem file
=== /etc/letsencrypt/live/your_FQDN/privkey.pem ===
This is your private key for your certificate

The Let's Encrypt app keeps these symlinks always pointing to the latest certificate files under <nowiki>/etc/letsencrypt/archive/your_FQDN</nowiki>

===== Requirements =====

Before you can make these changes, you must already have Let's Encrypt certificates which cover the (sub-)domain(s) you are going to use for your e-mail server. As an idea, typical subdomains are smtp.example.com, pop.example.com, imap.example.com and mail.example.com but they do not have to be one of those. You can even just use example.com. You can have separate domains for the incoming and outgoing servers or just a single domain for both.

For the Howto I will assume smtp.example.com and imap.example.com are the subdomains. I also assume that the certificates obtained were for the Primary Domains of smtp.example.com and imap.example.com. If the certificate was for a different Primary Domain and smtp.example.com and imap.example.com were listed as Other Domains when creating the certificates, use the Primary Domain name in the configurations below instead.

===== Configuration =====
<note warning>
The HowTo was originally written using the "mail" group. There is no problem with this set up, but in order to extend the use of Let's Encrypt certificates to other apps, the HowTo has been changed to use the "ssl-cert" group
</note>
==== Let's Encrypt ====
<note important>Changing permissions and ownership is a common step for all the apps below</note>
Fix permissions and ownership on the Let's Encrypt folders:
<code>
chown root:ssl-cert /etc/letsencrypt/live /etc/letsencrypt/archive
chmod 0750 /etc/letsencrypt/live /etc/letsencrypt/archive
</code>

==== SMTP Server (postfix): ====

Add the following to /etc/postfix/main.cf:
<code>
smtpd_tls_CAfile = /etc/pki/tls/certs/ca-bundle.crt
smtpd_tls_cert_file = /etc/letsencrypt/live/smtp.example.com/fullchain.pem
smtpd_tls_key_file = /etc/letsencrypt/live/smtp.example.com/privkey.pem
</code>

Add the postfix user to the ssl-cert group:
<code>
usermod -a -G ssl-cert postfix
</code>

Then restart postfix:
<code>
systemctl restart postfix.service
</code>

Now to force postfix to restart to read the new certificates each time they are updated by creating a file /var/clearos/events/lets_encrypt/postfix with 0755 permission and the following contents:
<code>
#!/bin/sh

sleep 10

systemctl condrestart postfix.service
</code>

If you copy and paste the following into a terminal this will create the file correctly for you:
<code>
echo '#!/bin/sh' > /var/clearos/events/lets_encrypt/postfix
echo >> /var/clearos/events/lets_encrypt/postfix
echo 'sleep 10' >> /var/clearos/events/lets_encrypt/postfix
echo >> /var/clearos/events/lets_encrypt/postfix
echo 'systemctl condrestart postfix.service' >> /var/clearos/events/lets_encrypt/postfix
chmod 0755 /var/clearos/events/lets_encrypt/postfix
</code>

==== POP and IMAP Server (cyrus-imapd): ====

Change the following in /etc/imapd.conf:
<code>
tls_key_file:    /etc/letsencrypt/live/imap.example.com/privkey.pem
tls_cert_file:   /etc/letsencrypt/live/imap.example.com/fullchain.pem
</code>

Add the cyrus user to the ssl-cert group:
<code>
usermod -a -G ssl-cert cyrus
</code>

Then restart cyrus-imapd:
<code>
systemctl restart cyrus-imapd.service
</code>

Now to force cyrus-imapd to restart to read the new certificates each time they are updated by creating a file /var/clearos/events/lets_encrypt/cyrus-imapd with 0755 permission and the following contents:

<code>
#!/bin/sh

sleep 10

systemctl condrestart cyrus-imapd.service
</code>

If you copy and paste the following into a terminal this will create the file correctly for you:
<code>
echo '#!/bin/sh' > /var/clearos/events/lets_encrypt/cyrus-imapd
echo >> /var/clearos/events/lets_encrypt/cyrus-imapd
echo 'sleep 10' >> /var/clearos/events/lets_encrypt/cyrus-imapd
echo >> /var/clearos/events/lets_encrypt/cyrus-imapd
echo 'systemctl condrestart cyrus-imapd.service' >> /var/clearos/events/lets_encrypt/cyrus-imapd
chmod 0755 /var/clearos/events/lets_encrypt/cyrus-imapd
</code>

==== Zarafa ====
Zarafa Webapp uses the Apache web server.  This makes configuring Let's Encrypt certificates trivial and can be done through the Webconfig interface.  Navigate to Server --> Web Server and configure your default website, ensuring you select your Let's Encrypt certificate that will be used to access Zarafa webapp (eg. https://mail.example.com/webapp).

Since Apache also hosts the z-push (Active Sync), configuring your web server to use these certificates will extend out to this service as well without additional changes.

== POP3S/IMAPS ==
In order to use the Let's Encrypt certificates when connecting via POP3S or IMAPS from mail clients, you will need to make some changes to the following file:  /etc/zarafa/gateway.cfg

<code>
ssl_private_key_file = /etc/letsencrypt/live/example.com/privkey.pem
ssl_certificate_file = /etc/letsencrypt/live/example.com/fullchain.pem
</code>

Add the zarafa user to the ssl-cert group:
<code>
usermod -a -G ssl-cert zarafa
</code>

Once done, be sure to restart Zarafa Gateway service:
<code>
systemctl restart zarafa-gateway.service
</code>

Now to force zarafa-gateway to restart to read the new certificates each time they are updated by creating a file /var/clearos/events/lets_encrypt/zarafa-gateway with 0755 permission and the following contents:

<code>
#!/bin/sh

sleep 10

systemctl condrestart zarafa-gateway.service
</code>

If you copy and paste the following into a terminal this will create the file correctly for you:
<code>
echo '#!/bin/sh' > /var/clearos/events/lets_encrypt/zarafa-gateway
echo >> /var/clearos/events/lets_encrypt/zarafa-gateway
echo 'sleep 10' >> /var/clearos/events/lets_encrypt/zarafa-gateway
echo >> /var/clearos/events/lets_encrypt/zarafa-gateway
echo 'systemctl condrestart cyrus-imapd.service' >> /var/clearos/events/lets_encrypt/zarafa-gateway
chmod 0755 /var/clearos/events/lets_encrypt/zarafa-gateway
</code>

==== Kopano ====
The Kopano instructions are identical to the above Zarafa changes necessary, except, of course, with the Zarafa path and service names replace with Kopano.

===== References =====
[[content:en_us:7_ug_lets_encrypt|Let's Encrypt Certificate Manager]]
{{keywords>clearos, clearos7, mail, email, postfix, smtp, cyrus-imapd, cyrus, imap, pop, zarafa, kopano, Let's Encrypt, letsencrypt, howto, kb, maintainer_nhowitt}}