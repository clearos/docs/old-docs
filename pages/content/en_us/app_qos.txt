{{ :userguides:qos.svg?80}}
<WRAP clear></WRAP>

===== Bandwidth and QoS Manager =====
The Quality of Service (QoS) app is a network feature that allows administrators to prioritize certain types of Internet traffic.  Enabling QoS decreases the likelihood that at any given time, a single user or device might be able to absorb most, if not all, of the available bandwidth and preventing other services from working optimally (eg. VoIP).

.  Set-up correctly, QoS can regulate the maximum bandwidth a service or user can use.

This app is currently in beta testing - please contact developer@clearcenter.com with information on bugs or feature requests.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/network/qos|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_qos|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_qos|here]].

