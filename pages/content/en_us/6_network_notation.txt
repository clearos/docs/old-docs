===== Network Notation =====
On several ClearOS configuration screens, you will see places to configure network settings. What does this mean? Using special notation, you can specify a whole range of IP addresses (for example, a network).

There are two parts to **network notation**: the first part contains the first IP address in the network range; the second part contains the subnet mask (for example, the size of the network). There are two common ways to write the subnet mask. Unfortunately, both ways are something that only computer engineers enjoy.

Let's go through an example. The ClearOS mail server can be configured to only send out mail coming from a trusted network -- you don't want anyone on the Internet using your mail server and all your bandwidth! In our example, all of the machines on our LAN use IP addresses with 192.168.1.x IP addresses. The subnet mask for these 256 IP addresses can be specified with either /24 or 255.255.255.0. The full network notation ends up looking like:

  * 192.168.1.0/24, or
  * 192.168.1.0/255.255.255.0

By the way, the first and last addresses (192.168.1.0 and 192.168.1.255 in our example) are reserved and cannot be used. We really only have 254 usable IP addresses in our case.

===== Summary =====
The table below is a list of common netmasks.

^Netmask ^IPs ^Example Usable Range^
| /8 -- 255.0.0.0        | 16777214 | 192.168.0.1 - 192.255.255.254 | 
| /16 -- 255.255.0.0     | 65534    | 192.168.0.1 - 192.168.255.254 | 
| /20 -- 255.255.0.0     | 4094     | 192.168.0.1 - 192.168.15.254 | 
| /21 -- 255.255.255.0   | 2048     | 192.168.0.1 - 192.168.7.254 | 
| /22 -- 255.255.255.0   | 1022     | 192.168.0.1 - 192.168.3.254 | 
| /23 -- 255.255.255.0   | 510      | 192.168.0.1 - 192.168.1.254 | 
| /24 -- 255.255.255.0   | 254      | 192.168.0.1 - 192.168.0.254 | 
| /25 -- 255.255.255.128 | 126      | 192.168.0.1 - 192.168.0.126 | 
| /26 -- 255.255.255.192 | 62       | 192.168.0.1 - 192.168.0.62 | 
| /27 -- 255.255.255.224 | 30       | 192.168.0.1 - 192.168.0.30 | 
| /28 -- 255.255.255.240 | 14       | 192.168.0.1 - 192.168.0.14 | 
| /29 -- 255.255.255.248 | 6        | 192.168.0.1 - 192.168.0.6 | 
| /30 -- 255.255.255.252 | 2        | 192.168.0.1 - 192.168.0.2 | 
{{keywords>clearos, clearos content, clearos6, userguide, categoryinstallation, subcategoryappendix, maintainer_dloper}}
