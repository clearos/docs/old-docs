{{ :userguides:configuration_backup.svg?80}}
<WRAP clear></WRAP>

===== Configuration Backup and Restore =====
The configuration backup and restore app allows an administrator to take a snapshot (archive file) of all configuration settings of the system, allowing easy restoration in the event data is lost.  The backup file can be copied and stored to a desktop on the LAN, placed on a mass storage device (requiring modest space - under 1GB) or saved as part of the remote server backup solution (a paid remote backup solution available in the Marketplace from ClearCenter).

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/system/configuration_backup|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_configuration_backup|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_configuration_backup|here]].
==== Additional Notes ====
