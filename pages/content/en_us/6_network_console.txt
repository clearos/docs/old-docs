===== Network Console =====
ClearOS is configured via a web-based administration tool called [[:content:en_us:6_webconfig|Webconfig]].  Of course, before you can access Webconfig from a remote web browser, the network needs to be up and running!

Depending on your installation environment, your network might already be running and accessible. If that's the case, you will be shown IP address information to access [[:content:en_us:6_webconfig|Webconfig]].  If you need to initialize or change your network settings, follow the links on the screen to access the Network Console tool.  You can find detailed information on how to configure your network [[:content:en_us:6_ip_settings|here]].

===== Next Steps =====
Once you can access the webconfig web-based administration tool from a remote web browser, you can get started with the [[:content:en_us:6_first_boot_wizard|First Boot Wizard]].
{{keywords>clearos, clearos content, clearos6, userguide, categoryinstallation, subcategoryfirstboot, maintainer_dloper}}
