===== Ventrilo on ClearOS =====
Install Ventrilo on ClearOS Box.   With will provide Log Support, and integrated Webgui interface so you can see if it is running.  

===== Install Program Tools =====
SSH logon

You can pickup WinSCP here;
[[http://winscp.net/eng/index.php]]

Putty here;
[[http://www.chiark.greenend.org.uk/~sgtatham/putty/]]
This all could be done via PuTTY too if you want.

Get the latest version of Ventrilo
Download here  [[http://ventrilo.com/download.php]]

===== Install Ventrilo =====
=== Create user ===
Create a new user within WebConfig

  User:       Ventrilo
  Name:       Ventrilo
  Last Name:  Server 

Everything else is up to you, but don’t give it access to anything

=== Place install on server ===
Copy the ventrilo_srv-version-Linux-i386.tar.gz file into the /home/ventrilo/ user's directory

=== Install ===
Logon to you ClearOS box via SSH

  cd /home/ventrilo

Type:  

  tar -xzf ventrilo_srv-version-Linux-i386.tar.gz

You can delete the zip file if you like

Type:  

  chown -R ventrilo:allusers /home/ventrilo/ventsrv
  chmod -R 0755 /home/ventrilo/ventsrv

=== Configure ===
Next, edit the .ini file

The ventrilo_srv.ini file located under /home/ventrilo/ventsrv/

  vi /home/ventrilo/ventsrv/ventrilo_srv.ini
Here is where you can store the password for users, name of the server, etc.

You can find more about the .ini, at [[http://www.ventrilo.com/setup.php]]

   [Server]
   Name=yoursite.com
   Phonetic=Server 1
   Auth=1
   Duplicates=0
   AdminPassword=your password to create channels etc. 
   Password=password for your users if you want it? 
   SendBuffer=0
   RecvBuffer=0
   Diag=0
   LogonTimeout=5
   CloseStd=1
   TimeStamp=0
   PingRate=10
   ExtraBuffer=0
   ChanWidth=0
   ChanDepth=0
   ChanClients=0
   DisableQuit=0
   VoiceCodec=0
   VoiceFormat=1
   SilentLobby=0
   AutoKick=0

<note>Ventrilo is a paid program and has limitation. You will only be able to allow 8 users to log in at any time. If you need more than 8 users, please rent a server. Ventrilo is usually only available in greater numbers to service providers.</note>

=== Mac support ===
If you will ever have anyone connect using a Mac, you will want to change the following default values in your ventrilo_srv.ini file to support the Speex codec. For example:

  VoiceCodec=3
  VoiceFormat=21

===== Modifying your Script File =====

Next will be to create script file so you can load the service from the command line or later in Webconfig.

You need to run the service in order for it to create some files, including the .log file.

Copy the text below into a script file

Location of the file /etc/rc.d/init.d/ventrilo

  vi /etc/rc.d/init.d/ventrilo

Enter the following information into the script file. 

<note>You can do this many ways, by using vi, pico or nano in the CLI (Command Line Interface) or use WinSCP to create the edit the file.</note>

  #!/bin/bash
  #
  # vent This shell script takes care of starting and stopping
  # the Ventrilo subsystem (vent).
  #
  # chkconfig: - 90 10
  # description: Ventrilo Chat Interface.
  # processname: ventd
  # config: /do_not_remove/ventrilo_srv.ini
  # pidfile: /do_not_remove/ventrilo_srv.pid
  # Source function library.
  . /etc/rc.d/init.d/functions
  
  # CONFIG
  prog="Ventrilo Server"
  vent_path=/home/ventrilo/ventsrv      # <----  Replace with the path to your /ventsrv directory
  vent_user=ventrilo                    # <----  Replace with sudo user vent_bin=$vent_path/
  vent_server=ventrilo_srv
  vent_log=$vent_path/ventrilo_srv.log
  vent_pid=$vent_path/ventrilo_srv.pid
  # Startup ventrilo servers.
  start(){
  chmod 0775 $vent_path
  chmod 0775 $vent_log
  
  sudo -u $vent_user $vent_path/$vent_server -f$vent_path/$vent_server -d
  ret=0
  if [ $ret -eq 0 ]; then
  STARTTIMEOUT=30
  while [ $STARTTIMEOUT -gt 0 ]; do
  RESPONSE=`ls -al ventrilo_srv.pid 2>&1` && break
  echo "$RESPONSE" | grep -q "root" && break
  break
  sleep 1
  let STARTTIMEOUT=${STARTTIMEOUT}-1
  done
  if [ $STARTTIMEOUT -eq 0 ]; then
  echo "Timeout error occurred trying to start $prog Daemon."
  action $"Starting $prog: " /bin/false
  else
  action $"Starting $prog: " /bin/true
  fi
  else
  action $"Starting $prog: " /bin/false
  fi
  
  return $ret
  }
  stop(){
  VENTPID=`cat "$vent_pid" 2>/dev/null `
  if [ -n "$VENTPID" ]; then
  /bin/kill "$VENTPID" >/dev/null 2>&1
  ret=$?
  if [ $ret -eq 0 ]; then
  STOPTIMEOUT=60
  while [ $STOPTIMEOUT -gt 0 ]; do
  /bin/kill -0 "$VENTPID" >/dev/null 2>&1 || break
  sleep 1
  let STOPTIMEOUT=${STOPTIMEOUT}-1
  done
  if [ $STOPTIMEOUT -eq 0 ]; then
  echo "Timeout error occurred trying to stop $prog Daemon."
  ret=1
  action $"Stopping $prog: " /bin/false
  else
  action $"Stopping $prog: " /bin/true
  fi
  else
  action $"Stopping $prog: " /bin/false
  fi
  else
  ret=1
  action $"Stopping $prog: " /bin/false
  fi
  return $ret
  }
  restart(){
  stop
  start
  }
    
  # See how we were called.
  case "$1" in
  start)
  start
  ;;
  stop)
  stop
  ;;
  status)
  status vent
  ;;
  restart)
  restart
  ;;
  *)
  echo $"Usage: $0 {start|stop|status|restart}"
  exit 1
  esac

After, we need to add some options. Type the following in command line: 

  chown root:root /etc/rc.d/init.d/ventrilo
  chmod 755 /etc/rc.d/init.d/ventrilo
  chkconfig --add ventrilo 
  chkconfig --levels 345 ventrilo on && chkconfig --list ventrilo

Now you can start your server

  service ventrilo start

<note warning>You will get the following warning: **“chmod: cannot access `/home/ventrilo/ventsrv/ventrilo_srv.log': No such file or directory”**

You can safely ignore it, because the file is not there, as you just created it. Then you should see the system come back as “OK” in green.
</note>


  service ventrilo restart
  service ventrilo stop

===== Almost Done =====

Next we need to create a link file under the /var/log so we can view it via the webgui. Type: 

  mkdir /var/log/ventrilo
  cd /home/ventrilo/ventsrv/
  cp ventrilo_srv.log /var/log/ventrilo/ventrilo_srv.log
  rm -f ventrilo_srv.log
  chown -R ventrilo:allusers /var/log/ventrilo/ventrilo_srv.log
  ln -s /var/log/ventrilo/ventrilo_srv.log ventrilo_srv.log

Now restart the webconfig service

  service webconfig restart 

Now you should be able to access your log menu and see

  tail /var/log/ventrilo/ventirlo_srv.log

Next we'll want to add the necessary changes to the webgui in order to see the server as a service.

You need to locate and edit the following file.

 vi /var/webconfig/api/Daemon.inc.php

And insert this line.

  "ventrilo"     => array("httpd",        "ventrilo_srv",     "no",  "Ventrilo Server",     "no",  null),

Restart the webconfig
  
  Type:  service webconfig restart

You should be all done!!!

<note warning>You will need to open ports for Ventrilo will work.  These is based on your ini file. The default port if you left it unspecified is 3784.   If you need help please refer this this [[http://www.clearfoundation.com/docs/user_guide/clearos_enterprise_5.1/start#firewall]] </note>

===== Extras =====
==== Codec list ====
^Codec^Format^Description^
|0|0|GSM 6.10 (8000 Hz, 16 bit) 1625 bytes/sec|
|0|1|GSM 6.10 (11025 Hz, 16 bit) 2210 bytes/sec|
|0|2|GSM 6.10 (22050 Hz, 16 bit) 4420 bytes/sec|
|0|3|GSM 6.10 (44100 Hz, 16 bit) 8905 bytes/sec|
|1|0|DSP Group TrueSpeech (8000 Hz, 16 bit) 1056 bytes/sec|
|2|0|Lernout & Hauspie (8000 Hz, 16 bit) 600 bytes/sec|
|3|0|Speex (8 KHz, 16 bit, 0 Qlty) 424 bytes/sec|
|3|1|Speex (8 KHz, 16 bit, 1 Qlty) 620 bytes/sec|
|3|2|Speex (8 KHz, 16 bit, 2 Qlty) 865 bytes/sec|
|3|3|Speex (8 KHz, 16 bit, 3 Qlty) 1110 bytes/sec|
|3|4|Speex (8 KHz, 16 bit, 4 Qlty) 1110 bytes/sec|
|3|5|Speex (8 KHz, 16 bit, 5 Qlty) 1502 bytes/sec|
|3|6|Speex (8 KHz, 16 bit, 6 Qlty) 1502 bytes/sec|
|3|7|Speex (8 KHz, 16 bit, 7 Qlty) 1922 bytes/sec|
|3|8|Speex (8 KHz, 16 bit, 8 Qlty) 1922 bytes/sec|
|3|9|Speex (8 KHz, 16 bit, 9 Qlty) 2384 bytes/sec|
|3|10|Speex (8 KHz, 16 bit, 10 Qlty) 3168 bytes/sec|
|3|11|Speex (16 KHz, 16 bit, 0 Qlty) 620 bytes/sec|
|3|12|Speex (16 KHz, 16 bit, 1 Qlty) 865 bytes/sec|
|3|13|Speex (16 KHz, 16 bit, 2 Qlty) 1110 bytes/sec|
|3|14|Speex (16 KHz, 16 bit, 3 Qlty) 1355 bytes/sec|
|3|15|Speex (16 KHz, 16 bit, 4 Qlty) 1698 bytes/sec|
|3|16|Speex (16 KHz, 16 bit, 5 Qlty) 2188 bytes/sec|
|3|17|Speex (16 KHz, 16 bit, 6 Qlty) 2678 bytes/sec|
|3|18|Speex (16 KHz, 16 bit, 7 Qlty) 3070 bytes/sec|
|3|19|Speex (16 KHz, 16 bit, 8 Qlty) 3560 bytes/sec|
|3|20|Speex (16 KHz, 16 bit, 9 Qlty) 4344 bytes/sec|
|3|21|Speex (16 KHz, 16 bit, 10 Qlty) 5324 bytes/sec|
|3|22|Speex (32 KHz, 16 bit, 0 Qlty) 669 bytes/sec|
|3|23|Speex (32 KHz, 16 bit, 1 Qlty) 1061 bytes/sec|
|3|24|Speex (32 KHz, 16 bit, 2 Qlty) 1306 bytes/sec|
|3|25|Speex (32 KHz, 16 bit, 3 Qlty) 1551 bytes/sec|
|3|26|Speex (32 KHz, 16 bit, 4 Qlty) 1943 bytes/sec|
|3|27|Speex (32 KHz, 16 bit, 5 Qlty) 2433 bytes/sec|
|3|28|Speex (32 KHz, 16 bit, 6 Qlty) 2874 bytes/sec|
|3|29|Speex (32 KHz, 16 bit, 7 Qlty) 3266 bytes/sec|
|3|30|Speex (32 KHz, 16 bit, 8 Qlty) 3756 bytes/sec|
|3|31|Speex (32 KHz, 16 bit, 9 Qlty) 4540 bytes/sec|
|3|32|Speex (32 KHz, 16 bit, 10 Qlty) 5520 bytes/sec|
{{keywords>clearos, clearos content, kb, howtos, maintainer_dloper, maintainerreview_x, keywordfix}}