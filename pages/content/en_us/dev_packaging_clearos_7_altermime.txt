===== Altermime =====
A tool to alter MIME-encoded mail on the server.  Specifically, this tool is used to add a mail disclaimer to outbound mail.

===== Links =====
{{keywords>clearos, clearos content, AppName, app_name, version7, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
