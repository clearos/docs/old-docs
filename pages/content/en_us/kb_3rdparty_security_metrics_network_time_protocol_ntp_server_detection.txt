===== Network Time Protocol NTP Server Detection =====
This entry from Security Metrics indicates that your Network Time Server is exposed to the internet. Anonymous users can read the time running on your server and can synchronize their clocks to that time.

===== ClearCenter response =====
==== Short response ====
This is not a risk.

==== Long response ====
Knowing that the date and time of the server is accurately affixed to world clocks does not denote a risk.

==== Resolution ====
No action is required.

You can close anonymous access to your NTP server by removing port 123 UDP (NTP) from your incoming firewall rules. If you need access from the Internet to this server, consider closing the range of addresses that can access this server to a restricted list using the Custom Firewall module.

===== Links =====

  * [[http://www.clearcenter.com/marketplace/network/Custom_Firewall.html|Custom Firewall Module]]
  * [[http://www.clearcenter.com/support/documentation/clearos_guides/custom_firewall_module_examples|Custom Firewall Examples]]
{{keywords>clearos, clearos content, 3rd party, security metrics, non-cve, maintainer_dloper}}
