===== Bandwidth and QoS Manager =====
The Bandwidth and QoS (quality of service) Manager app is used to shape or prioritize network traffic. 

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:

<navigation>Network|Bandwidth Control|Bandwidth and QoS Manager</navigation>

===== How it works =====
The QoS application works by allocating traffic to 7 Priority buckets. The lower numbered Priority buckets are then allowed to take bandwidth from higher Priority buckets, so bucket 1 can take bandwidth from buckets 2-7, bucket 2 can only take bandwidth from buckets 3-7 and so on. Any bucket can take all the bandwidth if no other traffic is using the bandwidth.

===== Configuration =====
==== External Interface Upload/Download Settings ====
The upstream and downstream rates for your external (Internet) interfaces must be specified in order to optimize the underlying QoS engine. If you set these values below your actual upload/download rates, then you will find your bandwidth capped by these lower values.

We recommend the [[http://www.speedtest.net|SpeedTest.net]] online tool for measuring actual bandwidth.  Please perform these tests when network traffic is low (off hours) and **without** a web proxy running.

To do this, navigate to 'Network' > 'Settings' > 'IP Settings' in Webconfig.  Then click on the Speedtest icon, located next to your external interface, as shown below.

{{content:en_us:7_network-interfaces_2.png}}

Another dialog will appear where you will need to click 'Run Speed Test' to start the Speed Test.

{{content:en_us:7_network-interfaces_speedtest.png}}

<note warning>If you are on a connection with a large asymmetrical ratio (e.g. 25 MB download, but only 1 MB upload), you may need to adjust your upload value to a higher value.</note>

Once this is completed, you may make changes as recommend above or return to the 'Bandwidth and QoS Manager' app.

<note tip>If you want to manually enter your upstream and downstream bandwidths, you can do it in 'Webconfig' > 'Network' > 'Settings' > 'IP Settings' > 'your interface' > 'Edit'</note>

==== External Network Interfaces ====
{{7_ug_qos_external_network_interfaces.png}}

Use the <button> Enable Engine </button> button to enable and disable the Bandwidth and QoS engine.

<note warning>The only configurable option here is the Rate-to-Quantum and it is very strongly recommended that you leave it on Auto unless you are an advanced Admin. If you want to find out more, please see /etc/clearos/qos.conf</note>

===== Configuration Examples =====
QoS has 7 priority bands with 7 being the lowest. Any traffic which does not match any of the QoS rules gets allocated to band 7.

It is recommended that Priority 1 is left alone. The default Priority 1 rules cover ICMP (pings etc) and are important for traffic flow as the the DNS rule. In the background there is also a custom rule covering small packets such as ACK packets.

Priority 2 is possibly best left alone. It is there because at times sysadmins may need to rapidly access the server to sort out issues. User rules should start at Priority 3 (or possibly 2).
==== Priority for Remote Server ====
It is fairly common to require higher QoS for a particular remote IP or network.  For example, many VoIP solutions use Internet SIP servers for providing services, and these servers should be given high priority.  Below is an example step-by-step guide for providing high priority to IP 1.2.3.4.

  * Click on <button>Add</button> in **Upstream Priority Class**
  * Set the following parameters
     * Nickname: remote_upstream
     * Interface: All
     * Enabled: checked
     * Priority: 3
     * Protocol: Any
     * Destination Address: 1.2.3.4
  * Leave the remaining parameters blank
  * Click on <button>Add</button>

A similar rule needs to be added for downstream QoS:

  * Click on <button>Add</button> in **Downstream Priority Class**
  * The following values were added:
     * Nickname: remote_downstream
     * Interface: All
     * Enabled: checked
     * Priority: 3
     * Protocol: Any
     * Source Address: 1.2.3.4
  * Leave the remaining parameters blank
  * Click on <button>Add</button>

With the QoS engine enabled, traffic to and from 1.2.3.4 will be given higher priority over other traffic.

Typical high priority traffic is VoIP or other telephony. Traffic like file transfers would normally be considered as low priority but it really depends on the individual use case.

===== Tips and Tricks =====
==== LAN Subnets ====
If you want to create a rule for a whole LAN subnet, the Source Address and Destination Address boxes accept subnets in both the 172.17.2.0/24 form and in the 172.17.2.0/255.255.255.0 form.

<note tip>This also can be very useful within a LAN, especially if you change your DHCP scope to be a subnet of your LAN subnet. As an example, if you LAN subnet is 10.11.12.0/24, you can have DHCP start at 10.11.12.128 and finish at 10.11.12.191. This way your DHCP devices always are in the range 10.11.12.128/26 and you can assign them a priority accordingly. Static IP's must be outside this subnet and Static DHCP Leases can be outside this subnet and assigned a different priority.</note>

==== Custom Rules ====
It is possible to add custom rules. If you want to do this you need to edit /etc/clearos/qos.conf directly and add them to QOS_PRIOMARK4_CUSTOM setting. You can use many of the firewall switches for the <param> section. See the example in the file and the two TCP/ACK rules.
<note>If you are editing the file manually, please note that the Priority buckets are numbered 0 to 6 in the file and correspond to Priorities 1 to 7 in the Webconfig.</note>
If you need to add port ranges or more complex network matching, you can contact ClearCARE support.  The support team will be able to provide custom configuration rules for the QoS engine.

==== Advanced Configuration ====
It is possible to limit the amount of bandwidth available in each Priority bucket. Currently each one can take up to 100% of the available bandwidth. Similarly each bucket gets a similar proportion of the available bandwidth (before the lower numbered buckets steal from the higher numbered buckets), but it is possible to vary it. For more details see /etc/clearos/qos.conf
<html><!-- Before getting started with the QoS configuration, it is important to know about best practices.  There are two ways to approach bandwidth management:

  * Limit low priority traffic in an effort to improve speeds for high priority traffic
  * Reserve bandwidth for high priority traffic which will shuffle low priority traffic aside

It is impossible to predetermine what types of traffic will be low priority, but typically quite easy to identify important traffic (VoIP being an obvious one).  Therefore, **reserving bandwidth for high priority traffic** is the best way to proceed with QoS management.  You can always add rules for low priority traffic as well, but it is often not necessary. --></html>

==== Web Proxy Gotchas ====
Having a web proxy configured either on a ClearOS gateway or some other local proxy server complicates matters.  As soon as a web request is made via the proxy, the source IP address for the request is lost.  In other words, configuring bandwidth rules using an IP address on your local network will not have an effect for any traffic going through the proxy.  See the examples for ways to limit bandwidth to your proxy server.

==== Bandwidth Limits for Individual IP's ====
Please see [[https://www.clearos.com/clearfoundation/social/community/bandwidth-manager-advance-rules-not-working-on-clearos7#reply-232521|this forum post]]

===== Links =====
  * [[:content:en_us:7_ug_network_visualiser|Network Visualiser]]
{{keywords>clearos, clearos content, AppName, app-qos, clearos7, userguide, categorynetwork, subcategorybandwidthandqos, maintainer_dloper, maintainerreview_x}}
