{{ :userguides:incoming_firewall.svg?80}}
<WRAP clear></WRAP>

===== Firewall =====
The incoming firewall app allows administrators to simply open ports (or port ranges) for services running locally on the server.  If a service requires connections from outside your network to be made (i.e. running a web or mail service on a system configured for gateway and server), a corresponding port or port range will need to be added through this app.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/network/incoming_firewall|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_incoming_firewall|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_incoming_firewall|here]].
==== Additional Notes ====
