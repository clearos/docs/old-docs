{{ :userguides:process_viewer.svg?80}}
<WRAP clear></WRAP>

===== Process Viewer =====
Tabular display of all programs (processes) running on the server.  Specific information on each process is listed, including process ID (PID), running time, CPU and memory usage and owner.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/reports/process_viewer|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_process_viewer|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_process_viewer|here]].
==== Additional Notes ====
