===== Libuser =====
The libuser library implements a standardized interface for manipulating
and administering user and group accounts. 

===== Rebuild =====

This package needs to be rebuilt against a newer version of OpenLDAP.

{{keywords>clearos, clearos content, AppName, app_name, version5, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
