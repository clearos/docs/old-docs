===== Disabling DHCP and DNS in ClearOS =====
This guide covers disabling DHCP and DNS in ClearOS as services. Some installations of ClearOS may be implemented into environments with existing services for DHCP or DNS. In these environments, there may be conflicts or security reasons for removing these as running services from ClearOS.

===== Shutting Down DNSMASQ =====
ClearOS' DHCP and DNS Caching services are provided by default by the dnsmasq service. The following commands from a command prompt will stop and disable the service:

  chkconfig --level 2345 dnsmasq off
  service dnsmasq stop

If you ever want to revert this change, perform the following:

  chkconfig --level 345 dnsmasq on
  service dnsmasq start

===== I want to keep DNS Caching but NOT DHCP =====
Since this service is integrated, you cannot turn off DHCP without disrupting DNS services. However, you can unconfigure all settings and scopes available. To do this, log into Webconfig and navigate to the DHCP server in the Network section. Perform the following:

  * Click the edit button on the Settings section and change **Authoritative** to **Disabled**
  * In the Subnets section, hit the **Delete** button on all entries. There should ONLY be **Configure** buttons on all entries.
{{keywords>clearos, clearos content, AppName, app_name, kb, howto, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
