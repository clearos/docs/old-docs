===== OpenLDAP =====
OpenLDAP is an open source suite of LDAP (Lightweight Directory Access Protocol) applications and development tools.  LDAP is a set of protocols for accessing directory services (usually phone book style information, but other information is possible) over the Internet, similar to the way DNS (Domain Name System) information is propagated over the Internet. The openldap package contains configuration files, libraries, and documentation for OpenLDAP.

===== Changes =====
ClearOS 5.x uses the more recent OpenLDAP 2.4.x series.  This was a requirement for the newer replication feature.

===== Future =====
The upstream package will be sufficient in ClearOS 6.0.
{{keywords>clearos, clearos content, AppName, app_name, version5, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
