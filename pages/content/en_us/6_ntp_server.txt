===== NTP Server =====
NTP is the Network Time Protocol. This module is extremely important for managing the time on your server. It is important that your server runs the correct time because some protocols which require interaction relay on the time being relatively the same in order to properly conduct transactions.

===== Configuration =====
By default the configuration for the time server is set to use the following time sources provided by ClearCenter:

  * time1.clearsdn.com
  * time2.clearsdn.com
  * time3.clearsdn.com
  * time4.clearsdn.com

===== Changing time servers =====
If you would like to change the time servers you will need to modify the following lines in /etc/ntp.conf:

  server time1.clearsdn.com
  server time2.clearsdn.com
  server time3.clearsdn.com
  server time4.clearsdn.com

Then restart the time service.

  service ntpd restart
{{keywords>clearos, clearos content, AppName, app_name, clearos6, userguide, categorynetwork, subcategoryinfrastructure, maintainer_dloper, maintainerreview_x}}
