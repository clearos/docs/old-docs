{{ :userguides:ntp.svg?80}}
<WRAP clear></WRAP>

===== NTP Server =====
The Network Time Protocol app synchronizes the server's internal clock with Internet standard time servers.  Computers and Internet devices on the LAN can then sync clocks against the local time server, achieving a very high degree of accuracy.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/network/ntp|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_ntp|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_ntp|here]].
==== Additional Notes ====
