===== Google Apps Synchronization =====
The **Google Apps Synchronization** app synchronizes the users, groups and passwords on your ClearOS system with your Google Apps domain.  Whether you add new users to a group or change a user password, these changes will synchronize to the cloud.

<note>Please note, the synchronization tool is not available for the free version of Google Apps... sorry.</note>

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Server|Messaging and Collaboration|Google Apps Synchronization</navigation>

===== Getting Started =====

==== Enable User Provisioning ====
Access to Google Apps user/group provisioning must be enabled.  Log into your Google Apps account with administrator privileges and go to the [[https://admin.google.com|Google Apps Admin Console]].

  * Click on the **Security** icon (you may need to click on **More Controls**)
  * Click on the **API Reference** option
  * Check the **Enable API access** checkbox
  * Click on <button>Save Changes</button>

{{:content:en_us:google_apps_api_access.png|Google Apps API Access}}

==== Connect ClearOS ====
With API access enabled, you can now connect your ClearOS system to Google Apps.

  * Log into the web-based administration in ClearOS
  * In the menu, go to <navigation>Server|Messaging and Collaboration|Google Apps Account Sync</navigation>
  * Click on the <button>Obtain Request Token</button> button (see below)
  * Accept the request to manage users and groups

{{:content:en_us:google_apps_auth_token.png|ClearOS Google Apps Request Token}}


At this point, you should see a long access code provided by Google.  You will need to copy and paste this access code into the ClearOS app (see screenshot).  You will also need to provide the Google Apps domain and the administrator e-mail.

{{:content:en_us:google_apps_provision.png|Google Apps access code}}

===== Synchronization =====
==== Testing Synchronization ====
Before enabling automatic synchronization, you can test the process using the web-based administration tool in ClearOS.  

  * Login to the web-based administration in ClearOS
  * In the menu, go to <navigation>Server|Messaging and Collaboration|Google Apps Synchronization</navigation>
  * Click on the <button>Test Synchronization</button>

In most cases, you will see the following error:

**The Google Apps administrator account cannot be suspended**

That makes sense (I hope!).  Follow the link to create the account on your ClearOS system.  Please make sure the Google Apps option is enabled when you create the user!

==== Users ====
Go ahead and create a user with Google Apps enabled and then re-run the synchronization test.  One you are happy with the results, perform a synchronization and please read the following warning:

<note warning>It can take several minutes for a user or group change to appear in Google Apps.  Please be patient.</note>

==== Groups ====
Groups are also synchronized to Google Apps:

  * In the web-based administration tool, go to <navigation>System|Account Manager|Groups</navigation> 
  * Create a group and make sure **Distribution List** is enabled

After the synchronization is complete, it will take a minute or two to see the results in the Google Apps administration portal.  If you would like external users to be able to e-mail **groupname@your_domain.com**, you need to update this policy in the Google Apps portal.  By default, the mailing list is only available to internal users.

==== Enabling Automatic Synchronization ====
The final step in Google Apps Synchronization is the easiest.  Please make sure automatic synchronization is enabled.  This is especially important to catch the password changes that can be triggered by an end user.

===== Managing Users =====
In order to protect against losing user data, a user is put into **suspended** mode if Google Apps is disabled from the [[:content:en_us:6_users|ClearOS User Manager]].  In order to completely delete the user, please use the Google Apps administrator interface to do so.  You can also restore the user if the deletion was unintended.

===== Troubleshooting =====
==== Error 1300: EntityExists ====
You will see this error if you try to create a user that already exists, but is suspended.  See **Managing Users** above.
{{keywords>clearos, clearos content, Google Apps Synchronization, app-google-apps, clearos7, userguide, categorycloud, subcategoryservices, maintainer_bchambers}}
