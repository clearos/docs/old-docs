===== Migrating an Existing ClearOS Installation to Another Server =====
This guide covers a situation where you need to move a ClearOS installation from one server to another. This guide is intended for a migration between two ClearOS 6.5 servers but will have suggestions to apply for other versions of ClearOS.

<note>If you can update your ClearOS 6 server to the same version as you will be installing, it will save you lots of headache.</note>

===== Set up new server =====
Install ClearOS 6 on the new server. Will want to set up ClearOS in one of the following paradigms:

  * On-Network 
  * Off-Network/Parallel

==== On-Network migration overview ====
Inline migrations are the most common because you don't have to have special infrastructure to work. You do, however, need at least 2 network cards on your new ClearOS server so that you can properly test. Basically you will set up ClearOS in gateway mode with the external facing NIC on the existing network where the old ClearOS server resides. You will set up a bogus LAN network on the internal side. Using these settings, you will firewall off the existing network from the test network while at the same time you will be able to perform internet updates, data migrations, and sequestered testing. 

At the end of this howto, once the migration and testing is complete, you will re-IP the network interfaces to match the existing network and down the old server.

==== Off-Network migration overview ====
This paradigm allows you to migrate the server infrastructure as-is without messing with the old production network. In this scenario you will install ClearOS on the new server on a completely different network the old server is on the production network and the new server is on the test network. Once the entire migration is complete, you will simply switch the networks with the old server on the test network and the new server on the production network.

If you have user data that you need migrated, we suggest that you connect the server behind the existing networks to transfer the data. For this to work, you will need to have a spare network card or VLAN on the the existing ClearOS server and the new server. You will set them up using all the same IP addresses except for this spare NIC which you will use to have the servers communicate with each other for data migration.

===== Set up the new server =====
Set up the new server but don't yet run the registration process. You can run updates from command line (yum update) and validate the network settings from command line as well.

===== Backing up the old environment =====
There are four things that you need to backup from the existing server:

  * Configuration
  * App selection
  * Data
  * 3rd party and additional customizations

==== Configuration Backup ====
On the existing/old server perform a configuration backup. Once this is complete, you need to not make any additional changes that will not be reflected in this backup. You have read more about how to perform a configuration backup here:

[[http://www.clearcenter.com/support/documentation/user_guide/configuration_backup|http://www.clearcenter.com/support/documentation/user_guide/configuration_backup]]

If you are using ClearOS 5.x and are migrating to ClearOS 6.x, you will need to modify your ClearOS 5.x backup so that it will work with ClearOS 6.x. If you have a support subscription, you can have ClearCenter modify your file for you so that your import is as painless as possible.

==== App selection ====
When it comes time to install the new system you can either install apps manually or you can use a Quick Select File to add the apps you've installed already.

Create app installation quickpick file for installation of the same apps already installed on the old system from the marketplace. You will need to transfer off and modify the following file:

  /var/clearos/configuration_backup/installed_apps.txt

Edit the file and ensure that the the plugin apps removed (you will get an error on import of certain apps on this list. It is ok to remodify this file during the install if you get a rejected app). Additionally, modify the file by adding the QSF header and rename the file to create a .qsf extension.You can read more about QSF here:

[[http://www.clearcenter.com/support/documentation/marketplace_quick_select|http://www.clearcenter.com/support/documentation/marketplace_quick_select]]

==== Backing up user data ====
If you have user data on the server, you will need to identify what user data you want migrated. You will want SSH access to exist from the new server to the old server. In situations where the old server is compromised, has old access passwords, keys or other security holes, it is best to have the new server initiate all transfers. To expedite transfers, you can have the old server trust the public key of the new server:

[[http://www.clearcenter.com/support/documentation/clearos_guides/setting_up_ssh_trust_between_two_servers|http://www.clearcenter.com/support/documentation/clearos_guides/setting_up_ssh_trust_between_two_servers]]

Decide what data you will need to transfer and where that data is located. For example:

^Data Structure^Location^
|Home Folders|/home|
|Flexshares|/var/flexshare/shares|
|Login scripts|/var/samba/netlogon|
|Roaming Profiles|/var/samba/profiles|
|Samba Print Drivers|/var/samba/|
|Default Website|/var/www/html/|
|Virtual Websites|/var/www/virtual/|
|Bind Mount storages|/store/data*|

If you have server trust in place, you can set up a script to pre-migrate sync all the data from the old server to the new one. This can be done with a script that includes all the locations that need to be migrated. If you have the key-based trust involved, you can easily keep the servers in sync by running something similar from the new server as a script (don't forget to chmod 700 your script):

<code>
#!/bin/bash
rsync -av --delete root@old_server_ip:/path/to/user/data1 /path/to/staging/area/for/new/data1/
rsync -av --delete root@old_server_ip:/path/to/user/data2 /path/to/staging/area/for/new/data2/
rsync -av --delete root@old_server_ip:/path/to/user/data3 /path/to/staging/area/for/new/data3/
</code>

You will also need to migrate or backup other structures using data dump or migration methods:

^Service^Method^
|Zarafa|[[http://www.clearcenter.com/support/documentation/clearos_guides/setting_up_backups_for_zarafa|Data Dump of System Mysql database]]|
|IMAP|[[http://www.clearcenter.com/support/howtos/migrating_mail_to_clearos|Use IMAP migration methods]]|
|MySQL Database|Use Mysql dump methods|

==== 3rd party and other apps ====
Other apps may not address their user stores, installations, or configuration components under the ClearOS paradigms listed above. For these applications, you will need to account for their installs, configuration and data. We recommend that your installation retain the old server in some form so that if you forget something after the migration, that you have a path back to the configurations.

===== Create an evaluation license =====
In order to make your transition painless, you can set up an evaluation license that you will assign to your OLD server to hold you over while you are in the migration. This step is not necessary if you do not need to run the new setup in parallel for testing while the production is running (ie. you are doing ALL of the migration on a single weekend and do not need services to work properly on the old server once you register the existing license to the new box)

===== Run the wizard on the new server =====
When you run the wizard on the new server, you will keep or set it to on network or off network for the migration as stated above. Try to set the settings the same as on the existing server (except for the network setting if doing an on network migration). When you get to the registration section, you will select 'Reinstall a system that was previously registered'. This will usurp the license from your production machine. If you have a QSF file, you will import it during the marketplace section of the wizard.

<note warning>
Some services like Dynamic VPN are tied to your license. These services will switch to the new server. If you want these services to work with the old server, you will need to reset them in the ClearOS Portal.
</note>

===== Re-register the existing server =====
If you are keeping the production server in production while you migrate the data, you can use an evaluation license to keep services online. Be advised that you may have other services that need to be configured in the Service Delivery Network.

This guide will help you re-register your existing system:

[[http://www.clearcenter.com/support/documentation/clearos_guides/reset_registration|http://www.clearcenter.com/support/documentation/clearos_guides/reset_registration]]

===== Apps =====
If you did now use the QSF method to recover your apps, you will want to reinstall them now from the ClearCenter Marketplace.

===== Restore =====
Using the configuration backup, restore the settings for your ClearOS server.

[[http://www.clearcenter.com/support/documentation/user_guide/configuration_backup|http://www.clearcenter.com/support/documentation/user_guide/configuration_backup]]

===== Testing =====
It is important to test your installation to make sure that things are working well. This may include domain logins if your server was a domain controller or other activities. If things are not well, you may have to revert back so make sure that you have the capability to cover yourself against a catastrophic migration scenario.

===== Summary =====
Many tools exist to make your migration as easy as possible. Please contact support using your support subscription if you have additional questions or found steps that you needed to perform that were not in this guide.

{{keywords>clearos, clearos content, categoryinstall, bestpractices, clearos6, maintainer_dloper}}