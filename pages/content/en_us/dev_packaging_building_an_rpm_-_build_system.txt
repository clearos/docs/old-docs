===== Building an RPM - ClearOS Development Environment =====
This document describes how to get started with building RPMs in the ClearOS build environment.  If RPM packaging is new to you, then a review of the following document should be your first stop: [[:content:en_us:dev_packaging_building_an_rpm_-_the_basics|Building an RPM - The Basics]].

Please remember, one of the most important reasons why we have created the build system is so that **everyone in the community can participate**!  Other advantages of the build system:

  * Simplifies support for 32-bit, 64-bit and ARM builds
  * Provides transparency with [[http://koji.clearos.com/koji|package changes]]
  * Reduces potential library and dependency errors

===== Prepare Your Development Environment =====
Before getting underway, the first thing you need to do create a development environment.  Follow [[:content:en_us:dev_development_environment|these instructions]] and come back to this document when you're done.

===== Accessing Source Code =====
The source code for ClearOS apps and RPMs is stored on GitHub:

  * [[https://github.com/clearos|ClearOS]]
  * [[https://github.com/clearos-contribs|Free 3rd Party ClearOS Apps]]

For contributing to existing apps, the usual [[https://help.github.com/articles/using-pull-requests/|fork/pull method]] is used for contributing patches.  If you are creating your own app, you can either manage it through [[https://github.com/clearos-contribs|ClearOS contribs]] or through your own git repo.
===== Directory Structure =====
There are 4 types of packages maintained in the source code system:

  * Upstream packages derived from CentOS, RedHat, EPEL
  * Upstream packages that we maintain internally
  * In-house source code
  * ClearOS apps - those app-xyz packages

Each of these types share the following traits:

  * A branch maintained for every ClearOS version: clear6, clear7
  * Tags are used to mark specific releases

However, the workflow for managing an upstream package is different from building an internal project.  We won't get into the details here, instead, we'll jump right into an example - the [[https://github.com/clearos/duc.git|duc (disk usage)]] package.  duc is an upstream package that we maintain internally and you will also need the **common** git repository to hook in the build tools:

  git clone https://github.com/clearos/duc.git
  git clone https://github.com/clearos/common.git

The default branch is set to the current release - at the time of writing this document, that's clear7:

  $ cd duc
  $ git branch
  * clear7

Feel free to switch to the development branch if you wish with: **git checkout master**.  To hook in the build system tools, symlink the Makefile from the common repository:

  ln -s ../common/Makefile

Now let's take a look at the files:

  duc-1.3.3-html-fixes.patch
  duc-1.3.3-no-header.patch
  duc.spec
  Makefile -> ../common/Makefile
  README.md
  sources-clearos

This directory holds the spec file, patches and other helper files to build the RPM.  You may have noticed that one thing is missing -- the upstream tar-zip file(s).  Since these files can be rather large, these are sometimes (but not always) kept on a download server instead of git.  The **sources-clearos** file holds information on which files to download when building the RPM.

That's the basic structure, now it is time start building.

===== Local Build =====
The build system uses //make// commands to perform tasks.  The **make srpm** command will build the SRPM in your local ClearOS environment by:

  * Downloading the source tar-zip files (if required and permitted)
  * Running the rpm command to build the source RPM

The automatic download of the source code is not yet permitted, so you must download the source code manually.  Go ahead and give it a try for the **duc** package:

  wget http://duc.zevv.nl/release/duc-1.3.3.tar.gz
  make srpm
  
Now you can build the RPM using the source RPM:

  rpmbuild --rebuild //name of source rpm//

Not every library and development package is installed, so you may see some build dependencies like this:

  error: Failed build dependencies:
    abc is needed by xyz
    def is needed by xyz

That's nothing to worry about.  Install the required dependencies using yum (su -c 'yum install abc def' -), and then re-run the **rpmbuild** command. 

===== Mock Build =====
The build system uses the [[http://fedoraproject.org/wiki/Projects/Mock|mock]] tool to build RPMs in a chroot environment.  With mock, the RPMs for all the dependencies are downloaded from the ClearOS build system or related mirrors.  By using the build system RPMs instead of your local server environment, we can ensure that builds use the correct dependencies and versions.  Mock requires special privileges, so you need to add your developer user account to the **mock** group:

  su -c 'usermod --append --groups mock your_username' -

<note warning>The RPMs for //all// the dependencies are downloaded from the download mirrors.  Even a small package like **duc** can result in hundreds of megabytes of download.  These RPMs are cached on your local file system, so subsequent mock builds will take less time.</note> 

When you issue the **mock abc.srpm** command, go grab a cup of coffee or watch the video below; it can take time to download, unpack and build even a small RPM in a mock/chroot environment. 

  mock duc-1.3.3-2.v7.src.rpm

{{youtube>large:OfgfnZZdMlI}}

==== Mock Build Logs ====
The build logs can be found in the /var/lib/mock/clearos-7-x86_64/result
 directory.  The following table summarizes the log files.

^Log File^Description^
|build.log|Contains the log from running rpmbuild|
|root.log|Contains log information on the creation of chroot environment|
|state.log|Contains mock log information|

===== Next Steps =====
If you can build an RPM using **make srpm** and **mock**, then you should be able to build it in the Koji build system.

If you plan on distributing RPMs, please go through the [[:content:en_us:dev_packaging_contributing_and_patching_rpms|Contributing and Patching RPMs]] document. 

===== Links =====
  * [[:content:en_us:dev_packaging_building_an_rpm_-_the_basics|Building an RPM - The Basics]]

{{keywords>clearos, clearos content, AppName, app_name, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
