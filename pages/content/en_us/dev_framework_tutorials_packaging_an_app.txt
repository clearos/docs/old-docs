===== Tutorials Packaging an App =====
In order to ease development and provide a standard packaging format, RPMs (spec files) for ClearOS apps are automatically generated.  There's no need to learn about the complexities of [[http://www.rpm.org/max-rpm/ch-rpm-inside.html|RPM spec files]], that's all done by the ClearOS development tools.

===== Generate the RPM Spec File =====
Though you don't need to know about spec files, we'll take you through the process of what is going on in the background.  Go to your apps base directory and run **clearos spec**:

  cd apps/myapp/trunk
  clearos spec

This command uses the information in **deploy/info.php** to generate the RPM spec file.  After running this command, you will find the newly generated spec in the **packaging** directory.  This file should not be hand edited, it's always automatically generated!  Even though it is generated, please make sure the spec file is added to the source code management system (SVN).

===== Build the App - Local RPM =====
Now that the RPM spec exists, you can build a test RPM using **clearos local**:

  cd apps/myapp/trunk
  clearos local

You can then install the RPM on test systems or share it.  If you want to distribute the app to the entire ClearOS community via yum, read on.

===== Build the App - Build System =====
<note>Please contact developer@clearfoundation.com for information on access to the build system.</note>

At the moment, the build system uses SVN trunk for building a package.  So, the first thing you need to do is make sure trunk is up-to-date and in build-ready shape.  Take a moment to review the version and release information in **deploy/info.php**:

  * Never submit the same version to the build system, so bump the version if necessary
  * The release should always be set to 1

If you make changes, run **clearos spec** to regenerate the spec file and don't forget to check in changes to SVN!  When you are ready, run the following to submit the app to the build system:

  * plague-client build app-XYZ trunk clearos6

Check the [[http://buildsys.clearfoundation.com/plague/builders.psp|status of the build]] to make sure all went well.  After an hour or so, you should see the package in the yum repositories.  If this is a brand new package, the RPM will end up in the **clearos-dev** repository.  If this an update to an existing package, the RPM will land in **clearos-test**.  You can learn more about the different repositories in the [[developer:packaging:workflow]] doc.

{{keywords>clearos, clearos content, dev, framework, maintainer_dloper, maintainerreview_x, keywordfix}}
