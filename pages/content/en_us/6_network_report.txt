===== Network Report =====
The **Network Report** provides a historical view of average network speeds on each network interface.  This report is invaluable when diagnosing network performance issues.

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Reports|Network|Network Report</navigation>

===== Report =====
{{ :omedia:network_report.png?475|ClearOS Network Report}}  The **Network Report** provides the average speed received and transmitted on each network interface.  The adjacent screenshot comes from a small office environment with very modest network requirements.  The spikes come from large downloads and video streaming.  

If you see network speed clamped at the maximum all day long, a plan of action is recommended.  Increasing network capacity is certainly one option, but [[:content:en_us:6_content_filter|taking control]] over network usage is another way forward.

===== Links =====
  * [[:content:en_us:6_network_detail_report|Network Detail Report]]
  * [[:content:en_us:6_network_visualiser|Network Visualiser]]
  * [[:content:en_us:6_resource_report|Resource Report]]
  * [[:content:en_us:6_bandwidth_manager|Bandwidth Manager]]
{{keywords>clearos, clearos content, AppName, app_name, clearos6, userguide, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}

{{keywords>clearos, clearos content, Network Report, app-network-report, clearos6, userguide, categoryreports, subcategorynetwork, maintainer_dloper}}