===== Boot a VM from an ISO Image =====

You may install any operating system on a virtual machine using an installation ISO image. There is a number of installation ISOs available in the Public Store but you may also add any other installation ISO to your Private Library (see [[:content:en_us:vm_adding_custom_vm_templates|Add Templates]]).

  * Click **Start** on an installation ISO image
  * Now configure the VM and hit **Power On**
  * Wait until the VM has started and select the VM, then click **Connect**
  * Follow the guide to establish a VNC connection to the VM
  * From your VNC client you can now perform the guest OS installation

==== Installing Windows Server 2008 R2 ====

During installation there are two drives (CD-ROMs), drive D: and drive E:. Drive D: is the image you specified as template and drive E: (WITSBITS) is where the VirtIO drivers for Windows are located. During Windows installation Windows will not find any hard drive. Click Load Driver, Browse and go to drive E: (WITSBITS), select the folder **E:\WINDOWS\2008\DISK**. A list with drivers will be listed. Select **Red Hat VirtIO SCSI**. Now the disk is found and available for partitioning. Once the operating system is installed, go to Hardware and update the driver for the unknown network interface card. Browse to **E:\WINDOWS\2008\NET**. In list select **Red Hat VirtIO** ethernet.

<note>If you power off your VM and then start it again D: and E: will not be available anymore.</note>

{{keywords>clearvm, kb, howto, maintainer_dloper}}