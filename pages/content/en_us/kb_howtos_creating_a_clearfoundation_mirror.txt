===== Creating a Clearfoundation Mirror =====
This guide will help you create a ClearFoundation software mirror. The purpose of mirrors is to allow update and other package information to be easily disseminated. We thank you for helping us by making a mirror to help reduce the costs of distributing ClearOS.

A mirror requires around 65G of space (growth of about 10G per year). Changes to the data are typically minimal around 300MB/day. 

===== Types of Mirrors =====
There are two types of mirrors, 'push' and 'pull'. We prefer to set up push mirrors because that allows us to tell you server when it is time to update. It also allows us to determine which resource should use its bandwidth to update your mirror. To perform a push, we will need to have limited 'key-based' access to your server. If you want to be a top tier server, we require that you allow push. Push mirrors get their updates the fastest and typically become part of the 'core' mirror framework from which other pull mirrors rely.

Pull mirrors are less dynamic and don't allow us to determine which source you are pulling from. This means that your mirror may try to pull its data from a source that is already taxed too much while another mirror has little to do. The advantage of a pull mirror is that coordination with ClearFoundation is minimal and you can get started right now.

===== Setting up a Push Mirror =====
The best place to start is to contact us at **info at clearfoundation dot com**. From there we can chat with you, get to know you and see how to best proceed. This gives you a chance to understand our framework as well and our team. Whether you are using a VM that we have limited access to, or a box that we also have limited access to, we can help you engineer a tightly-controlled and secure push environment.

===== Setting up a Pull Mirror =====

First, you will need to contact us to find out how this should be best organized. If you want a pull mirror, please mention that in your email. We'll give you the address you should instead of 'sourcemirror.address.example.com' in the sync line below.

  rsync -v --stats --bwlimit=0 -prltHSB8192 --timeout 3600 --max-delete=10000 --delay-updates --delete --delete-after --delete-excluded  --exclude .~tmp~/  sourcemirror.address.example.com::clearos/ /path/to/mirror/
  
  # ^^^ This line included to allow you to easily copy the above ^^^

Change the bwlimit if you need to. Also if you don't want the verbose output remove the "-v --stats" from the front. Change the '/path/to/mirror' to reflect your path on your server. 

We sync the mirrors every two hours at 20 minutes past the even hours MST.  If you schedule your mirror to sync 15-20 minutes after that you should always have an up-to-date mirror.

Once you have set this up, contact us again and we will add your mirror(s) to our checking script. Once they have shown to be stable (less than 6 hours out of date for 72 hrs straight) they will start to have traffic directed to them.

On behalf of the ClearFoundation, we thank you very kindly. Thank you for supporting Open Source! -DaveLoper

===== Status of Mirrors =====
You can see the status of the ClearFoundation mirrors by visiting here:

  * [[http://mirror2-houston.clearsdn.com/clearos/mirrorlist/|Houston]]

{{keywords>clearos, clearos content, kb, clearos6, clearos7, categorysdn, howtos, maintainer_dloper}}
