===== Kernel reports 'HTB- quantum of class 10001' errors =====
This guide covers manipulation of 'r2q' values to compensate for messages in the messages log concerning IMQ bandwidth warnings. This guide is intended for skilled admins.

<note warning>This guide is intended for users of ClearOS 6.3 and earlier. It is written for the 6.x code base as the paths are different. Beginning shortly after the release of 6.4, the bandwidth manager is capable of making these calculations itself</note>

===== Symptoms and Remedies =====
You may get a message in /var/log/messages stating something like:

  kernel: HTB: quantum of class 10001 is big. Consider r2q change.

This issue can be alleviated by performing a change to the r2q value in the following file:

  /usr/clearos/apps/firewall/deploy/firewall.lua

Navigate the .lua file until you find the following r2q entries:

<code>
    for _, ifn in pairs(IMQIF_UPSTREAM) do
        execute(string.format("%s link set %s up 2>/dev/null", IPBIN, ifn))
        execute(TCBIN .. " qdisc del dev " .. ifn .. " root handle 1: htb >/dev/null 2>&1")
        execute(TCBIN .. " qdisc add dev " .. ifn .. " root handle 1: htb default 2 r2q 1")
    end

    for _, ifn in pairs(IMQIF_DOWNSTREAM) do
        execute(string.format("%s link set %s up 2>/dev/null", IPBIN, ifn))
        execute(TCBIN .. " qdisc del dev " .. ifn .. " root handle 1: htb >/dev/null 2>&1")
        execute(TCBIN .. " qdisc add dev " .. ifn .. " root handle 1: htb default 2 r2q 1")
    end
</code>

Change the values to 10 for both r2q parameters:

<code>
    for _, ifn in pairs(IMQIF_UPSTREAM) do
        execute(string.format("%s link set %s up 2>/dev/null", IPBIN, ifn))
        execute(TCBIN .. " qdisc del dev " .. ifn .. " root handle 1: htb >/dev/null 2>&1")
        execute(TCBIN .. " qdisc add dev " .. ifn .. " root handle 1: htb default 2 r2q 10")
    end

    for _, ifn in pairs(IMQIF_DOWNSTREAM) do
        execute(string.format("%s link set %s up 2>/dev/null", IPBIN, ifn))
        execute(TCBIN .. " qdisc del dev " .. ifn .. " root handle 1: htb >/dev/null 2>&1")
        execute(TCBIN .. " qdisc add dev " .. ifn .. " root handle 1: htb default 2 r2q 10")
    end
</code>

Restart the firewall and observe the logs.

  service firewall restart

If the problem persists, increment the r2q value by 5 and restart again. Repeat until the problem goes away.

===== Help =====
==== Navigation ====

[[:|ClearOS Documentation]] ... [[..:|Knowledgebase]] ... [[:Knowledgebase:Troubleshooting:|Troubleshooting]]
{{keywords>clearos, troubleshooting, help, support, kernel, bandwidth, qos, r2q, quantum of class, htb, maintainer_dloper}}
