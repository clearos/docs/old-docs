===== Remote Disaster Recovery Support for Clearbox =====
ClearBOX comes with a number of tools for remote support. This includes tools on the Compact Flash which allow for diagnosis, configuration, and repair of the ClearOS installation on ClearBOX. 

Features of the compact flash currently include:

  * Bypass bootloader. This item tries to boot ClearOS without using the GRUB installation on the hard disks. This can be useful if GRUB is corrupt on that device.
  * Installation of ClearOS onto ClearBOX for single drive and dual drive systems.
    * This configures all systems as a RAID one mirror. Even on dual drive systems. While this does report as a degraded array on single systems, it is done intentionally so that the drive can easily be replaced should a predictive failure occur.
  * Re-installation of ClearOS while preserving user data. ClearBOX uses bind mounts to store various user data directories on a separate partition. This re-install feature will preserve those points. You should still have a good backup.
    * NOTE: This option only works with systems configured with this version of the compact flash. If you purchased the system before Aug 2010 and did NOT re-install your system using this version of the compact flash, do not use this option. If you are unclear whether or not you can use this option, run the 'mount' command from command line. If it says: '**/dev/mapper/data-data0** on /store/data0 type ext3 (rw)' then you are using a qualified system.
  * Includes image of 'Parted Magic', an open source graphical utility which allows for some disk diagnosis, repair, and imaging. This utility also has some remote control capabilities explained below.
  * Includes image of installation rescue mode.
  * Memory tests.

===== Qualified systems =====
Beginning with shipments of ClearBOX in August 2010, these utilities are already embedded on the compact flash included on the system. For older systems you can download the image here and use these steps to image the Compact Flash.

  * Validate that you have at least 10 GB of free space on your ClearBOX's data partition.
  * From command line run the following:

  wget http://www.clearcenter.com/media/dev/clearbox-image/ClearBOX-with-ClearOS52.image.gz

  * This will take a while to download as it is over 1 Gigabyte in size.
  * Run the following to make sure the image is untainted. Do not use the file if the file checksum is different than what is listed here:

  md5sum ClearBOX-with-ClearOS52.image.gz
  
  dd2522c6f773b438ff868cea3f604a2b  ClearBOX-with-ClearOS52.image.gz

  * If the checksum is correct extra the data to the disk. And image the compact flash. Both of these steps take a while to complete. It is normal for the second step to take 40 minutes or more.

  gunzip ClearBOX-with-ClearOS52.image.gz 

  dd if=ClearBOX-with-ClearOS52.image of=/dev/hda bs=512

  * You may get a message about no more space left on device. If this occurs, use the instructions below in the section called "Fixing Partition Three"

===== Testing it out =====
Reboot the box and note the initial menu page for the boot loader. The only options here that are destructive are the installation and re-installation methods. The other are OK to test and run at any time. 

===== Using Parted Magic =====
Parted Magic will boot into a graphical user interface. You should connect a USB keyboard and mouse to interact with the system.

==== Start the Network ====
To start the network, click the icon entitled, "Start Network". You will be able to select the appropriate network type for your system. When the network comes up it may launch a browser to Google to show that it is working.

==== Enable Remote SSH ====
To enable remote SSH, click on the terminal icon down on the bottom bar (looks like a black monitor). Type the following to find your IP address:

  ifconfig eth0

Set the root password by running the following:

  passwd

It will ask you to type a password. If you type a poor password it will complain that it is too weak but it will do it anyways.

To start the SSH server type the following:

  /usr/sbin/sshd

You should now be able to remote SSH into the box as root using the password specified above.

===== Getting Output from Boot Screen =====
ClearBOX comes with a serial port that can be used for a modem or for other COM/Serial port related activities. The console cable is beige and has an RJ-45 jack on one end and a DB-9 on the other end and is configured as a NULL modem. The serial port is located:

  * ClearBOX 300: above the USB ports on the front panel
  * ClearBOX 100: above the VGA port and between the power and USB ports

In case of bad activity like a kernel panic, you can use this method to capture the data from the boot process. This data can then be transferred to USB and be read by parted magic when working remotely. To set this up, run Hyperterminal from your workstation and connect it to the serial port on ClearBOX using the cable provided. Set the data rates for 8-None-1 and disable flow control. When booting ClearBOX, proceed to the second screen (green) and stop the boot using the arrow keys. Highlight the kernel you are booting from and type 'e' to edit the line. Arrow down to the kernel line and type 'e' again. At the end of the line append the following:

  console=tty0 console=ttyS0,9600

Now when you boot, your output should display through the serial.

===== Fixing Partition Three =====
Partition three is designed to eventually provide the capability to boot your ClearBOX to a mode where gateway functions of your box are running and functional even if you hard drive is toast. This feature in on the current roadmap and will be implemented at some other time. If you received an error message while imaging the compact flash which stated that there was not any space left on the device, then you compact flash has a slightly different geometry from the original. To fix this, boot to your ClearOS system and start a command prompt. Run the following commands:

  parted /dev/hda rm 3

Next run the fdisk utility to re-create partition 3.

  fdisk /dev/hda

You will get an interactive menu driven utility type the bolded characters at the appropriate time. If you get stuck type q <enter> to quit or CTRL+c.

=== fdisk utility ===
The number of cylinders for this disk is set to 15538.
There is nothing wrong with that, but this is larger than 1024,
and could in certain setups cause problems with:
1) software that runs at boot time (e.g., old versions of LILO)
2) booting and partitioning software from other OSs
   (e.g., DOS FDISK, OS/2 FDISK)

Command (m for help): **n**
Command action
   e   extended
   p   primary partition (1-4)
**p**
Partition number (1-4): **3**
First cylinder (7890-15538, default 7890): **<press enter>**
Using default value 7890
Last cylinder or +size or +sizeM or +sizeK (7890-15538, default 15538): **<press enter>**
Using default value 15538

Command (m for help): **w**
The partition table has been altered!

Calling ioctl() to re-read partition table.
Syncing disks.

=== Completing the process ===
Finish up by formatting the partition:

  mkfs.ext3 /dev/hda3

This will format the partition. After it is done, confirm that your partition 3 exists by running:

  fdisk -l /dev/hda

It should look similar to this:

  Disk /dev/hda: 8019 MB, 8019099648 bytes
  16 heads, 63 sectors/track, 15538 cylinders
  Units = cylinders of 1008 * 512 = 516096 bytes
  
     Device Boot      Start         End      Blocks   Id  System
  /dev/hda1   *           1         252      126976+  83  Linux
  /dev/hda2             253        7889     3849048   83  Linux
  /dev/hda3            7890       15538     3855096   83  Linux

{{keywords>clearos, clearos content, clearbox, userguide, features, kb, maintainer_dloper}}
