===== Connecting to the Secure Shell =====
You can use SSH (Secure Shell) to access the command line on a ClearOS system. 

==== Linux and MacOS ====
Linux and MacOS X already have SSH clients installed by default. On Mac, the terminal program can be found under the Utilities folder in Applications. Open up a terminal window and type:

  ssh root@192.168.1.1

Change 192.168.1.1 to the IP address of your ClearOS system.  Type in your system password and you will find yourself at the ClearOS command prompt.

==== Windows =====
For Windows users, you can download the free [[http://www.chiark.greenend.org.uk/~sgtatham/putty/|Putty SSH]] tool for remote shell / command line access.  After you start up the software:

  * type in the IP address or hostname into the //host name// field
  * click on <button>Open</button>
  * type in //root// at the login prompt and then your system password 

You will find yourself at the ClearOS command prompt.

==== Connecting Externally to Your Server via SSH =====
You can open up external access to your server by allowing incoming SSH (port 22) in <navigation>Network|Firewall|Incoming</navigation>.

{{:omedia:incoming-ssh.png?550|}} 

Select **SSH** under Standard Services and click <button>Add</button>

<note warning>Because SSH is a common management port, you should enable the Intrusion Detection and Prevention modules to protect this port if you open it up on the outside.
</note>

===== Secure Copy =====
Part of the capabilities of the SSH protocol is the ability securely transfer data to and from the server. This is commonly called secure copy or SCP. This can be done using command line on Mac and Linux. In Windows, a graphical program called [[http://winscp.sourceforge.net|WinSCP]] is free and useful for performing these tasks. You can also use [[http://rsug.itd.umich.edu/software/fugu/|Fugu]] on Mac as a graphical frontend for SCP.

SCP works similarly to the CP (copy) program in POSIX systems.  For example, if you wanted to copy a file to the server you could type the following:

  scp test.txt root@server.example.com:/var/
  
This would copy the file test.txt to the **/var** directory of the server named //server.example.com// using **//root//** as the username.  For more information, visit [[http://www.clearfoundation.com/docs/man/index.php?n=scp|this link]].

===== Links =====
  * [[http://www.chiark.greenend.org.uk/~sgtatham/putty/|Putty]]
  * [[http://winscp.sourceforge.net|WinSCP - Secure Copy]]
  * [[http://www.clearfoundation.com/docs/man/index.php?n=scp|scp man page]]
{{keywords>clearos, clearos content, clearos7, userguide, categoryinstallation, subcategoryappendix, maintainer_dloper}}
