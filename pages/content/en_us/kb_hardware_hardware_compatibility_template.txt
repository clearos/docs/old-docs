===== Manufacturer's Name - Hardware Category - Hardware Model =====
The above title should specify the hardware manufacturer's name, the hardware category, and the model name. This should also be a brief description of the item. Hardware pages should be located in the sub-folder of the manufacturer.

===== Compatibility Matrix =====
This matrix should be copied (delete this sentence.) 

| ClearOS 5.2 ^ Rating ^
^ClearCenter Tested| No |
^Manufacturer Support| [[ http://manufacturer.example.com|Yes, Provide Link if yes ]] |
^3rd Party Support(Support organization)| [[ http://partner.example.com|ClearCenter Partner Link]] |
^ClearFoundation Positive Feedback Count| Decrement/Increment this number |
^ClearFoundation Negative Feedback Count| Decrement/Increment this number |

| ClearOS 6 ^ Rating ^
^ClearCenter Tested| No |
^Manufacturer Support| [[ http://manufacturer.example.com|Yes, Provide Link if yes ]] |
^3rd Party Support(Support organization)| [[ http://partner.example.com|ClearCenter Partner Link]] |
^ClearFoundation Positive Feedback Count| Decrement/Increment this number |
^ClearFoundation Negative Feedback Count| Decrement/Increment this number |

===== Users who have tested this hardware =====
The above will have the title and the below will have a list of users that have used this hardware. In addition, this paragraph should have a link to the forum thread(s) that deal with this particular piece of hardware. Please start a forum thread if one does not exist. [[http://forumlink.clearos.example.com/|Check out the discussion for this hardware item on the forum]].

  * [[http://www.clearos.com/community/my-profile/68-dloper|Example User]] - This hardware works ok. I use it all the time (example brief statement).
  * [[http://www.clearos.com/community/my-profile/68-dloper|Example User2]] - I have problems with this locking up (example brief negative statement).

===== Help =====
==== Links ====
  * [[http://link.example.com|Link to Manufacturer's support site for this device]]

==== Navigation ====
[[:|ClearOS Documentation]] ... [[:Knowledgebase:|Knowledgebase]] ... [[:Knowledgebase:Hardware:|Hardware Compatibility]]

Update the keywords below:
{{keywords>clearos, hardware, insert_manufacture_name, insert_model_name_and_number, other}}
