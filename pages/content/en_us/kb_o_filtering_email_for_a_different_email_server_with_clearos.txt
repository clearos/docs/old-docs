===== Filtering Email for a Different Email Server with ClearOS =====
This guide is designed to help you use ClearOS as a filter for your email that is destined to another email provider. This can be useful to provide anti-virus and anti-spam services for a different email server such as Microsoft Exchange Server*

===== Setup =====
To setup mail filtering for a different server, install the SMTP service and any filtration services like Antispam and Anti-virus that you desire.

===== Configuration =====
You will need to configure the 'Domain' under the SMTP Server **Settings** section of the SMTP Server module. This domain MUST be different than your email domain. For example, if your email address was bob@example.com, you can set the domain to be example.lcl. This is important because if the name matches, ClearOS may look up the user on the local system to determine if email should be delivered or rejected. You want the other server to make that determination so the parameter must NOT match.

Under the **Mail Forwarding** section click add and add the domain name and server hostname or IP address of the final email server. Change the port number as well if this is not the same. Click Add.

===== Testing =====
Next, compose a message against your ClearOS server and determine if it reaches its final destination by checking the box you specify. To do this, hand compose and email message from the ClearOS server perform the following and the following responses will be given by the server.

You type:
  telnet localhost 25

Server responds:
  Trying 127.0.0.1...
  Connected to localhost.
  Escape character is '^]'.
  220 server.example.lcl ESMTP Postfix

You type:
  helo example.com

Server responds:
  250 home.daveloper.net

You type:
  mail from: bob@example.net

Server responds:
  250 2.1.0 Ok

You type:
  rcpt to: dloper@daveloper.net

Server responds:
  250 2.1.5 Ok

You type:
  data

Server responds:
  354 End data with <CR><LF>.<CR><LF>

You type:
  Subject: test1
  
  This is a test. This is only a test.
  .

Server responds:
  250 2.0.0 Ok: queued as 01234567890

You type:
  quit

Server responds:
  221 2.0.0 Bye
  Connection closed by foreign host.

If this works, you can proceed to the next section. Feel free to test as many mailboxes as you need.

===== Cutting over services =====
If you are using the mail server behind this ClearOS server as your firewall you probably already have a port 25 forwarding rule in place. Remove this rule and replace it with a incoming port 25 firewall rule.

If you are using this server at a different IP address or location, you will want to move the MX record to use the ClearOS server instead. One way to 'ease' in the address is to have ClearOS configured as an additional MX record and setting it higher, then the next day the same, then the next day to a lower setting than the existing server. Finally, when all is working well, remove the old MX record so that all mail goes through your filtration gateway.

{{keywords>clearos, clearos content, AppName, app_name, kb, howto, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
