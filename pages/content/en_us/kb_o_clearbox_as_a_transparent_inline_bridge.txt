===== ClearBOX as a Transparent Inline Bridge =====
The purpose of this document is to configure a ClearBOX 100 (units that have network bypass), ClearBOX 300 Series, or ClearOS in general as a inline, transparent bridge. The typical purpose of ClearOS as a transparent bridge may include ClearOS as a content filtration system, security capture and logging, or inline firewalling.

For the purposes of this demonstration we will be configuring a ClearBOX 300 (Gen1) server as a content filtering bridge with a 'fail open' configuration on the bypass. We will have a system that is configured through the wizard with a gateway server, with no packages installed via the wizard, and with SSH open in the 'incoming firewall' rules. In addition, all updates were applied using 'yum update'.

It is possible to set up non-ClearBOX hardware as transparent filtering bridges but it is not recommended because if the system is down or rebooting, no traffic will be able to propagate on the network.

===== ClearOS 7 =====
==== Overview ====
<note warning> The following guide is NOT complete and should be considered a work in progress.
</note>

The steps to setting up ClearBOX and ClearOS are as follows:

  * Setup Default Hardware and Software
  * Create Bridge interface and dependent NICs
  * Fix-up Default firewall conditions and test bridge
  * Setup Content Filter and Proxy software
  * Setup bridge firewall stack and capture
  * Setup network bypass
  * Test, test, test

<note warning>You need to read all of these instructions before proceeding. Many the steps may require an advanced technical skill. Please make sure you are comfortable editing files in Linux and troubleshooting basic conditions before proceeding.</note>

==== Setup Default Hardware and Software ====
On a fresh install of ClearOS 7, make sure that you have two network interfaces that you can use for your bridge. On ClearBOX 300 or other models with a network bypass, you should use the network bypass segment. This is port 0 and 1 or alternately ports 2 and 3.

==== Create Bridge interface and dependent NICs ====
Fire up and SSH client and log into the command line interface of ClearOS. Run the **'ip addr'** command to list your interfaces:

<code>
[root@bridge ~]# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN 
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP qlen 1000
    link/ether 08:00:27:00:4f:9e brd ff:ff:ff:ff:ff:ff
    inet 192.168.1.119/24 brd 192.168.3.255 scope global dynamic enp0s3
       valid_lft 85808sec preferred_lft 85808sec
    inet6 fe80::a00:27ff:fe00:4f9e/64 scope link 
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN qlen 1000
    link/ether 08:00:27:64:10:8d brd ff:ff:ff:ff:ff:ff
</code>

Here I have two NICs, **enp0s3** and **enp0s8**. I want to add these interfaces to my bridge so that I can bridge accross them. First I need to create a virtual bridge NIC on the system:

  vi /etc/sysconfig/network-scripts/ifcfg-br0

<note>The contents of this file will contain the following (it is recommended that you use a static IP address for the management of the bridge interface</note>

<code>
DEVICE=br0
TYPE="Bridge"
ONBOOT="yes"
USERCTL="no"
BOOTPROTO="static"
STP="on"
IPADDR="192.168.1.119"
NETMASK="255.255.255.0"
GATEWAY="192.168.1.1"
</code>

Now, bring up your interface with the following command:

  ifup br0

Validate that your interface is listed now in **'brctl'**

<code>
[root@bridge ~]# brctl show
bridge name	bridge id		STP enabled	interfaces
br0		8000.000000000000	yes
</code>

Next, you can see our interface listed and can also see that we now have two interfaces listed with the same IP:

<code>
[root@bridge ~]# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN 
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP qlen 1000
    link/ether 08:00:27:00:4f:9e brd ff:ff:ff:ff:ff:ff
    inet 192.168.1.119/24 brd 192.168.3.255 scope global dynamic enp0s3
       valid_lft 84964sec preferred_lft 84964sec
    inet6 fe80::a00:27ff:fe00:4f9e/64 scope link 
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN qlen 1000
    link/ether 08:00:27:64:10:8d brd ff:ff:ff:ff:ff:ff
4: br0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN 
    link/ether 7a:15:b4:ff:5b:9f brd ff:ff:ff:ff:ff:ff
    inet 192.168.1.119/24 brd 192.168.3.255 scope global br0
       valid_lft forever preferred_lft forever
</code>

Time to fix that.

<note warning>
If you are accessing your box from the external NIC of your ClearOS server, you will want to skip to the next section briefly and add the bridge interface to your /etc/clearos/network.conf file before proceeding.
</note>

Make changes to the interface that you are not using to SSH to the box first:

  vi /etc/sysconfig/network-scripts/ifcfg-enp0s8

My file looks like this:

<code>
DEVICE=enp0s8
NAME=enp0s8
TYPE="Ethernet"
ONBOOT="yes"
USERCTL="no"
BRIDGE=br0
</code>

Take the interface down and bring it back up again:

  ifdown br0 && sleep 2 && ifdown enp0s8 && sleep 2 && ifup enp0s8 && sleep 2 && ifup br0

Now repeat this with the other interfaces you want on the bridge, including the one you are using to access the server (presumably.)

If all goes well, you won't get dropped from your connection. Validate that your bridge has an IP address:

<code>
[root@bridge ~]# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN 
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast master br0 state UP qlen 1000
    link/ether 08:00:27:00:4f:9e brd ff:ff:ff:ff:ff:ff
    inet6 fe80::a00:27ff:fe00:4f9e/64 scope link 
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast master br0 state UP qlen 1000
    link/ether 08:00:27:64:10:8d brd ff:ff:ff:ff:ff:ff
    inet6 fe80::a00:27ff:fe64:108d/64 scope link 
       valid_lft forever preferred_lft forever
6: br0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP 
    link/ether 08:00:27:00:4f:9e brd ff:ff:ff:ff:ff:ff
    inet 192.168.3.119/24 brd 192.168.3.255 scope global br0
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe00:4f9e/64 scope link 
       valid_lft forever preferred_lft forever
</code>


==== Fix-up Default firewall conditions and test bridge ====
You need to tell the firewall about what you just did. The ClearOS Webconfig takes into account the addition of interfaces and does some 'magic' in the background...but you just changed stuff outside of Webconfig with the interfaces and need to do the 'magic' yourself.

The specific changes you need to make are in **/etc/clearos/network.conf**. Specifically, you need to add the br0 interface to the mix and you ALSO need to activate an hidden feature called 'trustedgateway'.

Here is my file which contains both interfaces as well as the bridge interface and the changes to the 'MODE' line:

<code>
# Network mode
MODE="trustedgateway"

# Network interface roles
EXTIF="enp0s3"
LANIF="enp0s8 br0"
DMZIF=""
HOTIF=""

# Domain and Internet Hostname
DEFAULT_DOMAIN="clearos.com"
INTERNET_HOSTNAME="bridge.clearos.com"

# Extra LANS
EXTRALANS=""

# ISP Maximum Speeds
</code>

Now that your firewall is in order and your bridge is in place, you should be able to get out the the network or what ever is on one side of the bridge from another side of the bridge. Test this...a lot. Reboot to make sure it comes up as a bridge.




===== ClearOS 6 =====
==== Bridge ====
The first step is to configure ClearOS as a bridge. At the time of this writing, the ClearOS 6 GUI supports and recognizes bridges but cannot be used to configure them.

Edit the following files (leave the HWADDR and UUID lines alone):

/etc/sysconfig/network-scripts/ifcfg-eth0:

  DEVICE=eth0
  TYPE="Ethernet"
  ONBOOT="yes"
  USERCTL="no"
  HWADDR="00:00:00:00:00:00"
  UUID="00000000-0000-0000-0000-000000000000"
  BRIDGE=br0

/etc/sysconfig/network-scripts/ifcfg-eth1:

  DEVICE=eth1
  TYPE="Ethernet"
  ONBOOT="yes"
  USERCTL="no"
  HWADDR="00:00:00:00:00:00"
  UUID="00000000-0000-0000-0000-000000000000"
  BRIDGE=br0

/etc/sysconfig/network-scripts/ifcfg-br0:

  DEVICE=br0
  TYPE="Bridge"
  ONBOOT="yes"
  USERCTL="no"
  BOOTPROTO="static"
  BRIDGE_STP="yes"
  IPADDR="192.168.1.2"
  NETMASK="255.255.255.0"
  GATEWAY="192.168.1.1"

Next, you need to make changes to the firewall.  The firewall needs know about your new br0 network interface. In the /etc/clearos/network.conf configuration file, update the MODE, LANIF and EXTIF parameters.

MODE="trustedgateway"
EXTIF="br0"
LANIF="br0"

After you have made these changes, reboot the system and test the bridge.

==== Content Filtration ====
From the Marketplace, install the 'Content Filter' module. Once it is installed, enable Transparent mode in the Proxy module and start the service. Start the Content Filter service as well in the Content Filter module.

You will also want to install the ebtables module:

  yum install ebtables

Edit the file **/etc/clearos/firewall.d/local** and include the following lines:

<code>
ebtables -t broute -A BROUTING -p IPv4 --ip-protocol 6 --ip-destination-port 80 -j redirect --redirect-target ACCEPT
iptables -t nat -A PREROUTING -i br0 -p tcp --dport 80 -j REDIRECT --to-port 8080
</code>

If you are ONLY doing proxy services, use the following rules instead:

<code>
ebtables -t broute -A BROUTING -p IPv4 --ip-protocol 6 --ip-destination-port 80 -j redirect --redirect-target ACCEPT
iptables -t nat -A PREROUTING -i br0 -p tcp --dport 80 -j REDIRECT --to-port 3128
</code>

==== Bypass/Fail-Open ====
Next, you will need a service to keep the shut down the bypass when the Content Filter is running. Begin by following the documentation for [[http://www.clearcenter.com/support/documentation/clearbox_100|ClearBOX 100]] or [[http://www.clearcenter.com/support/documentation/clearbox_300|ClearBOX 300]] Series for enabling the bypass on your system.

You will need to configure your system to disable the bypass when the content filter and proxy server are running. Create a file called **/etc/init.d/bypassd**. Next, change it so that it becomes a script:

  chmod +x /etc/init.d/bypassd

=== bypassd ===
Fill the contents of this file with the following:

<note>An app will soon be available to perform all of this.</note>

<code>
!/bin/bash
#
# bypassd          Start/Stop the Bypass daemon.
#
# chkconfig: 2345 99 99
# description: Inline Bypass Daemon
# processname: bypassd

# Source function library.
. /etc/init.d/functions


#!/bin/sh
daemon=1
pid=0
# Set timeout period (how many seconds before the watchdog triggers)
period=7
# Set 'cf' to 0 if Content Filter is not used. Set to 1 if CF is required.
cf=1
if [ -f /sys/class/bypass/bypass-CAD0205VD-0/bp_type ]; then
	bypassdir=/sys/class/bypass/bypass-CAD0205VD-0/
fi
if [ -f /sys/bus/i2c/devices/0-0026/bp_type ]; then
	bypassdir=/sys/bus/i2c/devices/0-0026/
fi

echo 2 > $bypassdir/wdt0
echo $period > $bypassdir/period0

while [ $daemon = 1 ]; do 

#look for squid and dansguardian
pid=0
        while [ $pid = 0 ]; do
                if [ -f /var/run/squid.pid ]; then
			if [ $cf = "1" ]; then
                        	if [ -f /var/run/dansguardian-av.pid ]; then
                                	pid=1
                        	fi
			else
				pid=1
			fi
                fi
		sleep 3
        done


#Run WDT
	while [ $pid = 1 ]; do
		pid=1
		if [ ! -f /var/run/squid.pid ]; then
			pid=0
		else
			if [ $cf = "1" ]; then
	                       	if [ ! -f /var/run/dansguardian-av.pid ]; then
					pid=0
				fi
			fi
		fi
		echo 1 > $bypassdir/wdt0
		sleep 1
	done
sleep 1
done

}

</code>


===== Links =====
  * [[https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Networking_Guide/sec-Network_Bridging_Using_the_Command_Line_Interface.html#sec-Create_a_Network_Bridge|Redhat's Bridging Guide]]
{{keywords>clearos, clearos content, AppName, app_name, kb, howto, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
