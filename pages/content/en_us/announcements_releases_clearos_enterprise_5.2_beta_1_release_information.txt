===== ClearOS Enterprise 5.2 Beta 1 Release Information =====
**Released: June 11, 2010**

The ClearOS Enterprise 5.2 Beta 1 release is here!  Apart from squashing the important bugs, the big news with this release is the available upgrade path from ClearOS 5.1.  Please keep in mind that this is still a beta, so proceed with the [[:content:en_us:announcements_releases_test_releases|usual caution]].  Other changes since 5.2 Alpha 1 include: 

  * Completed password policy engine
  * Fixed disk usage report
  * Updated SpamAssassin
  * Many bug fixes 

<note>If you have not already done so, please review the [[:content:en_us:announcements_releases_test_releases|introduction to test releases]].</note>

===== What's New in ClearOS Enterprise 5.2 =====
What's new in ClearOS Enterprise 5.2?  Along with updating the base system to CentOS 5.5, this release adds several administrator tools.  In addition, the release provides improved integration with third party mail services; notably Google Apps and Zarafa.

  * Password policy engine 
  * Detailed disk usage reporting
  * Network traffic analyzer tool
  * Custom firewall tool
  * H323 support
  * Mail aliases added to directory (improved support Google Apps, Zarafa)
  * Base system upgraded to CentOS 5.5
  * Bug fixes

In addition, the following features are in the pipeline

  * 64-bit version (shipping later this year)
  * Basic RADIUS support
  * [[:content:en_us:dev_architecture_central_management_start|Central management]] beta 1 milestone

===== Download =====
==== New Install ==== 
[[http://download.clearfoundation.com/clearos/enterprise/5.2/iso/clearos-enterprise-5.2-beta1.iso|Download ClearOS Enterprise 5.2 Beta 1]] 

MD5: 198ec62d63102e0f3bb555e92de905f1

==== Upgrades from ClearOS 5.1 ====
To upgrade from ClearOS 5.1, run the following:

  yum clean all
  yum install app-upgradeto52
  yum upgrade 
 
Upgrades from ClarkConnect are not yet supported (testing is incomplete).

===== Feedback =====
Please post your feedback in the [[http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,39/func,showcat/|ClearOS Development and Test Release Forum]]. 

===== Feature Details =====
==== Password Policy Engine ====
Webconfig Menu: <navigation>Directory|Setup|Password Policies</navigation>

{{ :release_info:clearos_enterprise_5.2:password_policies.png|ClearOS Password Policies}} The password policy engine allows an administrator to enforce password policies:

  * Password minimum length
  * Password history
  * Password maximum age (expiration)
  * Password minimum age
  * Password strength

This feature is mostly functional in the alpha, but there are some rough edges.  If you do not see the password policy engine in the menu system, you can install the module with **yum install app-password-policies**

==== Network Traffic Analyzer ====
Webconfig Menu: <navigation>Reports|Network|Network Traffic</navigation>

The network traffic analyzer gives you a view into what is going on at the network level.  This can be a handy administrator tool for:

  * Locating virus infected desktops sending out spam, viruses, etc
  * Pinpointing network hogs (who is downloading that HD movie?)
  * Checking for unusual network activity

To install the traffic analyzer, run **yum install app-jnettop**
  
{{:release_info:clearos_enterprise_5.2:network_traffic.png|ClearOS Network Traffic Analyzer}}

==== Mail Aliases in LDAP / Directory ====
Webconfig Menu: <navigation>Directory|Accounts|Users</navigation>

{{ :release_info:clearos_enterprise_5.2:aliases_to_ldap.png|Mail Aliases in ClearOS Directory - LDAP}}
Mail aliases are now part of the ClearOS Directory (LDAP).  Why is this a "Good Thing"?  With mail aliases in the Directory, it is possible to integrate alternative mail solutions including Google Apps (to be released later this year) and Zarafa.  You can manage these aliases through the usual User Manager found in webconfig.  The adjacent screenshot shows two aliases for username **tim**:

  * thorton
{{keywords>clearos, clearos content, announcements, releases, clearos5.2, beta1, previoustesting, maintainer_dloper}}
