===== Configuring Partitions and RAID =====
For some installations, you may want to define a custom partition scheme instead of using the default.  Typically, custom partitioning is required for:

  * Software RAID
  * Creating a separate /home, /var/flexshare/shares, or /store/data0 partition

If you decide to use the default partitioning method, the system will create a small partition for '/boot', it will create a swap partition to match the size of RAM you have allocated to the system, and will place the remaining space for the root filesystem '/'.

^ **Partition** ^ **Size** ^ **Type** ^
| /boot | 500MB | XFS |
| Volume Group: clearos | All Remaining Space | LVM |
^ ^ ^ ^
| Logical Volume: swap | depending on RAM | located under VG 'clearos' |
| Logical Volume: root | All Remaining Space in VG | XFS under VG 'clearos' |





===== Advanced Partitioning =====
If you do not wish to use the default partitioning scheme on your system, then select **Create Custom Layout** in the installer's partitioning screen.  The tool allows for creating software RAID, logical volumes, swap space, and regular partitions.  The system is capable of creating ext2, ext3, ext4, swap, LVM, RAID, and vfat partition types.  You should be familiar with disk partitioning concepts and Linux requirements when using this option|

<note warning>When you launch the partition tool, you may get a message indicating that the partition is unreadable. This is normal for blank disk drives or when non-standard partition tables are used with your existing disk. Chose <button>Yes</button> to create the partition table.</note>

==== Storage Devices ====
ClearOS supports many advanced storage options and basic ones too. You will need to configure your Storage in order to proceed.


{{:content:en_us:7_installation_destination.png?550|ClearOS Installer - Installation Destination}}

=== Hard Disk Partitioning ===
{{ :documentation:clearos_professional_6:user_guide:installer_partitioning.png|ClearOS Installer - Partitioning}}Partitioning can be the trickiest part of the installation process.  In fact, there is a whole section of the User Guide dedicated to this topic and RAID.  Here are some suggestions to help guide the way.

**Use All Space** -- Select this option if you do //**not**// require software RAID nor have a need for a large data partition.  This option also assumes that ClearOS is not sharing the hard disk with other operating systems.

**Replace Existing Linux System** -- Similar to //Use All Space//, but this option preserves non-Linux partitions. 

**Shrink Current System** and **Use Free Space** -- Since ClearOS is usually the only operating system on the target system, these two options are rarely used.

**Create Custom Layout** -- If you use software RAID, have a need for a dedicated data share, have multiple disks installed, then please read  [[:content:en_us:7_c_configuring_partitions_and_raid|Configuring Partitions and RAID guide]] for more information. 

=== Encrypt System ===
{{ :content:en_us:7_installation_encrypt_choice.png?225}}
ClearOS supports encrypting the filesystem.  If your ClearOS system is not in a secure location and you are concerned about protecting the data in the scenario where the ClearOS system is physically stolen, then you may want to consider this option.  Please keep in mind, encrypting the filesystem also means that a decryption password is required on every reboot!  In other words, unattended reboots and headless operations are not supported.
{{ :content:en_us:7_installation_encrypt_settings.png?450 }}

=== Review and Modify Layout ===
If you would like to review the default partition layout, please select the **review and modify partition layout** option before proceeding to the next step.

<note warning>Please be careful!  The contents of all your hard disks on the target computer can be completely erased. This may include any attached hard drives like USB and external SATA!</note>


===== RAID Types =====
There are different RAID types to suit different deployment needs.  The following is a brief description of RAID types used in ClearOS deployments.

==== RAID 0 - Striping ====
RAID 0 is typically used when speed is the only concern.  This form or RAID is also called **striping**.  All the data in this type of array is spread over all the disks and the server is able to write and read the data quickly because it can read from all disks in the array simultaneously. RAID 0 is often used for high performance application servers and database servers where the data does not need protection or is preserved in some other manner.

**Advantages**
  * Fast Read/Write access
  * Can make very large volumes

**Disadvantages**
  * Failure rate higher than single disk and failure rate increases with each additional drive
  * Not bootable

[[:content:en_us:7_d_raid_0_-_striping|Configuring RAID 0]]

==== RAID 1 - Mirroring ====
RAID 1 is often used as a way to protect the drives on which the operating system is running or as an entry-level solution for basic data protection.  It is also called **mirroring** or **duplexing** (if the drives are on separate controllers). All data on the drive is mirrored from one partition to another. Data read occur from one drive only and data write operations are performed to both. RAID 1 is a well-rounded solution if basic redundancy is your goal. Here is a step-by-step guide to implement Software RAID 1 on regular IDE/SATA/SAS hard disks.

**Advantages**
  * Bootable
  * High level of protection

**Disadvantags**
  * Costly
  * No improvement for disk performance

[[:content:en_us:7_d_raid_1_-_mirroring|Configuring RAID 1]]

==== RAID 5 - Redundancy ====
RAID 5 is typically used on volumes where redundancy is required and optimum capacity is needed.  This form of RAID Partition is also called **striping with parity** and works by spreading the data across all the disks. Moreover a checksum is maintained and spread across all disks in such a manner that a single drive failure out of the whole array is tolerable. RAID 5 is typical for many storage servers.

**Advantages**
  * Fast Read Access
  * Cost effective
  * Can make very large volumes

**Disadvantages**
  * Nominal Write speed improvements
  * Multiple disk failure destroys all data
  * Not bootable

[[:content:en_us:7_d_raid_5_-_redundancy|Configuring RAID 5]]

==== RAID 6 - Extra Redundancy ====
RAID 6 is very similar to RAID 5 except that there are two drives allocated to parity instead of one. RAID 6 is more effective than RAID 5 with hot spare because the parity is maintained throughout instead of creation at the point of failure. This form or RAID Partition is typically used on volumes where extra redundancy is required and optimum capacity is needed. This form of RAID is similar to HP's ADG RAID and works by spreading the data across all the disks. Moreover a two checksums are maintained and spread across all disks in such a manner that the RAID can tolerate two disk failures. RAID 6 is typical for many storage servers.

**Advantages**
  * Fast Read Access
  * More reliable than RAID 5
  * Can make very large volumes

**Disadvantages**
  * Nominal Write speed improvements
  * Multiple disk failure destroys all data
  * Not bootable

[[:content:en_us:7_d_raid_6_-_extra_redundancy|Configuring RAID 6]]

===== Configuring Software RAID =====
Follow the links for a detailed configuration steps:

  * [[:content:en_us:7_d_raid_0_-_striping|RAID 0 - Striping]]
  * [[:content:en_us:7_d_raid_1_-_mirroring|RAID 1 - Mirroring]]
  * [[:content:en_us:7_d_raid_5_-_redundancy|RAID 5 - Redundancy (striping with parity)]]
  * [[:content:en_us:7_d_raid_6_-_extra_redundancy|RAID 6 - Extra Redundancy (striping with extra parity)]]


===== Testing Software RAID =====
You can validate your RAID system by looking at the status of the drive using the 'cat /proc/mdstat' command. You can also validate RAID1 and RAID 5 systems by removing a RAID member. 

==== Gathering RAID Statistics ====
The Proc subsystem will show you up-to-date information about your RAID members. Issue the following command to see the status of your RAID:

  cat /proc/mdstat

==== Redundancy Validation =====
Another way to validate redundancy is to manually create a 'failed' condition by physically disconnecting a RAID member. This is recommended for new systems as a way to validate this feature before production data is committed to the volume.

  * Power down the machine
  * Unplug the data connector from the drive (just unplugging the power is going to make the BIOS unhappy and the system will not be bootable)
  * Power up the machine
  * Check the data and the volume status
  * Power down the machine and re-attach the drive
  * Use 'watch cat /proc/mdstat' and monitor the volume in recover mode

===== Links =====
  * [[:content:en_us:7_d_raid_0_-_striping|RAID 0 - Striping]]
  * [[:content:en_us:7_d_raid_1_-_mirroring|RAID 1 - Mirroring]]
  * [[:content:en_us:7_d_raid_5_-_redundancy|RAID 5 - Redundancy (striping with parity)]]
  * [[:content:en_us:7_d_raid_6_-_extra_redundancy|RAID 6 - Extra Redundancy (striping with extra parity)]]
  * [[http://www.tldp.org/HOWTO/Software-RAID-HOWTO.html|Software RAID Howto]]
  * [[https://access.redhat.com/knowledge/docs/en-US/Red_Hat_Enterprise_Linux/6/html/Installation_Guide/s1-diskpartitioning-x86.html|Red Hat Installation Guide - Custom Layout]]
 
{{keywords>Partition RAID}}
{{keywords>clearos, clearos content, clearos7, userguide, categoryinstallation, subcategoryinstaller, subsubcategoryfullinstalliso, maintainer_dloper}}
