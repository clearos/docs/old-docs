===== Intrusion Detection =====
The **Intrusion Detection** app is included with ClearOS to make users more aware of some of the daily hostile traffic that can pass by your Internet connection. The software is able to detect and report unusual network traffic including attempted break-ins, trojans/viruses on your network, and port scans.

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Gateway|Intrusion Protection|Intrusion Detection</navigation>

===== Intrusion Protection Updates and ClearCenter =====
{{:omedia:clearsdn-icon-xxs.png }} The ClearCenter [[http://www.clearcenter.com/Services/clearsdn-intrusion-protection-4.html|Intrusion Protection Updates]] service is strongly recommended for deploying an effective intrusion protection system.  These signatures are compiled from third party organizations as well as internal engineering resources from ClearCenter.  We keep tabs on the latest available updates and fine tune the system so you can focus on more important things.  

The [[:content:en_us:6_intrusion_protection_updates|Intrusion Protection Updates]] app:

  * Provides 13,000+ additional signatures (compared to the base 1,150 signatures)
  * Weekly updates to keep up with the latest threats

===== Configuration =====
There are two different types of rules for the intrusion detection system.  The **Security** rules detect issues related to overall system security, while **Policy** rules detect issues related to your organization's Internet usage policies.  For example, the //chat// policy rules will detect instant messaging traffic that goes through your ClearOS system.

<note warning>Intrusion detection system does require some computing horsepower. If you find your system sluggish, you might want to take corrective action.</note>

===== Security ID - SID =====
A security ID (SID) is referenced in various parts of the intrusion detection and prevention systems.  These IDs reference individual signatures and come from various sources including [[http://www.sourcefire.com/|SourceFire]] and [[http://www.emergingthreats.net/|Emerging Threats]].  The following table provides information on the most common signatures detected by the [[:content:en_us:6_intrusion_prevention|Intrusion Prevention]] System:

^SID^Description^
|3000001|SSH brute force attack|
|3000002|FTP brute force attack|
|3000003|POP3 brute force attack|
|3100001|Scan detected via telnet attempt|

If you would like more information for a particular ID, use the [[http://www.snort.org/search/|SourceFire Snort ID Search]] for some of the signatures available in ClearOS.  If you are familiar with the command line, you can also find information on signatures be searching the files in /etc/snort.d/rules/clearcenter.

===== Links =====
  * [[http://www.snort.org/|Snort Intrusion Detection website]]
{{keywords>clearos, clearos content, Intrusion Detection, app-intrusion-detection, clearos6, userguide, categorygateway, subcategoryintrusionprotection, maintainer_dloper}}
