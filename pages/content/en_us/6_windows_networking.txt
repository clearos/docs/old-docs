===== Windows Networking =====
Your ClearOS system provides network and file serving capabilities for a Windows network.  Among other tasks, you can use the software for domain control, file storage and sharing printers.

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:6_Marketplace|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Server|Windows Networking|Windows Settings</navigation>

===== Configuration =====
==== Global Settings ====
=== Server Name ===
This is a one-word descriptive name of the system, for example: clearserver

=== Comment ===
This is a short description of the server.  When connecting to this system, this short description might be shown to guide end users on the purpose of the system.  An example: File and Print Server for Toronto

=== Printing ===
If you have a printer attached to your ClearOS system, you can share it via Windows networking.  There are two supported modes:

  * Raw - drivers must be installed on client system
  * Point and Click - drivers must be configured using the [[:content:en_us:6_advanced_print_server|Advanced Print Server]]

=== Windows 10 Domain Logons ===
Enable to support Windows 10 Domain Logons

<note warning>Since the Fall Creators Update (1709), Windows 10 is no longer shipping with the SMB1.0 protocol enabled. Without SMB1.0 support in Windows and with "Windows 10 Domain Logons" enabled you will not be able to join a ClearOS Domain or share files in Simple File Sharing mode with Windows 10. Please see the [[content:en_us:6_windows_networking&#tips_and_tricks|Tips and Tricks]] section below for the fix</note>

=== WINS Support / WINS Server ===
Even for small networks, we recommend using WINS for your Windows networks.  Among other things, this helps Windows systems find each other using system names (for example, browsing through the old //Network Neighborhood//).  You can either enable the WINS server on your ClearOS system, or you can specify an IP address of an existing WINS server on the network.

=== Administrator Password ===
The **winadmin** account is used for the Windows domain administrator.  Among other things, this account is used to add computers to the ClearOS domain.  Follow the link to change the password for this account.

==== Mode ====
ClearOS supports two modes for your network. 

=== Simple File and Print Server ===
This mode should be used for creating a basic file and print server.  Features such as network logons, logon scripts, and roaming profiles are disabled.  In order to access file shares, a client system can connect to the ClearOS system using standard file manager tools.

=== Primary Domain Controller / PDC ===
When configured as a primary domain controller, the following parameters must be specified:

**Windows Domain** - the domain name, for example: Toronto.

**Logon Script** - the script to execute when a user logs into the domain.  You can upload this script to the //netlogon// directory by logging into your Windows workstation as the Windows administrator (winadmin) and connecting to your ClearOS netlogon share (Start >> Run >> \\**servername**\netlogon).

**Roaming Profiles** - the state of roaming profile support for all users. 

**Logon Drive** - the drive letter used for the user's network drive on the ClearOS system.  This drive maps to the /home///username// directory on the ClearOS file system.  

==== List of Shares ====
The list of built-in shares is shown for convenience.  Depending on the settings selected in webconfig, shares will be active or inactive.  


===== Tips and Tricks =====
==== Windows 10 ====
Windows 10, since the Fall Creators Update (1709), is no longer shipping with SMB 1.0 support enabled. This means that if you enable "Windows 10 Domain Logons", Windows 10 machines can no longer access Windows Networking (Samba) Domains or Flexshares. If you try to join a ClearOS Domain you may get the following popup:

{{7_ug_samba_no-smb1.png}}

The cause is that SMB1.0 support is now disabled by default in Windows 10. The link takes you to [[https://support.microsoft.com/en-gb/help/4034314/smbv1-is-not-installed-by-default-in-windows|this Microsoft document]]. To enable SMB1.0 support see [[https://support.microsoft.com/en-us/help/2696547/how-to-detect-enable-and-disable-smbv1-smbv2-and-smbv3-in-windows-and|this Microsoft document]] or just go Control Panel > Programs and Features > "Turn Windows Features on and off" then scroll down to SMB 1.0/CIFS File Sharing Support and enable it. You will need to reboot afterwards. There is also a PowerShell method in the document.

Note you also need to do the registry settings below.

==== Windows 7 and later Registry Changes ====
Windows 7/8/10 systems can be joined to a ClearOS Domain Controller by adding or changing two registry settings: 

  HKLM\System\CCS\Services\LanmanWorkstation\Parameters
      DWORD  DomainCompatibilityMode = 1
      DWORD  DNSNameResolutionRequired = 0

After making the registry change, a reboot is required.

==== Windows 7 ====
Just after you have joined the Windows 7 system to the domain, you will see the following warning message:

  Changing the Primary Domain DNS name of this computer to "" failed.
  The name will remain "MYDOM".  The error was:
    
  The specified domain either does not exist or could not be contacted

You can ignore this message.  Also note: there is a hotfix available from Microsoft to address this issue, see [[http://support.microsoft.com/kb/2171571|Knowledge Base article]] for details.



===== Troubleshooting =====
Due to a feature in Microsoft networking, you may not see the ClearOS system in Network Neighborhood right away; sometimes it takes several minutes to appear. 

You can directly access the share by typing in the UNC path for the server in the Run section of your workstation.

  * Click on <navigation>Start|Run</navigation>
  * Type in \\serverIPaddress (for example \\192.168.1.1)
  * Click OK or press Enter.

Another good tool for troubleshooting problems in Windows is nbtstat. This tool will allow you to look and validate the name of your server and the domain as it appears to the network. If your server's address is 192.168.1.1 you could do the following:

  nbtstat -A 192.168.1.1
{{keywords>clearos, clearos content, AppName, app_name, clearos6, userguide, categoryserver, subcategoryfileandprint, maintainer_dloper, maintainerreview_x}}
