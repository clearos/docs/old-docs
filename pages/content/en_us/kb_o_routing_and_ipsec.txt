===== Routing and Ipsec =====
This Howto provides information on extra routes in IPsec VPN (Dynamic VPN).

===== The Problem: No Foreign Routes Allowed =====
The IPsec VPN is strict about what network traffic is allowed to flow through a the VPN tunnel.  Consider this scenario:

  * Headquarters with 2 LAN segments: 192.168.0.0/24 and 192.168.1.0/24
  * Remote office with 1 LAN segment: 10.0.0.0/24

The Dynamic VPN system will automatically connect the first LANs between the two systems.  When the tunnel is established, systems on 192.168.0.0/24 and 10.0.0.0/24 are connected.  However, systems from the headquarters' second LAN - 192.168.1.0/24 - will not be connected to 10.0.0.0/24.

You may be tempted to add a static route to force traffic down the IPsec VPN tunnel.  However, the IPsec policy engine will not allow this traffic to pass and the traffic is quietly dropped.

===== Solution =====
The Dynamic VPN settings on the headquarters system can be tweaked to include **both** LANs.  This can be done by adding/editing the LANNET parameter in /etc/firewall (version 5.x) or /etc/clearos/firewall.conf (version 6), or /etc/clearos/dynamic_vpn.conf for ClearOS Version 7.x.

  LANNET="192.168.0.0/23"
  
The /23 means that both 192.168.0.x and 192.168.1.x are included in the IPsec routing policy.  

After making this configuration change, restart the Dynamic VPN software on both ends of the connection.
{{keywords>clearos, clearos content, kb, howtos, maintainer_dloper, maintainerreview_x, keywordfix}}