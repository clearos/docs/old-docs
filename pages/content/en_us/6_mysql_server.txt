===== MySQL Server =====
The MySQL database is one of the world's most popular database systems.

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:

<navigation>Server|Database|MySQL Server</navigation>

===== Configuration =====
From this webconfig page, you can:

  * Start and stop the MySQL database
  * Set the database password
  * Access the link to the web-based database manager

When you start the database for the first time, you will see a warning message about setting a database password.  Please do so and remember it for later!  If you need to install web applications that depend on MySQL, you will need this password to configure the application.

Once you have a database password set, a link to the database manager will be shown.  Follow the link and login with the database //root// account (this is different than the system root account).  You can find detailed information on how to use this software [[http://www.phpmyadmin.net/home_page/docs.php|here]].


===== Help =====
==== Links ====
  * [[:content:en_us:kb_troubleshooting_resetting_the_mysql_root_password|Resetting The MySQL Root Password]]
  * [[http://www.phpmyadmin.net/home_page/docs.php|phpMyAdmin Database Manager documentation]]
  * [[http://www.mysql.com/ MySQL home page]]

==== Navigation ====

{{keywords>clearos, clearos content, MySQL, app-mysql, clearos6, userguide, categoryserver, subcategorydatabase, maintainer_dloper}}
