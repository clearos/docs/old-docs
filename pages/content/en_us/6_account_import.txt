===== Account Import =====
New users to the system can be imported from a CSV (Comma Separated Values) file. This can save time especially if you have a large user count and need an automated way to create users. You can also export users from the system to a spreadsheet.

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>System|Accounts Manager|Account Import</navigation>

===== Configuration =====
==== Importing Users ====
To import users you must have a well-formed CSV file of the users.  A template is available with field headers and sample data to guide you through the process.  The template is dynamically created - fields will be added or removed based on the user plugins you have installed (OpenVPN, Dropbox etc.).

Click on the "Download CSV Template" to copy a current version of the template to your desktop.

To start the upload:

  * Click on the <button>Choose File</button> button. This will open a window on your browser. 
  * Find and select the file and click the confirmation button.
  * Click the <button>Upload CSV</button> button and the system will indicate the number of users it will try to import.
  * Click the <button>Import Users</button> button and the users will begin importing into the system.

<note warning>
It is very important that the format or your CSV file is correct.  Some characters and symbols may not import properly can can cause some issues.

If your import reports errors, note the users that had problems and fix them. The system will not import users that have already been added.
</note>

===== CSV Format =====
It is important to ensure that the utility you use to create your import CSV file follow the //exact// same format as the export sample file.  One common problem users encounter is that when exporting/saving their CSV from a spreadsheet program like Excel or OpenOffice Calc, the export file will strip (remove) the double quotations around fields and data.  This will lead to import errors.

==== Good CSV Format Example ===
    "core.username","core.password","core.first_name","core.last_name"
    "wshatner","1234","William","Shatner"
==== Bad CSV Format Example ====
    core.username,core.password,core.first_name,core.last_name
    wshatner,1234,William,Shatner

===== Saving your CSV Import File =====
==== Open Office ====
When you are finished editing the template in Open Office Calc, select "File --> Save As".
{{:omedia:account_import_csv1.png|Save As}}

Select the "File Type" down arrow and select "Text CSV".  In doing so, the "Edit filter settings" checkbox will become enabled.

{{:omedia:account_import_csv2.png|CSV File Type}}

Make sure to check the box "Edit filter settings" before clicking "Save".

{{:omedia:account_import_csv2.png|CSV File Type}}

Once you click "Save", you may be asked whether you want to keep the existing format or save in ODF Format.  Select "Keep Currnet Format".  A dialog box will open allowing you to customize your CSV settings.  Ensure the following settings:

Character Set: UTF-8
Field Delimiter: , (comma)
Text Delimiter: " (double quote)

{{:omedia:account_import_csv4.png|Settings}}

Click "Save".  You can now upload this import file to your ClearOS server and begin the import.
{{keywords>clearos, clearos content, Account Import, app-account-import, clearos6, userguide, categorysystem, subcategoryaccountmanager, maintainer_dloper}}
