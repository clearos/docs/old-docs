===== Microsoft Active Directory Connector is not compatible error =====
If you have installed the OpenLDAP directory on your system and want to change to the Active Directory connector, you will get a message stating that:

  * The app Microsoft Active Directory Connector is not compatible with app-openldap-directory which is already installed on your server. See Marketplace documentation on the correct procedure to remove an app.

Or if you tried uninstalling from the Marketplace you still may encounter a message stating:

  * The app Microsoft Active Directory Connector is not compatible with app-openldap-directory-core which is already installed on your server. See Marketplace documentation on the correct procedure to remove an app.

{{:content:en_US:adconnectornotcompatible.png?550}}

Because the 'core' app is not visible from the marketplace, you need to uninstall it from command line. Unfortunately there are a lot of files associated with this app and so you have to ensure that you fix that issue once you uninstall.

===== To The Command Line =====
Open up a command prompt either at the terminal window of the physical box itself (Ctrl+Alt+F2) or use a terminal program such as PuTTY to SSH to the server. Once logged in under the root account, run the following:

  no | yum remove app-openldap-directory-core app-openldap-directory

At the very least, you will get a list like this:

<code>

================================================================================
 Package                      Arch    Version          Repository          Size
================================================================================
Removing:
 app-openldap-directory-core  noarch  1:2.1.6-1.v7     @clearos-verified  206 k
Removing for dependencies:
 app-samba                    noarch  1:2.2.20-1.v7    @clearos-verified  204 k
 app-samba-core               noarch  1:2.2.20-1.v7    @clearos-verified  151 k
 app-samba-extension-core     noarch  1:2.1.6-1.v7     @clearos-verified   27 k

Transaction Summary
================================================================================
Remove  1 Package (+3 Dependent packages)
</code>

You may possibly get a list that is bigger than this and even one that contains apps that you already have installed and configured. We suppose that you don't want to configure them again.

We passed a parameter which effectively says no to the uninstall process. The purpose of exercise was to demonstrate why the marketplace cannot simply remove the openldap-directory. Doing so would prove dangerous in some cases. 

Now run the following command:

  rpm -e --nodeps app-openldap-directory app-openldap-directory-core

Follow up by installing the AD Connector

  yum install app-active-directory

===== Validation =====
To see that these are now switched, log into Webconfig and run the marketplace. Attempt to install the OpenLDAP directory (Directory Server app) and you will see that the compatibility constraint is now reversed.

{{:content:en_US:directoryservernotcompatible.png?550}}
