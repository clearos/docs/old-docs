===== Mail Queue =====
The **Mail Queue** feature lets you examine mail messages in the queue.  On a ClearOS mail system, you will typically find a number of messages in the queue:

  * Delayed messages due to remote mail server outages
  * Delayed messages due to blocking policies on remote mail servers
  * Invalid bounce messages from remote mail servers

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
{{keywords>clearos, clearos content, userguide, Mail Queue, app-mail_queue, clearos5, categoryserver, subcategorymail, maintainer_dloper}}
