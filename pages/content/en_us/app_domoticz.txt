{{ :userguides:domoticz.svg?80}}
<WRAP clear></WRAP>

===== Domoticz Home Automation =====
Domoticz is a light weight home automation system that lets you monitor and configure miscellaneous devices, including lights, switches, various sensors/meters like temperature, rainfall, wind, ultraviolet (UV) radiation, electricity usage/production, gas consumption, water consumption and many more.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/server/domoticz|here]]
===== Documentation =====
==== Version 6 ====
Domoticz is not available for ClearOS 6.
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_domoticz|here]].
==== Additional Notes ====
