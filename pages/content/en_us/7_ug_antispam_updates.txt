===== Antispam Updates =====
The **ClearCenter Antispam Updates** app provides additional daily signature updates to improve the effectiveness of the antispam system.  These signatures are compiled from third party organizations as well as internal engineering resources from ClearCenter.  We keep tabs on the latest available updates and fine tune the system so you can focus on more important things.

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Server|Mail|Antispam Updates</navigation>

===== Activation =====
  * Login to your [[https://secure.clearcenter.com/portal/|ClearCenter account]].
  * Hover over the 'Systems' submenu in the top navigation bar.
  * Click on <button>Antispam Updates</button> under the 'System Services' heading.
  * Select the target system from the list of active systems in your account.
  * Select a subscription from the drop down list and make sure the service is enabled.
  * Click on <button>Update</button> to complete the activation.
===== Configuration ======
{{keywords>clearos, clearos content, Antispam Updates, app-antispam-updates, clearos7, userguide, categorycloud, subcategoryupdates, maintainer_bchambers}}
