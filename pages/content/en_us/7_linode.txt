===== Linode =====
This document provides information on how to deploy ClearOS on Linode.

===== Launching =====
To create a Linode server with ClearOS:

  * Login to [[https://www.linode.com/?r=b6c99e77c3f9385a81140f8f486fd42ce4900a7e|Linode]] (there's a referral code in the URL -- that funds more mirrors!)
  * Create or rebuild a node
  * Click on the **Deploying using StackScripts** link 
  * Search for **clearos**
  * Select the **ClearOS 7** StackScript and continue

===== First Boot Wizard =====
It takes a few minutes for the system to upgrade the CentOS image to ClearOS.  When the system is ready, you will be able to login via the web-based administration tool:

  * <nowiki>https://w.x.y.z:81</nowiki>

Proceed through the rest of the web-based install wizard to tune your system and install apps!

{{keywords>clearos, clearos content, clearos7, userguide, categoryinstallation subcategoryinstaller subsubcategoryvirtualmachines, maintainer_dloper}}
