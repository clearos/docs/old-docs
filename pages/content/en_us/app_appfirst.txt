{{ :userguides:appfirst.svg?80}}
<WRAP clear></WRAP>

===== AppFirst System Monitor =====
The AppFirst, cloud-based network and server monitoring service delivers unified visibility from many data sources.  Custom dashboards, historical datasets, data correlation and the ability to easily graph data provides administrators with an essential tool to proactively manage the system.

AppFirst uses a freemium model for up to 3 systems per account with restricted bandwidth usage.  Two paid levels offer higher data usage and longer retention time of collected stats.

This app requires the creation of an <a href='http:www.appfirst.com' target='_blank'>AppFirst account</a>.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/cloud/appfirst|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_appfirst|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_appfirst|here]].
==== Additional Notes ====
