===== RAID 5 - Striping with Parity =====
Linked from: [[:content:en_us:7_c_configuring_partitions_and_raid|Configuring Partitions and RAID]]

===== Preparing the Hardware =====
It is easy to set up software-based RAID5 from the installer of ClearOS. First, make sure that your hardware is installed and operating. ClearOS will automatically detect your hardware but you will need to make a custom partition if you are going to install ClearOS on a RAID5. 

===== Limitations =====
It is important to note that ClearOS will NOT want to use RAID5 for your boot partition. This means that the kernel and initrd files need to be located in a partition where they can be plainly read. We recommend then that you place these in a RAID1 partition even if that means that there are more than just 2 members. The good news is that this is generally a small partition of about 500 MB and will give you the ability to boot the system from various drives. We further recommend, although not demonstrated here, that you place your OS across mirrored partitions to allow the system to boot even if your RAID is down. Generally you can get by with about 10 G for the /root partition if you also segregate the /var and /var/log partitions elsewhere.

===== Custom Layout =====
==== Getting to the custom partitioning ====
During your installation, you will come to the second page which details an installation summary as depicted here:

{{:content:en_us:7_ug_installation_summary.png?550|Installation Summary}}

Choose Installation Destination. Select all the disks that you want to use for your installation of ClearOS which involves your hard disks. They will appear with black 'checks' when selected. Also choose the option "**I will configure partitioning.**" under the 'Other Storage Options' section.

{{:content:en_us:7_ug_raid5-step1.png?550|Step 1}}

When these are all selected, press '**Done**' in the upper left corner. 

==== Previous Partitions ====
If you have previous partitions on these drives, you may want to get rid of them so that you can use all the space needed. If you don't have previous partitions, skip this section. Expand an old installation and highlight a partition. Then click the 'minus' sign to remove it.

{{:content:en_us:7_ug_raid5-step2.png?550|Step 2}}

You may be prompted to get rid of all the other partitions as well at this time. If that suits your purpose, check the box and click '**Delete It**'.

{{:content:en_us:7_ug_raid5-step3.png?550|Step 3}}

==== Creating New Partitions ====
With a blank slate, your configuration may look like this:

{{:content:en_us:7_ug_raid5-step4.png?550|Step 4}}

At a minimum you will want to add the following partitions:

  * /boot
  * swap
  * /

These three are the most basic you will want/need for RAID5. If you are going to run without swap, the system will let you but it will complain. The /boot partition cannot be on RAID5 so we will create it as a RAID1 mirror. The swap partition should ALSO NOT be RAID 5 for performance reasons and should be RAID1. It is a mistake to create 'swap' as a RAID0 even though RAID0 would have a high performance. If one of the disks crashes, it will crash the memory of the system which kind of defeats the whole purpose of RAID 5 for redundancy.

=== /boot ===
Click the '**plus**' icon to start adding partitions. In the mount point dialog window, add '/boot' as the Mount Point and set the Desired Capacity to '500 MB'. Click '**Add mount point**'.

{{:content:en_us:7_ug_raid5-step5.png?300|Step 5}}

In the next window, we will change some of the behavior. Under the 'Device Type:' section, change the value from 'Standard Partition' to 'RAID'. This will also add the 'RAID Level' dialog and set it to the default of 'RAID1 (Redundancy)'. 

<note>You may also want to change the 'File System:' type from '**xfs**' to '**ext4**'. While both are very supported, the main advantage of XFS is the ability to have large volumes (which we don't need for our boot partition) and the advantage of ext4 is the ability to not only grow but also shrink. There are also better tools for repairing corrupted volumes under ext4.</note>

{{:content:en_us:7_ug_raid5-step6.png?550|Step 6}}

When you are done making your modifications, press the '**Update Settings**' button.

=== swap ===
Click the '**plus**' icon to add the swap partition. In the mount point dialog window, add 'swap' as the Mount Point and set the Desired Capacity to '1 GB' or '2GB'. The amount of swap that you want really depends on your situation. Usually adding more that 2GB is rarely neccessary. Click '**Add mount point**'.

{{:content:en_us:7_ug_raid5-step7a.png?300|Step 7}}

In the next window, we will change some of the behavior. Leave 'Device Type:' as 'LVM'. Click '**Modify**' under the 'Volume Group' section. Change the 'RAID Level' dialog and set it to 'RAID1 (Redundancy)'. 

{{:content:en_us:7_ug_raid5-step7b.png?300|Step 7}}

Click Save.

<note>The 'File System:' type will be set to 'swap'. This is REQUIRED!!</note>

{{:content:en_us:7_ug_raid5-step8.png?550|Step 8}}

When you are done making your modifications, press the '**Update Settings**' button.

=== / (root) ===
For purposes of this demonstration we are going to set up root as RAID5. Our recommendation is to have root also mirrored and configured similar to how you would do the /boot partition and then use a combination of additional mounts for /var, /var/log/ and /store/data0 for all of your other data which could happily reside on a RAID5 partition. You can then use mounts and [[:content:en_us:kb_o_storage_manipulation_using_bindmounts|bindmounts]] to put it all together.

Click the '**plus**' icon to add the root partition. In the mount point dialog window, add '/' as the Mount Point and leave the Desired Capacity blank or specify an amount. Click '**Add mount point**'.

{{:content:en_us:7_ug_raid5-step9.png?300|Step 9}}

In the next window, we will change some of the behavior. Leave 'Device Type:' set to 'LVM'. Under the 'Volume Group' click '**Modify**'. Change the 'RAID Level' dialog and set it to 'RAID5 (Distributed Error Checking)'. 

{{:content:en_us:7_ug_raid5-step10.png?350|Step 10}}

Click **Save**. Click **Update Settings**.

When you are done making your modifications, press the '**Update Settings**' button.

==== Finishing Up ====
Once the partitions are the way that you want them, click '**Done**'. If you are running without swap, you will need to click 'Done' twice.

{{:content:en_us:7_ug_raid5-step11.png?350|Step 11}}

At the '**SUMMARY OF CHANGES**' dialog box, click '**Accept Changes**'.


===== Links =====
  * [[:content:en_us:7_c_configuring_partitions_and_raid|Configuring Partitions and RAID]]
  * [[:content:en_us:7_d_raid_0_-_striping|RAID 0 - Striping]]
  * [[:content:en_us:7_d_raid_1_-_mirroring|RAID 1 - Mirroring]]
  * [[:content:en_us:7_d_raid_6_-_extra_redundancy|RAID 6 - Extra Redundancy]]

{{keywords>clearos, clearos content, clearos7, userguide, categoryinstallation, subcategoryinstaller, subsubcategoryfullinstalliso, maintainer_dloper}}
