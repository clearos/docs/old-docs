===== Splitting DNS Caches to use Specific DNS Server for Domains =====
In certain cases you need the ClearOS server to use a different DNS server for its general lookups and a specific server for a certain domain. This is useful when using the Active Directory connector to maintain a DNS connection for your domain while using a different DNS server generally. This is particularly useful if your DNS servers are being overwritten by DHCP on your External interface.

===== Configuration =====
In this example, we will use the internal DNS server 192.168.1.10 to resolve any queries for mydomain.lan. You can modify the /etc/dnsmasq.conf, but it is better to create a new file /etc/dnsmasq.d/{anything_you_want}):

  nano /etc/dnsmasq.d/domain.conf

Add the following to the end of the file:

  server=/mydomain.lan/192.168.1.10

Save the file and exit. Then restart the dnsmasq service:

  service dnsmasq restart

===== Multiple DNS Servers =====
Adding additional DNS servers from within your organization can help to strengthen the robust nature of your DNS, especially for Active Directory configurations. To list additional servers, simply add them as additional lines:

  server=/mydomain.lan/192.168.1.10
  server=/mydomain.lan/192.168.2.10
  server=/mydomain.lan/192.168.3.10
  server=/mydomain.lan/192.168.4.10

Your AD DNS servers contain a list of all your domain controllers via SRV records that are discoverable via DNS. This way if your main server is down, looking up your other servers is possible from within Samba.

{{keywords>clearos, clearos content, KB Old ClearOS Guides Splitting DNS Caches to use Specific DNS Server for Domains, app_name, versionx, xcategory, maintainer_dloper, maintainerreview_x, keywordfix, userguides}}
