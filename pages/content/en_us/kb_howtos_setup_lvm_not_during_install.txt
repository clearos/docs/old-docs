===== Setup LVM post-install =====
LVM devices are capable of wonderful things such as resizing and even snapshotting. Snapshots are great devices for backups! You can snapshot a volume and back it up instead of your main partition. This allows you to backup a point in time since backing up a volume 'in use' means that it is in a state of constant change. LVM volumes can be created on non-RAID and [[:content:en_us:kb_howtos_setup_raid_not_during_install|RAID devices]]. You can also create a snapshot volume that can be used to backup our disk. Snapshot volumes only need to be as big as the changes that will occur during our backup or while it is needed. For example, if 1 GB or less of data is expected to change during the backup cycle, your volume only needs to be 1 GB in size. If you run out of space on the snapshot, the volume is dropped. 

After you finish creating your LVM volume, you will need to [[:content:en_us:kb_howtos_setup_formatting_disks_post_install|format your partition]].

===== Using LVM =====
Start by making a partition on your RAID or standard volume that uses all of the disk except for the snapshot size. On larger disks, you can reserve even more. Remember, it is trivial to grow a volume into empty space so there is little to lose by setting the volume size to 90% of its capacity as a standard practice and then grow it later.

  pvcreate /dev/md0

Next, you can view your creation using:

  pvs

Next, create a volume group for your data. For this, we will use the ‘data’ volume group:

  vgcreate data /dev/md0

You can view your volume group with:

  vgs

From here you can see how much space is available you can create the logical volume now while reserving some space for your snapshots, decide how big it will be. For this example, I’m making it 8GB in size and calling it ‘data0’:

  lvcreate -L 8G -n data0 data

You can view the logical volume with:

  lvs

The ‘vgs’ command will also show you the consumption of the logical volume on the volume group.

===== Managing LVM =====
==== Adding to your LVM ==== 
You can manage LVM with in many ways like adding more space, shrinking volumes and even making snapshots. To add more space, run:
  lvextend -L +500M /dev/mapper/data-data0
  lvs
  vgs

==== LVM Attributes ====
Looking at Attributes with ‘lvs’: The ‘Attr’ section has flags for various things. here is their meaning.

  * Volume type: (m)irrored, (M)irrored without initial sync, (o)rigin, (O)rigin with merging snapshot, (r)aid, (R)aid without initial sync, (s)napshot, merging (S)napshot, (p)vmove, (v)irtual, mirror or raid (i)mage, mirror or raid (I)mage out-of-sync, mirror (l)og device, under (c)onversion, thin (V)olume, (t)hin pool, (T)hin pool data, raid or thin pool m(e)tadata
  * Permissions: (w)riteable, (r)ead-only, (R)ead-only activation of non-read-only volume
  * Allocation policy: (a)nywhere, (c)ontiguous, (i)nherited, c(l)ing, (n)ormal This is capitalised if the volume is currently locked against allocation changes, for example during pvmove(8).
  * fixed (m)inor
  * State: (a)ctive, (s)uspended, (I)nvalid snapshot, invalid (S)uspended snapshot, snapshot (m)erge failed, suspended snapshot (M)erge failed, mapped (d)evice present without tables, mapped device present with (i)nactive table
  * device (o)pen
    * Target type: (m)irror, (r)aid, (s)napshot, (t)hin, (u)nknown, (v)irtual. This groups logical volumes related to the same kernel target together. So, for example, mirror images, mirror logs as well as mirrors themselves appear as (m) if they use the original device-mapper mirror kernel driver; whereas the raid equivalents using the md raid kernel driver all appear as (r). Snapshots using the original device-mapper driver appear as (s); whereas snapshots of thin volumes using the new thin provisioning driver appear as (t).
  * Newly-allocated data blocks are overwritten with blocks of (z)eroes before use.
  * (p)artial: One or more of the Physical Volumes this Logical Volume uses is missing from the system.

Some typical outputs might be ‘-wi-a-----’ or ‘-wi-ao----’ which represents a:

  -wi-a-----
writeable, inherited, active

  -wi-ao----
writeable, inherited, active, open/mounted
 

==== Activating and Deactivating an LVM ====
To toggle between an active LVM partition and inactive partition you can use the vgchange command. Dismount volumes before deactivating them (o in sixth attribute column). You can look at the status with the ‘lvs’ command. If it has an ‘a’ in the fifth column deactivate it.
  vgchange -an data

To activate a volume:
  vgchange -ay data

==== Reduce a volume size ====
You can reduce size of a logical volume. NOTE: YOU MUST RESIZE AND REDUCE THE PARTITION FIRST. If your partition is already reduced in size or if you don’t have a partition yet you can reduce it at will now, run:
  lvreduce --size -500M /dev/mapper/data-data0

==== Creating a snapshot of a volume ====
You can snapshot a volume provided that you have free space in the volume group containing the target drive. Review this with ‘vgs’. Create a snapshot in the volume group of your target drive and call it after the name of your volume and a timestamp. For the date stamp, you can place the epoch time on the snapshot. Use this:
  datestamp=$(date +%s) && lvcreate --size 1G  --snapshot --name snap-data0-${datestamp} /dev/mapper/data-data0

When the volume is created, it will tell you the name of the volume created. At this point, the snapshot volume can be mounted. We suggest mounting as read-only which is appropriate for backup. NOTE: ignore the ‘cow’ (Copy on Write) volume. It is the original snapshot that is preserved so that if you do mount the snapshot as read/write, it will preserve the original.
  mkdir -p /store/snap-data0
  mount -o ro,nouuid /dev/mapper/snap-data0-${datestamp} /store/snap-data0
 
To remove the snapshot after your backup, run:
  lvremove /dev/mapper/data-snap--data0--${datestamp}

To remove a regular logical volume, unmount the disk and run:
  lvremove /dev/mapper/data-data0
