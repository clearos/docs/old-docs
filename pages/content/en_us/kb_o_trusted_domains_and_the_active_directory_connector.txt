===== Trusted Domains and the Active Directory Connector =====
The Active Directory Connector is capable of many great things. But it has to bridge the gap between the way Microsoft does things and the way that [[http://en.wikipedia.org/wiki/POSIX|POSIX]] does things. This guide will show you why problems with Trusted Domains on your Windows side of things can cause problems on your ClearOS Active Directory Connector and what you can do about it. Once you know what is wrong, you will better be able to make a few changes based on your decisions about what you want to happen on your ClearOS server.

Here are some acronym meanings that will prove useful in this guide:

  * UID - User IDentifier
  * GID - Group IDentifier
  * SID - Security IDentifier
  * RID - Resource IDentifier

===== Why and how this is a problem =====
In ClearOS, users and groups have ID numbers, called GIDs and UIDs, that track the ownership of objects. Windows uses SIDs and RIDs for both users and groups. The SID in Windows is a long string of numbers which describes the domain. This numbered prefix is common to ALL users and ALL groups on a Windows domain and shows up as an attribute on every file in the file system. With the Active Directory Connector attached, you can look at what this SID is on your server and compare it to your Windows Domain:

  net getdomainsid

This will yield a result such as:

  SID for local machine SERVER is: S-1-5-21-234567890-123456789-3001234321
  SID for domain MYDOMAIN is: S-1-5-21-234567890-123456789-3001234321

In POSIX and in ClearOS we don't have such a thing as a SID at the file system level. To track the ownership then, a conversion is made and ClearOS typically uses the RID only as a unique identifier. For example, on Windows the full SID for the Domain Admins group is the 'Domain SID' plus the RID 512. Using our example above, every file that has the group ownership by the 'Domain Admins' group has the SID:

  S-1-5-21-234567890-123456789-3001234321-512

On ClearOS, we take the RID only and assign it as the group permission:

  512

For example:

  drwxrwxr-x.   7 flexshares 512         4096 Oct 23 10:36 directoryname

So this is why we have a problem when we have multiple trust domains. How does the Active Directory connector (which by default uses the RID to describe the UID and GID in ClearOS) compensate for the fact that there are two or MORE 'Domain Admin' groups in the groups trusted in a trust situation on Windows? It can't using the default mechanism. That is why it will fail and we end up reading documents like this.

The default mechanism in the Active Directory Connector is to create a map between the user on Windows and the user on POSIX. By default, we have it set to use the RID as the primary method. We do this because this means that ALL ClearOS boxes that use the domain will have the same UID and GID on their files from one box to the next. The purpose for this is that if you are sharing a remote resource like a NAS, iSCSI target or other then your UID and GID numbers won't get mixed up. Another method is to use an arbitrary assignment of numbers that is box specific. Another method is to set up a range of numbers for each domain and add the RID. Each of these has their own strengths and weaknesses. Since a majority of environments are single domain, we've chosen to use the RID method.

These are the three mechanisms typically used:

  * TDB
  * LDAP
  * RID

=== TDB ===
The TDB method makes arbitrary assignments. This is what you would use typically for a very 'standalone' box for file sharing that didn't have a backend like LDAP or an Active Directory connector. We don't use this method on ClearOS because of its limitations. This fails for us in the Active Directory Connector because the moment that you assign random numbers is the moment that your permission get out of whack when you start sharing a storage volume between devices.

  idmap config * : backend        = tdb
  idmap config * : range          = 1000000-1999999

=== LDAP ===
The LDAP method is good for ClearOS as a standalone server or if you are using Account Synchronization of the LDAP database between ClearOS servers. Basically the UID and GID are set arbitrarily as well but they then are replicated using LDAP. This way the UIDs and GIDs are the same across all boxes.

  idmap config * : backend = ldap
  idmap config * : range = 20000000-29999999

===RID===
RID is great because the RID correlates with Active Directory and the values are persistent across the whole domain. This means that no matter how many ClearOS systems that use the Active Directory Connector, the mapping will be the same because they are based on the RID part of the SID.

  idmap config * : backend = rid
  idmap config * : range = 20000000-29999999

RID falls down though during trusts and there are two ways to deal with this. The first method works well because you merely tell the Active Directory Connector to ignore the other domain:

  idmap config * : backend = rid
  idmap config * : range = 20000000-29999999
  winbind:ignore domains = TRUSTED_DOMAIN1 TRUSTED_DOMAIN2

That doesn't always work though because sometimes you need ClearOS to see the users from the trusted domains as well. No problem, merely add ranges for them as well. Just keep this config the same between all your ClearOS and POSIX boxes.

  idmap config MYDOMAIN : backend  = rid
  idmap config MYDOMAIN : range = 20000000-29999999
  idmap config TRUSTED_DOMAIN1 : backend = rid
  idmap config TRUSTED_DOMAIN1 : range = 30000000-39999999
  idmap config TRUSTED_DOMAIN2 : backend = rid
  idmap config TRUSTED_DOMAIN2 : range = 40000000-49999999
{{keywords>clearos, clearos content, KB Old ClearOS Guides Trusted Domains and the Active Directory Connector, app_name, versionx, xcategory, maintainer_dloper, maintainerreview_x, keywordfix, userguides}}
