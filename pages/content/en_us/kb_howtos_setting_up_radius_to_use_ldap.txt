===== Setting up Radius to Use LDAP =====
This guide covers the installation of FreeRADIUS and does not include EAP or encryption. Moreover, FreeRADIUS is being replaced by FreeRADIUS2 in subsequent versions of ClearOS. Please use the [[:content:en_us:kb_howtos_setting_up_freeradius2_to_use_ldap|guide for FreeRADIUS2]] instead of this HOWTO unless you absolutely need the original FreeRADIUS.


===== Deprecated =====
<note warning>The [[http://www.clearcenter.com/marketplace/network/RADIUS_Server.html|RADIUS Server]] app is now available via Marketplace. The following document is here for historical purposes only.</note>

===== Installing RADIUS =====

  yum -y install freeradius


The results may say something about installing some dependencies:

  Dependency Installed: lm_sensors.i386 0:2.10.7-4.el5 net-snmp.i386 1:5.3.2.2-7.el5_4.2 net-snmp-utils.i386 1:5.3.2.2-7.el5_4.2
Complete!

This install will also create a directory in /etc called raddb. We will be working out of this directory to configure RADIUS.

===== Setup Webconfig =====
Create a group called 'radius_users' in Webconfig. Set any users you want to be members of this group.

While you are here create a test user that is NOT part of the radius_users group. We will use this user to test that they cannot access the system via RADIUS.

===== Configuring LDAP =====

Uncomment the //ldap// line in the /etc/raddb/radiusd.conf file. This line is in the authorize section (you can find it near the search match for 'ldap module will').

**BEFORE**

        #  The ldap module will set Auth-Type to LDAP if it has not
        #  already been set
        #ldap

**AFTER**

        #  The ldap module will set Auth-Type to LDAP if it has not
        #  already been set
        ldap

Uncomment the three lines for 'Auth-Type LDAP.

**BEFORE**

  #       Auth-Type LDAP {
  #               ldap
  #       }

**AFTER**

         Auth-Type LDAP {
                 ldap
         }

Replace this section in radiusd.conf. It's a good idea to backup the file too (cp radiusd.conf radiusd.conf~).

**BEFORE**

        ldap {
                server = "ldap.your.domain"
                # identity = "cn=admin,o=My Org,c=UA"
                # password = mypass
                basedn = "o=My Org,c=UA"
                filter = "(uid=%{Stripped-User-Name:-%{User-Name}})"
                # base_filter = "(objectclass=radiusprofile)"
                # set this to 'yes' to use TLS encrypted connections
                # to the LDAP database by using the StartTLS extended
                # operation.
                # The StartTLS operation is supposed to be used with normal
                # ldap connections instead of using ldaps (port 689) connections
                start_tls = no
                # tls_cacertfile        = /path/to/cacert.pem
                # tls_cacertdir         = /path/to/ca/dir/
                # tls_certfile          = /path/to/radius.crt
                # tls_keyfile           = /path/to/radius.key
                # tls_randfile          = /path/to/rnd
                # tls_require_cert      = "demand"
                # default_profile = "cn=radprofile,ou=dialup,o=My Org,c=UA"
                # profile_attribute = "radiusProfileDn"
                access_attr = "dialupAccess"
                # Mapping of RADIUS dictionary attributes to LDAP
                # directory attributes.
                dictionary_mapping = ${raddbdir}/ldap.attrmap
                ldap_connections_number = 5
                #
                # NOTICE: The password_header directive is NOT case insensitive
                #
                # password_header = "{clear}"
                #
                # Set:
                #       password_attribute = nspmPassword
                #
                # to get the user's password from a Novell eDirectory
                # backend. This will work *only if* freeRADIUS is
                # configured to build with --with-edir option.
                #
                #
                #  The server can usually figure this out on its own, and pull
                #  the correct User-Password or NT-Password from the database.
                #
                #  Note that NT-Passwords MUST be stored as a 32-digit hex
                #  string, and MUST start off with "0x", such as:
                #
                #       0x000102030405060708090a0b0c0d0e0f
                #
                #  Without the leading "0x", NT-Passwords will not work.
                #  This goes for NT-Passwords stored in SQL, too.
                #
                # password_attribute = userPassword
                #
                # Un-comment the following to disable Novell eDirectory account
                # policy check and intruder detection. This will work *only if*
                # FreeRADIUS is configured to build with --with-edir option.
                #
                # edir_account_policy_check=no
                #
                # groupname_attribute = cn
                # groupmembership_filter = "(|(&(objectClass=GroupOfNames)(member=%{Ldap-UserDn}))(&(objectClass=GroupOfUniqueNames)(uniquemember=%{Ldap-UserDn})))"
                # groupmembership_attribute = radiusGroupName
                timeout = 4
                timelimit = 3
                net_timeout = 1
                # compare_check_items = yes
                # do_xlat = yes
                # access_attr_used_for_allow = yes
                #
                #  By default, if the packet contains a User-Password,
                #  and no other module is configured to handle the
                #  authentication, the LDAP module sets itself to do
                #  LDAP bind for authentication.
                #
                #  You can disable this behavior by setting the following
                #  configuration entry to "no".
                #
                #  allowed values: {no, yes}
                # set_auth_type = yes
        }

**AFTER**

        ldap {
                server = "localhost"
                basedn = "dc=clearos,dc=lan"
                password_attribute = "userPassword"
                filter = "(uid=%{Stripped-User-Name:-%{User-Name}})"
                start_tls = no
                groupname_attribute = cn
                groupmembership_filter = "(|(&(objectClass=GroupOfNames)(member=%{Ldap-UserDn}))(&(objectClass=GroupOfUniqueNames)(uniquemember=%{Ldap-UserDn})))"
                groupmembership_attribute = radius_users
                #Default values
                dictionary_mapping = ${raddbdir}/ldap.attrmap
                ldap_connections_number = 5
                timeout = 4
                timelimit = 3
                net_timeout = 1
                # set_auth_type = yes
        }

Configure the users file /etc/raddb/users. Blow the file away or move it aside (mv users users~)

Create a new /etc/raddb/users file that contains the following:

  DEFAULT LDAP-Group != "radius_users", Auth-Type := Reject
  DEFAULT Auth-Type := LDAP
        Fall-Through = 1

===== Testing RADIUS authentication to LDAP =====
By default, the clients.conf will allow for local testing using the password 'testing123'.

To start radius in debug mode, ensure that RADIUS is stopped and run the following:

  service radiusd stop && radiusd -X -A

In a different terminal, run the following against users. Test both the user that is part of the 'radius_users' group and the user that you implicitly did NOT add to the group in the Webconfig section above.

  radtest testuser "testuserpassword" localhost 2 testing123

Replace 'testuser' and 'testuserpassword with the appropriate information. 

The results will look like this:

**Success**
  Sending Access-Request of id 200 to 127.0.0.1 port 1812
	User-Name = "testuser"
	User-Password = "testuserpassword"
	NAS-IP-Address = 255.255.255.255
	NAS-Port = 2
  rad_recv: Access-Accept packet from host 127.0.0.1:1812, id=200, length=20

**FAILURE**
  radtest testuser "badpassword" localhost 2 testing123
  Sending Access-Request of id 234 to 127.0.0.1 port 1812
	User-Name = "testuser"
	User-Password = "badpassword"
	NAS-IP-Address = 255.255.255.255
	NAS-Port = 2
  rad_recv: Access-Reject packet from host 127.0.0.1:1812, id=234, length=20

**FAILURE**
  #> radtest noradius-user "goodpassword" localhost 2 testing123
  Sending Access-Request of id 238 to 127.0.0.1 port 1812
	User-Name = "noradius-user"
	User-Password = "goodpassword"
	NAS-IP-Address = 255.255.255.255
	NAS-Port = 2
  rad_recv: Access-Reject packet from host 127.0.0.1:1812, id=238, length=20

===== Configure client to access the server =====
When we talk of client we are talking about the system that uses the RADIUS server. For instance, if you are configuring RADIUS for wireless access, the wireless access point is the RADIUS **client**, not your workstation that uses the wireless.

Edit the /etc/raddb/clients.conf file. There are 3 fields you need to concern yourself for each client that access the RADIUS server.

=== client ===
This is the host IP address, DNS name, or CIDR (network/subnet)

=== secret ===
This is the shared secret between the server and the client. For security reasons, you should NOT use the same password as you do for other administrative accounts (mysql, root, et al).

=== nastype ===
This specifies the NAS type for the device connecting to the RADIUS server. The default value is 'other'.

Possible choices are:

  * cisco
  * computone
  * livingston
  * max40xx
  * multitech
  * netserver
  * pathras
  * patton
  * portslave
  * tc
  * usrhiper
  * other

**EXAMPLE**
An example might look like this:

  client 10.1.1.34 {
        secret          = sup3rs3cr3t
        shortname       = wireless-access-point1
  }
  client 192.168.2.0/24 {
        secret          = s3cr3t
        shortname       = private-network-1
  }


===== Set RADIUS to automatically start =====
  chkconfig --level 2345 radiusd on && chkconfig --list radiusd

radiusd        	0:off	1:off	2:on	3:on	4:on	5:on	6:off

===== What else =====
ClearOS 5.2 and later will likely have this built in. Additional parameters that will likely be set may include:

  * bind_address: This will likely be gone
  * listen: This will likely be all the interfaces that are enumerated as DMZ, Hot-LAN, and Internal. External will likely NOT be included as a valid interface. Setup a VPN if you need RADIUS across the Inter-webs.

===== Links =====
  * [[:content:en_us:dev_packaging_clearos_5.x_freeradius2|Spec for ClearOS 5.2]]
  * [[http://freeradius.org/doc/|FreeRADIUS Documentation]]

{{keywords>clearos, clearos content, kb, howtos, clearos5, app-radius, categoryserver, completedproject, skunkworks, maintainer_dloper}}
