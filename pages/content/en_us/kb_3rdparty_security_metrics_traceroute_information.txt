===== Traceroute Information =====
This entry from Security Metrics indicates that your server can be reached via trace route.

===== ClearCenter response =====
==== Short response ====
This is not a risk or vulnerability.

==== Long response ====
Traceroute is standard, best practice internet protocol. There does not exist a vulnerability due to this protocol being enabled or disabled. While traceroute can be disabled on the endpoint for this host, it doesn't disable traceroute for upstream routers so it isn't particularly significant or protective to hide the last hop.

==== Resolution ====
No action required.

You can disable traceroute using a custom firewall rule to block this ICMP type if you want.

{{keywords>clearos, clearos content, 3rd party, security metrics, non-cve, maintainer_dloper}}
