===== SSH Server on ClearOS 6=====
The SSH Server module allows for configuration of the Secure Shell Daemon service in ClearOS.

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Network|Infrastructure|SSH Server</navigation>

===== Configuration =====
You can customize the port, whether password authentication to SSH access is allowed, and whether or not root is allowed to log in.

{{:omedia:ssh_server_eg1.png?550|}}

===== Disabling root Login =====
The **root** account is the most sought after and the most common account to try and hack. Disabling root login lets you still log in as regular users but then requires those users to switch user (su) or use privilege escallation (sudo) for all root commands on the system. This is considered best practice and we highly recommend it.

==== Port ====
Changing the port is a good idea because hackers will know to try port 22 for SSH and it is typically the first place they try. If you change the port, be sure to select a port that is not in use by another protocol on the system. (Valid range theoretically range from 0-65535)

==== Password Authentication ====
By turning off password authentication you tell your system that you will use key based authentication. This is typically considered stronger authentication than passwords.

===== Links =====
  * [[http://www.cyberciti.biz/tips/ssh-public-key-based-authentication-how-to.html|Setting up key-based authentication for SSH]]
  * [[http://www.tuxmagazine.com/node/1000148|sudo vs. su]]
{{keywords>clearos, clearos content, SSH Server, app_name, clearos6, categorynetwork, subcategoryinfrastructure, maintainer_dloper, maintainerreview_x, userguide}}
