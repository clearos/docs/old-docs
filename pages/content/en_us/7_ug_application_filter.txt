===== Application Filter =====
The Application Filter can detect and block apps like Facebook, Netflix, Snapchat, and many others.  The app uses deep packet (DPI) and SSL certificate analysis provided by the [[https://www.egloo.ca/products/netify/features/application-filter|Netify Application Detection Engine]] to categorize and block dozens of applications and web sites.  Take control of your network and avoid those significant productivity drains in the workplace and/or time-wasters at home.

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
**Gateway >> Filtering >> Application Filter**

===== Overview =====
Navigate to the Application Filter in your Webconfig interface. By default, there should be no rules and the service should be running:

{{:content:en_us:7_ug_application-filter_1.png?550|Overview}}

===== Manipulating the Service =====
By default the service is running. If the service is running and you would like to stop it, Click the 'Stop' button on the right. To start it if stopped, press the 'Start' button in the same location. You can also uninstall this app from this location as well as leaving a review of how you like it.

{{:content:en_us:7_ug_application-filter_2.png?330|Overview}}

===== Configure Filtered Active Applications =====
To modify the blocked applications in the filter, click the **'Edit'** button in the Active Applications section. Select the applications that you wish to block and deselect applications that you wish to permit.

{{:content:en_us:7_ug_application-filter_3.png?440|Overview}}

===== White List Application Access for Certain Users =====
You can allow specific IP addresses to bypass the Application blocks altogether. To add an exempted IP, click the **'Add'** button in the 'White List' section. Type the IP address in the field provided and click 'Add'.

{{:content:en_us:7_ug_application-filter_4.png?440|Overview}}

<note tip>The whitelist feature is most useful if the LAN devices you want to whitelist have Static IP's or you use Static Leases in the [[:content:en_us:7_ug_dhcp|DHCP Server]] app.</note>
===== References =====
  * [[https://www.netify.ai/features/application-detection|Netify Application Detection Engine]]  
  * [[https://www.netify.ai/resources/applications|Netify Application and Domain Lookup]]


{{keywords>clearos, clearos content, Application Filter, app-application-filter, clearos7, userguide, categorygateway, subcategoryfiltering, maintainer_dloper}}
