===== Sites that Have Trouble with the Content Filter / Proxy Server =====
Some sites fail to work properly through the content filter or through the web proxy. There are a number of reasons why this can occur but most typical are:

  * Site uses authentication method that requires non-proxied connection
  * Site uses proprietary protocols, wrappers, or methods which the content filter or proxy interprets as garbage
  * Site uses proprietary protocols, wrappers, or methods which the content filter or proxy interprets as a virus or malware
  * Site uses malware, spyware, or other known tracking vectors which is correctly classified as such and you have those options enabled
  * Site violates HTML standards
  * Site has browser specific requirements which cannot be proxied well

===== Overcoming broken sites =====
The primary reaction would be to white list the main site but often the error comes from an element of the site which fails to register in the URL bar. You can find out what that element is and whitelist it or add it to the web proxy bypass by [[http://www.clearcenter.com/support/documentation/clearos_guides/live_monitoring_of_web_traffic_in_proxy_and_content_filter|following this guide]]. Using this process, you can discover the particular elements that are getting jammed and add their domain names (NOT THE URL) to the web proxy bypass. Please also [[http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,27/func,view/id,56944/#56945|join the discussion on the community site]] and add any sites that you find.


===== Common bad actors =====
The following are some sites which are reported as being 'bad actors' with the content filter or web proxy. Some administrators add similar lists to either their 'Web Proxy Bypass' and/or to their WPAD configuration file and firewall exemption lists.

=== Adobe ===
== Unconfirmed ==

  adobe.com	
  platformdl.adobe.com	
  sales.liveperson.net	
  adobe.tt.omtrdc.net	
  aihdownload.adobe.com	
  www.adobetag.com	
  fpdownload.macromedia.com	
  ox-d.adobe.com	
  get.adobe.com	
  dlmping2.adobe.com	
  www.flash.com	
  96.125.164.52	
  stats.adobe.com	
  www.adobe.com	
  wwwimages.adobe.com	

=== Microsoft ===
== Confirmed ==
  codecs.microsoft.com	

=== Oracle - Sun - Java ===
== Unconfirmed ==
  javadl.sun.com	
  javadl-esd.sun.com	
  java.sun.com	
  sdlc-esd.sun.com	

=== Typekit ===
== Confirmed ==
  use.typekit.com


===== Help =====
===== Links =====
  * [[http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,27/func,view/id,56944/#56944|Old community discussion on this topic]]
==== Navigation ====

[[:|ClearOS Documentation]] ... [[..:|Knowledgebase]] ... [[:Knowledgebase:Troubleshooting:|Troubleshooting]]
{{keywords>clearos, clearos-content, app-content-filter, app-web-proxy, troubleshooting, help, support, bad sites, sites that don't work, maintainer_dloper}}
