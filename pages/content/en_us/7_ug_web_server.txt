===== Web Server =====
ClearOS includes the Apache web server -- the same software that powers many of the world's largest web sites.

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Server|Web|Web Server</navigation>

===== Configuration =====
The web server comes with built-in SSL encryption for enhanced security.  If your website requires a username and password for login, then it is a good idea to use encryption.  For instance, if you have the webmail or groupware solution installed, you should access their respective login pages via the secure web server.  In your web browser, you should use the encrypted **https://your.domain.com** instead of the un-encrypted **http://your.domain.com** (https vs http).  When enabled, all communication between the web server and user's web browser is encrypted using a 128-bit security key.

==== General ====
=== Server Name ===
The server name is a valid name (for example, www.example.com) for your web server.  This name is used on some infrequently used error pages, so it is not all that important.

<note warning>SSL encryption requires a web site certificate.  ClearOS automatically generates a default certificate that is secure.  However, this certificate is not verified by one of the web site certificate authorities. Your users will see the warning when connecting to the secure web server. Fully verified certificates can be purchased commercially or created using the Let's Encrypt app</note>

=== Web Sites ===
The web server includes support for hosting different domains, a feature that is sometimes referred to **virtual hosting**.  For example, you can a default website for **example.com** along with a virtual web site **anotherexample.com**.  However, the first thing you need to do is configure the default site.

==== Web Site Settings ====
{{7-WebSite-WebSite.png}}

=== Web Site Hostname ===
Specify the base domain for the web site, e.g. example.com. 

=== Alias ===
You may want your web site to respond to other subdomains, notably:

  * <nowiki>www.example.com</nowiki>
  * blog.example.com
  * download.example.com

If that is the case, you can set an alias for your web site to ***.example.com**.  Alternatively, you can have a list of aliases separated by spaces, for example:

  * <nowiki>www.example.com</nowiki> download.example.com

Using an explicit list instead of a wildcard '*' is handy if you plan on configuring web applications for a subdomain.  For example, you may want to create a second web site **blog.example.com** which will have the WordPress blogging software installed.

==== Web Site Upload Access ====

{{7-WebSite-UploadAccess1.png}}

This controls who can upload to the Webserver. Not all options above will necessarily show here. It depends on which other components are installed in ClearOS.
=== Group ===
Choose the group who can upload to the Web Server

=== FTP Upload ===
Requires the ClearOS [[content:en_us:7_ug_ftp|FTP Server]]. Select if upload by FTP is allowed. If FTP is allowed, use the default port, tcp:21. This takes you to the parent folder of all flexshares and websites. Select your web_site _hostname and if using the Standard Layout (see below) upload to there. If using the Sandbox Layout (see below) upload into the html subfolder.\\

=== File Server Upload ===
Requires the ClearOS [[content:en_us:7_ug_samba|Windows Networking (Samba)]]. Select if upload by Samba Shares/Windows Network Drive is allowed. The same comments about file locations apply as for the FTP server.

== File Server access using Windows ==
You can access the resource in Windows by running the IP address as a UNC in the Run dialog box.
  \\ip_address
If you want to map a network drive, the share name is <nowiki>\\your_server\your_web_site_hostname</nowiki>.

== File Server access using Mac OSX ==
From Mac OSX, type Command+K in finder and type the address using the CIFS protocol.
  cifs://ip_address
==== Web Site Options ====

{{7-WebSite-Options.png}}
=== Folder Layout ===
This defaults to Standard for the default web site and gives a document root of /var/www/html
For virtual web sites there are two options, Standard and Sandbox:\\
**Standard** gives a document root of /var/www/virtual/your_web_site_hostname \\
**Sandbox** gives a document root of /var/www/virtual/your_web_site_hostname/html \\

Sandbox is the new default and allows you to keep the web site, logs and error pages separated from other web sites with the folder structure: \\
/var/www/virtual/your_web_site_hostname/html/ -> Where you put your web accessible files \\
/var/www/virtual/your_web_site_hostname/logs/ -> Access and error logs \\
/var/www/virtual/your_web_site_hostname/error/ -> To override default Apache error pages
<note>Once this setting has been saved, it cannot be changed. If you do want to change it, you must delete the virtual web site and recreate it.</note>
=== Accessibility ===
By default the web site is accessible to all. You can choose to restrict it to LAN only here.
=== Require Authentication ===
By default the website access is unrestricted so any user can access it. You can enable authentication, in which case only users with a valid ClearOS account can access the server.
=== Digital Certificate ===
You can select which certificate you'd like to use for the web site. You can choose between the ClearOS Default self-signed certificate, any [[content:en_us:7_ug_lets_encrypt|Let's Encrypt certificate]] or any External Certificate uploaded through the [[content:en_us:7_ug_certificate_manager|Certificate Manager]].
=== Show Index ===
Default is enabled. Sets the Options +Indexes or -Indexes in /etc/httpd/conf.d/flex-80.conf. If //Show Index// is enabled, browsers will display a listing of all files if there is no index page (for example, index.html, index.php etc.).  This is normally only desirable if using the Flexshare as a file access service (similar to FTP).  If you are running a website, this option should be disabled.
=== Follow Symlinks / Allow Rewrite ===
Default is enabled. Sets the Options +FollowSymLinks or -FollowSymLinks in /etc/httpd/conf.d/flex-80.conf.
=== Allow Server Side Includes ===
Default is disabled. Sets the Options +IncludesNOExec or -IncludesNOExec in /etc/httpd/conf.d/flex-80.conf.
=== Allow [.htaccess] Override ===
Default is enabled. Sets or removes "AllowOverride All" in /etc/httpd/conf.d/flex-80.conf. If you are installing a web application into your Flexshare, you will likely need to enable this feature.
=== Enable CGI ===
Default is disabled. If enabled, CGI code should go in /var/www/virtual/your_web_site_hostname/cgi-bin/. 
==== Web Site PHP ====

{{7-WebSite-PHP1.png}}

=== Enable PHP ===
Default is enabled. Choose whether to enable PHP for the web site or not.
=== PHP Engine ===
If you have the PHP Engines app installed from the [[:content:en_us:7_ug_marketplace|Marketplace]], this is where you choose the version you want for the web site.

===== Adding Dynamic Content to Your Site =====
There are many options for adding dynamic content to a website:
  * Perl and CGI
  * PHP
  * JSP
  * ASP

PHP and perl CGI are installed by default.  The set-up and configuration of other engines are beyond the scope of this help document. 

===== Custom Configuration =====
If you would like to customize the web server configuration, please review the following document:

  * [[:content:en_us:kb_o_custom_apache_web_server_configuration|Custom Apache Web Server Configuration]]
<note>This section is under review and will be deleted when multiple versions of PHP become available from the marketplace.</note>
===== Troubleshooting =====
==== ISP Blocking ====
Some ISPs are known to block web (port 80) traffic to residential broadband connections in an attempt to cut down on illegal sites hosted on their network.  If you think your configuration is set-up correctly and you suspect your ISP is blocking HTTP traffic, try a remote port scan.

==== Firewall Rules ====
A web server listens to client requests coming in on port 80 (HTTP) or 443 (HTTPS/secure).  Did you remember to open the correct port(s)?

===== Links =====
  * [[:content:en_us:7_ug_incoming_firewall|Adding incoming firewall rules]]
  * [[content:en_us:7_ug_lets_encrypt|Let's Encrypt certificates]]
  * [[content:en_us:7_ug_certificate_manager|Certificate manager]]
  * [[content:en_us:7_ug_ftp|FTP Server]]
  * [[content:en_us:7_ug_samba|Windows Networking (Samba)]]
  * [[content:en_us:7_ug_flexshare|Flexshares]]
  * [[content:en_us:7_ug_php_engines|PHP Engines]]
{{keywords>clearos, clearos content, Web Server, app-web-server, clearos7, userguide, categoryserver, subcategoryweb, maintainer_nhowitt, maintainerreview_x}}
