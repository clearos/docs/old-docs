===== ClearOS Community 6.2.0 Final Release Information =====
**Released: April 24, 2012**

===== What's New in ClearOS Community 6.2.0 =====
Along with updating the base system built on source code from Red Hat Enterprise Linux ((Trademark is the property of the respective owner.  ClearFoundation is not affiliated with Red Hat.)) 6.2, this release includes the following major changes.  

==== Base System ====
  * Marketplace
  * Base system built from Red Hat Enterprise Linux ((Trademark is the property of the respective owner.  ClearFoundation is not affiliated with Red Hat.)) 6.2 source code
  * 64-bit support
  * Graphical installer
  * Windows BDC support
  * App developer framework

===== Known Issues =====
  * The last step in installer may take a couple of minutes to complete

===== ClearOS 5.2 Differences =====
For ClearOS 6, the barrier for having an app available in the web interface was raised.  If the quality or features in ClearOS 5.2 were not up to the standards of a modern small business server/gateway, the feature was either upgraded or deprecated.

==== Mail Stack ====
<note warning>
The mail stack (e.g. Mail Antispam, SMTP Server, etc) will be available in June 2012 (beta starts in May).  The default mail solution is now be based on [[http://www.zarafa.com|Zarafa]].</note>

==== Horde/Kolab ====
Horde/Kolab has been deprecated and is no longer supported.  This policy may change in a future ClearOS 6 release.

==== Reports ====
The reporting engine in ClearOS 6 is being completely overhauled.  An administrator does not want to sift through 5 different reports to see what a particular user was doing on the network.  Instead, a unified reporting tool is under development.  The reports in version 5.x have been deprecated.

==== Advanced Firewall ====
The Advanced Firewall app was marked as deprecated in version 5.2.  It is no longer available.  The [[http://www.clearcenter.com/support/documentation/marketplace/custom_firewall|Custom Firewall]] can be used as an alternative.

==== Bandwidth - Advanced Rules ====
The advanced mode in the bandwidth app has been removed in order to improve multi-WAN support.  Advanced mode will be made available in the next release (June 2012).

==== Administrators ====
The Administrators app does not fit into the group-based policy engine in ClearOS 6.  The underlying mechanism that existed in ClearOS 5 has been ported to version 6, but it requires command line configuration. 

==== IPsec ====
{{keywords>clearos, clearos content, announcement, releases, clearos6.2.0, previousfinal, maintainer_dloper}}
