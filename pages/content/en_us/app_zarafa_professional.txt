{{ :userguides:zarafa_professional.svg?80}}
<WRAP clear></WRAP>

===== Zarafa Professional =====
Zarafa Professional for ClearOS is an open-source, full-featured collaboration and groupware platform.  Zarafa includes a web-based interface for performing mail, address book and calendar functions.  Zarafa Professional for ClearOS is MAPI-compliant and can be used as a drop-in replacement for Microsoft Exchange[TM] Server with native support for MS Outlook[TM] mail client.  It is designed for organizations that require advanced features such as BlackBerry Support, archiving, automatic updating and high-availability set-ups, combined with the assurance of professional support.

This app includes a license for 20 users.  Additional users can be added by purchasing Zarafa user packs from the Marketplace.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/server/zarafa_professional|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_zarafa_professional|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_zarafa_professional|here]].
==== Additional Notes ====
