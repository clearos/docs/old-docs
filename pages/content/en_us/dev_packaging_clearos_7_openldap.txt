===== OpenLDAP =====
OpenLDAP is an open source suite of LDAP (Lightweight Directory Access
Protocol) applications, servers and clients.

===== Changes =====
  * The slapd.conf configuration methodology is still in use instead using the convoluted and command line unfriendly slapd.d configlets.

  * The system startup script was modified to allow binding the LDAP server using different policies (e.g. LAN, localhost only).

{{keywords>clearos, clearos content, dev, packaging, version7, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
