===== Sudo =====
The sudo package allows system access for specified users.

===== Patches =====
==== sudo-no-solaris-bug-stuff.patch ====
The sudo software will hang if the network and/or DNS is not configured properly.  This is not that uncommon on a ClearOS system (especially at install time).  This one line patch removes a Solaris bug workaround, but fixes serious sudo hanging issues.  More information is in [[https://bugzilla.redhat.com/show_bug.cgi?id=479464|Red Hat Bugzilla - 479464]].

===== Rebuild =====
{{keywords>clearos, clearos content, AppName, app_name, version5, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
