===== Groups =====
The **Group** app allows you to add, delete and manage groups on the system.  By creating groups, you can set access permissions for various applications in ClearOS.  For example, you can create a [[:content:en_us:7_ug_flexshare|Flexshare]] file share that can be managed by a //Sales// group.

===== Installation =====
If you [[:content:en_us:7_ug_marketplace|install an app]] that depends on users/groups, this app will automatically be installed.

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>System|Accounts|Groups</navigation>

===== Video Tutorial =====
{{youtube>large:gQKF4QsPn9k|2-minute ClearOS Enterprise Groups and Users}}

===== Group Listing =====
The first thing you will see on the group manager page is a summary of existing groups.  These are broken into two categories:

  * User Defined Groups
  * Windows Groups

==== User Defined Groups ====
With user defined groups, you can create and manage your own groups. 

==== Windows Groups ====
Some pre-defined groups are included in ClearOS and these are summarized in the following table.  

^Group^Description^Purpose^
|allusers|All Users|A group containing all the users on the system|
|domain_admins|Domain Admins|Windows domain administrators|

You can learn more about the purpose of Windows groups in the [[:content:en_us:7_ug_samba|Windows Networking]] section of the User Guide.

===== Adding a Group =====
To add a group, click on //Add// in the group list form.  Two simple parameters are required:

  * The **group name** is a simple one-word group, for example //sales//
  * The **description** is where you can describe the group in a few words, for example //Sales and Marketing Team//

If you have installed the Zarafa, Kopano or IMAP and POP app, you will see one additional option allowing you to enable a mail distribution list.  If enabled, the group becomes a mail distribution list (e.g. sales@example.com will go to all the members of the sales group).

Once you have created a group, an admin can then add members to the group.  From the main Groups webconfig page, click on //Edit Members//.  Check which members should have access to the group properties. You can also assign users to groups in the [[content:en_us:7_ug_users|User Manager]] app.

{{keywords>clearos, clearos content, Groups, app-groups, clearos7, userguide, categorysystem, subcategoryaccounts, maintainer_dloper}}
