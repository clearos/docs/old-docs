===== Validating Your Download with Checksums =====
/* This document covers checksums */

When you download ISOs and content from ClearOS, it is best practice to validate your ISO against the release notes for your version so that you know that the content really comes from us.

To do this, the checksum is included in the release notes of each [[:announcements:releases:start|version]].

===== Checksum =====
With ClearOS 7 the checksum that is included is a SHA256 sum. After you download the ISO, you will run a program to calculate the cryptographic sum of the ISO file. If the sum matches then you can have high confidence that the file you downloaded is an exact copy of the file that was created by the ClearOS build systems when they crafted the ISO.

==== Checking the SHA256SUM in Linux ====
If you have downloaded the ISO simply run the following command against the file you downloaded:

  sha256sum /root/ClearOS-DVD-x86_64.iso 

This would result in the following on the 7.2 ISO

  b4da8ffa10945e4d95478edb32f7a3dd9691f9a6c6d7820b1ffa346f6eb6a878  ClearOS-DVD-x86_64.iso

==== Checking the SHA256SUM in Mac ====
Checking the SHA256 sum is easy on Mac. Download the ISO image for ClearOS. Then launch the Terminal application located in Applications >> Utilities. At that prompt, type the following:

  shasum -a 256 ~/Downloads/ClearOS-DVD-x86_64.iso 

This will give you the following result for the ClearOS 7.2 ISO image:

  b4da8ffa10945e4d95478edb32f7a3dd9691f9a6c6d7820b1ffa346f6eb6a878  /Users/username/Downloads/ClearOS-DVD-x86_64.iso
==== Checking the SHA256SUM in Windows ====
Under Windows, you will need to download a SHA test programs such as this one:

[[http://www.labtestproject.com/files/win/sha256sum/sha256sum.exe]]

Download the ClearOS ISO and locate its path. (eg. C:\Documents and Settings\Owner\My Documents\My Downloads)

Next, you will need to open your Command Prompt by running 'cmd' from your Run dialog. On Windows 10 you can use Cortana for that and in prior versions, look for the 'Run' option or the 'Search Programs and Files' and insert 'cmd' and press <Enter>.

Next, change into your directory where you downloaded the ISO and run the sha256sum program against the ISO from command line:

  cd "C:\Documents and Settings\Owner\My Documents\My Downloads\"
  sha256sum.exe ClearOS-DVD-x86_64.iso 

You should get a result similar to this:

  b4da8ffa10945e4d95478edb32f7a3dd9691f9a6c6d7820b1ffa346f6eb6a878 *ClearOS-DVD-x86_64.iso

{{keywords>clearos, clearos content, ISO, download, clearos7, categoryinstall, maintainer_dloper}}
