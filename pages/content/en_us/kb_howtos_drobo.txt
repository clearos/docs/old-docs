===== Drobo =====
Drobo is a hardware device that allows for expanded storage. It is useful because it allows you to add disks dynamically. When used in conjunction with Centralized Storage and LVM, you can create an expandable and redundant hardware/software solution for your data on ClearOS.

You can connect a Drobo using USB, Firewire, iSCSI, or through other NAS methods.

===== iSCSI =====
Setup and configure your Drobo Pro or Drobo Elite using Firewire or USB. Configure the iSCSI section to use an IP address on your subnet. On my ClearBOX, I configured one of the extra network interfaces for this purpose. 

By default ClearOS is an iSCSI based initiator and you should be able to connect to a Drobo Pro or Drobo Elite using the already configured tools. There are, however some very useful tools that you will want including a package called drobo-utils.

You can download drobo-utils from Sourceforge. As of this writing, the current drobo-utils package is R-0.6.2.2. You will also need to install python-ctypes.

  mkdir /root/support /root/support/drobo && cd /root/support/drobo
  wget http://downloads.sourceforge.net/project/drobo-utils/drobo-utils/r0_6_2/drobo-utils-0.6.2.2.tgz?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Fdrobo-utils%2Ffiles%2Fdrobo-utils%2Fr0_6_2%2F&ts=1295035375&use_mirror=voxel
  tar xvzf drobo-utils-0.6.2.2.tgz 
  cd drobo-utils-0.6.2.2
  yum install python-ctypes

Now, tell the utils about where your Drobo is located. In this case mine is configured for 172.31.1.2

  ./drobom info settings
  ./drobom set IPAddress 172.31.1.2
  ./drobom set NetMask 255.255.255.0
  ./drobom set UseStaticIPAddress True
  
In the hosts file of my server, I gave the IP address the name drobo1.example.lan and drobo1. I tested with ping to ensure I could reach my Drobo.

Now, I tell ClearOS how to get to the Drobo.

  iscsiadm -m discovery -t sendtargets -p drobo1.example.lan
  iscsiadm -m node

It should tell me that my drobo is something like...

  172.31.1.2:3260,0 iqn.2005-06.com.datarobotics:drobopro.tdb104140143.node0

Now if I want this device to come up automatically each time I need to change the mode to automatic. I can see the status of the device by running the following:

  iscsiadm -m node -o show  | egrep 'node\.(name|conn\[0\]\.startup|startup)'

To change it to automatic for conn[0] I run:

  iscsiadm -m node -o update -n 'node.conn[0].startup' -v automatic

And check it again:

  iscsiadm -m node -o show  | egrep 'node\.(name|conn\[0\]\.startup|startup)'

Now log in to the Drobo.

  iscsiadm --m node --targetname iqn.2005-06.com.datarobotics:drobopro.tdb104140143.node0 -p 172.31.1.2 --login

Running './drobom info settings' should indicate connectivity to the drobo and reveal the device used. I can also see the device using 'fdisk -l'.

Next steps would be to give it a label and partition. Setting this volume up as LVS allows us to grow the volume on the drobo as we grow the capacity. Only ext3 is supported by the drobo UI for being able to see capacity but you will likely want to use ext4 if you are going to expand the volume to be greater than a few gigabyte.

{{keywords>clearos, clearos content, kb, clearos6, categoryother, howtos, maintainer_dloper}}
