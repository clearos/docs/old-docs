===== DNS Deployments with ClearOS =====
This guide covers best practices of deploying name services within an infrastructure when using ClearOS. It covers single site deployments, deployments with AD, and multi-site deployments of ClearOS. This guide is intended to be used by admins with a good understanding of DNS.

===== How DNS works on ClearOS =====
There are two main elements to DNS resolution on ClearOS:

  * Name lookup through NSSwitch for ClearOS itself
  * DNS services with DNSMasq

NOTE: Usage of 3rd party DNS servers like BIND are not addressed by this deployment guide but the concepts presented should be beneficial.

=== NSSwitch ===
The behavior of ClearOS for name server lookups is determined by the /etc/nsswitch.conf file. In this file, the default behavior is defined as:

  hosts:      files dns

The settings listed here should typically be left alone to whatever mode is configured by Webconfig. The defualt behavior is for hosts to be looked up in **files** (ie. /etc/hosts) and then queried through the settings in **dns** (ie. /etc/resolv.conf) if the files does not resolve the DNS.

=== DNSmasq ===
ClearOS comes with a caching DNS server called DNSMasq. This service handles both DNS services and DHCP services.
{{keywords>clearos, clearos content, AppName, app_name, kb, howto, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
