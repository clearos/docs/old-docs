{{ :userguides:samba.svg?80}}
<WRAP clear></WRAP>

===== Windows Networking (Samba) =====
The Windows Networking app installs SAMBA - an open source suite of services that allows file and print interoperability between systems running under Microsoft Windows and this server.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/server/samba|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_samba|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_samba|here]].
==== Additional Notes ====
