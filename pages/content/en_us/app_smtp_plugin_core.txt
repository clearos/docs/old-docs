{{ :userguides:smtp_plugin_core.svg?80}}
<WRAP clear></WRAP>

===== SMTP Plugin =====
Account manager plugin for the SMTP mail service.  This app is required for the master node only in a master/slave environment.  The app allows an administrator to define user access to email services hosted within the master/node infrastructure.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/system/smtp_plugin_core|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_smtp_plugin_core|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_smtp_plugin_core|here]].
==== Additional Notes ====
