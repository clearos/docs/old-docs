===== User Profile =====
The user profile page gives end users a place to change their password and other personal settings. All users, by default, have access to this app.

===== Configuration =====
An end user or admin can access their profile page by providing their username and password in [[:content:en_us:6_webconfig|Webconfig]]. The user can navigate to: https://IP_ADDRESS_OR_HOSTNAME:81/app/user_profile

<note>Change the 'IP_ADDRESS_OR_HOSTNAME' part of the URL listed here to the actual IP address or hostname of your ClearOS server.</note>

If a user navigates to just the Dashboard which is the default app for the URL https://IP_ADDRESS_OR_HOSTNAME:81, they will be greeted with an 'Access Denied' page and when clicking the button it will land them here at the User Profile App instead.

{{:omedia:user_profile_access_denied.png?550|}}
{{keywords>clearos, clearos content, AppName, app_name, clearos6, userguide, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}

{{keywords>clearos, clearos content, User Profiles, app-user-profile, clearos6, userguide, categorymyaccount, maintainer_dloper}}
