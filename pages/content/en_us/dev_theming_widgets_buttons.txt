===== Widgets Buttons =====
Theme information for buttons.

^Type^CSS Class^
|Add|clearos-form-submit-add|
|Delete|clearos-form-submit-delete|
|Update|clearos-form-submit-update|

===== HTML =====
ClearOS buttons call the //**_form_submit**// function. 

==== Parameters ====
function _form_submit($name, $text, $importance, $class, $id)

=== name ===
The name of the button.

=== text ===
The text associated with the button.

=== importance ===
The importance level of the button: 'low' or 'high'.  On certain forms, you may want to highlight a particular button while downplay another.  For example, when an administrator goes to edit user "sarah" and is presented with the web form to do so, the "Update" button should be more prominent than "Cancel".  Typically, this prominence is mapped to a class (e.g. button-low-importance and button-high-importance)

=== class ===
The class passed into the function should typically be added to the class definition in your button HTML element.

=== id ===
The element ID.

==== Examples ====
<code php>
function _form_submit($name, $text, $importance, $class, $id)
{
    $importance_class = ($importance === 'high') ? "clearos-form-important" : "clearos-form-unimportant";

    return "<input type='submit' name='$name' id='$id' value=\"$text\" class='clearos-form-submit $class $importance_class' />\n";
}
</code>

===== CSS =====
==== Summary ====
The //**clearos-form-submit-***// CSS classes are outlined in the following table:

^Type^CSS Class^
|Add|clearos-form-submit-add|
|Delete|clearos-form-submit-delete|
|Update|clearos-form-submit-update|

If you want to add images to your standard buttons, you can use the specific class to meet your needs.   

==== Example ====
<code css>
/* ------------------------------------------------------------------------- */
/* Buttons */
/* ------------------------------------------------------------------------- */

/* Common button class */
input.clearos-form-submit { font-size: 20pt !important; }
input.clearos-form-important { color: red !important; }
input.clearos-form-unimportant { color: gray !important; }

/* Standard buttons */
input.clearos-form-submit-add { }
input.clearos-form-submit-delete { }
input.clearos-form-submit-update { }
/* ... etc ... */

/* Custom button */
input.clearos-form-submit-custom { }
</code>

===== Javascript =====
==== Summary ====
You can also use the button classes to implement changes via JavaScript.  The following code shows the example used in a jQueryUI-based theme.

==== Example ====
<code javascript>
$(function(){
    $(".clearos-form-submit").button();
});
</code>

===== Links =====
  * [[:content:en_us:dev_framework_reference_guide_button_sets|Button Sets]] 
  * [[:content:en_us:dev_theming_widgets_buttons|Calling button functions in the view]] 
{{keywords>clearos, clearos content, dev, theming, maintainer_dloper, maintainerreview_x, keywordfix}}
