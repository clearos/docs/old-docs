===== SoftLayer =====
To add a SoftLayer cloud, you will need to supply the username of your SoftLayer account and an API key.

To get the API key, login to  SoftLayer's portal and enter your user's profile page by clicking on your username on the top right.

Then, on the bottom of the User Profile page, you'll see an "API Access Information". This is the API key we'll use in ClearGLASS 

[image]

To add it in ClearGLASS, click on "Add cloud" and select "SoftLayer" on the drop down list.

Enter the username of your SoftLayer account and API key and press Add.

[image]

ClearGLASS will try to authenticate with these credentials and if they are valid the SoftLayer cloud will be added. Clicking on the Machines link, will show you a list of all your VMs in SoftLayer. 

[image]

==== Related Articles ====
  * [[content:en_us:cg_setting-up-clearglass|Quick Start Guide]]
  * [[content:en_us:cg_monitoring-on-linux|Setting up monitoring on Linux]]
  * [[content:en_us:cg_manage-infrastructure|Managing Infrastructure]]