===== Web Proxy =====
The web proxy in ClearOS is a high-performance proxy caching server for web clients, supporting HTTP, FTP and some lesser known protocols.  The software not only saves bandwidth and speeds up access time, but also gives administrators the ability to track web usage via web-based reports.

If you are new to ClearOS and/or setting up a proxy server, you may want to refer to the [[:content:en_us:kb_o_clearos_guides_setting_up_content_filter_policies|Guide to Setting up Web Proxy, Content Filter and Access Control Guide]].

<note warning>Please note that the Proxy/Content Filter and Gateway Management apps are mutually exclusive. If you have the Proxy/Content Filter running, you should not use Gateway Management and vice-versa</note>
===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Gateway|Content Filter and Proxy|Web Proxy</navigation>

===== Configuration =====
==== Settings ====
=== User Authentication ===
If you would like to require a username/password for web access, you can enable user authentication.

<note warning>User authentication cannot be used in conjunction with transparent mode.  If you require user authentication, then non-transparent mode is required.  This is not a limitation of the software, but a limitation of the way the web protocol was designed!</note>

=== Transparent Mode ===
In **transparent mode**, all web requests from the local network automatically pass through the proxy.  The advantage: no configuration changes are required on the workstations.  The disadvantage: secure web sites (HTTPS) can not flow through the proxy.

Since network traffic needs to be intercepted before going out the Internet, this his mode is only available when ClearOS is configured as a gateway. 

<note warning>These days, in Transparent Mode, the proxy is becoming less and less effective as the internet increasingly using https. Google now prefers to return https links rather than http links. The transparent proxy con only intercept http traffic and not https traffic. It is now only recommended to use one of the Non-Transparent modes only.</note>

=== Performance Level ===
The **Performance Level** indicates the size of network that your system can support.  The Community Edition is designed for home/small networks and is already optimized for 10 users or less.  The Professional Edition does automatic optimization based on available system resources.

==== Cache Settings ====
=== Maximum Cache Size ===
The maximum size on your hard disk to use for the proxy server cache.

=== Maximum Object Size ===
Any file (image, web page, PDF, etc) above the maximum object size will still go through the proxy but will not be cached.  Large files (for instance, a movie file) can take up a lot of space in your proxy cache.  If you have a cache size of 2 Gb and two people happen to download 1 Gb files at the same time, then these two files would replace everything else in your cache.  You can limit the maximum object size to prevent this kind of scenario.    

=== Maximum Download File Size ===
If you want to limit downloads of large files (for instance, movies) you can set a maximum size.  Any file above this size limit will get blocked.

<note tip>If your internet connection is fast you will need a very fast disk to keep up or you will need to run the Proxy cacheless. See the [[content:en_us:kb_o_optimizing_performance_for_proxy_and_content_filter#cacheless|Optimizing Performance for Proxy and Content filter]] guide.</note>

==== Web Site Bypass ====
In some circumstances, you may need to by-pass the proxy server.  Typically, this is required for web sites that are not proxy-friendly.  Some notable examples:

  * Older Microsoft IIS web servers send invalid web server responses 
  * Microsoft IIS web servers can be configured with non-standard authentication
  * [[http://www.tivo.com|Tivo personal video recorders (PVRs)]] are unable to connect via a proxy server.  Adding Tivo's network **204.176.0.0/14** to the proxy by-pass list solves the issue.

You can use the following format for the bypass:

  * Web site name (destination)
  * IP address (destination or source>
  * [[:content:en_us:5_network_notation|Network Notation]] (recommended)

<note tip>Bypassing the proxy for a particular Website name may not be a good idea. As an example, google.com resolves to lots of different IP addresses. If you bypass "google.com", it will look up the IP it maps to and bypass that single IP. Next time, if google resolves to a different IP it will not be bypassed. For google.com you would need to bypass a whole list of subnets.</note>

If you are running the proxy in non-transparent mode, then you also have to adjust your web browser's proxy server settings.  The web site or IP address that you add to the ClearOS web proxy bypass list should also be added to your browser's proxy exception list. See the [[:content:en_us:kb_howtos_non-transparent_proxy_and_content_filter_bypass|Non-Transparent Proxy and Content Filter Bypass]] guide.
==== Authentication Exception Sites ====
Some sites, especially those with logins, do not play well with the proxy and need to be exempted. You may be able to spot these by using the guide [[:content:en_us:kb_bestpractices_live_monitoring_of_web_traffic_in_proxy_and_content_filter|here]] to look for DENIED messages. Then try whitelisting the resulting FQDN.

<note warning>Never whitelist a domain and subdomain of the same domain at the same time or the proxy will refuse to start e.g do not have subdomain.example.com and sub-subdomain.example.com in the whitelist at the same time. Having subdomain1.example.com and subdomain2.example.com at the same time is OK.</note>
==== Web Browser Configuration ====
In non-transparent mode, you must change the settings on all the web browsers running on your local network.  The following describes the steps for configuring Internet Explorer, but other browsers have similar procedures.  In Internet Explorer

  * Click on <button>Tools</button> in the menu bar
  * Select **Internet Options**
  * Click on the <button>Connections</button> tab 
  * Click on the <button>LAN Settings</button> button

In the **Proxy Server** settings box, specify your ClearOS IP address and the proxy port (see next section).  You may not be able to access websites on your system or local network unless you select //bypass proxy server for local addresses//.

**Which Port Should I Use?**

So which port should be configured in your web browser's proxy settings?

  * Are you using transparent mode?  If yes, then no web browser changes are required!  If not, continue.
  * Are you using the content filter?  If yes, use port 8080.  If no, use port 3128.
  
===== FTP Proxy =====
From the Squid Web Proxy FAQ:

**Question:** Can I make my regular FTP clients use a Squid cache?

**Answer:** It's not possible.  Squid only accepts HTTP requests.

===== Troubleshooting =====
====  Requested web page or file is too large ====
If you see the message **Requested web page or file is too large** in your web browser, change the **Maximum Download File Size** parameter described above.

===== Links =====
  * [[:content:en_us:7_ug_content_filter|Content Filter]]
  * [[:content:en_us:7_ug_content_filter_updates|Content Filter Updates]]
  * [[:content:en_us:7_ug_antivirus|Gateway Antivirus]]
  * [[:content:en_us:7_ug_antimalware_updates|Antimalware Updates]]
  * [[content:en_us:kb_o_optimizing_performance_for_proxy_and_content_filter|Optimizing Performance for Proxy and Content filter|
]]
  * [[:content:en_us:kb_o_clearos_guides_setting_up_content_filter_policies|Guide to Setting up Web Proxy, Content Filter and Access Control Guide]]
  * [[content:en_us:kb_bestpractices_live_monitoring_of_web_traffic_in_proxy_and_content_filter|Live Monitoring of Web Traffic in Proxy and Content Filter]]
  * [[:content:en_us:kb_howtos_non-transparent_proxy_and_content_filter_bypass|Non-Transparent Proxy and Content Filter Bypass]]

{{keywords>clearos, clearos content, Web Proxy, app-web-proxy, clearos7, userguide, categorygateway, subcategorycontentfilterandproxy, maintainer_dloper}}
