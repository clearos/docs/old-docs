{{ :userguides:system_report.svg?80}}
<WRAP clear></WRAP>

===== System Report =====
The System Report app includes information on the operating system and underlying hardware.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/reports/system_report|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_system_report|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_system_report|here]].
==== Additional Notes ====
