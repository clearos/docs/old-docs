===== Recover Lost Root Password for ClearOS 5 =====
If you no longer know the root password for your ClearOS 5.x system, you can use the following steps to reset it.  For security reasons this procedure cannot be carried out remotely; it must be done from the console (keyboard and monitor) while physically at the server. For ClearOS 6.x systems, [[:content:en_us:6_kb_forgot_root_password|click here]].

===== Boot Into Maintenance (Single-User) Mode =====
First, you need to boot your system into maintenance (single user) mode:

  * At the boot splash screen, press [[http://en.wikipedia.org/wiki/Any_key|any]] key to stop the countdown timer.

{{:omedia:grub-splash-timer.jpg?550|}}

  * Select the //**ClearOS**// version with the version of the Linux kernel that you wish to boot. This will usually already be selected or there may be one item listed.

  * Type //**e**// to modify the boot procedure.

{{:omedia:grub-default-options.jpg?550|}}

  * Select the line that begins with **'kernel'** and type //**e**// to modify the line. 

{{:omedia:grub-boot-parameters.jpg?550|}}

  * Make sure that the cursor is at the end of the line and press the //**Spacebar**// and then type //**single**//. Press //**Enter**// to exit edit mode.

{{:omedia:grub-kernel-parameters.jpg?550|}}

  * Type //**b**// to boot the selected item and begin booting up your system in //single// mode.

{{:omedia:grub-modified-options.jpg?550|}}

The boot will continue as it usually does with the exception that it will not run any of the traditional items (init3 or init5).

===== Changing the password for root =====

Once the operating system has loaded you will be at a prompt similar to the following:

  sh-3.2#

  * Type the command //**passwd**// and press //**Enter**//. You will see the following:

  sh-3.2# passwd
  
  Changing password for user root
  New UNIX password:

<note info>Make sure that you set a strong password - i.e. it should not be based solely on a dictionary word and should contain numbers or other valid non-alpha characters. Also, the system will not stop you from using a bad password. In addition we suggest passwords that do not have a percent sign '**%**' which is then followed by a hexidecimal capable set of numbers. This can be interpreted by some web forms (including Webconfig) as a special character.</note>

If you have used a weak password (one based on a dictionary word) it will state:

  BAD PASSWORD: it is based on a dictionary word

In this mode, it will take weak passwords in spite of the fact that they are weak.

  * You will then be asked to re-enter the password to confirm it and will subsequently receive a confirmation that the root password has been reset.

  Retype new UNIX password:

  passwd: all authentication tokens updated successfully.

  * Now type in the command //**reboot**// and press //**Enter**// to restart your ClearOS system.

  sh-3.2# reboot

{{keywords>clearos, clearos content, clearos5, kb, categorysystem, maintainer_dloper}}