====== Mail Antispam ======
The **Mail Antispam** app works in conjunction with your mail server. The software identifies spam using a wide range of algorithms on e-mail headers, networks, IP addresses and mail contents.

In the background it uses the SpamAssassin engine to classify all e-mails on a number of criteria. Each criteria gets a score (positive or negative - see the X-Spam-Status in the e-mail header) and all the scores are added up (see the X-Spam-Score in the e-mail header). The higher the score, the more likely it is for the email to be spam

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Server|Mail|Mail Antispam</navigation>

===== ClearCenter Antispam Updates =====
The open source [[http://spamassassin.apache.org|SpamAssassin]] solution is the antispam engine used in ClearOS.  This software automatically checks for base-only updates on a weekly basis. This is already included in ClearOS for free! 

In addition, the ClearCenter [[http://www.clearcenter.com/Services/clearsdn-antispam-updates-8.html|Antispam Updates]] service provides //additional// daily signature updates to improve the effectiveness of the antispam system.  These signatures are compiled from third party organizations as well as internal engineering resources from ClearCenter.  We keep tabs on the latest available updates and fine tune the system so you can focus on more important things. 

===== Configuration =====
{{content:en_us:7_mail_antispam.jpg?1000|Mail Antispam Summary}}
==== Settings ====
{{content:en_us:7_mail_antispam_settings.jpg?1000|Mail Antispam Settings}}
=== Discard Policy (Block Policy) ===
If you want to discard spam before it reaches users' mailboxes, you can configure the mail discard policy.  For example, you can discard spam marked with **high probability** (or higher) by using this tool.

More emails will be discarded at a lower number. For example, a setting of 5 will discard almost any email. Setting the value to 5 has the possibility of discarding legitimate email and is not recommend. A setting of 25 will discard virtually no emails and is not recommend either. Basically, the closer to 5, more emails will be discarded.

=== Subject Tag ===
The Subject Tag setting is used to allow Spam through to the users' mailboxes, but flag it as Spam.
  * **Use Subject Tag** - enable/disable e-mail subject tag when e-mail is marked as spam
  * **Subject Tag Threshold** - spam score required to trigger a change in the e-mail subject and headers
  * **Subject Tag** - the subject tag to use when e-mail is marked as spam

A subject tag can be added to messages marked as spam.  For instance a spam message with the subject "Premier Invest0r Rep0rt" will be transformed into "[SPAM] Premier Invest0r Rep0rt".  This feature makes it easy for end users to identify and filter spam.

More emails will be tagged as '[SPAM]' at a lower number. For example, a setting of 3 will tag almost any email. A setting of 20 will tag no emails. Basically, the closer to 3, more emails will be tagged. Tagged emails are not deleted. Users will need to delete both tagged and un-tagged emails that they believe to be Spam.

<note tip>Some e-mail clients such as Thunderbird can be set to trust e-mail headers set by SpamAssassin and will automatically move the e-mails to the junk folder. If enabled, Thunderbird will check the X-Spam-Status flag set by SpamAssassin in the e-mail header based on the Subject Tag Threshold.</note>
==== Lists ====
The antispam engine includes both a whitelist and blacklist.  The whitelist is used to mark e-mail addresses that send non-spam, while the blacklist is used to mark e-mail addresses that are known spam. Entries should be listed one per line.

**Whitelist** - a list of e-mail addresses that should never be marked as spam
{{content:en_us:7_mail_antispam_whitelist.jpg?1000|Mail Antispam Whitelist}}
<note info>Among others, newsletters and legitimate e-commerce e-mail can sometimes be marked as spam.  The e-mail addresses for these messages can be added to the whitelist to prevent the message from becoming marked as spam.</note>
<note tip>To whitelist a whole domain use the form *@example.com</note>
**Blacklist** - a list of e-mail addresses that should always be marked as spam
{{content:en_us:7_mail_antispam_blacklist.jpg?1000|Mail Antispam Blacklist}}

{{keywords>clearos, clearos content, Mail Antispam, app-mail-antispam, clearos7, userguide, categoryserver, subcategorymessaging, maintainer_bchambers, maintainerreview_x}}
