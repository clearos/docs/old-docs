===== TLS/SSL Server Supports 3DES Cipher Suite =====
/**
 * This is the notes section. Rapid 7 response documents should ONLY be created by employees of ClearCenter with the authority to make statements on behalf of the company. If you have content that would be useful to the statement, please contact ClearCenter.
 */


'Transport Layer Security (TLS) versions 1.0 (RFC 2246) and 1.1 (RFC 4346) include cipher suites based on the 3DES (Triple Data Encryption Standard) algorithm. Since 3DES only provides an effective security of 112 bits, it is considered close to end of life by some agencies. Consequently, the 3DES algorithm is not included in the specifications for TLS version 1.3. ECRYPT II (from 2012) recommends for generic application independent long-term protection at least 128 bits security. The same recommendation has also been reported by BSI Germany (from 2015) and ANSSI France (from 2014), 128 bit is the recommended symmetric size and should be mandatory after 2020. While NIST (from 2012) still considers 3DES being appropriate to use until the end of 2030.'

  * [[https://www.rapid7.com/|Statement from Rapid 7]]

===== ClearCenter response =====
This protocol is needed to support older browsers in the ability to update to newer browsers.

==== Short response ====
Because ClearOS is often used as a first line of defense for weaker systems, 3DES is still included in ClearOS for backwards compatibility. This enables newly provisioned, legacy systems to get updates and fixes in order to modernize them. It is the browser that will negotiate lower or higher forms of encryption. To ensure that you communications are not compromised, please update ensure that your browser is up to date. Modern browsers can only use more modern methods which preclude 3DES. 
==== Long response ====
Because ClearOS is often used as a first line of defense for weaker systems, 3DES is still included in ClearOS for backwards compatibility. This enables newly provisioned, legacy systems to get updates and fixes in order to modernize them. It is the browser that will negotiate lower or higher forms of encryption. To ensure that you communications are not compromised, please update ensure that your browser is up to date. Modern browsers can only use more modern methods which preclude 3DES. 

The risk here is in the data exchange being snooped by an outside listener. Modern and updated systems will not be affected and the protocol is included in order to give older machines the opportunity to use tools to upgrade themselves. If your system is not a gateway or infrastructure piece to systems which need a path to upgrade to more modern systems, feel free to disable this protocol.
==== Resolution ====
You can test positive for supporting 3DES with the following:

  openssl s_client -cipher ECDH+3DES:DH+3DES:RSA+3DES  -connect localhost:81

You will get output like this indicating that the DES encryption session is supported and negotiated.

<code>New, TLSv1/SSLv3, Cipher is ECDHE-RSA-DES-CBC3-SHA
Server public key is 2048 bit
Secure Renegotiation IS supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
SSL-Session:
    Protocol  : TLSv1.2
    Cipher    : ECDHE-RSA-DES-CBC3-SHA
    Session-ID: 53D53B590D800716CAFAEB80B89DDEAED7A2F0B3B5576185FF909B7CC260A4A6
    Session-ID-ctx: 
    Master-Key: 62FDAF15F63FDCC5931822B1DBFFDE29F4BDD13939F282FDA8EDE17EA8F435C5673D1CDC30E028C142E26115932B9A74
    Key-Arg   : None
    Krb5 Principal: None
    PSK identity: None
    PSK identity hint: None
    TLS session ticket lifetime hint: 300 (seconds)
    TLS session ticket:
    0000 - f7 b9 7e fa 4a 6c 0c b3-f4 6c 99 49 cb 51 62 b9   ..~.Jl...l.I.Qb.
    0010 - f3 51 bf 97 d3 b4 55 89-39 8f be 82 bd 97 02 fa   .Q....U.9.......
    0020 - 72 eb 2f ad 7e 35 a4 e6-fc 0a 37 c0 c7 72 46 ba   r./.~5....7..rF.
    0030 - 13 c6 a6 37 66 bf f2 be-03 21 fd 85 2e e6 35 f8   ...7f....!....5.
    0040 - 1a 4c b2 df 16 34 29 e7-a8 31 6a ef 7a a4 50 a4   .L...4)..1j.z.P.
    0050 - 41 73 b3 86 44 9f 13 1f-e0 c1 2c 5f 42 a0 ab 09   As..D.....,_B...
    0060 - 3d 72 af fd cd 31 53 49-87 c2 d0 9d 3d fd f9 cd   =r...1SI....=...
    0070 - fd ab 63 39 58 0c df 94-57 09 71 f1 30 0a fd 4b   ..c9X...W.q.0..K
    0080 - 34 02 7d f9 6e 45 20 08-74 eb 9f bf ab 38 83 45   4.}.nE .t....8.E
    0090 - a1 d8 91 a9 0d d2 4b 59-0e a8 02 c2 8e b5 6e aa   ......KY......n.
    00a0 - f4 92 f1 21 87 44 34 54-36 0b 06 75 68 68 fc 7d   ...!.D4T6..uhh.}
    00b0 - 66 30 e2 c9 1f 40 c3 9a-44 b0 cf be 81 47 16 08   f0...@..D....G..

    Start Time: 1538578276
    Timeout   : 300 (sec)
    Verify return code: 0 (ok)
---
</code>

<note>Press <Enter> to escape this https session.</note>


If your ClearOS system is not involved with client workstations that may need to update to newer versions or patch levels (ie. not a gateway or proxy for older workstations looking to get updates), you can likely disable 3DES without any loss of function to older computers that HAVE already achieved update status. To disable 3DES, repeat this process for both Webconfig and the Web Server app (if installed):

Modify the following files for Webconfig (ClearOS 7):

  /usr/clearos/sandbox/etc/httpd/conf.d/framework.conf
  /usr/clearos/sandbox/etc/httpd/conf.d/ssl.conf

Modify the 'SSLCipherSuite' to be the following (feel free to comment the existing and add the following):

  SSLCipherSuite EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH

Test the connection with the following:

  openssl s_client -cipher ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES -connect localhost:81

You should get output similar to this indicating that these methods are not allowed:

<code>CONNECTED(00000003)
140259018504080:error:14077410:SSL routines:SSL23_GET_SERVER_HELLO:sslv3 alert handshake failure:s23_clnt.c:769:
---
no peer certificate available
---
No client certificate CA names sent
---
SSL handshake has read 7 bytes and written 141 bytes
---
New, (NONE), Cipher is (NONE)
Secure Renegotiation IS NOT supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
SSL-Session:
    Protocol  : TLSv1.2
    Cipher    : 0000
    Session-ID: 
    Session-ID-ctx: 
    Master-Key: 
    Key-Arg   : None
    Krb5 Principal: None
    PSK identity: None
    PSK identity hint: None
    Start Time: 1538578149
    Timeout   : 300 (sec)
    Verify return code: 0 (ok)
---
</code>

Make sure to restart Webconfig (and httpd for Web Server, if installed and reconfigured) to make sure your services go into effect:

  systemctl restart webconfig

  systemctl restart httpd

===== Links =====
  * [[https://www.rapid7.com/|Rapid 7]]

{{keywords>clearos, clearos content, vulnerabilities, Rapid7, xapp-name, maintainer_dloper}}
