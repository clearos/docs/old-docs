===== Overview =====

Marketplace Quick Select is a feature that can first be accessed during the "Getting Started" configuration wizard.  It is also available from the Marketplace in Webconfig.

As its name implies, Quick Select is designed to efficiently select and install apps and packages for ClearOS.  Quick Select Files (QSF) contain metadata used by the ClearOS Marketplace engine.  Rather than browsing through long lists of apps, advanced users who are familiar with ClearOS, the Marketplace model and available apps will appreciate the reduced time spent in configuring ClearOS for the first time.
 
In addition to ClearOS apps, Quick Select also supports non-app packages.  If you are not familiar in this distinction, refer to the [[:content:en_us:cc_doc_marketplace_quick_select#app_vs_non-app_packages|section below]].

<note important>Marketplace Quick Select is available in all ClearOS versions 6.4.1 and greater.</note>

===== Where do I use QSF files? =====

Marketplace Quick Select Files can be used in two areas of the ClearOS Webconfig UI.
  - Post install //Getting Started// wizard
  - In the ClearOS Marketplace

You will notice, post installation, just a minimum set of apps and supporting packages are installed on your server.  The //Getting Started// wizard guides the admin through required settings.  Once you reach the Marketplace step, you are presented with four options to proceed.  One of these options is to use QSF to select what apps and packages should be installed.  Select this option by highlighting it, and click on //Next// to upload your QSF file.
{{:documentation:user_guide:ss_marketplace_wizard_qsf.png?550|QSF in Wizard}}

On the following step in the Wizard, you will be asked to upload your QSF file.

{{:documentation:user_guide:ss_marketplace_qsf_file_select.png?550|File Upload}}

Once you have uploaded your QSF file, if successful, the form will display how many apps and packages you have installed.  It will also display, in tile format, all apps that are available.  Apps that were included in the QSF file will already be highlighted for installation.  If you want to customize, you can do so before proceeding to the final step - installing the apps.

{{:documentation:user_guide:ss_marketplace_qsf_file_select_done.png?550|Apps Selected}}

If you chose to skip the Quick Select option during the wizard or would like to run through another QSF upload, you can do so by clicking on the Marketplace link in the top right hand corner of Webconfig.  On the Marketplace page, under //Tools// in the left hand navigation bar, select //Quick Select File//.  You will be presented with a similar set of forms and steps as previously described and during the wizard.

{{:documentation:user_guide:ss_marketplace_qsf.png?550|QSF in Marketplace}}

===== Where can I get QSF files? =====

==== ClearCenter and the ClearFoundation Community ====

ClearCenter provides a number of Quick Select Files on the QSF Listing.  In addition, ClearFoundation community members have also contributed their own QSF definitions along with a brief description of the target audience/use.

  * [[:content:en_us:cc_doc_marketplace_quick_select|ClearCenter Quick Select Files]]

==== Create Your Own ====

There is nothing magical about a QSF file.  If you deploy ClearOS on a regular basis for either on-premise or in the cloud application hosting, creating your own custom QSF file(s) can significantly reduce your deployment time.

Using any text editor (vim, nano, gedit, notepad etc.), create a text document with any extension, but .txt is recommended.  Follow the format described in the [[:content:en_us:cc_doc_marketplace_quick_select#quick_select_file_format|section below]].

==== App vs. Non-App Packages ====
Any software installed via the ClearOS package manager engine (YUM) are collectively referred to as packages.

=== Apps ===
In ClearOS, an app is just a specialized package in that it contains code that builds upon Webconfig - the ClearOS User Interface.  Examples of apps for ClearOS are always prefixed by 'app-'.  Examples include:
  * app-smtp (SMTP server)
  * app-zarafa-small-business (Zarafa SMB Collaboration Suite)
  * app-firewall-incoming (ClearOS incoming firewall wizard)
  * etc.

Apps can always be found in the ClearOS Marketplace by scrolling through the listing or searching on specific keywords.  You can also find an up-to-date list on the [[http://www.clearcenter.com/marketplace|ClearCenter website]].

<note tip>At the time of writing, there about about 110+ apps developed for ClearOS with a handful being exclusively available to the [[http://www.clearcenter.com/Software/clearos-professional-overview.html|ClearOS Professional platform]].</note>

=== Non-App Packages ===

Packages that are not specifically related to ClearOS's Webconfig (eg. not prefixed with 'app-') are simply referred to as packages or RPM's - no different to other RPM-based distributions such as [[http://www.centos.org|CentOS]] or [[http://www.redhat.com|RedHat]].  In earlier versions of ClearOS, an administrator had to drop down to the command line to install packages not selected for install by default.  Examples of non-app packages are:
  * wget (Network file transfer utility)
  * iptraf (Network traffic and statistics monitor)
  * vim-enhanced (VI editor with enhanced features/functions)
  * etc.

With Quick Select Files, admins can now select any number of non-app packages that they would normally install manually to have access to the tools and features used in deploying and managing ClearOS.

===== Quick Select File Format =====
The following is an example of a Quick Select File.  Comments are preceded with a hash (#) whereas apps and non-apps (packages) are listed by package name (not filename).  There are a couple of parameters that can be set using a "key=value" format.  These are described below.

  # Quick Select File - Version 1.0
  app-content-filter
  app-web-proxy
  app-proxy-report
  vim-enhanced
  enable_repo=clearos-updates-testing

<note warning>The first line containing "# Quick Select File - Version 1.0" is *required* at the top of every QSF file and is used both to sanity check that the file was originally intended to provide QSF definitions in addition for future-proofing against future versions of Quick Select that may provide additional features.</note>

==== Comments ====
Any line preceded by a hash (#).

==== Apps/Non-Apps (Packages) for inclusion ====
Any line that does not precede with a hash, nor start with a reserved keyword (qsf_flag or enable_repo), is considered an app or package for installation.

Almost all apps have dependencies...a requirement of other packages to be installed to support the one you have selected.  Dependencies do not have to be included in your QSF file since they are handled by the package manager (YUM) at the time of installation.

<note warning>If you see more apps installing during the installation progress period than you have originally selected, dependencies are the reason.  Do not be alarmed...it is perfectly normal.  Some apps (like IMAP server) can literally pull in dozens of supporting packages.</note>

==== Flags and Parameters ====
=== Enabling Repositories ===

  enable_repo=REPO_NAME
  
Used to enable repos not enabled by default allowing packages in these repositories to be defined in the app list and installed.

=== Disabling/Excluding Paid Apps ===
  qsf_flag_no_paid

Flag to ignore any paid app that is not potentially included in the subscription services a registered system is subscribed to.
