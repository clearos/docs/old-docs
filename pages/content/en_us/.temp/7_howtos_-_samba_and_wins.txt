===== Using Samba and WINS =====
WINS provides a mechanism for resolution of a NetBIOS name to its IP address. WINS is like a Dynamic-DNS service for NetBIOS networking names. WINS is the Windows Internetworking Name Service.

When NetBIOS is run over TCP/IP NetBIOS names can be resolved using UDP broadcasts. Broadcast-based name resolution works adequately on networks with few Windows clients.  As the size of the network grows, the impact of UDP broadcasts soon takes its toll on overall network performance.

Browsing of MS Windows networks is possible via two mechanisms:
a) Resolution of NetBIOS names to IP addresses
b) A machine on each network segment acting as a master-browser

The master-browser collates the information that is propagated by each the NetBIOS nodes (Windows clients) so that it can be queried by local machines.  The Windows client that seeks to browse a network environment will send a broadcast message over UDP to the local network segment seeking to find the master-browser.  The master-browser will answer the request, thus informing the inquiring client of its IP address. The Windows client then asks the master-browser to provide the list of machines it knows about (has in its cache).  The client now has a list of machines that will initiate the browsing operation for the workgroup or domain.

When someone clicks on a machine in the browse list, the Windows client sends out a broadcast UDP packet seeking the owner of that machine name to respond.  As it does the Windows client obtains its IP address and can proceed to open a connection to that address, after which it can request the list of resources that are available on that machine. At this point there is already a TCP/IP connection from which connections can be made to the shares and services that are available to the user.

So far, so good.  This works quite well over a local network.  There can be a propagation delay of up to 15 minutes - the time interval between (or propagation delay) the various types of machine announcements that are sent over the UDP protocol.

It must be mentioned, that UDP broadcast packets are **not** forwarded by routers.  It they did, it would be easy to initiate a UDP broadcast storm with devastating effect on network traffic.  This means that broadcast-based name resolution is ineffectual for routed (segmented) networks.  WINS is a means for solving this dilemma.

===== Which Ports are Used by NetBIOS =====
NetBIOS networking uses the following network ports:

  * UDP port 137 - the NetBIOS name registration / resolution port
  * UDP port 138 - the NetBIOS datagram port
  * TCP port 139 - the NetBIOS session service (file and print) port

Windows 2000 and later allow network configuration without use of NetBIOS protocols. The NetBIOS-less protocols include use of the following ports:

  * TCP port 53 - the DNS port
  * TCP port 88 - the Kerberos port
  * TCP port 135 - the DCE RPC port   
  * TCP port 389 - the LDAP port
  * TCP port 445 - is the NetBIOS-less SMB (server message block) port

NetBIOS-less network configurations generally involves the use of Active Directory, however, DNS names can be used to make connections to Windows File and Print services without Active Directory. Such use involves use of TCP ports 139 and/or 445.


===== WINS to the Rescue =====
WINS is to NetBIOS names as is Dynamic DNS (DDNS) to IP Hostnames.

A NetBIOS name is 16 characters in length. The 16th character is binary-encoded according to various roles the name can play in the overall NetBIOS world.  The remaining 15 characters can be used to provide a human-friendly name for a personal computer (machine). The NetBIOS protocol calls for registration of that name with the appropriate role-code in the 16th character. These names are then propagated as part of a host announcement and the workgroup announcement.  These announcements are sent over the UDP protocol regardless of the use of WINS as a broadcast UDP message.  Broadcast messages are sent to the all 1's MAC address and thus interrupt every network control interface (NIC) on that network segment.

Where a WINS server (a special service provided only on MS Windows servers and by Samba) is used instead of using UDP broadcast packets to register NetBIOS names into the wild ether (local network), the name registration request can be sent via directed (unicast) UDP packet.  Unicast UDP packets are sent to the MAC address of the target machine, therefore they do **not** interrupt all NICs on the segment.

Windows clients can send a directed request (query) to the WINS server, for example to find the name of the current master-browser.  The use of WINS can reduce UDP broadcast traffic by up to 90%, and significantly accelerate the speed of name resolution processes - thus providing much faster network browsing.

The really good new about WINS is that direct (unicast) UDP **is** forwarded across routers.  This provides a mechanism that enables cross-subnet browsing support.

===== Samba Without WINS =====
Some network administrators despise the use of WINS.  While the argument against use of WINS is often ill-founded, we must respect the decision not to use WINS.  Fortunately, Samba provides tools to help rescue the anti-WINS community.  The following parameters may be added to the [global] section of the Samba ''/etc/samba/smb.conf'' file:

  * remote announce = 123.45.67.255
  * remote browse sync = 123.45.67.255/MYWORKGROUP


===== Samba With WINS =====
There are two (2) Samba parameters that enable one or the other manner in which Samba can use WINS.  The two can **not** both be enabled at the same time - they are mutually exclusive specifications.

  * wins support = [Yes | No]
  * wins server = 123.45.67.89

The //wins support// parameter, if set to //Yes//, enables //nmbd// (one of the Samba daemons) to be configured as a WINS server.

The //wins server// parameter points Samba at the WINS server.

Normally there will be only one WINS per Windows network. Any number of machines can use a common WINS server.  A single WINS server can be used by clients and members of multiple domains.

The WINS server can be provided either by Samba, or by MS Windows server products with the appropriate service installed.

In a multi-segment network, a WINS server can be configured on one segment and all clients and servers will use that central WINS server.  On MS Windows clients this is configured in the TCP/IP protocol settings under the //Advanced// tab -> WINS.

With Samba, the specification //wins support = Yes// enables Samba's //nmbd// to perform the WINS server functionality.  All remaining Samba servers are then configured to use the WINS server by specifying //wins server = ip_address//.

{{keywords>clearos, clearos content, clearos5, clearos7, bestpractices, howto, categoryserver, subcategorywindowsnetworking, maintainer_dloper}}
