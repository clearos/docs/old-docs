===== Static IPSec VPN =====
The following document provides information on how to configure the **IPsec VPN** service for your ClearOS system.  

The app is available in two flavours, the full version and a free basic version.

The Basic version provides:-
  * Static IPsec PSK tunnel creation between ClearOS <-> ClearOS (Openswan)
  * Host-to-Host or Subnet-to-Subnet tunnels in initiator or responder modes
  * Default secure encryption tunnels
  * Permits gateway-to-gateway communication without additional tunnels

The Full version comes with the above plus options to support connections with third party hardware:-
  * Dead peer detection
  * Additional options for IKE and ESP Encryption, Rekeying and Keylife, IKEv2 and Aggressive mode for maximum support with other hardware
  * Manual tunnel control with reloading of specific tunnels
  * Live AJAX tunnel status display
  * Use of tunnel ID's to identify local / remote connections
  * Support for road warrior connections with multiple remote IP addresses
  * Log and IPsec policy information for diagnostics

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:6_Marketplace|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:
                       
<navigation>Network|VPN|Static IPsec VPN</navigation>

===== Configuring Connections with Static IPsec VPN =====
The Static IPsec VPN app enables you to configure secure IPsec encrypted tunnels between two or more networks, in host-to-host or subnet-to-subnet type arrangements. In order to create a connection between two systems, you need to configure **both** VPN systems.

When creating tunnels it is often helpful to have a third party connection which can be used to connect to either tunnel in the event of problems. This is because the route for packets between hosts will be lost once the tunnel is configured, but may not be functional.

IPsec VPN's are a widely supported method of connecting sites together by creating an encrypted tunnel across the public internet between to private networks.

IPsec VPN is ideally suited for use in scenarios which have a static IP at each end. For dynamic IP configurations, please see the Dynamic VPN app available from the Marketplace.

In most implementations subnets are used to separate one side of the VPN from the other. Below is a basic example of a 'hub and spoke' configuration using the 'third' network address space to differentiate the sites:

<important>The LAN networks at either end of the VPN connection must not overlap!</important>

Local Site
192.168.1.XXX

Remote Site 1    
192.168.2.XXX

Remote Site 2    
192.168.3.XXX

===Local-to-Remote1 Site-to-Site Tunnel configuration===
  * Local WAN IP: 1.2.3.4
  * Local Gateway(Optional): 2.3.4.5
  * Local Subnet: 192.168.1.0/24
  * Local Source IP: 192.168.1.1
  * Remote WAN IP: 4.5.6.7
  * Remote Gateway(Optional): 5.6.7.8
  * Remote Subnet: 192.168.2.0/24
  * Remote Source IP: 192.168.2.1

===Local-to-Remote1 Host-to-Host Tunnel configuration===
  * Local WAN IP: 1.2.3.4
  * Local Gateway(Optional): 2.3.4.5
  * Remote WAN IP: 4.5.6.7
  * Remote Gateway(Optional): 5.6.7.8

===Local-to-Remote1 using Default Route *===
  * Local WAN IP: %defaultroute
  * Local Subnet: 192.168.1.0/24
  * Local Source IP: 192.168.1.1
  * Remote WAN IP: remote.domain.com
  * Remote Subnet: 192.168.2.0/24
  * Remote Source IP: 192.168.2.1

===Local-to-Remote1 using Road Warrior and ID's *===
  * Local WAN IP: %defaultroute
  * Local Subnet: 192.168.1.0/24
  * Local Source IP: 192.168.1.1
  * Local ID: clearos
  * Remote WAN IP: %any
  * Remote ID: roadwarrior

====Tunnel Creation====
From the webconfig tool, click on <button>add</button> in the Static VPN Connections box.  In summary to create a tunnel you need to:

  * Give your tunnel a short name
  * Select the tunnel startup mode
  * Select your Local WAN IP from the drop down box
  * Fill in the Remote WAN IP in the input field
  * Consider completing the Local/Remote Subnet fields for site-to-site tunnels.
  * Consider completing the Local/Remote Source IP fields to enable traffic between VPN gateways in a site-to-site tunnel.
  * Type in a pre-shared key (password)
  * Select the type of PSK ID fields to be used to make sure your PSK is unique and matches your tunnel configuration*
  * Review the Phase1 and Phase2 encryption options, matching your remote configuration. Most can be left as default or unspecified.*
  * Review the IPsec other options, again matching your remote configuration*

<note important>Note: * denotes options that are not available in the basic version of the app</note>

For the best chance of success connecting to third party IPsec hardware, connection configurations must match at both ends.

==== Configuration Notes ====
Most router appliances targeted at the small business market (such as Netgear / D-Link) use IKEv1.

As IPSEC has developed vendors have adopted different naming conventions for describing either side of the tunnel: head office / satellite, left / right. The convention used in the ClearOS app is Local, Remote. 

There are a number of 'accepted' default values:
  * Perfect Forward Secrecy (pfs): yes
  * Aggressive Mode (aggrmode): no
  * Rekey: yes
  * Phase1 key life (ikelifetime): 28800 seconds (8 hours).
  * Phase2 key life (salifetime): 3600 seconds (1 hour).
  * Compress: no
  * Dead Peer Detection: Hold route

Subnet masks are required to be be noted in their full CIDR notation (e.g. 192.168.1.0/24)

Preshared key – similar to a good password policy, use a combination of numbers and letters, to ensure maximum compatibility avoid using special characters. The preshared key must be the same for all tunnels with a Remote WAN IP = %any.

Interface MTU - Ethernet interfaces have a default MTU of 1500, if directly applied, the additional IPSEC header will cause packet fragmentation, which will likely result in data transfer problems. It is recommended that the MTU for WAN interfaces supporting IPSEC VPN tunnels is reduced. There are several popular choices, but 1430 seems the most widely adopted. As of 6.4 setting the MTU for an eth interface is not available from the GUI, so the line MTU="1430" will need to be added to /etc/sysconfig/network-scripts/ifcfg-ethx (remember to reload using ifdown ethx && ifup ethx for the change to take effect). 

<note important>Note: It is strongly recommended that if you intend to have any Road Warriors connecting to you by any VPN method (IPsec, OpenVPN, PPTP) that you do not use LAN subnets of 192.168.0.0/24 and 192.168.1.0/24. These are the common defaults of domestic routers and most domestic users do not change them. A Road Warrior behind one of these routers will be able to connect but no traffic will pass through the VPN</note>

===== Technical =====

The IPsec VPN app uses Openswan, and has been tested for compatibility with the Libreswan fork.

The app configures tunnels by using files within the /etc/ipsec.d/ subfolder. Each tunnel is managed by a separate tunnel.conf (containing the connection parameters) and tunnel.secrets (containing the pre-shared key) file. Global parameters can be applied by editing /etc/ipsec.conf

===== Troubleshooting =====
There are several things you can do to determine what is going on with your tunnel

  * check the logs at /var/log/ipsec (filter by tunnel name aids viewing)
  * check the route table, and the presence of IPsec routing policies
  * ping the remote host WAN IP, and remote source IP
  * use a tool like tcpdump to verify that ESP type packets are passing between hosts, note that if you inspect traffic on the gateway you will see both unencrypted and encrypted packets.
  * use a third party connection to connect to your remote end
  * reload the specific tunnel from the webconfig (can help to clear stale SAs)*
  * restart the firewall service 
  * restart the ipsec service at both ends
  * post in the forums and we'll try to help as best we can. When doing this please consider running 'ipsec barf > /tmp/ipsec.config' and attaching the output as this collects all the relevant IPsec information from your system.

                                                                                                                                    ===== Configuration Parameters =====
                                                                                                                                    For further detailed information on each of the configuration parameters please refer to the [[http://linux.die.net/man/5/ipsec.conf|IPsec manual pages]]. <important>The items denoted with * are not available in the basic version of the app.</important>
                                                                                                                                    === Connection Name ===
                                                                                                                                    Tunnel name, no spaces permitted
                                                                                                                                    === Connection Mode ===
                                                                                                                                    Tunnel connection mode at startup, 'automatic' tunnels will be brought up on boot, 'listen' only implies responder type tunnels only, 'manual start' ignores the tunnel unless manually brought up via the user using the reload button or from the command line
                                                                                                                                    ===Local WANIP/FQDN===
                                                                                                                                    The IP address of the left participant's public-network interface. There are several magic values. If it is %defaultroute, and the config setup section's, interfaces specification contains %defaultroute, left will be filled in automatically with the local address of the default-route interface (as determined at IPsec startup time); this also overrides any value supplied for leftnexthop. (Either left or right may be %defaultroute, but not both.) The value %any signifies an address to be filled in (by automatic keying) during negotiation.  If using IP addresses in combination with NAT, always use the actual local machine's (NAT'ed) IP address, and if the remote (eg right=) is NAT'ed as well, the remote's public (not NAT'ed) IP address. Note that this makes the configuration no longer symmetrical on both sides, so you cannot use an identical configuration file on both hosts.
                                                                                                                                    ===Local Subnet IP===
                                                                                                                                    Private subnet behind the left participant, expressed as network/netmask; If omitted, essentially assumed to be left/32, signifying that the left end of the connection goes to the left participant only
                                                                                                                                    ===Local Gateway IP===
                                                                                                                                    Next-hop gateway IP address for the left participant's connection to the public network; defaults to %direct (meaning right). If the value is to be overridden by the left=%defaultroute method (see above), an explicit value must not be given.  Relevant only locally, other end need not agree on it.
                                                                                                                                    ===Local LAN IP===
                                                                                                                                    The IP address for this host to use when transmitting a packet to the other side of this link. Relevant only locally, the other end need not agree. This option is used to make the gateway itself use its internal IP, which is part of the leftsubnet, to communicate to the rightsubnet or right. Otherwise, it will use its nearest IP address, which is its public IP address. This option is mostly used when defining subnet-subnet connections, so that the gateways can talk to each other and the subnet at the other end, without the need to build additional host-subnet, subnet-host and host-host tunnels. 
                                                                                                                                    ===Local ID *===
                                                                                                                                    ID used to identify the local, can either be a string beginning with @ or the WAN IP address. This is often used with %any type connections to identify remote hosts
                                                                                                                                    ===Remote Options===
                                                                                                                                    As above but for the remote system. Note that the local/remote terminology is only for reference, Openswan will determine which connection is which during negotiation and therefore it is possible to use exactly the same configuration at both ends.  
                                                                                                                                    ===Pre shared key===
                                                                                                                                    Pre shared key used to authenticate the tunnel connection between two hosts. 
                                                                                                                                    ===Local/Remote PSK ID Type *===
                                                                                                                                    What type of ID used to identify your PSK. This is typically your local and remote IP addresses, but can also be friendly ID names. Note that one PSK secrets file is setup for each tunnel, therefore the use of %any should be considered carefully as it will match more than one tunnel. You may also select FQDN which will force Openswan to determine the IP address via DNS first. Useful for remote connections on dynamic IPs in conjunction with 'right=%any'
                                                                                                                                    ===IKE Version *===
                                                                                                                                    IKEv2 (RFC4309) settings to be used. Currently the accepted values are permit, (the default) signifying no IKEv2 should be transmitted, but will be accepted if the other ends initiates to us with IKEv2; never or no signifying no IKEv2 negotiation should be transmitted or accepted; propose or yes signifying that we permit IKEv2, and also use it as the default to initiate; insist, signifying we only accept and receive IKEv2 - IKEv1 negotiations will be rejected. 
                                                                                                                                    ===Phase1 Cipher/Hash/DH Group *===
                                                                                                                                    IKE encryption/authentication algorithm to be used for the connection (phase 1 aka ISAKMP SA). Any left out option will be filled in with all allowed default options. For best compatibility with third party hardware, set these to be consistent with the remote end of the tunnel
                                                                                                                                    ===Phase2 Cipher/Hash/PFS Group *===
                                                                                                                                    Specifies the algorithms that will be offered/accepted for a phase2 negotiation. If not specified, a secure set of defaults will be used.
                                                                                                                                    ===IKE Connection Lifetime *===
                                                                                                                                    How long the keying channel of a connection (buzzphrase: "ISAKMP SA") should last before being renegotiated; The two-ends-disagree case is similar to that of keylife.
                                                                                                                                    ===SA LIfetime *===
                                                                                                                                    How long a particular instance of a connection (a set of encryption/authentication keys for user packets) should last, from successful negotiation to expiry;(default 8h, maximum 24h). Normally, the connection is renegotiated (via the keying channel) before it expires. The two ends need not exactly agree on salifetime, although if they do not, there will be some clutter of superseded connections on the end which thinks the lifetime is longer.
                                                                                                                                    ===Dead Peer Detection Action *===
                                                                                                                                    When a DPD enabled peer is declared dead, what action should be taken. hold (default) means the eroute will be put into %hold status, while clear means the eroute and SA with both be cleared. restart means the the SA will immediately be renegotiated, and restart_by_peer means that ALL SA's to the dead peer will renegotiated. A hold option is often used for permanent site-to-site tunnels to prevent traffic leaving the gateway unencrypted. When a remote connection is likely to change IP address or you are located on a hub with many incoming tunnels, it may be better to clear the route instead. DPD can be configured at both ends of the tunnels, and does not need to match, however it is usually better to enable DPD on the initiating end of the tunnel.
                                                                                                                                    ===Compress Data *===
                                                                                                                                    Whether IPComp compression of content is proposed on the connection (link-level compression does not work on encrypted data, so to be effective, compression must be done before encryption); acceptable values are yes and no (the default). The two ends need not agree. A value of yes causes IPsec to propose both compressed and uncompressed, and prefer compressed. A value of no prevents IPsec from proposing compression; a proposal to compress will still be accepted.
                                                                                                                                    ===Perfect forward Secrecy (PFS) *===
                                                                                                                                    Whether Perfect Forward Secrecy of keys is desired on the connection's keying channel (with PFS, penetration of the key-exchange protocol does not compromise keys negotiated earlier); Since there is no reason to ever refuse PFS, Openswan will allow a connection defined with pfs=no to use PFS anyway. Acceptable values are yes (the default) and no.
                                                                                                                                    ===Rekey *===
                                                                                                                                    Whether a connection should be renegotiated when it is about to expire; acceptable values are yes (the default) and no. The two ends need not agree, but while a value of no prevents Pluto from requesting renegotiation, it does not prevent responding to renegotiation requested from the other end, so no will be largely ineffective unless both ends agree on it.
                                                                                                                                    ===Aggressive Mode *===
                                                                                                                                    Use Aggressive Mode instead of Main Mode. Aggressive Mode is less secure, and vulnerable to Denial Of Service attacks. It is also vulnerable to brute force attacks with software such as ikecrack. It should not be used, and it should especially not be used with XAUTH and group secrets (PSK). If the remote system administrator insists on staying irresponsible, enable this option. Aggressive Mode is further limited to only proposals with one DH group as there is no room to negotiate the DH group. Therefore it is mandatory for Aggressive Mode connections that both IKE and PHASE2 options are specified with only fully specified proposal using one DH group. Acceptable values are yes or no (the default).

{{keywords>clearos, clearos content, Static IPSec VPN, app-static-vpn, clearos7, userguide, categorynetwork, subcategoryvpn, maintainer_dloper}}
