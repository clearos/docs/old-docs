===== IPtables =====
The iptables utility controls the network packet filtering code in the Linux kernel.

===== Changes =====
  * Patch and files to support bandwidth management [[http://www.linuximq.net|IMQ]]
  * iptables init script replaced by firewall script

{{keywords>clearos, clearos content, AppName, app_name, version6, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
