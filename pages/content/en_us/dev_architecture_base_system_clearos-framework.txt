===== Base System - clearos-framework =====
We're almost getting to the point where you will have the web-based interface up and running!  Before we can do that, the **clearos-framework** needs to be installed.  This package provides the base PHP developer framework used to create apps for ClearOS. 

In order to keep the ClearOS framework isolated from the operating system's PHP and web server installations, a separate Apache/PHP engine is installed.  Following the ClearOS mantra of not re-inventing the wheel, the ClearOS Apache/PHP uses the exact same source code version found in CentOS/RHEL, but installed neatly into /usr/clearos/sandbox.  The package names that you will see installed are:

  * webconfig-httpd-* - the Apache engine
  * webconfig-php-* - the PHP engine

The clearos-framework itself is the PHP development framework based off of [[http://www.codeigniter.com|CodeIgniter]].  There are a few tweaks in there of course, but its still 99% untouched CodeIgniter.  There we are again -- no wheels re-invented!

If you are interested in more information on the framework, you can find a high level overview [[:knowledgebase:developmentframework|here]].

To install the package on our CentOS to ClearOS bootstrap install:

  yum install clearos-framework

The result should be similar to the following:
  ==========================================================================================
  Package                     Arch          Version              Repository           Size
  ==========================================================================================
  Installing:
  clearos-framework           noarch        6.0-0.3.v5           base-updates        333 k
  Installing for dependencies:
  webconfig-httpd             i686          2.2.15-4.v6          base-updates        940 k
  webconfig-php               i686          5.3.2-4.v6           base-updates        1.1 M
  webconfig-php-cli           i686          5.3.2-4.v6           base-updates        2.2 M
  webconfig-php-common        i686          5.3.2-4.v6           base-updates        515 k
 
You may see other dependencies installed.

===== Start Webconfig =====
You will now be able to start webconfig!  From the command line, run:

  service webconfig start
 
At this point, you should be able to point your web browser to https://your.ip.address:81 and see a Obi-Wan "no apps installed" warning message. 


{{keywords>clearos, clearos content, dev, architecture, maintainer_dloper}}
