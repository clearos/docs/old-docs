===== OLD FreeRADIUS 2 Howto =====
<note warning>This is deprecated. It was designed for ClearOS 5.2 SP2. The following information has been deprecated as this module has been moved to the extras repository for 5.2 and is to be included in future releases. For instructions on how to install FreeRADIUS [[:content:en_us:7_ug_radius|click here]]</note>

===== Manual method =====
This guide covers encrypted connections between clients and the RADIUS server through a supplicant or wireless access device. This guide is also limited in the scope of FreeRADIUS configuring it to be an integrated solution to provide WPA2 Infrastructure mode.

===== Installing FreeRADIUS =====
Download the freeradius2 package from one of the CentOS mirrors, ie:

  yum install freeradius2-ldap
  yum install freeradius2-utils

This should install with the dependency for 'freeradius2'. Let it install that and any other dependency.

This install will also create a directory in /etc called raddb. We will be working out of this directory to configure RADIUS.

===== Setup Webconfig =====
Be default all users of the system that properly authenticate will have access. To specifically limit access to a particular group, create a group called 'radius_users' in Webconfig. Set any users you want to be members of this group.

While you are here create a test user that is NOT part of the radius_users group. We will use this user to test that they cannot access the system via RADIUS.

We will be limiting the users by group later.

===== Security =====
Before starting the service, we will configure security. We will want to tie the existing security between the local certificate authority and the local server keys. We will do this instead of creating new certificates because you will be able to add the CA for the server to your workstation as a trusted certificate authority for a number of services on ClearOS.

Run the following:

  mv /etc/raddb/certs /etc/raddb/certs~
  mkdir /etc/raddb/certs
  cp /etc/ssl/ca-cert.pem /etc/raddb/certs/
  cp /etc/ssl/sys-0-cert.pem /etc/raddb/certs/
  cp /etc/ssl/dh1024.pem /etc/raddb/certs/
  cp /etc/ssl/private/sys-0-key.pem /etc/raddb/certs/
  chmod 640 /etc/raddb/certs/*
  chgrp -R radiusd /etc/raddb/certs


Next, change the /etc/raddb/eap.conf file to use these certificates instead of creating new default ones.

  mv /etc/raddb/eap.conf /etc/raddb/eap.conf~
  touch /etc/raddb/eap.conf && chgrp radiusd /etc/raddb/eap.conf
  vi /etc/raddb/eap.conf

Insert the following:

  eap {
  	#default_eap_type = md5
  	default_eap_type = ttls
  	timer_expire     = 60
  	ignore_unknown_eap_types = no
  	cisco_accounting_username_bug = no
  	max_sessions = 2048
  	md5 {
  	}
  	leap {
  	}
  	gtc {
  		auth_type = PAP
  	}
  	tls {
                certdir = ${confdir}/certs
                cadir = ${confdir}/certs
                #private_key_password = whatever
                private_key_password = 
                #private_key_file = ${certdir}/server.pem
                private_key_file = ${certdir}/sys-0-key.pem
                #certificate_file = ${certdir}/server.pem
                certificate_file = ${certdir}/sys-0-cert.pem
                #CA_file = ${cadir}/ca.pem
                CA_file = ${cadir}/ca-cert.pem
                #dh_file = ${certdir}/dh
                dh_file = ${certdir}/dh1024.pem
                #random_file = ${certdir}/random
                random_file = /dev/urandom
                cipher_list = "DEFAULT"
                #make_cert_command = "${certdir}/bootstrap"
                cache {
                      enable = no
                      max_entries = 255
                }
  	}
  	ttls {
  		default_eap_type = md5
  		copy_request_to_tunnel = no
  		use_tunneled_reply = no
  		virtual_server = "inner-tunnel"
  	}
  	peap {
  		default_eap_type = mschapv2
  		copy_request_to_tunnel = no
  		use_tunneled_reply = no
  		virtual_server = "inner-tunnel"
  	}
  	mschapv2 {
  	}
  }

==== LDAP Module ====

Replace the entire file named ldap with this content. It's a good idea to backup the file too (cp /etc/raddb/modules/ldap /root/raddb-modules-ldap~).

  mkdir /etc/raddb/modules/backup
  mv /etc/raddb/modules/ldap /etc/raddb/modules/backup/
  touch /etc/raddb/modules/ldap && chgrp radiusd /etc/raddb/modules/ldap
  vi /etc/raddb/modules/ldap

<note>(NOTE: you will need to change the identity, password, and basedn based on your configuration, see /etc/kolab/kolab.conf for bind_dn, bind_pw, and base_dn respectively)</note>

  ldap {
  	server = "localhost"
  	identity = "cn=manager,cn=internal,dc=clearos,dc=lan"
  	password = fMOMGUmGJr9ZKBLz
  	basedn = "dc=clearos,dc=lan"
  	filter = "(uid=%{%{Stripped-User-Name}:-%{User-Name}})"
  	ldap_connections_number = 5
  	timeout = 4
  	timelimit = 3
  	net_timeout = 1
  	tls {
  		start_tls = no
  	}
  	dictionary_mapping = ${confdir}/ldap.attrmap
  	edir_account_policy_check = no
  	groupname_attribute = cn
  	groupmembership_filter = "(|(&(objectClass=GroupOfNames)(member=%{control:Ldap-UserDn}))(&(objectClass=GroupOfUniqueNames)(uniquemember=%{control:Ldap-UserDn})))"
  	groupmembership_attribute = radius_users
  }

=== Activate LDAP in the Inner Tunnel ===
You will need to uncomment the **ldap** line in inner-tunnel in the authorize section. It should be the first ldap you find there.

  vi /etc/raddb/sites-enabled/inner-tunnel

That section should now look like this:

          #
          #  If you are using /etc/smbpasswd, and are also doing
          #  mschap authentication, the un-comment this line, and
          #  configure the 'etc_smbpasswd' module, above.
  #       etc_smbpasswd
  
          #
          #  The ldap module will set Auth-Type to LDAP if it has not
          #  already been set
          ldap
  
          #
          #  Enforce daily limits on time spent logged in.
  #       daily


===== Setting up Clients =====

  mv /etc/raddb/clients.conf && touch /etc/raddb/clients.conf && chgrp radiusd /etc/raddb/clients.conf
  echo '$INCLUDE clearos.clients.conf' > /etc/raddb/clients.conf
  touch /etc/raddb/clearos.clients.conf && chgrp radiusd /etc/raddb/clearos.clients.conf && vi /etc/raddb/clearos.clients.conf

Paste the following:

  client 192.168.1.50/32 {
        secret          = examplepassword
        shortname       = downstairs_AP
  }
  client 127.0.0.1/32 {
        secret          = examplepassword
        shortname       = test
  }

===== Setting up Group Membership Restrictions =====
  mv /etc/raddb/users /etc/raddb/users~ && touch /etc/raddb/users && chgrp radiusd /etc/raddb/users
  echo '$INCLUDE clearos.users' > /etc/raddb/users
  touch /etc/raddb/clearos.users && chgrp radiusd /etc/raddb/clearos.users && vi /etc/raddb/clearos.users

Paste the following:

  DEFAULT LDAP-Group != "radius_users", Auth-Type := Reject


===== Starting the Service =====
Start the service:

  radiusd -X

<note>Please note the capital X.</note>

After you have run this command the server should respond:

  Listening on authentication address * port 1812
  Listening on accounting address * port 1813
  Listening on command file /var/run/radiusd/radiusd.sock
  Listening on proxy address * port 1814
  Ready to process requests.

Hit control+C to stop the process and run the following:

  service radiusd start

Stop the radiusd service. For the meantime, we will run the service in debugging mode in a separate window.

  service radiusd stop
  radiusd -X


===== Testing (Helpful tips in case something is wrong) =====
You will need a file included on in the wpa_supplicant package but not currently available under the one from the repository called 'eapol_test' because it has to be compiled out of the code and the repository version does not include it. [[http://www.clearcenter.com/media/dev/eapol_test.tar.gz|Here is the file]]. Then place it in /usr/local/bin/ and extract it.

  cd ~
  wget http://www.clearcenter.com/media/dev/eapol_test.tar.gz
  md5sum /root/eapol_test.tar.gz

It should report: 0c0dd022cf54d141b1dc0718126d981a  eapol_test.tar.gz


Create a 'good user' test file where the user is part of the radius_users group (identity is the username, password is the password, ca-cert is a copy of the server certificate)

  vi testgooduser

  network={
  eap=TTLS
  eapol_flags=0
  key_mgmt=IEEE8021X
  identity="testuser"
  password="testtest"
  #anonymous_identity="anonymous"
  ca_cert="/etc/raddb/certs/ca-cert.pem"
  phase2="auth=MSCHAP"
  }


Create a 'bad user' test file where the user is **NOT** part of the radius_users group:

  vi testbaduser

  network={
  eap=TTLS
  eapol_flags=0
  key_mgmt=IEEE8021X
  identity="notwpa"
  password="testtest"
  #anonymous_identity="anonymous"
  ca_cert="/etc/raddb/certs/ca-cert.pem"
  phase2="auth=MSCHAP"
  }

In this command 'examplepassword' is the shared secret between the clearos.clients.conf and the supplicant. In this case the supplicant is our command line but in the case of your environment, the supplicant is the Wireless Access Device.

  eapol_test -s examplepassword -c testgooduser

===== Set RADIUS to automatically start =====
Run the following to automatically start radiusd:

  chkconfig --level 2345 radiusd on && chkconfig --list radiusd

This command should report:

  radiusd        	0:off	1:off	2:on	3:on	4:on	5:on	6:off
{{keywords>clearos, clearos content, clearos5, categoryserver, skunkworks, completedproject, maintainer_dloper, maintainerreview_dloper}}
