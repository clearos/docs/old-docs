{{ :userguides:content_filter_updates.svg?80}}
<WRAP clear></WRAP>

===== Content Filter Blacklists =====
Studies reveal the average employee spends 75 minutes per day using office computers for non-business related activity, costing employers thousands of dollars per year, per employee.  If you are a business owner, government agency or academic institution, receiving blacklist updates may be required by law (eg. CIPA) in order to meet certain government regulations.  The ClearCenter Content Filter Updates service provides updates to the URL blacklist to improve the effectiveness of the filter.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/cloud/content_filter_updates|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_content_filter_updates|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_content_filter_updates|here]].
==== Additional Notes ====
