===== webconfig =====
Webconfig-httpd is the Apache web server engine used by the ClearOS web interface.  The service runs on port 81 (SSL) and 82 (non-SSL) as well as port 83 for webmail.  The software is isolated from the base Apache web server that is used by the end user. 

===== Developer Notes =====
The version and RPM spec file is based of off the default upstream httpd release.   The modifications include:

  * Changing the install root to /usr/clearos/webconfig
  * Tweaking the default configuration file 
  * Tweaking some helper files (logrotate, openssl certificate generation)
  * Patching a path in the apxs script
{{keywords>clearos, clearos content, dev, packaging, version7, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
