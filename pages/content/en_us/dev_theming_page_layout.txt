===== Page Layout =====
The **page.php** defines the various types of page layouts that are required in a ClearOS web interface. 

^Page Type^Description^
|MY_Page::TYPE_CONFIGURATION|A standard configuration page (meat and potatoes)|
|MY_Page::TYPE_WIDE_CONFIGURATION|A configuration page that requires extra width|
|MY_Page::TYPE_REPORTS|A report page|
|MY_Page::TYPE_REPORT_OVERVIEW|A report overview page|
|MY_Page::TYPE_SPOTLIGHT|Marketplace and potentially other special pages|
|MY_Page::TYPE_DASHBOARD|Dashboard|
|My_Page::TYPE_SPLASH|A minimalist splash page (e.g. web proxy warning)|
|MY_Page::TYPE_SPLASH_ORGANIZATION|A minimalist splash page that allows company logos/themes|
|MY_Page::TYPE_LOGIN|The login page|
|MY_Page::TYPE_WIZARD|A wizard page|
|MY_Page::TYPE_CONSOLE|The graphical console page|

===== Hook =====
The theme engine loads the **core/page.php** file in your theme directory and expects to find the **theme_page** hook defined.  

The parameter **page** provides a whole bunch of information that you can use to build out your layouts.
{{keywords>clearos, clearos content, dev, theming, maintainer_dloper, maintainerreview_x, keywordfix}}
