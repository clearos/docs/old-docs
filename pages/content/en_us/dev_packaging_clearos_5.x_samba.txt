===== Samba =====
Samba is the protocol by which a lot of PC-related machines share
files, printers, and other information (such as lists of available
files and printers). The Windows NT, OS/2, and Linux operating systems
support this natively, and add-on packages can enable the same thing
for DOS, Windows, VMS, UNIX of all kinds, MVS, and more. This package
provides an SMB server that can be used to provide network services to
SMB (sometimes called "Lan Manager") clients.

===== Changes =====
{{keywords>clearos, clearos content, AppName, app_name, version5, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
