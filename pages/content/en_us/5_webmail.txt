===== Webmail =====
A web-based e-mail solution ideal for allowing users on the road and without a mail client to access mail on the server using any computer connected to the Internet.

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Accessing Webmail =====
  * The webmail system runs on port 83 on the HTTPS protocol.  To access the system type https://192.168.1.1:83/ or https://yourdomain.com:83/

  * If webmail access is required from the Internet, please [[:content:en_us:5_firewall_incoming| allow connections to port 83 (webmail) on the firewall ]].

  * Web-based mail requires the IMAP server to be running.

  * Users will receive a pop-up warning in their web browser similar to that shown below.  This is normal and does not diminish the fact that the connection is encrypted and secure.  If desired, you can customize and manage the secure certificate using the [[:content:en_us:5_certificate_manager|Certificate Manager]].

===== Vacation / Auto-Reply =====
The webmail system includes a vacation / auto-reply system.  To access this feature:

  * Login to your webmail account
  * Click on <navigation>Mail|Filters</navigation> in the menu
  * Select the **Vacation** filter

===== Links =====
{{keywords>clearos, clearos content, Webmail, app-webmail, clearos5, userguide, categoryserver, subcategorymail, maintainer_dloper}}
