===== TNEF =====
TNEF provides a way to unpack those pesky Microsoft MS-TNEF MIME attachments. It operates like tar in order to unpack any files which may have been put into the MS-TNEF attachment instead of being attached separately.

  * [[http://sourceforge.net/projects/tnef/|TNEF Project Page]]

===== Required By =====
{{keywords>clearos, clearos content, AppName, app_name, version5, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
