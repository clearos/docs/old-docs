===== Known Issues with ClearVM =====

  * In nested virtualization using ClearVM in second virtualization layer the performance is not good. Same setup on native is much much faster. More info about nested I/O performance [[https://www.usenix.org/conference/fast12/understanding-performance-implications-nested-file-systems-virtualized-environment|here]] (paper [[https://www.usenix.org/system/files/conference/fast12/le.pdf|here]]).
  * In nested virtualization using NFS storage at second virtualization layer Windows storage may not be reliable.
  * Kernel error “kvm: 10626: cpu0 unhandled rdmsr: 0xc0010001″ is sometimes shown at the console. It is considered [[http://www.mail-archive.com/kvm@vger.kernel.org/msg09096.html|harmless]].

{{keywords>clearvm, kb, howto, maintainer_dloper}}