===== System Requirements =====
==== Summary ====
For hardware, virtual and cloud environments, the base requirements are as follows:

^Base System^^
|Processor/CPU  | 32-bit or 64-bit|
|Memory/RAM     | At least 1 GB is recommended (see guidelines below)|
|Hard Disk      | At least 10 GB is recommended (see guidelines below)|
|Network        | Ethernet, cable, DSL|

For hardware installs, the requirements are as follows:

^Peripherals^^
|Network Cards  | A network card is required, two for gateway mode|
|CD-ROM Drive   | Required for CD installation only|
|USB            | Required for USB key installation only|
|Mouse          | Not required|
|Monitor and Keyboard| Required for installation only|

===== Size Requirements =====
The following are guidelines for estimating the right hardware for your system.  Keep in mind, the hardware required depends on how you use the software.  

^ RAM and CPU   ^ 5 users ^ 5-25 users ^ 25-50 users ^ 50-250 users ^ 250+ users ^
| Processor/CPU | Low-Power | Basic | Dual-Core | Quad-Core | Multi-Core + Multi-Processor |
| Memory/RAM    | 1-2 GB | 2-4 GB | 4-8 GB | 8-16 GB | 16-32 GB |
^ Hard Disk ^^^^^^
| Hard Disk 	| Installation and logs require 1 GB - optional storage is up to you |||||
| RAID          | Recommended for mission critical systems|||||

The following software modules are processor and memory intensive:

  * Intrusion Detection and Prevention
  * Content Filtering
  * Zarafa [[http://doc.zarafa.com/7.0/Administrator_Manual/en-US/html/_installing.html#_hardware_recommendations|See Hardware Recommendations]]
  * Mail
  * Antispam
  * Antivirus

===== Hardware Requirements =====
==== CPU ====
All modestly modern hardware includes a compatible CPU.  For those installing ClearOS on very old hardware, the CPU requirement is as follows:

  * Intel architectures from [[http://en.wikipedia.org/wiki/P6_(microarchitecture)|Pentium 6]] and onward
  * AMD architectures from Athlon and onward

==== Network Cards ====
Generally, ClearOS does a good job at auto-detecting hardware and most mass-market network cards are supported. [[http://www.clearcenter.com/Hardware/clearbox-hardware.html|ClearBOX]] also includes wireless support and you can read more about this hardware solution [[http://www.clearcenter.com/Hardware/clearbox-hardware.html|here]]. Though wireless card drivers are included in ClearOS, wireless is not officially supported in the software version. 

==== Internet Connection ====
ClearOS supports most DSL (including PPPoE), cable modem broadband Internet connections and standard Ethernet connections.  ISDN and satellite broadband are not supported unless terminated with a standard Ethernet connection.

==== RAID Support ====
See [[:content:en_us:6_c_raid_support|RAID Support]].

===== Links =====
  * [[:content:en_us:6_d_compatibility|Compatibility]]
  * [[:content:en_us:6_e_downloading|Downloading]]
  * [[:content:en_us:6_c_raid_support|RAID Support]]
{{keywords>clearos, clearos content, clearos6, userguide, categoryinstallation, subcategorypreparation, maintainer_dloper}}
