===== File Scanner =====
The **File Scanner** provides a quick way to check for viruses on your file shares.  The scanner check:

  * User home directories
  * [[:content:en_us:5_flexshares|Flexshares]]
  * [[:content:en_us:5_web_server|Uploaded Web Site Files]]
  * [[:content:en_us:5_web_proxy|Web Proxy Cache]]

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:

<navigation>Server|File and Print|File Scanner</navigation>

===== Configuration =====
{{keywords>clearos, clearos content, AppName, app_name, clearos5, userguide, categoryserver, subcategoryfileandprint, maintainer_dloper}}
