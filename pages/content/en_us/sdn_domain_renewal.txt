===== Domain Renewal =====
It is extremely important to renew domains before they expire.  Failure to renew a domain can result in loss of ownership!  Our system automatically tracks the expiry of all domains in your account.  The system sends an e-mail to the contact listed in your account sixty (60) days prior to expiry.  Thirty (30) days prior to expiry, an additional e-mail is sent to the administrative contact for the domain.

===== Activation =====
  * Login to your [[https://secure.clearcenter.com/portal/|ClearCenter account]].
  * Click on <navigation>DNS|Renew Domain</navigation> in the top navigation bar.

===== Configuration =====
  * Select the domain and renewal term
  * Confirm your renewal and proceed through the online store checkout 

===== Auto-Renewals ====
==== Why Auto-Renew ====
Enabling the domain/DNS auto-renewal option will help to better protect your domains and ensure uninterrupted service.  If you are managing a number of domains, keeping track of expiry dates can become a daunting task.  In some cases, an e-mail address used to register a domain or create an account is no longer in use - resulting in missed notifications of an upcoming domain expiry.

==== How Do I Enable Auto-Renewal? ====
To enable auto-renew:

  * Login to your [[https://secure.clearcenter.com/portal/|ClearCenter account]].
  * Click on <navigation>DNS|DNS/Domain Expiry</navigation> in the top navigation bar.
  * Click on <button>View</button>
  * Select the //yes// in the **Auto-Renew** field.

Selecting //yes// will automatically check your payment options setting.  If you are already configured for pre-authorized credit card payment or have sufficient credit terms with ClearCenter, the domain will be set to Auto-Renew.  Otherwise, you will be presented with a link that will allow you to set up a method of payment.

==== When Does a Domain/DNS Renew? ====
If our system detects that your DNS service or domain registration (or both) will expire in 15 days and the auto-renewal is activated, the appropriate renewal action will then be done automatically.  An invoice or receipt will be sent to you, depending on whether you selected pre-authorized credit card payment or renewal on terms of credit.

===== Warning About Deceptive Marketing Techniques =====
Some registrars have been known to practice deceptive sales and marketing techniques by sending unsolicited e-mail to domain owners warning of an impending expiry. If you have registered or successfully transferred your domain to your account, you can disregard all e-mail that does not originate from ClearCenter.  A sample e-mail warning from our system is shown below.

<code>
 To: you@yourdomain.com
 From: domains@clearcenter.com
 Subject: Domain yourdomain.com is due to expire

 Dear username,

 Our records indicate the following domain managed under your ClearCenter
 account is due to expire in sixty (60) days:

 Domain: yourdomain.com

 Online renewal for up to 10 years is available through your ClarkCenter
 account. Please login at https://secure.clearcenter.com/portal and follow
 the appropriate links. Renewal fees are USD 25.00/domain and include
 re-registration and DNS service for one (1) year. Help is available at
 http://www.clearcenter.com/support/user_guides/. Should you have any
 additional questions, please contact us at info <at> clearcenter.com.
 
 Thank you,
 Domains @ ClearCenter
</code>
{{keywords>clearos, clearos content, sdn, SDN Domain Renewal, categorydns, subcategoryservices, maintainer_dloper, userguide}}
