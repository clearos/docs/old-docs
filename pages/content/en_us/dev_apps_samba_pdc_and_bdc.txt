===== Windows Networking with PDC and BDC Support =====

===== The Goal =====
In ClearOS 5, with an integrated LDAP gives the fantastic possibilities for multiple CIFS servers using Samba. Having centralized users and groups helps to keep the file systems between servers consistent for user rights and therefore becomes tenable.

<note>This prototype for ClearOS 5 is production code in ClearOS 6 and 7 and is called Account Synchronization in ClearOS 6 and ClearOS Master/Slave Synchronization in ClearOS 7. This guide is only useful for ClearOS 5 systems that need similar functionality. This method is not support.</note>

===== Challenges =====
Replication of data, joining the workstation to the domain through the backup domain controller, updating passwords and keeping it all together. These are some of the challenges that face this implementation.

===== Design =====
You will need to make sure that your primary box is the master replicate for LDAP and that it is running on the PDC operations. For this cause, you should NOT use this on a production server but make these changes on test boxes. We will later cover converting a standalone server to a master server.

To blow away your LDAP configuration, user accounts, groups and everything and install this server as a master replicate, run the following from command line:

  ldapsetup -f

Choose master mode. This will also ask for a password, list a strong password that will be used for the ldap database (must not contain spaces). You will use this same password on all the replicates.

===== Notes and Code =====

Setup the PDC fully using Webconfig. Please use a different 'workgroup' name than the one provide which is 'Enterprise'. Start it up.


**NOTE: I'm currently running this with the profiles OFF. This is because I don't have a way to replicate the profile. I will test it more but what should happen is that if profiles are properly configured, a cached copy should exist on the local workstation....more later.**

Run the following on the PDC:

  net rpc getsid

Please take note of this SID.


==== Samba BDC parameters ====

It is best to set up our testing for the BDC with a fresh new box as opposed to an existing box in production.

=== Installation steps ===
This is an approximate guide. There may be some steps missing

== Name services /etc/hosts ==
Setup hosts files on both servers so that they know about eachother:

  192.168.1.1 system1.clearos.lan system1 server1.clearos.lan server1
  192.168.1.2 system2.clearos.lan system2 server2.clearos.lan server2
  
First setup the replicate by running:

  ldapsetup -f

This will ask some questions. You should use the same password for the LDAP server that you specified for the master server. You can get this password from /etc/kolab/kolab.conf on the other server.

  [root@system3 ~]# ldapsetup -f
  What is the role of this server (standalone, master, replicate)?: replicate
  What is your domain (e.g. example.com)?: clearos.lan
  What is the hostname of the master LDAP server?: 192.168.1.1
  Specify the master LDAP password?: The following settings will be used to set up LDAP
  
  Role:     replicate
  Master:   192.168.1.1
  Domain:   clearos.lan
  Password: **************
  
  Stopping slapd: [  OK  ]
  Starting slapd: [  OK  ]
  Stopping ldapsync: [  OK  ]
  Starting ldapsync: [  OK  ]

Once this is completed, you can validate this synchronization both in the Users section of the Webconfig on the replicate, or by running a dump of the LDAP.

  slapcat -n3

== Samba configuration ==

  * netbios name (should be unique)
  * workgroup (should match the setting on the PDC)
  * domain master (should be set to 'No')
  * make sure the following match what you have on the PDC
    * logon drive
    * logon path
    * logon home

  wins support = Yes
  wins server = 192.168.1.1


On the second server. Make sure to configure smb.conf on backup
set name, workgroup and domain master to no

copy smb.ldap.conf from server1
copy smb.winbind.conf from server1

  touch /etc/system/initialized/sambalocal

  service winbind start

  smbpasswd -w securepassword

  net rpc getsid MYDOMAIN

  net rpc join -Uwinadmin%clearos

  pdbedit -L

  service smb restart
  service nmb restart
  service winbind restart

After the PDC is running run the same command on the BDC, the numbers should match exactly:

  net rpc getsid

Start up the BDC from the command line:

  service smb start
  service nmb start

Webconfig should safely keep you from mucking things up because the BDC will be stuck at the changing of the winadmin screen which is impossible to do so long as the LDAP replicate on the local machine prohibits password changes.


Do NOT use Webconfig to setup any windows settings, users or groups if you are using the webconfig of the backup domain controller. Set the following manually in /etc/samba/smb.conf:


{{keywords>clearos, clearos content, ClearOS Master-Slave Synchronization, app-account_synchronization, version5, howto, skunkworks, kb, maintainer_dloper}}
