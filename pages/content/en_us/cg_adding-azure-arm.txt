===== Azure Resource Manager (ARM) =====
You can add your Azure ARM cloud using either of the two ways: 

  * **option A**: through the Azure cli tool . This is the easiest way and highly recommended, as it requires a few steps and will produce the credentials for you. 
  * **option B**: by generating keys manually. This needs to perform more actions and visit Azure ARM and Azure Classic portals. 
==== Add Azure ARM cloud through Azure cli ====
If you haven't already installed the az utility, follow instructions on  https://docs.microsoft.com/en-us/cli/azure/install-azure-cli to get it working on your system. Mac/Linux/Windows are supported. For Linux this will be

<code>
curl -L https://aka.ms/InstallAzureCli | bash # this will install a few python packages and the az utility
</code>
and 
<code>
exec -l $SHELL #restart shell for some changes to take effect.
</code>
We now have to login to Azure portal on   https://portal.azure.com and take a note of the SUBSCRIPTION ID that we are going to add to ClearGLASS. This will be Subscription ID field you specify on the ClearGLASS add cloud wizard. In our case this is 3101d491-f614-47b6-b1c6-f50a0ec1f277.

{{content:en_us:cg_azure-01.png}}

We can now run the command to get the credentials needed for ClearGLASS

<code>
az ad sp create-for-rbac --role="Owner" --scopes="/subscriptions/SUBSCRIPTION_ID"
</code>

Don't forget to replace SUBSCRIPTION_ID with your subscription ID. We are prompted to login, through the command 'az login'. 

<code>
az login
</code>

We are prompted to open a browser, open page  https://aka.ms/devicelogin and insert a code. From that place provide the credentials for your Azure ARM account and you will get this message

{{content:en_us:cg_azure-02.png}}

We can close the browser window now and re-run the az command

<code>
az ad sp create-for-rbac --role="Owner" --scopes="/subscriptions/3101d491-f614-47b6-b1c6-f50a0ec1f277"
{
  "appId": "38a7decc-b45e-400e-bac7-eaab55daeb94",
  "displayName": "azure-cli-2017-06-12-11-22-41",
  "name": "http://azure-cli-2017-06-12-11-22-41",
  "password": "63107820-83dc-4903-ab0a-70a25520ec32",
  "tenant": "35a32b70-7f55-4520-9c67-63e9b5db8166"
}
</code>
	
Output  includes all the credentials we need. TENANT ID is tenant (35a32b70-7f55-4520-9c67-63e9b5db8166 in my case), SUBSCRIPTION ID the one we already have, Client key is the appId (38a7decc-b45e-400e-bac7-eaab55daeb94 in my case) and Client secret is the password (63107820-83dc-4903-ab0a-70a25520ec32 in my case) .We enter these values on ClearGLASS wizard for add Azure ARM cloud

{{content:en_us:cg_azure-03.png}}

Upon pressing ADD CLOUD you will have the Azure ARM account added on your ClearGLASS accounts and will be able to 

  * list existing Virtual Machines
  * see status, public and private ips, metadata as location, resource group, disk image and pricing information. 
  * run actions (reboot, shutdown, start, destroy), 
  * associate an ssh key and open the web shell, 
  * run scripts through ssh and see the output, or schedule tasks
  * enable ClearGLASS monitoring and see diagrams/set rules to get alerted
  * add/edit ClearGLASS tags 
Deployment of new VMs is going to be added soon. 

==== Add Azure ARM cloud through manual generation of keys ====
For manual generation of keys, you will need to create a client id and specify it to ClearGLASS, along with a few other IDs. You will have to use both the old (manage.windowsazure.com) and the new (portal.azure.com) Azure portals because the functionality to get these credentials is under development on the new portal. 

Assuming you follow the steps below this process should not take more than 5 minutes.

=== Step 1 ===
With your Azure Administrator account login to the old portal on  https://manage.windowsazure.com. 

On the left side bar select **ACTIVE DIRECTORY**

{{content:en_us:cg_azure-04.png}}

 then select **Default Directory**

{{content:en_us:cg_azure-05.png}}

then select the **APPLICATIONS** tab and select **ADD** to add a new one. On the popup dialog select **Add an application my organization is developing**, give it a unique name, select **WEB APPLICATION AND/OR WEB API** and on the last dialog of the popup specify https://ClearGLASS as the SIGN-ON URL and APP ID URI. These values won't be used, they just need to be unique if you're having multiple Azure clients. 

In our case the client name is **ClearGLASS add**.

Save the app, this takes a few seconds and you get informed that the App has been added. 

[image]

Click on the 'VIEW ENDPOINTS' link on the bottom of this page and you will see all endpoints have a unique ID after the URL part. Make sure you copy this, it is the Tenant ID field you specify on the ClearGLASS add cloud wizard and in our case this is '35a32b70-7f55-4520-9c67-63e9b5db8166'

{{content:en_us:cg_azure-07.png}}

Close this dialog and select the Configure tab on the Application page we are in. The default settings are just fine for us, and we need to copy the CLIENT ID which will be the Client key field you specify on the ClearGLASS add cloud wizard., in our case this is '8e86ab2c-2aff-4f54-8278-75875bdee6c2'

{{content:en_us:cg_azure-08.png}}

Scroll on the bottom of the page, on the keys section. We will create a new key for this client id. Set the duration as 2 years and press ' SAVE' on the bottom of the page. After a few seconds the secret appears for this key and you have to save it. This is the Client secret field you specify on the ClearGLASS add cloud wizard. This is h6D0hVplz1rZnfNId9zPKkLsGGVDZ8c+00/JviXvYM8= in our case. 

{{content:en_us:cg_azure-09.png}}

=== Step 2 ===
We now have to login to the new portal on  https://portal.azure.com and authorize the client id to be used on our account. Of course we need to login with the same account as we did on the old portal. 

Login and select '**SUBSCRIPTIONS**' on the left side bar. Copy the SUBSCRIPTION ID that you are going to add to ClearGLASS, in case you have more than one. This will be **Subscription ID** field you specify on the ClearGLASS add cloud wizard. In our case this is 3101d491-f614-47b6-b1c6-f50a0ec1f277.

{{content:en_us:cg_azure-10.png}}

Then click on the Subscription, next click on **Access Control (IAM)** on the Overview section.

On the new tab that opens click **Add**, select **Owner** as role and select the account by specifying the client name (this has been **ClearGLASS add** for our case).You just need to start typing the name on the Select field and it will appear, so you may click on it.

[image]

Make sure you click on '**Select**' on the bottom of the page, then **OK**. This takes a few seconds before it informs you that the user has been added!

[image]

You can now see the client name on the users, for this subscription of yours. With all the credentials in place, enter the ClearGLASS add cloud wizard and provide them

{{content:en_us:cg_azure-03.png}}