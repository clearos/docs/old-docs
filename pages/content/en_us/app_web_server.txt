{{ :userguides:web_server.svg?80}}
<WRAP clear></WRAP>

===== Web Server =====
The web server app provides an instance of the Apache server along with basic tasks for creating new website assets.  The Apache webserver is the worlds most popular web server, no doubt as a result of its track record for security, features, scalability and stability.

Many of today's services are web-based - with the Apache web server as the foundation.  Any application such as CMS, CRM, web-based mail client, web-services etc. using a LAMP (Linux, Apache, MySQL and PHP) stack or alternative technologies use the Apache web server.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/server/web_server|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_web_server|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_web_server|here]].
==== Additional Notes ====
