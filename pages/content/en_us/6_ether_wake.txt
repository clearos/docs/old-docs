===== Ether Wake =====
Ether Wake is a tool to send a Wake-On-LAN "Magic Packet" to configured network devices, interactively or from a set schedule. This app was never fully released in ClearOS. The source code can be [[https://github.com/dsokoloski/app-ether-wake|found here]].

===== Installation =====
This app is not yet available in the ClearOS  [[:content:en_us:6_marketplace|Marketplace]].

===== Menu =====
If installed, this app should be located in the menu system at the following location:
 
<navigation>Network|Infrastructure|Ether Wake</navigation>

===== Configuration =====
===== Additional Reading =====
{{keywords>clearos, clearos content, Ether Wake, app_ether_wake, clearos6, userguide, categorynetwork, subcategorysettings, maintainer_dloper}}
