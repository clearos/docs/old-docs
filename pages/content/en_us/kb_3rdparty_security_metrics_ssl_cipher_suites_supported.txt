===== SSL Ciphers Supported =====
This entry from Security Metrics indicates that they are able to see which SSL Ciphers are supported.

===== ClearCenter response =====
==== Short response ====
SSL protocol negotiation is part of the protocol. The ability to negotiate this does not indicate a specific vulnerability.

==== Long response ====
Part of the protocol for SSL requires negotiation of the SSL encryption to be used between the hosts. While this displays what is possible, it does not give an attacker any particular advantage.

==== Resolution ====
No action required.

===== Links =====
{{keywords>clearos, clearos content, 3rd party, security metrics, non-cve, maintainer_dloper}}
