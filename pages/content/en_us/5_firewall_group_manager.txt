===== Firewall Group Manager =====
The Firewall Group Manager makes it easy to categorize and manage related firewall rules.  All rules not assigned to a group will be listed at the top of the page.  You can change the rules Nickname or assign it to a new or existing group by clicking on Edit.

===== Installation =====
This module is part of the base firewall package which is always installed.

===== Menu =====
You can find this feature in the menu system at the following location:

<navigation>Network|Firewall|Groups</navigation>

===== Configuration =====
There are three sections to the Group Manager page.

  * Individual rule listing (rules that are not assigned to a group)
  * Group listing
  * Group manager, useful for enabling/disabling or deleting an entire group

==== Assigning Rules to Groups ====
To assign a rule to a group, click on the rule's <button>Edit<button> button.  This will bring up the rule editor dialog.

The top of the edit dialog shows the fields of the firewall rule; the protocol, address, port, and parameter (optional, contains extended information).  This is displayed to help you identify the rule.  Below this information, you can enter a new or edit an existing Nickname to help identify the rule's purpose.  To the right you may assign this rule to an existing group using the drop-down, or add it to a new group by entering the desired name in the input box below.  Click on confirm to save your changes.

==== Removing a Rule From a Group ====
To remove a rule from a group, click on the rule's Edit button.  You will see the group name in the drop-down box.  Change this to "Remove from group" and then click on Confirm.  If there are no more rules in any given group, the group will no longer show up in the group drop-down list.

==== Group Management ====
At the very bottom of the Group Manager page you can enable/disable or delete a group.  Simply click on the appropriate button.

{{keywords>clearos, clearos content, Firewall Group Manager, clearos5, categorynetwork, subcategoryfirewall, userguide, maintainer_dloper}}
