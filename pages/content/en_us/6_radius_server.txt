===== RADIUS server =====
The **RADIUS Server** app provides a hook into your ClearOS accounts system for RADIUS clients.  This app can be used to allow external devices to authenticate against ClearOS:

  * Wireless access points
  * NAS devices
  * 3rd party appliances
  * Any other RADIUS-ready system

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Network|Infrastructure|RADIUS Server</navigation>

===== Configuration =====
After installing the RADIUS server, you can add client access settings.  When we talk of the client we are talking about a remote system that uses the RADIUS server.  For instance, if you are configuring RADIUS for wireless access, the wireless access point is the RADIUS client, //not// your workstation that uses the wireless.

==== Clients ====
Every device on your system that uses RADIUS should have client settings configured in ClearOS.  The following describes each parameter in detail.

=== IP Address ===
This is the host IP address or hostname of the remote system, for example your wireless access point.

=== Nickname ===
The nickname is just a simple word to describe the client configuration, for example **wireless_ap**.

=== Password ===
This is the shared secret between the server and the client.  For security reasons, you should //not// use the same password as you do for other administrative accounts (mysql, root, etc).
{{keywords>clearos, clearos content, AppName, app_name, clearos6, userguide, categorynetwork, subcategoryinfrastructure, maintainer_dloper, maintainerreview_x}}
