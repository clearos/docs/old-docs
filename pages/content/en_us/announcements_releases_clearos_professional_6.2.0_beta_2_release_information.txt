===== ClearOS Profesional 6.2.0 Beta 2 Release Information =====
**Released: December 22, 2011**

If you do not want to read these release notes (you should!), please at least read the following two bullet items:

  * **//Please do not run this in a production environment//**
  * **//Please do not upgrade from previous versions//**

ClearOS Enterprise 6.2.0 Beta 2 has arrived!  Hey, what happened to the version number - wasn't it version 6.1.0?  Well, the latest ClearOS release is built on source code from a prominent North American Linux vendor.  This upstream vendor released their 6.2 update earlier this month and we quickly followed suit.  For the curious, you can find more information on ClearOS version numbering [[:content:en_us:announcements_releases_version_numbering|here]].

Along with the release of more than a dozen new apps (see below), this release continues the process of creating a stable base system: installer, RPM packages, users, groups, system tools, LDAP, network, firewall, framework, and Marketplace.

<note>If you have not already done so, please review the [[:content:en_us:announcements_releases_test_releases|introduction to test releases]].</note>

===== What's Coming in ClearOS Enterprise 6.2.0 =====
So what can you expect in the final release of ClearOS Enterprise 6?  Along with updating the base system built on source code from Red Hat Enterprise Linux ((Trademark is the property of the respective owner.  ClearFoundation is not affiliated with Red Hat.)) 6.2, this release includes the following major changes.  Please note: these features will be rolled out during the alpha/beta phase.

==== Base System ====
  * Marketplace
  * Base system built from Red Hat Enterprise Linux ((Trademark is the property of the respective owner.  ClearFoundation is not affiliated with Red Hat.)) 6.2 source code
  * 64-bit support
  * Graphical installer
  * Intelligent RAID configuration
  * Wireless support (ClearBOX)
  * Windows BDC support
  * App developer framework

==== New Apps ====
  * Google Apps synchronization
  * Active Directory connector
  * Master/Slave support
  * Kaspersky Gateway Antivirus
  * Kaspersky Mail Antivirus
  * Kaspersky File Scan
  * RADIUS
  * Zarafa Standard and Community (coming soon)

===== Changes Since Beta 1 =====
Along with many bug fixes and enhancements, the following new apps have been released with beta 2.

==== Gateway ====
  * Antimalware Updates (ClearCenter App)
  * Intrusion Protection Updates (ClearCenter App)
  * Protocol Filter

==== Network ====
  * 1-to-1 NAT Firewall
  * Bandwidth Manager
  * DMZ Firewall
  * Egress Firewall
  * Multi-WAN
  * Network Visualizer 
  * NTP Server
  * SSH Server
  * Shell Extension

==== Server ====
  * Active Directory Connector (ClearCenter App)
  * Directory Server
  * MySQL Server

==== System ====
  * Security Audit (ClearCenter App)
  * System Monitor (ClearCenter App)

===== Known Issues =====
  * Marketplace will show "DNS lookup failed" messages from time to time
  * Buttons and widgets in the web interface need some tweaking
  * Special validation handling has not been complete, for example:
    * password checks when creating user
    * duplicate check when adding a group
  * The hostname must be resolvable (add it to DNS server if required) or some services will not start (notably, FTP)
  * Some translations are missing
  * The last step in installer may take a couple of minutes to complete
  * Network manager needs to be able to handle installer defaults

===== Feedback =====
Please post your feedback in the [[http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,39/func,showcat/|ClearOS Development and Test Release Forum]]. 

===== Download =====
No longer available.

===== Screenshots =====
==== Graphical Installer ====
{{:release_info:clearos_enterprise_6.1.0:alpha2_installer.png?600|ClearOS Enterprise Alpha 2 -Installer}}

==== Marketplace ====
{{:release_info:clearos_enterprise_6.1.0:marketplace-ss.png?600|ClearOS Marketplace by ClearCenter}}

==== Older Releases ====
  * [[:content:en_us:announcements_releases_clearos_professional_6.0.0_developer_release_information|Developer Release Information]]
  * [[:content:en_us:announcements_releases_clearos_professional_6.0.0_developer_release_2_information|Developer Release 2 Information]]
  * [[:content:en_us:announcements_releases_clearos_professional_6.1.0_alpha_1_release_information|Alpha 1 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_professional_6.1.0_alpha_2_release_information|Alpha 2 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_professional_6.1.0_beta_1_release_information|Beta 1 Release Information]]
{{keywords>clearos, clearos content, announcements, releases, clearos6.2, beta2, previoustesting, maintainer_dloper}}
