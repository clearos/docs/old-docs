====== System Registration ======
Registering your ClearOS installation to the ClearCenter Service Delivery Network (SDN) is required to access any features that communicate with the SDN, such as:

  * [[content:en_us:7_ug_software_updates|Software Updates]]
  * [[content:en_us:7_ug_marketplace|Marketplace]]
  * [[content:en_us:7_ug_dynamic_dns|Dynamic DNS]]

=====Community vs. Business Edition=====
The Community Edition and the Business Edition differ in that registration using a key/subscription is required in the Business Edition.  Keys can be obtained in one of four ways:

  * Purchasing a [[http://www.clearcenter.com/Hardware/clearbox-hardware.html|ClearBOX]] (no additional fees - your subscription is included)
  * Purchasing a [[https://secure.clearcenter.com/portal/pro.jsp|ClearOS Business subscription]]
  * Purchasing a [[http://www.clearcenter.com/Solution/solutions.html|ClearOS Business Software Bundle]]
  * Requesting a free [[https://secure.clearcenter.com/portal/evaluation.jsp|30-day evaluation subscription]]

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>System|Operating System|System Registration</navigation>

In addition, system registration is part of the post-installation wizard, which, while not mandatory, is recommended.  Clicking on any page related to the ClearCenter Marketplace will also automatically redirect you to the registration page.

===== ClearCenter Account =====
Any number of ClearOS systems can be registered back to a single account, regardless of whether you are registering a ClearOS Community or Business install.

==== ClearCenter vs. ClearFoundation ====
Some users may have already have created a ClearFoundation account on the [[http://clearfoundation.com|ClearFoundation website]].  You *cannot* use this account to register your ClearOS server.  Why?  ClearFoundation's website is designed to support forums and community social-building.  ClearCenter's portal is responsible for handling sensitive data like billing information, invoicing, etc.   Keeping these two datasets completely isolated (in both software and hardware) was done to ensure best-practises are implemented in keeping users data private and secure.

<note>
If you have forgotten your username for either of these accounts, you can call ClearCenter Support at +1 (801) 851-5555 or +1 (800) 977-0574 between 15:00 and 23:00 UTC (10:00-18:00 EST).  Dial option 2 and be sure to have your email address ready.
</note>

===== Creating a new ClearCenter Account =====
New to ClearOS?  You can create a new account [[https://secure.clearcenter.com/portal/new_account.jsp|here]] or during ClearOS registration.  Creating a ClearCenter Portal account is quick, secure, and free.

{{content:en_us:7_install_wizard_create_account_button_highlighted.png|Create Account Button}}

{{content:en_us:7_install_wizard_create_account.png|Create Account Screen}} 

==== Account Username ====

Select a unique username - please note, this is not the same as logging into webconfig.  Your username will be used to authenticate you to ClearCenter's SDN for actions requiring access to your account.

<note warning>A ClearCenter account is not related to an account you may have created on the [[http://www.clearfoundation.com|ClearFoundation Community Portal]].  If you are a member of ClearFoundation's Community, you will still need to create a ClearCenter account.</note>
==== Password / Confirm Password ====
If you are using ClearOS Business or intend on purchasing paid apps via ClearCenter's Marketplace, there will be confidential information such as contact and invoice details.  We recommend the use of a strong password.

===== Registration Options =====
Below is a screenshot of the registration form for ClearOS Business and ClearOS Community.

{{content:en_us:7_install_wizard_business_02.png|System Registration on ClearOS Business}}

<note>
The registration form for ClearOS Home is an exact copy of the registration form for ClearOS Business; however, it has a gray theme.
</note>

{{content:en_us:7_install_wizard_community_02.png|System Registration on ClearOS Community}}

If you already have a ClearCenter account, complete the form and click on //Register System//.  If you are new to ClearOS, you will need a ClearCenter account.  Click on //Create Account//.

==== Account (ClearCenter) ====
This is your ClearCenter account, used to secure login to the [[https://secure.clearcenter.com/portal|ClearCenter portal]].
====Password====
Your ClearCenter account password.
<note important>The authentication information above should not be confused with your ClearOS Webconfig login, which is completely separate.</note>
====Type====
This is the type of registration your are performing.  Registrations fall into two categories:
    * New installations
    * Re-installs or Upgrades
A new installation is when you intend to register a ClearOS system for the first time.  Essentially, you are starting from a clean slate.

A re-install should be selected when you have a completely new installation, but it is intended to replace an existing system that was previously registered.  You will be presented with a list of valid systems in your ClearCenter account.  Select the one that you are replacing.

Common cases of re-installation are:
    * Moved to new hardware
    * Recovering from a disaster event - theft, damage, hardware failure etc.
    * Wanted to 'start again' but use same services originally configured

The screenshot below illustrates the dropdown menu that will automatically populate if you have entered the correct authentication credentials to your ClearCenter account *and* select "Reinstall/Upgrade" from the registration type.
{{:omedia:ss-registration-reinstall.png|Eligible Systems}}

====Eligible Systems====
The eligible systems dropdown is a list of all ClearOS systems registered to your account that could be used to re-register an existing system by way of a re-install or upgrade.

<note>If you are upgrading from ClearOS Commmunity to Business, you will also need to select a valid Business key.
</note>
====System Name====
This is a nickname used so you can easily identify the unit in your account.  For example, and organization with offices in Toronto, Vancouver and Halifax might use the city names as the system name to refer to each gateway/server.

The system name field will be disabled (read only) when the type of registration selected is //Reinstall/Upgrade//.  This is because the name will automatically be inherited from the previous registration.

<note>If you wish to change a system nickname after you have installed your system, you must do it from the ClearCenter portal.  Go [[https://secure.clearcenter.com/portal/device_rename.jsp|here]].</note>

===== Data Reporting ====
Your ClearOS server occasionally connects to the ClearCenter ClearSDN network.  The webservice payload is minimal (< 1Kbyte) and contains information that is used to help ClearCenter maintain services & infrastructure and plan for future upgrades.

==== Mandatory Data ====
The following data is always sent up to the ClearSDN web-services:
  - Device ID
  - Language
  - System Mode (Standalone, Master or Slave)
  - Locale setting (en_US, fr_FR etc.)
  - Uptime
==== Opt-Out Data ====
The following data is included by default; however, the user can opt-out of sending this information provided no apps requiring this data for licensing and audits are installed.
  - Number of users
  - Number of unique IP addresses
  - Number of unique MAC addresses

The apps where this information is required include:
  * Active Domain Connector
  * Google Apps Synchronization
  * Apps powered by Kaspersky Antimalware

If you would prefer **not** to send up one or more of the opt-out data sets, please run the following command, replacing PARAM for the field in the accompanying table:

  echo "exclude_PARAM = 1" >> /etc/clearos/registration.conf

|< 70% 20% 80% >|
^PARAM ^ Description ^
|user | Number of defined users in directory|
|ip | Number of unique IP addresses (passing traffic across an interface)|
|mac | Number of unique MAC addresses (passing traffic across an interface|

=== Example ===
To prevent your system from reporting the number of users in your directory, you would run:

  echo "exclude_user = 1" >> /etc/clearos/registration.conf

If you would like to re-enable the sending of this data, edit the registration.conf file and set the value to zero (0) or remove the line entirely and save the file.

{{keywords>clearos, clearos content, System Registration, app-registration, userguide, categorysystem, subcategorysettings, maintainer_bchambers}}
