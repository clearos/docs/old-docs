{{ :userguides:google_apps.svg?80}}
<WRAP clear></WRAP>

===== Google Apps Synchronization =====
The Google Apps Synchronization app provides a connector to Google's "in the cloud" API.  This app simplifies an administrator's job by provisioning a Google App account while creating an on-premise user account in ClearOS.

In addition, secure and transparent password synchronization enables the use of a single username/password pair to authenticate both Cloud-Based Google applications (including apps in the Google Apps Marketplace supporting single-sign on) and ClearOS local resources.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/cloud/google_apps|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_google_apps_synchronization|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_google_apps|here]].
==== Additional Notes ====
