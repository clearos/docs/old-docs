===== PPTP Server =====
The PPTP server is a secure and cost effective way to provide road warrior VPN connectivity.  The PPTP VPN client is built-in to Windows 2000, XP, Vista and 7.  No extra software is required and ClearOS provides full password and data encryption.

===== Configuration =====
==== Configuring the PPTP Server ====
=== Local IP and Remote IP ===
You must select a range of LAN IP addresses for the PPTP VPN connections. This range should be on the same network as your local area network.  By default, the [[:content:en_us:5_dhcp_server|DHCP Server]] on ClearOS only uses IP addresses above x.x.x.100.  All addresses below this number are reserved for static use.  We strongly suggest you use this sub-100 static range for PPTP.

=== Encryption Key Size ===
Most PPTP VPN clients support the stronger 128-bit encryption key.  However, some VPN clients (notably handheld computers and mobile phones) may only support 40-bit encryption.  Change the encryption key size to meet your needs.

=== Domain ===
This setting is no longer used.

=== WINS Server ===
The Microsoft Networking WINS server used by the PPTP client.  Depending on your network configuration, you may need to specify the WINS settings in VPN client configuration.

=== DNS Server ===
The DNS server used by the PPTP client.

=== Usernames and Passwords ===
PPTP users must have a valid account with the PPTP option enabled.  See the [[:content:en_us:5_users|User Manager]] for more information.

==== Configuring Microsoft Windows ====
=== Configuring Windows XP ===
The PPTP client is built-in to Windows XP.

  * Go to the **Control Panel**.
  * Click on **Network Internet Connections** (this step may not be necessary).
  * Click on **Network Connections**.
  * Click on **Create a New Connection** to start the configuration wizard 
  * Select **connect to the network at my workplace**.
  * Select **Virtual Private Network connection**.
  * Add a connection name, and dial settings, and hostname.
  * Click on the **Properties** button (or right-click on the new connection, and select **Properties** from the menu.
  * Select the **Security**
  * Make sure **Require data encryption** is selected. 
  * Select the **Networking** tab.
  * From the **Type of VPN** drop box, select **PPTP VPN**. 

===== PPTP Passthrough =====
PPTP requires special software when passing through firewalls.  This feature is included with ClearOS.  However, there is one important restriction for PPTP pass-through mode: **a PPTP server must not be running on the same gateway that has PPTP connections crossing it.**

If you run a PPTP server on a ClearOS gateway, you will not be able to have people from behind the same gateway make reliable outbound PPTP connections to other servers.  By default, the firewall will automatically disable PPTP pass-through when the firewall already allows connections to a PPTP server.  You will see warning messages in the web-based configuration about these configuration issues.

Depending on the circumstances, you may be able to have both PPTP pass-through and a PPTP server running at the same time.  To do this, you need to override the firewall behavior noted in the previous paragraph.  In the /etc/firewall file, add the following line:

**PPTP_PASSTHROUGH_FORCE="yes"**

Then restart the firewall with the following command:

**/sbin/service firewall restart**

===== Troubleshooting =====
==== Error 619, PPTP and Firewalls ====
PPTP requires special software when passing through gateways/firewalls. If you are having trouble connecting to a PPTP server, make sure any gateways/firewalls between your desktop and the ClearOS server support PPTP passthrough mode. If you see the following in the /var/log/messages log file on the ClearOS system, then it is likely a PPTP passthrough issue on the client side of the connection:

PTY read or GRE write failed

You can view log files via the web-based administration tool -- go to <navigation>Reports|Logs</navigation> in the menu.

Another quick way to diagnose the issue is by connecting to the PPTP server while connected directly to the local network. With a direct connection to the ClearOS PPTP server, you can eliminate the potential for the PPTP passthrough issue. 

==== PPTP Passthrough ====
If you are connecting a desktop from behind a ClearOS gateway to a remote PPTP server, then you need to have PPTP passthrough software installed and enabled on the firewall.  

However, we do not recommend running PPTP Passthrough and a PPTP server simultaneously.  By default, the ClearOS gateway will automatically disable PPTP Passthrough when the [[:content:en_us:5_firewall_incoming|firewall]] is configured to allow PPTP server connections.  If you would like to run PPTP Passthrough and a PPTP server simultaneously, see above. 

==== Two PPTP Connections to the Same Server ====
The PPTP protocol does not allow two VPN connections from the same remote IP address.  In other words, if you have two people behind a gateway (for example, ClearOS) connecting to the same PPTP server, then the connection should fail.  Note: it is fine to have two people behind a gateway connecting to **different** PPTP servers.

Some PPTP servers and gateways (including ClearOS) do make an exception for this shortcoming.  However, some PPTP servers may strictly follow the standard below:

<note info>The PPTP RFC specifies in section 3.1.3 that there may only be one control channel connection between two systems. This should mean that you can only masquerade one PPTP session at a time with a given remote server, but in practice the MS implementation of PPTP does not enforce this, at least not as of NT 4.0 Service Pack 4. If the PPTP server you're trying to connect to only permits one connection at a time, it's following the protocol rules properly. Note that this does not affect a masqueraded server, only multiple masqueraded clients attempting to contact the same remote server.</note>

===== Links =====
  * [[http://www.poptop.org/|PoPToP PPTP Server]]

{{keywords>clearos, clearos content, PPTP Server, app-pptpd, clearos5, userguide, categorynetwork, subcategoryvpn, maintainer_dloper}}
