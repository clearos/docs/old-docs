===== Encrypted File System =====
The encrypted volume module allows the creation of encrypted volumes that can be used to protect confidential data from unauthorized access in the event the server is physically removed from the premise or a portable mass storage device is lost/stolen while in transit.

Data is stored in an encrypted format when a volume has not been mounted. Mounting a volume requires the password. With a strong password, gaining access to the decrypted data (for example, usable information) is impossible in the event the volume is unmounted. A volume is unmounted whenever a server is restarted (for example, a shutdown, loss of power etc.) and must be mounted by an administrator having both webconfig access and the volume password. 

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:


<navigation>System|Security|Encrypted File System</navigation>

===== Configuration =====
==== Adding an Encrypted Volume ====
Any number of encrypted volumes can be created on the server - either on the local hard disk or an external mass storage devices.

<note warning>Volumes created on the local disk reside in parallel with other system/user data.  By contrast, volumes created on unmounted devices (for example, a USB attached hard disk) fill the entire physical disk size and formats all data.</note>

{{:omedia:ss_dmcrypt_add.png|}}

=== Volume Name ===
A unique name that describes the volume (for example, ArchivedMail, ExternalUSB etc.)

=== Mount Point ===
The location the volume will be accessible. By default, the mount point is created in /mnt/dmcrypt/<VolumeName>.

=== Storage Device ====
The physical device location.

=== Size === 
The size (in MB) of the encrypted volume. Keep in mind, encrypted volumes have an encryption overhead approximately equal to 1-5% of the total defined size of the volume.

=== Password and Verify Password ===
The password required to mount the encrypted volume.
===== Troubleshooting =====
==== FAQ ====
=== What if I forget my password? ===
If you forget a volume encryption password, there is absolutely no way to recover the data!

=== How can I auto-mount my encrypted volumes on bootup? ===
This would defeat the purpose of creating an encrypted volume.
{{keywords>clearos, clearos content, AppName, encrypted_file_system, clearos5, userguide, categorysystem, subcategorysecurity, maintainer_dloper}}
