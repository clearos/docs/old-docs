===== Gateway Antiphishing =====
Phishing is the malicious practice of misleading users to give away sensitive information through carefully crafted duplicates. Sites that look like legitimate sites can actually be careful imitations. The ClearOS Anti-phishing engine can help prevent users from accidentally being tricked into using phishing sites.

===== Installation =====
The Anti-phishing engine is available in the ClearCenter [[:content:en_us:7_ug_Marketplace|Marketplace]] and can be installed for free.

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Gateway|Antimalware|Gateway Antiphishing</navigation>

===== Configuration =====
You can enable or disable any of the following phishing aspects. We recommend running all of them.

  * Signature Engine	
  * Heuristic Engine	
  * Block SSL Mismatch	
  * Block Cloaked URLs	

{{:content:en_us:7_gateway_antiphishing.jpg?550|}}
{{keywords>clearos, clearos content, Gateway Antiphishing, app-antiphishing, clearos7, userguide, categorygateway, subcategoryantimalware, maintainer_dloper}}
