===== Zarafa Community for ClearOS =====
Zarafa Community for ClearOS provides a powerful mail and groupware solution with:

  * Mail / Webmail
  * Contacts
  * Calendars
  * Tasks

If you are looking to migrate from Exchange or require full backup tools, then [[:content:en_us:6_zarafa_professional_for_clearos|Zarafa Professional for ClearOS]] is what you need.

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Server|Messaging and Collaboration|Zarafa Community for ClearOS</navigation>

===== Migration from Exchange, Outlook, IMAP =====
If you are migrating from an existing mail / groupware system, please read the following documentation: [[:content:en_us:kb_o_migrating_mail_to_zarafa_community_and_zarafa_professional|Migrating To Zarafa On ClearOS]]

===== Configuration =====

The base mail domain is used by a number of apps: SMTP Server, IMAP, Mail Filter, etc.  Each app saves this information in its own configuration file, but we really only want to see the domain in one place in the user interface.  That's where the **Mail Settings** app comes in.  This might not be the way we'll do it in the final release, but that's how it is done right now:

  * Go to <navigation>System|Account Manager|Account Manager</navigation> to make sure the accounts system (users/groups) is running.

  * Go to <navigation>System|Settings|Mail Settings</navigation> to set the mail domain.

  * Go to <navigation>System|Accounts|Users</navigation> to create a few users.  Please make sure the **SMTP Server User** and **Zarafa Account** are enabled for the user.  Feel free to fill out the contact information too - this information is shown in Zarafa's address book! 

  * Go to <navigation>System|Accounts|Groups</navigation> to create a group or two.  You will see a new option when adding/editing a group: **Distribution List**.  If you leave this option enabled, the group becomes a mail distribution list (e.g. sales@example.com will go to all the members of the sales group).
   
  * Go to <navigation>Server|Messaging and Collaboration|Zarafa Community for ClearOS</navigation> and start all the servers (if not running).

That's it.  You should be able login to the Zarafa web client with your user account credentials.

===== Webmail Access =====
You can access the webmail interface by visiting 
  * https://w.x.y.z/webapp (new interface)
  * https://w.x.y.z/webaccess (legacy interface)

Check your firewall settings. If you firewall is activated, you need to add HTTPS to the list of "Allowed Incoming Connections".

===== Links =====
  * [[:content:en_us:kb_bestpractices_synchronizing_zarafa_mail_contacts_and_calendars_-_mobile_devices|Synchronizing Zarafa Mail Contacts and Calendars - Mobile Devices]]
  * [[:content:en_us:6_zarafa_professional_for_clearos|Zarafa Professional for ClearOS]]
  * [[:content:en_us:6_zarafa_small_business_for_clearos|Zarafa Small Business for ClearOS]]
{{keywords>clearos, clearos content, AppName, app_name, clearos6, userguide, categoryserver,subcategorymessagingandcollaboration, maintainer_dloper, maintainerreview_x}}
