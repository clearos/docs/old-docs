===== Mail Archive =====
The **Mail Archive** system logs all incoming and outgoing e-mail passing through the gateway and stores exact copies for archival.  Metadata extracted from each message is stored in a database for search and retrieval.  This module can be used to meet regulatory compliance or assist an organization to enforce internal policies for e-mail use in the workplace.

===== Compatibility =====
This app is compatible with Zarafa and Kopano mail systems.

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:7_ug_marketplace|install the app]] from the Marketplace.

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Server|Messaging|Mail Archive</navigation>

===== Main Page =====

{{:content:en_us:7_mail_archive.jpg?1000|Mail Archive Summary}}

===== Configuration =====
==== Settings ====
{{:content:en_us:7_mail_archive_settings.jpg?1000|Mail Archive Settings}}
=== Status ===

Enable or disable mail archiving.

=== Encrypt Archives ===

Compressed archives (done on a monthly basis on the first of the month) can be encrypted to prevent unauthorized access. This can be extremely important (and may be required by law) if e-mails contain confidential information and are removed from the server or stored remotely.

=== AES Encryption Password ===

The password used to encrypt the archive file if "Encrypt Archives" is set to "Enabled". By default, this password must be at least 8 characters and should contain both upper and lower case letters and at least 1 number. 

<note important>You can override the minimum password length by editing the /etc/clearos/mail_archive.conf file by setting the 'encrypt-password-length' parameter.</note>

==== Statistics and Searching ====

{{:content:en_us:7_mail_archive_search.jpg?1000|Mail Archive Search}}

Instead of storing email inside a database, only header information and keywords (tagging) extracted from the message body are stored.  This allows the database to be smaller and faster with respect to searching.

The mail message itself is stored as an exact copy on the file system with one exception.  Customer header fields starting with "X-Clear-Delivered-To" may be added to the original headers.  These fields provide information on who the message was delivered to in cases where the user was on the BCC list or for mailing list addresses (eg. sales@example.com, support@example.com).

The simple stats displayed on the main page gives an indication as to the number of messages that have passed through the server.  Click on the //Search// button to display the search form that allows you to find messages in the event of an audit requiring specific messages.

Up to 5 search queries can be added to fine-tune your search for messages.  Click on the //Add Filter// link to display additional search query fields or //Remove Filter// if one becomes unnecessary.

Once one or more messages have been found, you can select them for redelivery from the list table.

==== Redelivery of Messages ====
To redelivery messages to the original recipient list (both To: and Cc: fields), select enabled from the drop down next to //Redeliver to Original Recipient List//.

If you wish to redeliver to another email address, leave the field disabled, and add a valid email address to redeliver to.

==== Admin (root) account vs. Users Account ====

The mail archives can be searched by the system administrator (logged in under the 'root' account) or by users. To give users access to the archive, use the System Administration ACL to grant access to specific users to the Mail Archive module.

When logged in as 'root', all emails will be returned from a search query. However, when logged in as a 'user' system administrator, only email that has been sent to or by the user will be returned from a search query. In other words, users can view/restore mail that was sent or received by them, but no one else.

===== Managing Archives =====
Mail archived by the mail archive app are stored in compressed archive files (having a .tgz or .enc extension for unencrypted and encrypted archives, respectively).  Archives are stored using the format MMM-YYYY-X (where -X is an optional iteration sequence ID) since they are generated on the 1st of each month automatically for all mail received the prior month and are located in the folder:
  /var/clearos/mail_archive/

If you allow the system to automatically create archives, you will not have multiple copies per month and no iteration ID will be added.  If, however, you manually create archives from the command line, you will create multiple archives in a given month.

In cases where these archives become too large and/or numerous to retain on the server, they can be moved off the server.

<note warning>Redelivery of any message that is contained in an archive file will not be possible until the archive has been restored to the server in the original format.</note>

==== Accessing the Archive Files ====

=== Secure Shell ===
For more advanced users, login to the server as //root// and navigate to /var/clearos/mail_archive.  From there, you can use standard tools to copy, remove, link etc.

=== WinSCP ===
If you are a Windows user, download and install a copy of [[http://winscp.net/eng/download.php|WinSCP]].  Login to the server as //root// using WinSCP client, and copy/remove the archives from the /var/clearos/mail_archive folder to your desktop.

=== Flexshare ===
Create a Flexshare name //mail_archive//.  Edit /etc/fstab and add:

  /var/clearos/mail_archive    /var/flexshare/shares/mail_archive      none    bind,rw    0 0

Then execute the command:

  mount -a
  
You can now access the mail archives via FTP, HTTP, Fileshare (Samba).  See [[:content:en_us:7_ug_flexshare|Flexshare Documentation]] for more information.

===== Command Line (Advanced users) =====

The command line script for Mail Archive is
  /usr/sbin/mail-archive
  
A mandatory -a (action) flag is required.  Permitted value are:
  - redeliver
  - archive
  - import

==== Redeliver ====
Redelivers a message.  Add the "-r" flag to redeliver message to the original recipient list.  If you want to override and resend to another user, use the command line argument "-e=someusers@example.com".

The last mandatory argument will be the message ID.

==== Archive ====
Archive creates an archive file (encrypted if configured) of messages in the queue.  The default creates an archive for all messages created in the prior month.  Use the -s=YY-MM-DD to override the date.

==== Import ====
Import the metadata (headers and tags) from mail in the source directory into the database.  This action is performed daily, but if you want to run a manual job, use this command.

Valid arguments are -s (-s=YY-MM-DD) to override the start time and -d (-d=X) where X is the number of days you want to collect metadata back from the start time.

===== Links =====

  * [[:content:en_us:7_ug_flexshare|Using Flexshares]]
  * [[http://winscp.net/eng/index.php|WinSCP]]
{{keywords>clearos, clearos content, Mail Archive, app-mail-archive, clearos7, userguide, categoryserver, subcategorymessaging, maintainer_bchambers}}
