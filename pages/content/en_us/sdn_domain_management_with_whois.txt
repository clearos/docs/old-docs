===== Domain Management =====
All domains registered with or transferred to your account can be updated to ensure the accuracy of information contained in the worldwide domain (whois) database. In addition, you can:

  * Modify the DNS servers associated with your domain
  * Enable the domain locking feature to automatically reject any attempts to hijack your domain
  * Update domain settings specific to the type of domain (for example .ca)
  * Find the domain authorization code (auth code, or EPP) 

===== Activation =====
  * Login to your [[https://secure.clearcenter.com/portal/|ClearCenter account]].
  * Click on <navigation>DNS|Domain Management|DNS/Domain Summary</navigation> in the top navigation bar.
  * Click on <button>View</button> for the domain you want to manage
  * Click on <button>WHOIS</button> next to the WHOIS field

{{:content:en_us:kb_sdn_modify_whois_new.png?550}}
===== Configuration =====
Once you click on the <button>WHOIS</button> for a particular domain, a menu system along the top of the page will be displayed.  If you have registered multiple domains under a single account should pay attention to the domain listed in the Domain Name field. You can select a different domain through the web interface.
	
==== Contact Information ====
By following the appropriate **Contact** links, you can configure the following information for your domain:

^Contact^Description^
|Organizational Contact|Information about the company or entity which owns the domain name|
|Admin Contact|A person or entity who is named as authoritative for significant domain changes|
|Billing Contact|A contact responsible for billing issues|
|Technical Contact|A person or entity considered authoritative for DNS records for the domain|

<note warning>The information provided here is published in the domain system and available to the general public.</note>

==== DNS Servers / Name Servers ====
The link to **Name Servers** (DNS Servers) allows you to change the organization designated to be the DNS servers / name servers for your domain. Unless you are using an alternative service for your DNS service, the entries should match the servers listed in the table below. If you have transferred an existing domain to your account, you should first set up your DNS records. When you have been notified of a successful transfer, you will then be able to change the name server information.

^Hostname^IP Address^
|ns1.clearsdn.com|69.90.141.72|
|ns2.clearsdn.com|64.34.185.192|
|ns3.clearsdn.com|216.127.75.186|
|ns4.clearsdn.com|67.18.3.133|

==== Domain Locking ==== 
When **Domain Locking** is enabled for your domain, any requests to transfer your domain to another registrar or hosting company will automatically fail. This feature prevents fraudulent transfers. To lock/unlock your domain:
  * Login to your [[https://secure.clearcenter.com/portal|account]]
  * Click on the DNS tab and select DNS Summary
  * Click on "View"

The lock status will be displayed on domains registered through your account.

To change the lock status, click on the "WHOIS" link next to the WHOIS field.

A new window or browser tab will spawn, linking you directly to your domain's WHOIS management interface. Click on the "Domain Locking" link and update your domain's lock status as desired.

==== Authorization Code / Auth Code ====
A transfer authorization code is required when transferring any .com/.net, .org, .info, .biz, .us, and .name domain name from one registrar to another. The transfer authorization code is created at the time of registration.

{{keywords>clearos, clearos content, sdn, SDN Domain Management, categorydns, subcategoryservices, subcategorytroubleshooting, maintainer_dloper, userguide}}
