===== Packaging ClearOS 6.x DHCping =====
The dhcping package is used to check for DHCP servers on the network.  This provides a quick sanity check to make sure a user is not starting up DHCP on a network with an existing DHCP server.

===== Links =====
{{keywords>clearos, clearos content, dev, maintainer_dloper, maintainerreview_x, keywordfix}}
