===== System Registration =====
Registering you ClearOS Enterprise system will give you access to the following free services:

  * [[http://clearsdn.clearcenter.com/software/browse.php?pid=10510|Software Updates]]
  * [[http://www.clearcenter.com/Services/clearsdn-dynamic-dns-1.html|Dynamic DNS]]

You can also subscribe to professional [[http://www.clearcenter.com/Services/clearcare.html|ClearCARE technical support]], along with the following optional ClearSDN services:

  * [[http://www.clearcenter.com/Services/clearsdn-content-filter-updates-7.html|Content Filter Updates]]
  * [[http://www.clearcenter.com/Services/clearsdn-intrusion-protection-4.html|Intrusion Protection Updates]]
  * [[http://www.clearcenter.com/Services/clearsdn-remote-security-audit-5.html|Remote Security Audit]]
  * [[http://www.clearcenter.com/Services/clearsdn-remote-server-backup-6.html|Remote Server Backup]]
  * [[http://www.clearcenter.com/Services/clearsdn-antimalware-updates-7.html|Antimalware Updates]]
  * [[http://www.clearcenter.com/Services/clearsdn-antispam-updates-8.html|Antispam Updates]]
  * [[http://www.clearcenter.com/Services/clearsdn-remote-system-monitor-9.html|Remote System Monitor]]
  * [[http://www.clearcenter.com/Services/clearsdn-remote-bandwidth-monitor-6.html|Remote Bandwidth Monitor]]
  * [[http://www.clearcenter.com/Services/clearsdn-dynamic-vpn-6.html|Dynamic VPN]]
  * [[http://www.clearcenter.com/Services/clearsdn-internet-domaindns-services-6.html|Internet Domain and DNS Services]]

<note info>A 15-day free trial is available when your register a new system!  Kick the tires and try the services at no cost.</note>  

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>ClearCenter|Register|Register System</navigation>

===== Video Tutorial =====
{{youtube>large:pPYX9sF5MO8|ClearOS Enterprise System Registration to ClearSDN}}

===== System Activation =====
==== Create an Online Account ====
If you do not yet have a ClearCenter online account, you can create one [[https://secure.clearcenter.com/portal/new_account.jsp|here]].  It is quick, easy to do and free!

==== Complete Registration Wizard ====
With your online account information in hand, you are now ready to register your ClearOS system.

  * Login to your system via the [[:content:en_us:5_webconfig| web-based administration tool]].  
  * Click on <navigation>ClearCenter|Register|Register System</navigation> in the menu.
  * In the first step in the wizard, enter your online account username and password.
  * Continue with the registration wizard

===== Diagnostic Data =====
During the registration process, you are given the option to participate in sending diagnostic data.  In an effort to improve hardware compatibility information along with defining system requirements, the following diagnostic data is collected:

  * PCI hardware information
  * Hard disk and partition sizes
  * Number and type of network interfaces
  * Number of users
  * ClearOS modules installed
{{keywords>clearos, clearos content, System Registration, app-register, clearos5, userguide, categoryinstallation, subcategorypostinstall, maintainer_dloper}}
