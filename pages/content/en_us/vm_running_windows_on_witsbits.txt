===== Windows on ClearVM =====

If you’re thinking about different Windows versions and how to get them up and running on ClearVM, then this is you page!

**Enable Virtualization** – Windows will crash in a blue screen of death if your host’s virtualization extensions are disabled – [[:content:en_us:vm_enable_virtual_technology|make sure virtualization is enabled]] on your host server before you start Windows VMs.

**Windows 2012 (recommended)** is available as a preconfigured template which is very fast to deploy. Simply go to the [[https://witsbits.com/app.php#/store|Store]] and click **Start**, then log in with Remote Desktop or VNC. Alternatively you can [[:content:en_us:vm_windows-2012|install Windows 2012 from scratch]] (which involves much more work).

**Windows 2008 R2** is available in the [[https://witsbits.com/app.php#/store|Store]] as installer ISO images with different flavors and languages. See this detailed [[:content:en_us:vm_windows-2008|guide for installing Windows 2008 R2]] on Witsbits.

**Windows 8** is available in the [[https://witsbits.com/app.php#/store|Store]] as installer ISO images with different flavors and languages. Follow the [[:content:en_us:vm_windows-2012|guide for installing Windows 2012]] on Witsbits (the steps are the same for Windows 8 and Windows 2012 are the same).

**Other Windows Versions** may be added as [[:content:en_us:vm_adding_custom_vm_templates|custom templates]] to your Witsbits account. You have the option to add Installer ISO images from Microsoft or you own pre-installed Windows images. If you add a pre-installed image you can just boot it up and if you add an ISO image you need to connect with VNC to complete the Windows installation process.

Please [[support@witsbits.com|let us know]] if any questions, we’re here to help!

{{keywords>clearvm, kb, howto, maintainer_dloper}}