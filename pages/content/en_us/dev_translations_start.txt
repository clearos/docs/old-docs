===== ClearOS Translations =====
Parlez-vous français?  We do.  Internationalization support for ClearOS is available.  If you are interested in translating ClearOS to your language, please contact [[developer@clearfoundation.com]]. 

===== Quick Start =====
Translations for ClearOS are done through an online translation server which is located here: [[https://unify.inwhatlanguage.com|Unify]].  If you just need to update a handful of translations, you can go ahead and submit the updates on the translation server.  If you would like to see up-to-date translations on your local ClearOS system, please read the following guide:

  * [[:content:en_us:dev_translations_getting_started|Getting Started]]

If you are an app developer looking for instructions on how to import your translations, please read the following:

  * [[:content:en_us:dev_translations_adding_your_app_to_the_translation_server|Adding Your App To The Translation Server]]

===== Meet the Translators =====
^Language^Translator^
|Chinese|Ding Wei|
|Danish|Jan West|
|Dutch|[[https://www.clearos.com/community/my-profile/11300-bjorn-euren|Bjorn Euren]], Guus Ijzereef, Markus Koblitz,[[https://www.clearos.com/community/my-profile/11197-paul-van-voorst|Paul van Voorst]]|
|French|[[https://www.clearos.com/community/my-profile/12809-francis-poulizac|Francis Poulizac]], Michel Roux|
|German|[[https://www.clearos.com/community/my-profile/11666-wolfgang-schwickert|Wolfgang Schwickert]]|
|Greek|[[https://www.clearos.com/community/my-profile/19093-panagiotis|Panagiotis Drivilas]], Vasilis Charamidis|
|Hungarian|Karsai Gábor|
|Italian|Giovanni Bartoli|
|Norwegian|HotRod, Jan Erik Amundsen|
|Polish|[[https://www.clearos.com/community/my-profile/15628-piotr-smalira|Piotr Smalira]]|
|Portuguese|[[https://www.clearos.com/community/my-profile/15410-rafael|Nuno Rafael Gomes]]|
|Russian|Хрячков А.Л, Andrey Poletaev, Konstantin Barsukov|
|Slovak|Norbert Bede|
|Slovenian|Matic Gradišer|
|Spanish|[[https://www.clearos.com/community/my-profile/34186-jose-agudo|Jose Jagudo]], Dan Jaramillo|
|Swedish|Christer Gerhardsson, Joakim Forslund, Peter Svahn|
|Turkish|[[https://www.clearos.com/community/my-profile/33943-gunay-arslan|Gunay Arslan]], Görkem Çetin, Salih Giray|
{{keywords>clearos, clearos content, dev, Translations, maintainer_dloper, maintainerreview_x, keywordfix}}
