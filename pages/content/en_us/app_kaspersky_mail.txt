{{ :userguides:kaspersky_mail.svg?80}}
<WRAP clear></WRAP>

===== Premium Mail Antimalware by Kaspersky =====
Mail Antimalware Premium is powered by Kaspersky Lab to scan and remove malware before ending up in users inboxes where opening attachments or clicking on embedded links can cause significant damage to systems and networks.  Kaspersky Lab is firmly positioned as one of the world's leading IT security software vendors for endpoint users.

This app includes a license for up to 50 users.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/server/kaspersky_mail|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_kaspersky_mail|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_kaspersky_mail|here]].
==== Additional Notes ====
