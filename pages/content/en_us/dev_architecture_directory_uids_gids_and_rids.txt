===== Developer - UIDs, GIDs, and RIDs =====
The ClearOS Directory needs to handle the following IDs in a sane way:

  * User IDs
  * Group IDs
  * Relative IDs (part of the Windows Networking Security Identifier/SID)

To start, let's take a look at Linux user IDs (UIDs) and group IDs (GIDs).  Not too long ago, the maximum recommended UID/GID on a Linux system was 65,535.  Most modern Linux distributions can certainly handle more of course. Though ClearOS 5.x can handle UIDs/GIDs up to 2,147,483,647, the traditional limit of 65,535 is still in use in version 5.x.

Before we can design our ID ranges, it is important to look at the Windows Networking world.  The SID (Security Identifier) in Windows is a unique name used on Windows networks.  Every user, group and computer is assigned a SID using the following format:

  S-1-5-domain-relative ID (RID)
  
Where //domain// is an identifier for the Windows domain, and the //relative ID (RID)// is a unique identifier in the Windows domain.  Here's example:

  S-1-5-21-7623811015-3361044348-030300820-1013

You can also view SIDs by running **pdbedit -L -v** on a live ClearOS system.  Detailed information on SIDs can be found on [[http://en.wikipedia.org/wiki/Security_Identifier|Wikipedia]] and elsewhere.  For our purposes, the following rules must be followed.

  * RIDs below 1000 are reserved for special users/groups (e.g. the RID for Domain Admins is 512)
  * RIDs must be unique

===== Account Types =====
Now that we have laid out the basic rules around UIDs/GIDs/RIDs, we can now move on to account types.  In most Linux distributions, you can find the following account types:

^Account Type^Description^Examples^
|System Users|User accounts used by the underlying operating system|root, apache, mysql|
|System Groups|Group accounts used by the underlying operating system|lp, users|
|Normal Users|User accounts representing real people|dave, mike, ben, corinne|
|Normal Groups|Group accounts representing real groups of people|staff, students|

These account types are just a common convention used in most Linux distributions.  With the exception of the root account (UID/GID of 0), a Linux system does not really distinguish amongst the account types! 

In Windows, there are a number of different account types.  For this discussion, all we need to know is that a relative ID (RID) between 0 and 1000 is for these special accounts.

==== Local Linux Server Only ====
Before we dive into the specifics of LDAP, lets examine the range of IDs used by the Linux system //only//.  In other words, you won't find these accounts in LDAP or floating around as SIDs on a Windows network.  In ClearOS, the following ID ranges are used:

^ ^UID/GID^RID^Purpose^Examples^
|System Users|0-499|n/a|System user accounts|root, apache, mysql|
|System Groups|0-499|n/a|System group accounts|lp|
|Normal Users|500-999|n/a|User accounts outside of LDAP|devel|

**System Users** -- When you install a software daemon like the Apache web server, the software will typically run under its own account -- apache in this case.

**System Groups** -- System groups can be used for those cases where sharing permissions is a necessity.  For example, the ClamAV antivirus engine needs to share file permissions with the Amavis mail scanner.  On a ClearOS system, you will find the //clamav// user is in the //amavis// group.

**Normal Users** -- In some circumstances, you may need to add a normal user account outside of LDAP.  A range of UIDs have been set aside for this purpose and you can follow [[:content:en_us:kb_howtos_adding_a_user_account_from_the_command_line|this howto]].

==== Global LDAP Directory ====
In Linux, UIDs and GIDs are independent.  In other words, a user account //mike// with UID 1011 has no special ties or permissions to a GID with the same value of 1011.  Since the Windows world uses a unique number across users, groups, and computers, the ClearOS directory will do the same.

The UID, GID and RID ranges are summarized in the following tables.

^User^UID^RID^Purpose^Examples^
|System Users|0-499|n/a|Linux system users|root, apache|
|Built-in Users|300-399| |ClearOS built-in users|flexshare, guest|
|Normal Users|1,000-59,999|1,000-59,999|Normal user users|dave, mike, ben|
|Windows - Samba Server|10,000,000-19,999,999|auto|Winbind IDMap| |
|Windows - Active Directory|20,000,000-29,999,999|auto|Winbind IDMap| |

Note: the 500-999 range is reserved for creating non-LDAP user accounts.  This is mainly to support sloppy 3rd party packaging that automatically create user user accounts when system accounts are more appropriate.

^Group^GID^RID^Purpose^Examples^
|System Groups|0-499|n/a|Linux system groups|clamav|
|Built-in Groups|63,000-63,999|63,000-63,999|ClearOS built-in groups|allusers|
|Normal Groups|60,000-62,999|60,000-62,999|Normal group accounts|staff, students|
|Plugin Groups|900,000-999,999| |Plugin groups|plugin-proxy, plugin-openvpn|
|Windows Groups|1,000,000-1,001,000|0-1000|Special RID range|Domain Admins|
|Windows - Samba Server|10,000,000-19,999,999|auto|Winbind IDMap| |
|Windows - Active Directory|20,000,000-29,999,999|auto|Winbind IDMap| |

{{keywords>clearos, clearos content, dev, architecture, directory, maintainer_dloper}}
