===== Using Ad for Openldap User Authentication =====
This writeup is to demonstrate the use of Microsoft's Active Directory as an authentication provider for OpenVPN. This guide assumes knowledge of command line functions in a ClearOS terminal environment and familiarity with tools of Active Directory.

===== Getting Started =====
You will need the following to configure ClearOS 5.x to use Active Directory as your authentication provider:

  * A ClearOS 5.x server running as your gateway
  * An Active Directory Domain controller on the same subnet as a LAN interface of your ClearOS 5.x server
  * User accounts on your with the **exact** same name as those configured on Active Directory with OpenVPN selected as one of their valid services.
  * For best practices, your ClearOS server should be using the AD server(s) as its DNS server only.
  * Opened ports on the incoming firewall rules of the ClearOS server for 'OpenVPN' (1194 UDP)

On your ClearOS server, you will need to provision a user that also exists on Active Directory (AD). The reason to maintain this account on ClearOS as well as AD is that ClearOS uses 2-factor authentication by means of security keys. It will **NOT** matter that the passwords match but doing so will allow you to use other ClearOS services with this user should you choose to do so.

===== Testing =====
From command line, validate that you can ping the domain controller and that you can get access to the Access Directory. For this scenario, your domain on AD is called 'clearos.lan' and the domain controller resides on 10.1.1.10 and the ClearOS server resides on 10.1.1.1

==== Collecting Data ====
You need to know the Base DN, Bind DN, and password for your connections to AD. This can easily be collected from the "Active Directory Users and Computers" management console on your AD domain controller.

{{:omedia:ad-ldap_dn-info.png?550|}}

Looking at the image you can see that the base of this directory is //clearos.lan// which translates to the BaseDN value:

  dc=clearos,dc=lan

We add on the folder and name of the user to get the BindDN value. The folder being 'Users' and the user being 'Administrator'. So the BindDN would be:

  cn=administrator,cn=users,dc=clearos,dc=lan

<note>It is important to note that Active Directory will use the CN (canonical name) value to define specific 'builtin' folders where user created folders use the moniker of OU (organization unit). These CN folders include 'Builtin', 'Computers', 'ForeignSecurityPrincipals', and 'Users'. These folders in the "Active Directory Users and Computers" will appear slightly different. CN 'folders' look like blank folders whereas OU folders look like folders with a book inside them. If the administrator account is in a different, user-created folder it may very well look like this:  cn=backup administrator,ou=admins,ou=IT,ou=company,dc=clearos,dc=lan</note>

==== Connectivity ====

Test connectivity to the AD server from your ClearOS box.

  ping 10.1.1.10

(type CTRL+c to cancel pings)

  telnet 10.1.1.10 389

Telnet to port 389 to validate that you get a SYN packet which will indicate that the service is listening. This will reply with:

  [root@system ~]# telnet 10.1.1.10 389
  Trying 10.1.1.10...
  Connected to 10.1.1.10.
  Escape character is '^]'.

Type CTRL+] then type 'quit' and <ENTER> to exit.

  ^]
  telnet> quit
  Connection closed.

Next test to see if you can get data from Active Directory with the following (please substitute the host address, domain name, and bind DN with those from your environment):

  ldapsearch -x -h 10.1.1.10 -b "dc=clearos,dc=lan" -D "cn=administrator,cn=users,dc=clearos,dc=lan" -W

You will be prompted for a password. Because we used the 'administrator' account, we will supply that password. The result of this will spit out all the objects in Active Directory and their associated values for their attributes.

===== Setting up the connection =====
You will want to configure a group within Windows that will allow your Windows Administrators to tell which users may VPN into the network. For our purposes, we will create a group called 'OpenVPNUsers' in the folder 'Users' in our Active Directory.

Now that we can show that ClearOS can connect to AD we can place these values into the configuration located in /etc/openvpn/auth-ldap.

The file will look like this:

  # This file is automatically updated by ldapsync -- please do not edit.
  <LDAP>
  	# LDAP server URL
  	URL		ldap://10.1.1.10
  
  	# Bind DN (If your LDAP server doesn't support anonymous binds)
  	BindDN		cn=administrator,cn=users,dc=clearos,dc=lan
  
  	# Bind Password
  	Password	password
  
  	# Network timeout (in seconds)
  	Timeout		15
  
  	# Enable Start TLS
  	TLSEnable	no
  
  	# Follow LDAP Referrals (anonymously)
  	FollowReferrals yes
  
  	# TLS CA Certificate File
  	TLSCACertFile	/etc/ssl/ca-cert.pem
  
  	# TLS CA Certificate Directory
  	TLSCACertDir	/etc/ssl/certs
  
  	# Client Certificate and key
  	# If TLS client authentication is required
  	TLSCertFile	/etc/ssl/client-cert.pem
  	TLSKeyFile	/etc/ssl/client-key.pem
  
  	# Cipher Suite
  	# The defaults are usually fine here
  	# TLSCipherSuite	ALL:!ADH:@STRENGTH
  </LDAP>
  
  <Authorization>
  	BaseDN		"cn=users,dc=clearos,dc=lan"
  	SearchFilter	"sAMAccountName=%u"
  	RequireGroup	true
  	<Group>
  		BaseDN		"cn=users,dc=clearos,dc=lan"
  		SearchFilter	"cn=OpenVPNUsers"
  		MemberAttribute	Member
  	</Group>
  </Authorization>

<note>It has not gone unnoticed that the phrase "This file is automatically updated by ldapsync -- please do not edit." exists at the top of this document. Don't worry, we're just testing thusfar.</note>

==== Let's do this thing ====
With all the parts in place. Restart the openvpn service and try out the connection. For the purpose of testing whether the account is truly from Active Directory or not, try changing the passwords between the ClearOS server and AD for that particular account.

===== Set this in stone =====
Once we are satisfied that this works, we need to change some things to prevent the normal operations of ClearOS from altering our configuration file (vis-a-vis ldapsync).

If everything is copacetic, you can make these changes permanent by updating the /etc/openldap/templates/openvpn.auth-ldap.template file. Use the following script:

  mv /etc/openldap/templates/openvpn.auth-ldap.template /etc/openldap/templates/openvpn.auth-ldap.template~
  echo "KOLAB_META_START" > /etc/openldap/templates/openvpn.auth-ldap.template
  echo "TARGET=/etc/openvpn/auth-ldap" >> /etc/openldap/templates/openvpn.auth-ldap.template
  echo "PERMISSIONS=0600" >> /etc/openldap/templates/openvpn.auth-ldap.template
  echo "OWNERSHIP=root:root" >> /etc/openldap/templates/openvpn.auth-ldap.template
  echo "KOLAB_META_END" >> /etc/openldap/templates/openvpn.auth-ldap.template
  cat /etc/openvpn/auth-ldap >> /etc/openldap/templates/openvpn.auth-ldap.template
{{keywords>clearos, clearos content, dev, apps, maintainer_dloper, maintainerreview_x, keywordfix}}