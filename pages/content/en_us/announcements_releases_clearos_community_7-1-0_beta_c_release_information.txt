===== ClearOS 7.1.0 Beta 3 Release Information =====
**Released: 31 July 2015**

ClearOS 7.1.0 Beta 3 is [[http://mirror.clearos.com/clearos/testing/7/iso/x86_64/ClearOS-DVD-x86_64.iso|here]]! This release contains major improvements and insight to the eventual release of ClearOS 7 and replaces ClearOS 7.1.0 Beta 2 as the current running beta.

We are excited to add Samba Directory to the mix. Samba Directory allows for Active Directory equivalent functionality using ClearOS as your Directory Server. Simple user and group administration is available for Samba Directory in the usual places in ClearOS' Webconfig. If you comply with Microsoft's EULA for the tool, you can use [[https://www.microsoft.com/en-us/download/details.aspx?id=7887|Microsoft®'s Server administration tools]] to modify additional aspects of your Samba Directory including Group Policy Objects.

This beta is built using the new [[http://koji.clearos.com/koji/|Koji build system]]. The change to this build system is needed now since it fundamentally changes some structure that we will need to have working smoothly throughout the 7.x release cycle. This new build system should be much more automated and integrated than the past system. As a consequence of this change, updates from Beta 2 to Beta 3 will require a change in the yum repo data and a performance of a dist-upgrade.


ClearOS version 7.1 introduces:

  * Samba 4, Directory (Microsoft® Active Directory Replacement)
  * New Google® Apps Connector
  * Updated Microsoft Active Directory Connector
  * IPv6 Ready
  * Streamlined Theme System
  * Dynamic Dashboard
  * Updated Antispam and Antivirus Engines
  * Updated IDS and IPS Engines
  * Event and Alert Notification Framework
  * Internationalization

New Upstream Features Include:

  * XFS and BTRFS Filesystem Support
  * Improved VM Support, including Microsoft Hyper-V

ClearOS 7 will feature a Community, Home, and Business version. All versions of ClearOS will install from the same install image. You will be required to select your version at the time of installation in the wizard.

Some of the new apps in the beta are **NOT** available via the Marketplace but can be installed via **yum**. Others are still in progress or not yet feature complete. You can find installation and app details further below.

<note>If you have not already done so, please review the [[:content:en_us:announcements_releases_test_releases|introduction to test releases]].</note>

===== Changes Since Beta 2 =====
  * Samba 4 Directory (IMPORTANT!!)
  * Events System (IMPORTANT!!)
  * Process Viewer
  * Dropbox
  * Antimalware File Scan
  * Updated phpmyadmin

The full changelog can be found here:

  * [[https://tracker.clearos.com/changelog_page.php?version_id=65|Changelog - ClearOS 7.1.0 Beta 3]]

===== Known Issues =====
  * Updates from beta 2 require extra steps.

<code>
yum clean all
yum --disablerepo=* --enablerepo=clearos,clearos-updates,clearos-centos upgrade
yum clean all
yum upgrade
</code>
===== Feedback =====
Please post your feedback in the [[https://www.clearos.com/clearfoundation/social/community/clearos-7-1-beta-3-released-discussion|ClearOS 7.1 Beta 3 Discussion Forum]]. 



===== Download =====
==== ISO Images ====
^Download^Size^SHA-256^
|[[http://mirror.clearos.com/clearos/testing/7.1beta3/iso/x86_64/ClearOS-DVD-x86_64.iso|64-bit Full]]|796 MB|  05de765be54631b730ad7f990f979cb730b51e516cfd7c98b291d88eb27404ba |
|[[http://mirror.clearos.com/clearos/testing/7.1beta3/iso/x86_64/ClearOS-netinst-x86_64-7.1beta3.iso|64-bit Net Install]]|372 MB|  fb48b0a1987a5a74f6f674d05df9a794952607a9dbcdd383d25dc8a7de20a34f |


==== Virtual Machine and Cloud Images ====
Virtual Machine and Cloud Images are not available for this beta.

==== Upgrade from Earlier ClearOS Community 6.x Releases ====
There is no supported method yet from upgrading from ClearOS 6 to ClearOS 7. Additionally, the layout for Beta 3 is different due to the changes in the build system to Beta 2. Upgrades from Beta 2 require changes to the repo content and performance of a dist-upgrade. There may be support for migration of configuration backups.


{{keywords>clearos, clearos content, announcements, releases, clearos7.1, beta3, previoustesting, maintainer_dloper}}
