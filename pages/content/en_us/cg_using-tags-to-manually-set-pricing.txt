===== Using Tags to Set Custom Pricing =====
ClearGLASS calculates the estimated monthly costs for a machine using the specific service provider's APIs. 

If you have negotiated custom pricing or if you have a different price than the one displayed, or if you want to set a price on a machine that does not have one (OpenStack, KVM, a bare-metal server, etc.) the reserved **cost_per_month** tag will allow you to add a new price by simply using the reserved tag along with an integer value. 

ClearGLASS will use the new price to calculate the estimated monthly cost of the machine.

[image]