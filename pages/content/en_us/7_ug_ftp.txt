===== FTP Server =====
ClearOS provides a basic FTP server that can be used for user home folders and [[:content:en_us:6_flexshare|Flexshares]].

<note warning>For those of you upgrading from ClearOS 5.x, the default FTP ports for Flexshare are 21 (FTP) and 990 (FTPS).  Port 2121 is now used to FTP into home directories.</note>

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Server|File|FTP Server</navigation>


===== Help =====
===== Links =====
  * [[:knowledgebase:troubleshooting:ftp_issues|Troubleshooting FTP Issues]]
  * [[:content:en_us:6_flexshare|Flexshares]]
  * [[http://www.proftpd.org/|ProFTPd home page]]

{{keywords>clearos, clearos content, app-ftp, FTP Server, clearos7, userguide, ftp, categoryserver, subcategoryfile, maintainer_dloper}}
