===== ClearOS community 6.4.0 Alpha 1 Release Information =====
**Released: December 7, 2012**

ClearOS Community 6.4.0 Alpha 1 has arrived!  Along with the usual round of bug fixes and enhancements, this release introduces a new reports engine, a storage manager, an antimalware file scanner, a basic POP/IMAP server as well as upstream proxy support.  A number of new reports are available with the release of the reports engine:

  * [[http://www.clearcenter.com/support/documentation/user_guide/filter_and_proxy_report|Filter and Proxy Report]]
  * [[http://www.clearcenter.com/support/documentation/user_guide/resource_report|Resource Report]]
  * [[http://www.clearcenter.com/support/documentation/user_guide/network_report|Network Report]]

The purpose of the Alpha is to get some early feedback on some of the new apps, so this version should not be considered stable at all.  In addition, the new apps in the alpha are **NOT** available via the Marketplace but can be installed via **yum**.  You can find installation and app details further below.

<note>If you have not already done so, please review the [[:content:en_us:announcements_releases_test_releases|introduction to test releases]].</note>

===== What's New in ClearOS Community 6.4.0 =====
The following is a list of features coming in ClearOS Community 6.4.0.

  * [[http://www.clearcenter.com/support/documentation/user_guide/filter_and_proxy_report|Filter and Proxy Report]]
  * [[http://www.clearcenter.com/support/documentation/user_guide/resource_report|Resource Report]]
  * [[http://www.clearcenter.com/support/documentation/user_guide/network_report|Network Report]]
  *[[http://www.clearcenter.com/support/documentation/user_guide/storage_manager|Storage Manager]]
  * [[http://www.clearcenter.com/support/documentation/user_guide/imap_and_pop_server|Basic POP/IMAP Server]]
  * [[http://www.clearcenter.com/support/documentation/user_guide/upstream_proxy|Upstream Proxy Support]]
  * [[http://www.clearcenter.com/support/documentation/user_guide/antivirus_file_scan
|Antimalware File Scan]]

For businesses and organizations, ClearOS Professional 6.4.0 (not yet released) will also include:

  * Intrusion Protection Report
  * [[http://www.clearcenter.com/support/documentation/user_guide/network_map|Network Map]]
  * Content Filtering by IP
  * Amazon EC2 Images
  * Samba 4 Directory (Beta)

There have also been a couple of apps already released since ClearOS 6.3.0:

  * [[http://www.clearcenter.com/support/documentation/user_guide/smart_monitor|SMART Monitor]]
  * [[http://www.clearcenter.com/support/documentation/user_guide/dropbox|Dropbox]]

The full changelog can be found here:

  * [[http://tracker.clearfoundation.com/changelog_page.php?version_id=30|Changelog - ClearOS 6.4.0 Alpha 1]]

===== Feedback =====
Please post your feedback in the [[http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,39/func,showcat/|ClearOS Development and Test Release Forum]]. 

===== Download =====
==== Virtual Machines ====
For 6.4.0 Alpha 1, 32-bit virtual machine images are available:

|VMware Enterprise|[[http://www.clearcenter.com/support/documentation/clearos_professional_6/user_guide/vmware_enterprise|VM Instructions]]|[[http://images.clearfoundation.com/clearos-images/community/6.4.0-alpha1/images/vmware-enterprise/i386/clearos-community-6.4.0-i386-vmware-enterprise.zip|32-bit]]|350 MB|7a8a08ecfdf03886fdae47ff32f733cb|
|VMware Basic|[[http://www.clearcenter.com/support/documentation/clearos_professional_6/user_guide/vmware_basic|VM Instructions]]|[[http://images.clearfoundation.com/clearos-images/community/6.4.0-alpha1/images/vmware-basic/i386/clearos-community-6.4.0-i386-vmware-basic.zip|32-bit]]|346 MB|20a77af1dc48ad6774a615532f853770|
|VirtualBox|[[http://www.clearcenter.com/support/documentation/clearos_professional_6/user_guide/virtualbox|VM Instructions]]|[[http://images.clearfoundation.com/clearos-images/community/6.4.0-alpha1/images/virtualbox/i386/clearos-community-6.4.0-i386-virtualbox.vmdk.zip|32-bit]]|346 MB|42bd1a3cca51c610827d237fb245e483|
|VirtualPC|[[http://www.clearcenter.com/support/documentation/clearos_professional_6/user_guide/virtualpc|VM Instructions]]|[[http://images.clearfoundation.com/clearos-images/community/6.4.0-alpha1/images/virtualpc/i386/clearos-community-6.4.0-i386-virtualpc.vhd.zip|32-bit]]|346 MB|1e91dff30531a7be7b8fa03570fff046|


==== Upgrade from ClearOS Community 6.3.0 ====
As an alternative to the virtual machines, you can install ClearOS Community 6.3.0 and upgrade to 6.4.0 Alpha 1.  

  yum --enablerepo=clearos-test,clearos-updates-testing upgrade "app-*"
  
To really simulate the 6.4.0 install experience, run the following **before** going through the first boot wizard on a fresh 6.3.0 install:

  yum --enablerepo=clearos-test,clearos-updates-testing install app-storage app-upstream-proxy

===== Alpha Testing Details =====
==== Storage Manager ====
The new **Storage Manager** app provides a clean way to separate app data from the underlying operating system. Flexshares, web proxy cache, mail stores and other data intensive apps are mapped to /store on the file system.  This /store could be a separate RAID partition, an external store, or an Amazon S3 store.  

By default, the /store directory is used by the **Storage Manager**.  If you are in a VM environment, you can attach another disk to your virtual machine and then mount it to /store.  In future test versions, you will be able to do this via the ClearOS web-based administration tool.  For now, we're just kicking the tires on the command line.

  * [[http://www.clearcenter.com/support/documentation/user_guide/storage_manager|User Guide - Storage Manager]]

==== Upstream Proxy ====
One of the features added to ClearOS 6.4.0 is support for upstream proxies.  The initial testing for this feature requires some command line knowledge as well as direct access to the Internet (e.g. no proxy!).  Once we are comfortable with the stability of the new feature, we will roll it into the release.  In the ClearOS 6.4.0 Alpha 1 release, you should now see an **Upstream Proxy** step in the first boot wizard.

Upstream Proxy is installed by default on the virtual machine images.  For the 6.3.0 upgrade path, run:

  yum --enablerepo=clearos-test install app-upstream-proxy

==== IMAP and POP Server ====
To install the IMAP and POP Server run:

  yum --enablerepo=clearos-test install app-imap
  
You may need to stop/start the IMAP/POP Server after making a configuration change.  That's an alpha bug... sorry about that.  The documentation has the details: [[http://www.clearcenter.com/support/documentation/user_guide/imap_and_pop_server|User Guide]]  
 
==== Reports, Reports, Reports ====
To install the available reports, run:

  yum --enablerepo=clearos-test,clearos-updates-testing install "app-*report*"

You can find documentation on the reports here:

  * [[http://www.clearcenter.com/support/documentation/user_guide/filter_and_proxy_report|Filter and Proxy Report]]
  * [[http://www.clearcenter.com/support/documentation/user_guide/resource_report|Resource Report]]
{{keywords>clearos, clearos content, announcements, releases, clearos6.4, alpha1, previoustesting, maintainer_dloper}}
