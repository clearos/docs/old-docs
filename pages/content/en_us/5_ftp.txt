===== FTP Server =====
ClearOS provides a basic FTP server that can be used for user home folders and [[:content:en_us:5_flexshares|Flexshares]].

===== Links =====
  * [[http://www.proftpd.org/|ProFTPd home page]]
{{keywords>clearos, clearos content, AppName, app_name, clearos5, userguide, categoryserver, subcategoryfileandprint, maintainer_dloper}}
