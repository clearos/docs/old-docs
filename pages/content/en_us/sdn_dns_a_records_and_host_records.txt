===== A Records and Hosts Records =====
The bread and butter behind the DNS system is the A record. The A record (//address record//, also known as the //host record//) maps a domain name to an IP address on the Internet.  Please review the [[:content:en_us:sdn_dns_primer|DNS Primer]] for more help on DNS records.

===== Activation =====
  * Login to your [[https://secure.clearcenter.com/portal/|account]]
  * Click on <navigation>DNS|DNS Records|A Records</navigation> in the top navigation bar
  
===== Configuration =====
To add A records to your account:

  * Enter a subdomain name (this can be blank)
  * Select the domain from the drop-down list
  * Enter the IP address for the given subdomain.domain combination 

{{keywords>clearos, clearos content, sdn, SDN DNS a Records and Hosts Records, categorydns, subcategoryrecords, maintainer_dloper, userguide}}
