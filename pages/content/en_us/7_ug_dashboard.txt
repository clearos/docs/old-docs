===== Dashboard =====
The Dashboard app provides a bird's eye view of status of your ClearOS system.

===== Installation =====
This app is installed by default.

===== Menu =====
You can find this feature in the top-level menu.
==== Settings ====
To access the Settings, click on the gear wheel on the top right of the screen.
{{7_ug_dashboard_settings1.png}}

Here you can select the number of rows (max=8) and the number of columns in each row (max=4).
{{7_ug_dashboard_settings2.png}}

==== Main Screen ====
Having selected the number of widgets you like to appear on this screen in the Settings menu, you will see a number of drop-down boxes like:
{{7_ug_dashboard_dropdown.png}}

Here you choose which widget you'd like to have appear in that position of the main screen.

<note tip>The only way to remove a widget from the screen is to change the number of columns you which to appear in that row of the Settings screen. It may mean re-doing the whole row.</note>

{{keywords>clearos, clearos content, Dashboard, app-dashboard, clearos7, userguide, categorysystem, subcategorybase, maintainer_dloper}}
