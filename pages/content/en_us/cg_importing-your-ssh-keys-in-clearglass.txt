===== Importing your SSH keys in ClearGLASS =====
If your machines were created outside ClearGLASS, you can provide the ssh keys to ClearGLASS in order to get the web shell working and also be able to enable monitoring automatically, run scripts, and set actionable alerts and automation. RSA ssh keys are supported, of any length. Encrypted ssh keys are not supported, so make sure you provide an ssh key without a passphrase otherwise it won't be accepted.

From the ClearGLASS home page, navigate to the Keys section and click on the Create button.

[image]

Fill in a name and paste your private key in the text box or click on Upload and choose your private key file. Then click on the Create button.

[image]

You can have one or more ssh keys at any time. If an ssh key exists only, it is considered as the default. ClearGLASS will try to connect with this key on all servers, and get the server uptime. Then it will show a blue icon next to each machine it has successfully connected to. If it fails to connect, it will try to ping the machine, and display a ping connectivity diagram. You can set another key as the default, if you have more than one keys. ClearGLASS will remember the association of a server with a key so you won't have to provide it more than one time. 

[image]

==== Associate a key with a server ====
In order to associate a server with it's key, you have to visit the server's page, and select 'Associate Key' on the top right. There select the key. ClearGLASS will try to automatically connect with this key using user root and ssh port 22. If you want ClearGLASS to try to connect with another user/port, click on the advanced link. 

[image]