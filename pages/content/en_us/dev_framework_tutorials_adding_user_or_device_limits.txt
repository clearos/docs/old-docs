===== Tutorials Adding User or Device Limits =====
The following tutorial provides information on how to integrate an app that requires user or device limits.  For example, you may want to provide a free version of a paid app, but limit the version to 10 users.  The details on such an implementation are provided below.

===== Big Picture =====
There are two layers to providing user/device limits on an app:

==== Convenience Layer ====
The **convenience** layer is an open source hook into the user/device system that provides useful license information to a ClearOS administrator.  When an administrator hits or approaches a user/device limit:

  * warnings are shown in the web-based administration tool
  * the user/device manager automatically disables app-specific fields

As an app developer, you don't have to worry about the details of the user/device manager web interface, the framework handles it for you.

==== Verification Layer ====
Since the **convenience** layer is open source, it is trivial to circumvent.  To address this pitfall, an additional proprietary **verification** layer provides a way to verify app limits in a way that is difficult to disable.  Commercial apps like Zarafa Small Business already include license engines, so this layer is already implemented in some cases.

===== Convenience Layer =====
With the basic understanding of the two layers under your belt, let's move on to the implementation overview.  When it comes right down to the bottom line, the convenience layer requires a class with a single //get_info()// method that returns the following information:

^Description^Example^
|App name|Acme|
|Limit type|User or Device|
|Total|Total number included in the app, e.g. 10 users|
|Used|Total number in use, e.g. 3|
|Available|Total number available, e.g. 7|
|User Limits|User limit information for extensions/plugins|

With this //get_info()// method in place, the user/device system is able to manage thresholds.  The screenshot below is an example warning message in the **Users** app when the 10-user limit has been reached:

{{:developer:framework:tutorials:user_limit_summary_example_box.png|ClearOS Paid App User Limit - Developer Example}}

For digging deeper into the implementation details, we have two examples available:

  * [[:content:en_us:dev_framework_tutorials_adding_user_or_device_limits_simple_paid_app|Simple Paid App]]
  * [[:content:en_us:dev_framework_tutorials_adding_user_or_device_limits_multiple_tiers|Multiple Tiers]] (e.g. both free and paid versions)

Even if you plan on developing an app with different payment tiers, we recommend skimming over the first [[:content:en_us:dev_framework_tutorials_adding_user_or_device_limits_simple_paid_app|Simple Paid App]] example.

===== Verification Layer =====
{{keywords>clearos, clearos content, dev, framework, maintainer_dloper, maintainerreview_x, keywordfix}}
