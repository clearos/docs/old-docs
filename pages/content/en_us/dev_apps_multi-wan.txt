===== Multi-Wan =====
This document provides technical information on the multi-WAN feature in ClearOS.

===== Routing and Multi-WAN =====
Not surprisingly, advanced routing is required on a multi-WAN system.  Like most seasoned system administrators, the **route** command is part of the standard tool set.  With multi-WAN, you need to forget about using **route** and start becoming friends with the **ip route** command!  Read on for the technical details.

  * [[:content:en_us:dev_apps_routing_and_multi-wan|Routing and Multi-WAN]]

===== References =====
==== ClearOS User Guide ====
  * [[http://www.clearcenter.com/support/documentation/user_guide/multi-wan|Multi-WAN]]

==== Bugs and Features ====
  * [[http://tracker.clearfoundation.com/search.php?project_id=1&category=app-multiwan%20-%20Multi-WAN&sticky_issues=on&sortby=status&dir=ASC&hide_status_id=90|Multi-WAN App - Tracker]]
{{keywords>clearos, clearos content, dev, app, maintainer_dloper, maintainerreview_x, keywordfix}}
