===== Developer Tools =====
The Developer Tools provides helpful utilities for translators, developers and testers.

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>System|Developer|Developer Tools</navigation>
{{keywords>clearos, clearos content, Developer Tools, app-developer-tools, clearos6, userguide, categorysystem, subcategorydevelopers, maintainer_dloper}}
