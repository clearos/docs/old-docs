===== Upgrading from ClearOS 5.x to ClearOS 7.x =====

This guide contains an overview of the requirements and tasks associated with migrating from ClearOS 5.x to ClearOS 7.x. There are a number of changes which make ClearOS difficult to go from 5 to 7. 

The biggest difficulty comes with the fact that ClearOS 5 does NOT use the same LDAP schema as 6 or 7 which means that some conversion must take place. The best method is to create a virtual machine using ClearOS 6 and validate that the import will work and then re-export the file.

===== Inline Upgrade Not Possible =====

There is no supported method for upgrading directly from ClearOS 5 to ClearOS 7.  A fresh install of ClearOS 7 is required.  Configuration data can be migrated, but all other data will need to be manually moved to ClearOS 7.  It is imperative that you make a backup of your data to ensure no data is lost in the transition.

The steps for backing up configuration data are found below.

===== Configuration Backup =====

To upgrade the configuration files from ClearOS 6 to ClearOS 7 use the following steps: 

==== ClearOS 5 ====
 
  - Login to the ClearOS web-based interface, 
  - Go to 'System' > 'Configuration Backup' in the menu 
  - Create backup file 
  - Download backup file 

==== ClearOS 7 ====

<note warning>At the moment there is a problem restoring an Active Directory Connector configuration into a fresh installation ClearOS7. In order for it to work correctly you **must** install the Windows Networking (Samba) app from the Marketplace **before** you do the configuration restore. If you do not do this you will need to reinstall from scratch. Installing the Windows Networking (Samba) app after a config restore then doing another config restore does not fix the problem.\\
You may then need to start winbind and set it to restart automatically on boot:
<code>systemctl start winbind.service
systemctl enable winbind.service
</code>

Remember to rename the machine in IP settings while both the old and new machine are on the network as Active Directory will not like two machines of the same name joined to the domain at the same time.

If you still have problems please consult the [[:content:en_us:kb_troubleshooting_the_ad_connector|Active Directory Connector Troubleshooting]] guide, especially the "wbinfo -t" section.</note>

  - Install a ClearOS 7 system
  - Go through the install wizard after first boot
  - Do not select any Marketplace apps during the install wizard (but see warning above)
  - Go to 'System' > 'Backup' > 'Configuration Backup' in the menu and restore the ClearOS 6 backup on the ClearOS 7 system.

Many configurations will not get migrated. Use caution when migrating. We suggest using new disks so you can compare and manually migrate services, interfaces, and data.

===== Notes =====
Fire up a fresh ClearOS 6 VM. Register and run updates. Install Samba and OpenLDAP:

  yum install app-openldap-directory app-samba

Copy the configuration backup to a temp directory:

mkdir /tmp/restore1

- Inspect the /etc/openldap/backup.ldif file from the 5.x backup.  Note which features are enabled.  In an example, the PPTPFlag and FTPFlag were in use.  On the 6.x box, I install those plugins:

   yum install app-pptpd-plugin-core app-ftp-plugin-core


- On the ClearOS 6 box, create the script to import the 5.x configuration:

<code>
#!/usr/clearos/sandbox/usr/bin/php
<?php

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\configuration_backup\Configuration_Backup as Configuration_Backup;
clearos_load_library('configuration_backup/Configuration_Backup');

///////////////////////////////////////////////////////////////////////////////
// M A I N
///////////////////////////////////////////////////////////////////////////////

$backup = new Configuration_Backup();
$backup->restore_52('/tmp/backup-gateway.serverrname.lan-02-06-2014-10-35-33.tar.gz');
</code>

And run then run the script:

./myscript


{{keywords>clearos, clearos5, clearos7, clearos content, Upgrading from ClearOS 5.x to ClearOS 7.x, app_name, version7, xcategory, maintainer_cjones, maintainer_nhowitt, maintainerreview_x, keywordfix, userguides}}
