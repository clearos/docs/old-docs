===== NTP Server =====
NTP is the Network Time Protocol. This module is extremely important for managing the time on your server. It is important that your server runs the correct time because some protocols which require interaction relay on the time being relatively the same in order to properly conduct transactions.

If you do not have the NTP app, ClearOS will attempt to sync its time to internet time once a day. With the app it will attempt to sync every 17min or so.

The app also allows ClearOS to act as a time source for your LAN
===== Configuration =====
There are no configurable options for this app, only a <button>Start/Stop</button>

By default the configuration for the time server is set to use the following time sources provided by ClearCenter:

  * time1.clearsdn.com
  * time2.clearsdn.com
  * time3.clearsdn.com
  * time4.clearsdn.com

===== Changing time servers =====
If you would like to change the time servers you will need to modify the following lines in /etc/ntp.conf:

  server time1.clearsdn.com
  server time2.clearsdn.com
  server time3.clearsdn.com
  server time4.clearsdn.com

Then restart the time service.

  systemctl restart ntpd.service

Or with the <button>Start/Stop</button>

===== ClearOS as an NTP Server =====

If you want ClearOS to act at your network's time server you can set the NTP option on the [[7_ug_dhcp|DHCP Server]] to point to your ClearOS LAN. At the same time you may be able to configure your device directly to use ClearOS.

{{keywords>clearos, clearos content, AppName, app_name, clearos7, userguide, categorynetwork, subcategoryinfrastructure, maintainer_dloper, maintainerreview_x}}
