===== Dynamic Firewall =====
The Dynamic Firewall app allows you keep more ports closed while still having access to network resources remotely.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/network/Dynamic_Firewall|here]]
===== Documentation =====
==== Version 6 ====
This app is not available for ClearOS 6.
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_dynamic_firewall|here]].
==== Additional Notes ====
