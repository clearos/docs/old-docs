===== User Certificates =====
The **User Certificates** app provides a one-stop location for end users who need to access security settings for the desktop systems, namely:

  * SSL Certificates (including PKCS12)
  * OpenVPN configuration

<note warning>Unlike most of the other pages in the webconfig system, this one is designed to be accessed by the end user.  To view the information on this page, login with the end user's username (instead of root).</note>

To access the User Certificates, log in is the user then click on the users name on the top right of the screen. A menu will open up with a User Certificates option.

{{7_ug_user_profile_user_certificates.png}}

==== Security Certificates - E-mail / PKCS#12 ====
If your system administrator has enabled encryption and digital signatures on the mail system and has enabled Security Certificates in your ClearOS profile, click on the <button> Download </button> button to download the PKCS#12 file. You will be prompted for a password.
<note warning>Once the PKCS12 file has been created it is impossible to recover the password. If you forget the password you will need to hit the <button> Reset </button> button to regenerate the certificates.</note>

==== Personal Security Keys ====
If your system administrator has enabled Security Certificates in your ClearOS profile, you can download all the necessary security and configuration to connect via OpenVPN.  You will need to download the following files to configure your connection:

  * Certificate
  * Certificate Authority
  * Private Key
  * OpenVPN Configuration File

<note>It may be possible with some OpenVPN clients to use the PKCS12 file instead of the Certificate, Certificate Authority and Private Key files</note>
<note>If the System Administrator has regenerated the Certificate Authority file, you will need to Reset your files by hitting the <button> Reset </button> button and then re-downloading them.</note>

{{keywords>clearos, clearos content, User Certificates, app-user-certificates, clearos7, userguide, categorysystem, subcategorymyaccount, maintainer_bchambers, maintainerreview_x, keywordfix}}
