===== System Requirements =====
The general hardware requirements and recommendations are summarized in the following table.

==== Minimum Requirements ====

^Base Hardware^^
|Processor/CPU  | Up to 16 processors|
|Memory/RAM     | At least 512 MB is recommended (see guidelines below)|
|Hard Disk      | At least 2 GB is recommended (see guidelines below)|
|CD-ROM Drive   | Required for CD installation only|
|USB            | Required for USB key installation only|
|Video Card     | Almost any video card|
|Floppy Drive   | Not required|
|Sound Card     | Not required|
^Peripherals^^
|Mouse          | Not required|
|Monitor and Keyboard| Required for installation only|
^Network^^
|Broadband      | Ethernet, cable, DSL|
|Network Cards  | A network card is required, two for gateway mode|

==== Hardware Requirements ====
The following are //guidelines// for estimating the right hardware for your system.  Keep in mind, the hardware required depends on how you use the software.   For instance, a heavily used content filter requires more power than a system running a simple firewall.

^ RAM and CPU   ^ 5 users  ^ 5-10 users  ^ 10-50 users  ^ 50-200 users ^
| Processor/CPU | 500 MHz  | 1 GHz       | 2 GHz        | 3 GHz |
| Memory/RAM    | 512 MB   | 1 GB        | 1.5 GB       | 2 GB |
^ Hard Disk ^   ^^^^
| Hard Disk 	| Installation and logs require 1 GB - optional storage is up to you ||||
| RAID          | Recommended for mission critical systems||||

The following software modules are processor and memory intensive:

  * Intrusion Detection and Prevention
  * Content Filtering
  * Webmail 
  * Antispam
  * Antivirus

===== Network Cards =====
==== PCI Network Cards ====
Generally, ClearOS does a good job at auto-detecting hardware and most mass-market network cards are supported.  We recommend avoiding the latest available hardware since drivers may not yet be available. 

==== Wireless Network Cards ====
Though wireless card drivers are included in ClearOS, wireless is not officially supported.  Currently, we recommend purchasing a dedicated wireless router for your network.

===== Internet Connection =====
ClearOS supports most DSL (including PPPoE) and cable modem broadband Internet connections. 

==== Cable and Standard Ethernet ====
Cable and standard Ethernet connections are supported.

==== DSL and PPPoE ====
During the installation process, you will be asked which type of DSL service you use - PPPoE or Standard.  These are mutually exclusive implementations, so you will need to select the correct type during installation. It is very important to know how your Internet service provider configures your network. If you are not sure, ask the ISP's technical support staff before you begin.

==== ISDN and Satellite ====
We do not support ISDN or satellite broadband service providers unless they terminate with a standard Ethernet connection.

===== RAID Support =====
{{keywords>clearos, clearos content, System Requirements, clearos5, userguide, categoryinstallation, subcategorypreparation, maintainer_dloper}}
