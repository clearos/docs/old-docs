===== DMZ =====
The DMZ solution is used to protect a separate network of public IP addresses.  Typically, a third network card is used exclusively for the DMZ network.

  * If you are configuring a few extra public IPs (not a //whole network//), then go to the [[:content:en_us:5_1_to_1_nat|1 to 1 NAT]] section of the User Guide.

  * If you are configuring a separate private network (192.168.x.x or 10.x.x.x), then investigate **Hot LANs** in the [[:content:en_us:5_ip_settings|IP Settings]] section of the User Guide.

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:

<navigation>Network|Firewall|DMZ</navigation>

===== Configuration =====
==== Network Configuration ====
Before you can use the DMZ firewall configuration, you need to configure one of your network cards with the DMZ role.  In our example, we used the  [[:content:en_us:5_ip_settings|network settings tool]] to configure a third network card (eth2) with the following:

  * Role: DMZ
  * IP Address: 216.138.245.17
  * Netmask: 255.255.255.240
  * Network: 216.138.245.16/28

All the systems connected to this third network card can then be configured with an IP address in the 216.138.245.18 to 216.138.245.30 range.

==== Incoming Connections ====
By default, all inbound connections from the Internet to systems on the DMZ are blocked (with the exception of the ping protocol).  You can permit connections to systems on the DMZ by allowing:

  * all ports and protocols to a single public IP
  * all ports and protocols to the whole network of public IPs
  * a specific port and protocol to a single public IP

==== Pinhole Connections (DMZ-to-LAN) ====
{{keywords>clearos, clearos content, DMZ Firewall, app-dmz, clearos5, userguide, categorynetwork, subcategoryfirewall, maintainer_dloper}}
