===== Updating your ClearBOX 300 to ClearOS 7 =====
There are two main ways to update your ClearBOX 300 to ClearOS 7. Both options require a full reinstall of your system so backing up your data on ClearOS is vital to a successful strategy.

===== ISO for USB or DVD =====
The first method is to use the typical ISO image for ClearOS 7. Performing this install will typically require a USB DVD drive or ClearOS imaged to a USB thumb drive. Both options will consume the USB port on your ClearBOX system which will leave only one port for input devices. You can either use the tab and arrow keys with a single keyboard attached or you can use a USB hub for your input devices in order to use both a mouse and keyboard.

For the layout of your disk we recommend that you use the Bind Mount Guide to allocate partitions to your disk.

===== DD Image from FTP =====
An image exists that should allow you to create a USB install image that can be committed to a USB disk. This image is similar to that Compact Flash method that may have been included with your ClearBOX previously.

When using this method, remove the Compact Flash from your ClearBOX. You will then download the ClearBOX 7 image from:

  * ftp1.clearos.com
  * USERNAME: pub
  * PASSWORD: pub
  * Default Directory: /pub
  * Directory of File: /pub/ClearBOX/ClearBOX-7-USB-Image
  * Filename: ClearBOX7.img

Drill down through the directory and find the .img file for ClearBOX download this file and then use the 'dd' command in your existing ClearOS server to commit it to disk. You need to determine which disk is your USB drive. Be sure that it is correct so you don't kill your production data:

  dd if=/root/ClearBOX7.img of=/dev/sdc bs=1024000

