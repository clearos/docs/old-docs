===== Meta Tags =====
The **core/meta.php** can be used to add any <meta> tags required in your theme.

===== Hook =====
The theme engine loads the **core/meta.php** file in your theme directory and expects to find the **theme_page_meta_tags** hook defined.  Your hook should return any <meta> tags that you require.

===== Examples ===== 
The following returns the old 4.01 transitional doctype.

<code php>
function theme_page_meta_tags()
{
    return "
       <!-- Meta tags -->
       <meta charset='utf-8'>
       <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    ";
}
</code>



{{keywords>clearos, clearos content, dev, theming, maintainer_dloper, maintainerreview_x, keywordfix}}
