{{ :userguides:master_slave.svg?80}}
<WRAP clear></WRAP>

===== Master/Slave Synchronization =====
Master/Slave Synchronization provides the necessary tools to manage users, groups, passwords and other account information across a distributed infrastucture serving different geographic locations.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/system/master_slave|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_master_slave|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_master_slave|here]].
==== Additional Notes ====
