===== Using Apache Directory Studio with Samba Directory - Samba 4 =====
The Samba Directory / Samba 4 AD DC includes an LDAP implementation.  In other words, you can connect, view and manage the Samba Directory via a graphical LDAP client.  This guide shows you how to connect to your Samba Directory using the [[http://directory.apache.org/studio/|Apache Directory Studio]] (available for Windows, Mac and Linux).

===== Configuration =====
==== Connect ====
Head over to the [[http://directory.apache.org/studio/|Apache Directory Studio]] and install the latest version for your operating system.  

  * Start up Apache Directory Studio
  * In the menu, click on <navigation>LDAP|New Connection</navigation>
  * Enter the following details:
    * Connection Name: **pick a name**
    * Hostname: **the IP or hostname of the ClearOS system**
    * Port: **389**
    * Encryption Method: Try **Use StartTLS** first, but fall back to **No Encryption** if the authentication fails (see next section)

Click on <button>Check Network Parameter</button> to sanity check the settings.  You may see a certificate warning - you know the routine.  Continue on to the next step in the wizard.

{{:documentation:clearos_guides:apache_directory_studio_connect.png?400|Connect Apache Directory Studio to Samba 4 on ClearOS}}

==== Authentication ====
Authentication is the next step in the wizard.  Enter the following details:

  * Authentication Method: **Simple Authentication**
  * Bind DN or User: **Adminstrator@example.com** (realm) 
  * Bind Password: **the password you specified to initialize Samba Directory**

Click on <button>Check Authentication</button> to check authentication.  If all goes well, finish up the wizard.

{{:documentation:clearos_guides:apache_directory_studio_authentication.png?400|Authenticate Apache Directory Studio to Samba 4 on ClearOS}}

==== Browse ====
You can now browse around your Samba Directory.  We do not recommend changing your directory using this tool unless you know exactly what you are doing!

{{:documentation:clearos_guides:apache_directory_studio_browse.png?400|Browse Samba 4 on ClearOS using Apache Directory Studio}}

{{keywords>clearos, clearos content, KB Old ClearOS Guides Using Apache Directory Studio with Samba Directory - Samba 4, app_name, xcategory, maintainer_dloper, maintainerreview_x, keywordfix, userguides}}
