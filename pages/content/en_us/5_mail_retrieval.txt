===== Mail Retrieval =====
The **Mail Retrieval** package can conveniently retrieve mail from other servers allowing the centralization of e-mail on a single ClearOS server.

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Server|Mail|Retrieval</navigation>

===== Configuration =====
Any number of servers can be added to the mail retrieval list using the "Add Entry" form.  The interval polling time can be configured from 1 minute up to 3 hours.

  * **Server** - The server name.  For example, gmail.com.

  * **Protocol** - The server protocol.  Currently, POP3, IMAP and APOP protocols are supported.  If you do not know the protocol, you can have the system auto-detect by selecting 'auto'.

  * **Username** - This is the username on the source server.

  * **Password** - This is the password on the source server.

  * **Local User** - This is the username of a mail account configured to receive mail on the server you are configuring.

  * **Keep On Server** - Enable this checkbox to leave a copy of the mail on the server.

  * **Active** - Enable this checkbox to start polling the remote server for mail to fetch.

===== Troubleshooting =====
Have a look at the system logs if you are having problems.  The mail retrieval system (fetchmail) logs information to /var/log/maillog.  Ignore any entries you see similar to:

  Server CommonName mismatch: localhost.localdomain != mail.clearfoundation.com

{{keywords>clearos, clearos content, userguide, Mail Retrieval, app-mail_retrieval, clearos5, categoryserver, subcategorymail, maintainer_dloper}}
