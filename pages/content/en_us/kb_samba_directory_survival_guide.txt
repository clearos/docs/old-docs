===== Samba Directory BETA Survival Guide =====
This guide will help you use the ClearOS Samba Directory BETA in your network. Fundamentally, many things will just 'not work' while Samba Directory completes its beta for ClearOS.

When you install ClearOS and want to set up Samba Directory, you will need to do that almost immediately after you complete the wizard.

===== Best Practices =====
==== File Shares ====
Flexshares don't work with Samba Directory very well at this point, your best bet to getting file shares running is the following:

  * Make sure you are running acl support on your file system
  * Make sure you have set the SeDiskOperatorPrivilege for your management group (ie. Domain Admins)
  * Manually create the share
  * Use the RSAT tools to perform remote administration of the share.

=== ACL Support ===
If you are using the default XFS filesystem included with ClearOS 7 then you do not need to modify anything in order to have acl support. If you are using EXT4, there is ALSO nothing you need to do. If you are using EXT3 for your file system then you will need to modify your /etc/fstab to add acl support for example, if this was the partition where your flexshares are stored then you'd change this:

  /dev/mapper/main-data0  /store/data0            ext3    defaults        1 2

...to this:

  /dev/mapper/main-data0  /store/data0            ext3    defaults,user_xattr,acl,barrier=1    1 2

=== SeDiskOperatorPrivilege ===
The ability to set and manipulate permission in Samba Directory is controlled by the SeDiskOperatorPrivilege parameter. By default, this is unset and you should add the 'Domain Admins' group as having rights to change permissions on drives. To see if this is already set, run the following (replace 'DOMAIN' with your domain):

  net rpc rights list accounts -U'DOMAIN\administrator'

Once you authenticate, you will see the rights associated and will be able to see if the following exists:

  DOMAIN\Domain Admins
  SeDiskOperatorPrivilege

If it doesn't, add SeDiskOperatorPrivilege to the Domain Admins group with the following:

  net rpc rights grant 'DOMAIN\Domain Admins' SeDiskOperatorPrivilege -U'DOMAIN\administrator'

Then re-run the previous command and validate that SeDiskOperatorPrivilege now exists.

=== Create the Share ===
As was mentioned, Flexshares are not going to work properly until a successive beta. To create a share is simple, but you need to have all the previous steps working. 

Make the group in the Active Directory Users and Computers app running from your RSAT tool kit. For example, make a group called 'Billing Staff'.

Make this directory (for example 'billing'):

  mkdir /var/flexshare/shares/billing


Backup your smb.conf file:

  cp /etc/samba/smb.conf /etc/samba/smb.conf~

Then modify the file and add some lines like this:

  [billing]
  	path = /var/flexshare/shares/billing
  	read only = No


Change the permissions:

  chown administrator:"Billing Staff" /var/flexshare/shares/billing

=== Computer Management ===
The last step is to define permissions and this is done using the "Computer Management" app which is part of the RSAT tool. From your Windows workstation running the RSAT tools, launch the 'Computer Management' tool. Click on Action >> "Connect to another Computer" then put in the ip address or name of the ClearOS server. If you want, you can even find it on the list.

{{:content:en_us:kb_samba_survival1.png}}

Once the Computer management is open, expand 'System Tools' and 'Shared Folders' and click on 'Shares'. You may encounter this error.

{{:content:en_us:kb_samba_survival2.png}}

Your should see your share on the list of shares. Right-click on your share and choose 'Properties'. Click on the security tab.

{{:content:en_us:kb_samba_survival3.png}}
{{:content:en_us:kb_samba_survival4.png}}

Here you can change the permissions to allow your group to modify files. 

{{:content:en_us:kb_samba_survival5.png}}

You should also secure the 'Everyone' group by removing default permissions.

{{:content:en_us:kb_samba_survival6.png}}

When you are done, click OK. You may get this warning.

{{:content:en_us:kb_samba_survival7.png}}


===== Troubleshooting =====
==== DNS ====
Samba Directory MUST control the DNS. If the Samba service isn't running or will not start, make sure that there isn't a conflict with dnsmasq running the name server. You should have a line that says 'port=0' in /etc/dnsmasq.conf. This should disable the DNS on dnsmasq and allow for DHCP-only operation. 

While it is possible to remove dns on the samba server, it is not recommended because DNS if vital to the operation of AD.

===== Links =====
  * [[https://wiki.samba.org/index.php/User_home_drives|User Home Drives]]