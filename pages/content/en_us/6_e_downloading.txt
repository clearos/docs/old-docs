===== Downloading ClearOS =====
Along with the standard ISO image, ClearOS has specialized downloads available for virtual and cloud environments as well. 

If you just want to kick the tires and give ClearOS a spin, you can use the virtual machine images to get a system up and running. For running ClearOS in a live environment, a full install using the ISO image is strongly recommended (even in a virtual environment). A full install lets you choose disk sizes, partitioning, whole disk encryption and other install-only tunables.

===== Selecting a Download Image =====
There are a number of different virtual machine images available.  You can find more information on the [[http://www.clearcenter.com/Software/clearos-professional-downloads.html|ClearOS Professional Download]] page.

===== Verifying the Download Image =====
To verify the integrity of the download, you can check the MD5Sum of the file.  What is an MD5sum?  To make a long story short, an MD5sum is a simple way to verify the integrity of a downloaded file.

For Windows, you can often use CD burning software to verify an MD5sum.  Alternatively, you can download and install the free [[http://www.md5summer.com/|MD5Summer]] tool.  In Linux, you can use the command line [[http://www.clearfoundation.com/docs/man/index.php?s=1&n=md5sum|md5sum]] command.

===== Next Steps =====
Once you have the download ready, you are ready to begin the install process:

  * [[:content:en_us:6_a_starting_the_install|Starting the Install with the ISO]]
  * [[:content:en_us:6_vmware_enterprise|With VMware Enterprise]] - VMMware vSphere, ESX, ESXi
  * [[:content:en_us:6_vmware_basic|With VMware Basic]] - VMware Server, Player, Fusion (Mac)
  * [[:content:en_us:6_virtualbox|With VirtualBox]]
  * [[:content:en_us:6_virtualpc|With VirtualPC]]

{{keywords>clearos, clearos content, clearos6, userguide, categoryinstallation, subcategorypreparation, maintainer_dloper}}
