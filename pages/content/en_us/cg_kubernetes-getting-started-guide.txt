===== Kubernetes Getting Started Guide =====
This guide will demonstrate, step-by-step, how to setup, scale, and heal a Kubernetes cluster.

We will use ClearGLASS and Cloudify to automate the provisioning of the cluster and to simplify other management capabilities.

==== Step 1: Prepare your ClearGLASS account ====
Estimated time to complete steps: <6 minutes

Create or log in to your ClearGLASS account (ignore if you are already a ClearGLASS user)
From the ClearGLASS console:
Add a cloud (AWS, DigitalOcean, Packet.net, or OpenStack)
Configure SSH keys
==== Step 2: Add the Cloudify Kubernetes blueprint to your ClearGLASS account ====
You will use a Cloudify blueprint to provision the Kubernetes cluster on a public and/or private cloud. The Cloudify blueprint is maintained by ClearGLASS.

Estimated time to complete steps: <1 minute

From within the ClearGLASS console, click Template
Click Add Your Template
Select Catalog Templates
From the dropdown menu, select Kubernetes blueprint
Click Add

[image]

==== Step 3: Provision the Kubernetes cluster ====
Estimated time to complete steps: <1 minute

Provisioning: 10 - 20 minutes (varies per provider)

1. From the Template page, click Create Stack
2. Complete the form:
a. Give the stack a name
b. Select a Cloud
c. Select a Location
d. Select an instance Size
e. Select an image (any modern Linux distro: CoreOS, Ubuntu 16.04, CentOS will work)
f. Select a key for the master and worker nodes
g. Click Create Stack
3. Depending on the cloud provider, it will take 10 - 20 minutes to complete the provisioning and configuration processes
[image]

==== Step 4: Accessing the Kubernetes cluster ====
From the Stack page, navigate to the Output section. The Output section provides:
Kubectl command, user name, and password
Kubernetes Dashboard URL, username, and password

[image]

If you have trouble provisioning or scaling the Kubernetes cluster or have technical questions, contact the ClearGLASS support team for help.