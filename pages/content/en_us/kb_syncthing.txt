2018-02-19: We are currently testing an app, which will replace the manual steps below. Please see: http://wikisuite.org/How-to-install-Syncthing-on-ClearOS


===== Syncthing =====
"Syncthing replaces proprietary sync and cloud services with something open, trustworthy and decentralized. Your data is your data alone and you deserve to choose where it is stored, if it is shared with some third party and how it's transmitted over the Internet."
Source: http://syncthing.net/


==== Install Syncthing on ClearOS ====
=== Big picture steps ==
  - Install Syncthing
  - Open firewall ports for 1- management and 2- data sync
  - Set password for remote management
  - Connect to another device

=== Step by step ===
  - Get epel-release repo <code>rpm -ivh http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm?</code> 
  - Get ok-release repo <code>rpm -ivh http://repo.okay.com.mx/centos/6/x86_64/release/okay-release-1-1.noarch.rpm?</code>
  - Install Syncthing <code>yum install syncthing</code>
  - Open up firewall ports 8384/TCP (or 8080/TCP for older versions), 22000/TCP, and 21025/UDP at https://example.org:81/app/incoming_firewall as per https://github.com/syncthing/syncthing/wiki/Firewalls-and-Port-Forwards
  - Start Syncthing <code>syncthing</code>
  - Permit remote management (ClearOS is a server, and designed to be managed remotely via the GUI, but by default, Syncthing only accepts connections for localhost which makes sense for a desktop or mobile device): replace the gui address (default: 127.0.0.1) with your IP in the config file <code>nano ~/.config/syncthing/config.xml</code>
   - Access Syncthing via https://example.org:8384 (or 8080 for older versions)
    - Set a username and password 
   - To start automatically on boot<code>chkconfig syncthing on</code>
   - Follow Syncthing documentation at https://github.com/syncthing/syncthing/wiki/Getting-Started#configuring

==== Todo ====
  * Check how to get in services list
    * http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,26/func,view/id,60897/
  * Document how to use Firewall to restrict access to certain IPs for management
  * Document for a Flexshare directory 
  * Make an official app for ClearOS
    * The GUI should be at https://example.org:81/app/syncthing and replace what is normally done at http://localhost:8384/ (or 8080 for older versions)

==== Questions ====
  * Can http://www.clearcenter.com/support/documentation/clearos_guides/console_access_to_webconfig be used on http://localhost:8384 or 8080 ?
  * Will Syncthing be ported to ChromeOS and [[https://github.com/syncthing/syncthing/issues/102|iOS]] one day?
  * To manage ClearOS locally, could this work?: [[http://www.clearcenter.com/support/documentation/clearos_guides/console_access_to_webconfig|"The default graphical console that comes with ClearOS is a simplified xWindows session with a very simple browser"]]

==== Long term ====
=== Multi-user Syncthing roadmap ===
Syncthing has no concept of username or groups. It is mostly designed to be installed on diverse client devices for P2P sync.
  * Each user should have access to their Syncthing management panel via https://example.org:81/app/syncthing (like they do for OpenVPN). This would replace what they do locally at http://localhost:8384/ or 8080
  * Each user has their own running instance of Syncthing
   * Device IDs running in ClearOS could be in ClearOS user directory (OpenLDAP) to make it easier to share 
   * Users sharing data should use the ClearOS instance for availability and performance
  * To investigate
   * Setting up own discovery server https://github.com/syncthing/discosrv
    * So all devices "phone home"
   * interop with FlexShare
   * Use [[http://www.faqforge.com/linux/reduce-load-of-backup-scripts-with-nice-and-ionice/|nice and ionice]]
    * https://github.com/syncthing/syncthing/issues/1455#issuecomment-82853884
   * [[https://discourse.syncthing.net/t/v0-9-18-introducer-nodes/974|Introducer Nodes]]
    * Lost device procedure (remove from all sync)
    * Team / group devices / folders perhaps stored in OpenLDAP or wiki pages

==== Radar ====
  * [[https://github.com/syncthing/syncthing/issues/594|Allow file & file versions browsing over WebUI]], hopefully easy interop with Tiki File Gallery
  * [[https://github.com/syncthing/syncthing/issues/415|Folder listing should be a link to open in OS Explorer]]
  * [[https://github.com/syncthing/syncthing/issues/220|Conflict resolution]] (more likely in a disconnected usage scenario)
  * [[https://github.com/syncthing/syncthing/issues/109|File encryption]]
  * [[https://github.com/cebe/pulse-php-discover|A PHP implementation of the pulse/syncthing cluster discovery protocol]]
  * [[https://ind.ie/about/blog/pulse/|Pulse is now an officially-sanctioned fork of Syncthing.]]
  * [[https://github.com/syncthing/syncthing/issues/1167|Deal with nested folders]]

==== Related links====
  * Cross-platform GUI: https://github.com/syncthing/syncthing-gtk
  * https://okay.com.mx/en/linux/rpm-repositories-for-centos-6-and-7.html
  * https://bugzilla.redhat.com/show_bug.cgi?id=1164378
  * https://github.com/thunderbirdtr/syncthing_rpm
  * https://github.com/thunderbirdtr/syncthing_rpm/issues/3
  * http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,13/func,view/id,67950/
  * https://discourse.syncthing.net/t/rpm-package-for-fedora/610
  * https://bugzilla.redhat.com/show_bug.cgi?id=1164378
  * https://suite.tiki.org/Syncthing
  * https://discourse.syncthing.net/t/getting-started/46
  * https://github.com/Nutomic/syncthing-android
   * https://github.com/Nutomic/syncthing-android/issues/12
  * https://wiki.arkos.io/wiki/File_Sync
  * [[https://github.com/syncthing/syncthing/tree/master/gui/bootstrap|Bootstrap GUI]]
  * https://discourse.syncthing.net/t/frequently-asked-questions-faq/405
  * https://discourse.syncthing.net/t/v0-8-10-simple-file-versioning/259
  * https://discourse.syncthing.net/t/read-only-setting/57
  * https://discourse.syncthing.net/t/the-rest-interface/85/2
  * https://discourse.syncthing.net/t/why-is-syncthing-written-in-go/675
  * https://discourse.syncthing.net/t/config-file-and-directory/204
  * https://discourse.syncthing.net/t/why-is-the-setup-so-complicated-in-comparison-to-btsync/444
  * https://discourse.syncthing.net/t/getting-started/46
  * https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-syncthing-to-synchronize-directories-on-ubuntu-14-04
  * https://f-droid.org/forums/topic/syncthing-for-android/
  * https://issues.kolab.org/show_bug.cgi?id=3672
  * http://www.webupd8.org/2014/12/syncthing-gtk-gets-https-support.html
  * [[http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,13/func,view/id,67950/|ClearOS forums: Syncthing: File Sync & backups, FLOSS, cross-platform, and awesome!]]
  * https://discourse.syncthing.net/t/progress-on-rpm-package-review-for-inclusion-in-epel-comments-and-advice-please/1824

{{keywords>clearos, kb, howtos, tiki, skunkworks, maintainer_dloper, maintainerreview_dloper}}
