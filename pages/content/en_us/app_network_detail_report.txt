{{ :userguides:network_detail_report.svg?80}}
<WRAP clear></WRAP>

===== Network Detail Report =====
The Network Detail Report app provides a convenient report format for administrators to quickly determine top consumers of bandwidth.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/reports/network_detail_report|here]]
===== Documentation =====
==== Version 6 ====
<note important>
This app has been withdrawn due to performance issues.  It is highly recommended that current users uninstall this app.
</note>

Documentation for ClearOS 6 can be found [[content:en_us:6_network_detail_report|here]].
==== Version 7 ====
This app is not available for ClearOS 7.
==== Additional Notes ====
