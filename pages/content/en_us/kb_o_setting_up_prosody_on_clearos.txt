===== Setting Up Prosody on ClearOS =====
<note>This document is a work in progress. Please check back regularly. It works as is, and we want to make it even easier to install and to eventually become a package.</note>

Prosody is a XMPP service that works well on ClearOS. This guide will step you through the process of setting it up on ClearOS. This guide is intended for testers of Prosody ahead of its eventual integration to ClearOS. It is also intended to work as a general guide for those wishing to install Prosody on architectures similar to ClearOS.

===== Installation =====
Prosody is currently in the EPEL repository and requires dependencies from the ClearOS Core repository. Run the following to install Prosody:

  yum --enablerepo=clearos-core,clearos-epel install prosody

This should give you some of the following:

<code>
================================================================================
 Package              Arch        Version               Repository         Size
================================================================================
Installing:
 prosody              x86_64      0.8.2-7.el6           clearos-epel      184 k
Installing for dependencies:
 lua-dbi              x86_64      0.5-5.el6             clearos-epel       21 k
 lua-expat            x86_64      1.3.0-1.el6           clearos-epel       30 k
 lua-filesystem       x86_64      1.4.2-1.el6           clearos-epel       24 k
 lua-sec              x86_64      0.4.1-2.el6           clearos-epel       17 k
 lua-socket           x86_64      2.0.2-4.el6           clearos-epel       83 k
 postgresql-libs      x86_64      8.4.20-1.el6_5        clearos-core      201 k
</code>

It should be noted that this process sets up certificates and keys in your SSL store located at:

  /etc/pki/tls/certs/prosody.crt
  /etc/pki/tls/private/prosody.key

===== Starting Prosody =====
Prosody will install disabled by default. The following will cause Prosody to automatically start when the system is booted:

  chkconfig prosody on && chkconfig --list prosody 

This should show the following:

  prosody        	0:off	1:off	2:on	3:on	4:on	5:on	6:off

To start Prosody run:

  service prosody start

At different points in this configuration we will be making changes to configuration files. You will want to restart Prosody after making changes. To restart Prosody, run the following:

  service prosody restart

Once you restart Prosody, check the log for errors (which you won't be able to do until you complete the next section.)  

===== Debugging Prosody =====
To turn on logging for Prosody (recommended), make the following edit to the /etc/prosody/prosody.cfg.lua from:

<code>
        -- error = "prosody.err";
        "*syslog"; -- Uncomment this for logging to syslog
        -- "*console"; -- Log to the console, useful for debugging with daemonize=false
</code>

To: 

<code>
        -- error = "prosody.err";
        --"*syslog"; -- Uncomment this for logging to syslog
        debug="/var/log/prosody/prosody.log";
        -- "*console"; -- Log to the console, useful for debugging with daemonize=false
</code>

The reason for removing this logging is that ClearOS runs Prosody using the 'prosody' user account. This account does not have permissions currently to modify the existing log files. In addition to making this change, perform the following:

  mkdir /var/log/prosody
  chown prosody:prosody /var/log/prosody

<note warning>The debug log of prosody can get very, very big. Be sure to remove it and use the info log instead after you've worked out the bugs!</note>

To remove the debug log and replace it with the info log change these lines:

<code>
-- info = "prosody.log"; -- Change 'info' to 'debug' for verbose logging
debug="/var/log/prosody/prosody.log";
</code>

For these lines:

<code>
info = "/var/log/prosody/prosody.log"; -- Change 'info' to 'debug' for verbose logging
-- debug="/var/log/prosody/prosody.log";
</code>



===== Domain Configuration =====
To make Prosody work with your domain, you will need to define the domain as a 'VirtualHost' in the configuration file /etc/prosody/prosody.cfg.lua near the bottom of the file.

Simply add an additional domain above the **Components** section, for example:

<code>
VirtualHost "clearos.com"
        ssl = {
                key = "/etc/pki/tls/private/prosody.key";
                certificate = "/etc/pki/tls/certs/prosody.crt";
        }
</code>

===== Users =====
If you are going to allow anonymous creation of accounts or if you are going to manually create users within Prosody alone, please continue. If you are going to tie the Prosody service into the existing directory system, please [[:content:en_us:kb_o_configuring_prosody_to_work_with_pam_and_the_directory_behind_it|go to this guide]] and skip this section entirely.

==== Manual User Registration ====
If you are not doing integrated authentication with your directory, you can add users with the following:

  prosodyctl adduser mschmoe@clearos.com

It will have you specify a password for this user

  Enter new password: 
  Retype new password: 

Creating a user creates a plain text file in /var/lib/prosody/clearos%2ecom/accounts/ by that username. For example:

  cat /var/lib/prosody/clearos%2ecom/accounts/mschmoe.dat

Shows:

<code>
return {
	["password"] = "asdfasdfasdf";
</code>

==== Anonymous Registration ====
To enable anonymous registrations, change the **allow_registration** parameter in the /etc/prosody/prosody.cfg.lua configuration file.

  allow_registration = false;

To:

{{keywords>clearos, clearos content, kb, howto, Setting Up Prosody on ClearOS, version6, maintainer_dloper, categoryserver, subcategorycommunications}}
