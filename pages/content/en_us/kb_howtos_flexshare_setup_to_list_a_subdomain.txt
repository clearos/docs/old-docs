===== Flexshare Setup to List a SubDomain =====
Purpose of this How-To is to create a subdomain access via FlexShare.  There is a bit of confusion when creating this as the Webserver and FlexShare can conflict if not done properly. 

Enjoy. 

==== Step 1 ====

<note> ClearOS offers a very good summary on how to create a website you can find it  
[[http://www.clearcenter.com/support/documentation/clearos_enterprise_5.1/user_guide/web_server|here:]] </note>

1) Create a website under webserver.
 
2) Create something like subdomain.domain.ca

3) Modify the settings to suit 

4) Remove the "*." before the subdomain.domain.ca

==== Step 2 ====

1) Under your DNS record, I'm assuming you are using ClearSDN.
 
2) Create a subdomain under DNS -- DNS/Domain Summary 

3) Click the View Button - right top side of the subform 

4) Create a subdomain and link it to your domain

<note warning> There can be a 5 minute to 24 hour delay before your domain is active.  So if it doesn't work right away please be patience. </note>

==== Step 3 ====

<note> CLearOS provides a nice summary page on how to create a Flexshare, and you can read it here; 
http://www.clearcenter.com/support/documentation/clearos_enterprise_5.1/user_guide/flexshares</note>


1) Now Create a FlexShare 

2) Under Server --- File and Print ---- FlexShare 

3) You want to create the FlexShare the same name as the website you just created. 

4) Provide a good description of what the FlexShare will be used for. 
Click Add

5) This will open a new page, that will all you to set the different settings of the FlexShare, but we're wanting to focus on the "WEB" tab for now. 

<note> the purpose when I did this was to provide a more "Fancy" index looking than the standard one that comes with appache.   You can find the preview here 
http://reposstyle.com/  I used this for my FlexShare that will allow a users (Or users) to FTP to the FlexShare upload what they need, then allow others to download what they want.</note>

6) You can modify the settings as necessary. 

7) Click on the Enable button

8) Click on the Enable under the General Form 

9) You're all done, 

you should now be able to access your http://subdomain.domain.ca  

===== Other files of Interest =====

<note> if you like what you see from http://reposstyle.com/, let me know in the forms and I can provide you a detail how-to to add this function to ClearOS.</note>

<note important>You will need SSH access to ClearOS in order to modify or change the following files</note>

<note warning>Depending on the setting a specific file may need to be remove in order for the domain to appear properly.   This is where the webserver and FlexShare conflict, but you need both in order to have the subdomain.doamin to work properly.</note>

<note tip>Please remove this file and your FlexShare should work as indented - You don't have to delete it simply rename is or move it.</note>

      /etc/httpd/conf.d/welecome.conf

ClearOS will create the following files automatically. 

==== flex-80.conf ====

      /etc/httpd/conf.d/flex-80.conf
      
<note>This file will be earse and recreate everytime you create or modify a flexShare.  So if you change the FlexShare you may need to add your modification after you create or change any FlexShare</note>
      
<note>I'm hoping after this that some crazy smart Linux guy ... or gal will provide a way that we can make this perminate </note>
      
==== subdomain.domain.ca.vhost file ====

ClearOS automatically creates this file when you create a virtual website.       
      
      /etc/httpd/conf.d/subdomain.domain.ca.vhost

<note>This file is created via webserver and provides the necessary information to change or modify the settings for that specific virtual site. </note>
      
<note>You can also change the document root within this file. For example if you want the virtual website to link to the FlexShare Folder instead of the www folder you can do it here too.</note>


<note> You can find the Form Post  [[http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,13/func,view/id,25840/|here:]] - I'll add all the fancy Stuff soon</note>
 

{{keywords>clearos, clearos content, app-flexshare, troubleshooting, clearos6, categoryserver, howto, maintainer_dloper}}
