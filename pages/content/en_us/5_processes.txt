===== Processes =====
The **Processes** page provides an overview of processes running on your ClearOS system.  Among other uses, you can use this tool to view which processes are consuming the most system resources.

===== Installation =====
This feature is part of the core system and installed by default.

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>System|System Resources|Processes</navigation>
{{keywords>clearos, clearos content, AppName, app_name, clearos5, userguide, categorysystem, subcategorysystemresources, maintainer_dloper}}
