===== SMTP Server =====
The **SMTP Server** app (postfix) lets you can manage your own SMTP mail server and is part of the ClearOS on-premise / private cloud mail stack.  Using a private SMTP Server has several advantages:

  * Full control
  * Privacy
  * Regulatory and compliance control
  * Mailboxes limited only by hardware capacity
  * Custom [[Mail Antispam|antispam]] and [[Mail Antivirus|antimalware]] control

For those of you looking for a public cloud-based solution, the ClearOS [[:content:en_us:7_ug_google_apps_synchronization|Google Apps Synchronization]] solution is also available.

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Server|Mail|SMTP Server</navigation>

===== Configuration =====
==== SMTP Mail Configuration ====
=== Settings ===
The **Domain** field indicates the domain name this server will act as an SMTP/Mail server for.  If you have a single domain name that you receive mail for, enter the domain here.

The **Hostname** does not have to be related to the e-mail domains that you host. It can be ANY valid Internet name for your machine.   For example, you may wish to have a dedicated mail server on your network.  In this case, you might want to name this machine **mail.yourdomain.com**.  This would be the Hostname you would enter.

Some ISPs will block all traffic on port 25 unless it it destined for their mail servers.  In this case, you would want to specify your ISPs mail server as the **Relay Host**. If your ISP requires authentication, see [[content:en_us:kb_howtos_smtp_authentication_to_isp|this HowTo]].

Setting a **Maximum Message Size** is a good idea to limit users from trying to really large attachments through your mail server.

==== User Policies ====

If **SMTP Authentication** field is set to **on**, any client attempting to send mail through the server with a valid username/password will be permitted.

Admins are advised to block clear-text passwords from being transferred over public networks by enabling **Block Clear-Text Passwords**.  Enabling this feature will effectively force a mail client to use TLS/SSL encryption //before// sending user credentials over the network.

==== Destination Domains ====
If your company/organization has multiple domains and you wish to receive email sent to any user for any of the domains, enter additional domains to the **Destination Domains** list.  For example, if our primary domain was setup to be "clearfoundation.com" and we wanted all emails sent to the following registered domains to be valid:
  * clearos.com
  * clearfoundation.net

We would add the domain list above to the "Destination" domains list.

==== Trusted Networks ====
By default, SMTP Authentication is required to send mail through the SMTP Server.  This is considered a good security practice that helps prevent virus-infected mail clients from sending spam messages through your mail server.  In some circumstances - for example embedded electronic devices - it may be necessary to allow non-authenticated SMTP connections.  If this is the case, you can add the IP or network address of the system to the list of **Trusted Networks**.

=== Virtual Domains ===
Virtual domains are not supported; [[http://www.clearfoundation.com/docs/articles/support_for_multiple_mail_domains_in_clearos|here's why]].

==== Mail Forwarding ====
If the server you are configuring your server as a mail gateway, add the domain name to the "Mail Forward Domain list".  If the [[:content:en_us:7_ug_mail_antispam|Mail Antispam]] module is installed and running on the server, mail will be subject to the spam identification rules you have configured.  Similarly, if the [[:content:en_us:7_ug_mail_antivirus|Mail Antivirus]] module is installed and running, all mail for the domains will be scanned before passing the mail on to the destination server.

<note important>Ensure your domain is *not* set as the Mail Hostname, Mail Domain or listed in the Destination Domains.  If it is, mail will attempt to be delivered locally instead of forwarding to the final destination MTA.  For Mail Hostname and Mail Domain, you can use a subdomain (eg. mailgateway.example.com) to avoid this conflict.</note>

===== Security Considerations =====
If you have SMTP Authentication enabled and incoming port 25 open, then you are more exposed to user/password hacking so strong passwords are highly recommended.
By way of mitigation, it is recommended to install the [[content:en_us:7_ug_attack_detector|Attack Detector app]] and enable it for postfix-sasl. The [[content:en_us:7_ug_intrusion_protection_updates|IPS Updates]] enabled for SMTP are also recommended.\\

There is a further feature you can use. You can turn off Authentication (which only affects port 25) and still authenticate using SMTPS (SMTP/SSL) on port 465. All you need to do is open the port. The advantage of this is that there seems to be less user/password attacking on port 465.

If you choose to use SMTPS (SMTP/SSL), to avoid certificate warnings, you may want to install a [[content:en_us:7_ug_lets_encrypt|Let's Encrypt Certificate]] and configure the SMTP server to use it by following [[content:en_us:kb_howtos_using_letsencrypt_certificates_for_mail|this HowTo]].

===== E-mail Client Set Up =====
=== SMTP Authentication - Thunderbird ===
For Mozilla's Thunderbird, click on <navigation>Tools|Account Settings</navigation> and then click on the **Outgoing Server (SMTP)** field.

Ensure the **Use name and password** setting is checked and enter the username of the mail account in the username field.  The password will be requested by the mail client application on the first attempt to send mail.  There will be an option to save it to the **Password Manager** so that you do not have to enter each time you send mail through the server.

=== SMTP Authentication - MS Outlook/Outlook Express ===
If you are using MS Outlook/Outlook Express, click on <navigation>Tools|Accounts</navigation>.  Select the account which will use this mail server to send mail and click on the <button>Properties</button>.

Make sure the **My server requires authentication** is checked.  Click on the <button>Settings</button> button to enter the details of your username/password.

<note>Outlook will give certificate warning every time at start up if you use the built-in certificate. The suggested work around is to use a Let's Encrypt certificate as detailed further up this page</note>

===== Troubleshooting =====
==== Firewall ====
Do not forget to open up firewall ports for your e-mail server: port 25 on the firewall configuration page.

==== ISP Blocking ====
Some ISPs are known to block SMTP (port 25) traffic to residential broadband connections in an attempt to cut down on SPAM originating from their network.  If you think your configuration is set-up correctly and you suspect your ISP is blocking SMTP traffic, try a port scan.

==== Relay Access Denied ====
If you are see a //relay access denied// error in your mail client, double review the **SMTP Authentication** and **Trusted Network** settings described above.

===== Links =====
  * [[:content:en_us:7_ug_antimalware_updates|Antimalware Updates]]
  * [[:content:en_us:7_ug_antispam_updates|Antispam Updates]]
  * [[:content:en_us:7_ug_gateway_antivirus|Gateway Antivirus]]
  * [[:content:en_us:7_ug_greylisting|Greylisting]]
  * [[:content:en_us:7_ug_mail_antispam|Mail Antispam]]
  * [[:content:en_us:7_ug_mail_antivirus|Mail Antivirus]]
{{keywords>clearos, clearos content, SMTP Server, app-smtp, clearos7, userguide, categoryserver, subcategorymessaging, maintainer_bchambers}}
