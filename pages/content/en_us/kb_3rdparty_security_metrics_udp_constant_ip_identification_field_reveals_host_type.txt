===== UDP constant IP Identification field reveals host type =====
This entry from Security Metrics is followed up with the following CVE: [[:content:en_us:announcements_cve_cve-2002-0510|CVE-2002-0510]].


===== ClearCenter response =====
==== Short response ====
ClearCenter does not consider this to be a security bug.

==== Long response ====
There are multiple ways to mask your version of Linux and various underlying services. ClearCenter does not consider this to be particularly critical nor that it provides a specific advantage to a hacker.

==== Resolution ====
No action required. See [[http://www.infosecwriters.com/text_resources/pdf/nmap.pdf]]

===== Links =====

  * [[http://cve.mitre.org/cgi-bin/cvename.cgi?name=2002-0510|MITRE.org CVE database]]
{{keywords>clearos, clearos content, 3rd party, security metrics, non-cve, maintainer_dloper}}
