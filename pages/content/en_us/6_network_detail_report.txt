===== Network Detail Report =====
The **Network Detail Report** provides a historical view of network information per IP, user and device types.  This report is invaluable for monitoring network usage and abuse.

===== Installation =====

<note important>
This app has been withdrawn due to performance issues. It is highly recommended that current users uninstall this app.
</note>
This application has been withdrawn and cannot be installed from the Marketplace.

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Reports|Network|Network Detail Report</navigation>

===== Report =====
Depending on how you have deployed ClearOS, the **Network Detail Report** may show the following information:

  * Top IPs
  * Top Device Types
  * Top Users
  * Top External IPs

For standalone systems, only the **Top External IPs** report is available.  For gateway systems that are //not// running the [[:content:en_us:6_network_map|Network Map]] app, only the **IP** reports are available.

{{:omedia:network_detail_report.png|ClearOS Network Detail Report}}

===== Links =====
  * [[:content:en_us:6_network_report|Network Report]]
  * [[:content:en_us:6_network_visualiser|Network Visualiser]]
  * [[:content:en_us:6_resource_report|Resource Report]]
  * [[:content:en_us:6_bandwidth_manager|Bandwidth Manager]]
{{keywords>clearos, clearos content, AppName, app_name, clearos6, userguide, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}

{{keywords>clearos, clearos content, Network Detail Report, app-network-detail-report, clearos6, userguide, categoryreports, subcategorynetwork, maintainer_dloper}}