{{ :userguides:software_repository.svg?80}}
<WRAP clear></WRAP>

===== Software Repository =====
A list of repositories available to the server. A software repository is a storage location where updates and new packages can be downloaded and installed on the server.  Apps available in the Marketplace are dependant on which repositories are enabled.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/cloud/software_repository|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_software_repository|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_software_repository|here]].
==== Additional Notes ====
