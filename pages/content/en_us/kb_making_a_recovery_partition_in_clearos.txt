===== Making a Recovery Partition in ClearOS =====
/**
 * Comments can be given in this format.
 */

/* Comments can also be given in this format */
This guide will help you create a custom recovery partition for rapid recovery of ClearOS. This can be used to recover ClearOS and using this method, you can use this to reinstall ClearOS at a future date or to just automate the install process. Included in this guide is a template for the kickstart process that will give you the ability to automate your install process.

===== Deciding How You Will Perform This =====
There are a few steps that you will need. The basic layout is this:
  * Booting to an OS that will allow you to manipulate the disk
  * Formatting the correct target disk or partition
  * Laying down the OS and install assets
  * Making the target bootable
  * Booting into the recovery partition (optional, UEFI only)

===== Manipulate the Disk =====
===== Formatting Disk or Partition =====
===== Install OS and Assets =====
===== Making the Target Bootable =====
===== Booting the Recovery Mode =====

{{keywords>clearos, clearos content, deployment, clearos7, maintainer_dloper}}
