===== Microsoft SQL Server =====
MS SQL Server is a version of Microsoft's relational database management system (RDBMS) that was ported and made available to Linux operating systems in 2016.

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:7_ug_marketplace|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Server|Database|MS SQL Server</navigation>

===== Configuration =====
==== EULA ====
The first thing you will see is Microsoft's EULA and a form to initialise the SQL server's admin password.


{{keywords>clearos, clearos content, MSSQL, app-mssql, clearos7, userguide, categoryserver, subcategorydatabase, maintainer_dloper}}
