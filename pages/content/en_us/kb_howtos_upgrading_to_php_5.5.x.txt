===== Upgrading to PHP 5.5.x in ClearOS 6 =====
This is experimental and assumes you are already familiar with ClearOS. This will upgrade all your server, so only use if all your PHP apps support PHP 5.5  You should test this on a backup, as there is no rollback to PHP 5.3

<note warning>This will break your Zarafa instance and you will get the following error message: "Not Found: PHP mapi extension not found". In a quick test, WordPress, Joomla! and Tiki worked fine.</note>

Special thanks to [[http://blog.famillecollet.com/|Rémi Collet]]

====== General steps to get PHP 5.3 and MySQL working ======
You can skip some of these steps if you are deploying on an existing system which already has PHP & MySQL.
  * [[http://www.clearcenter.com/support/documentation/clearos_install_guide/linode|Install ClearOS 6.5 from Linode]]
  * You'll want to set a real domain name (subdomain is fine)
  * Get PHP (Web server) and MySQL from the marketplace
  * Initialize OpenLDAP with visiting PHP (Web server) 
  * Incoming Firewall: Open HTTP & HTTPS
  * Start MySQL and set root password 
  * Set default site

====== Get up to date ======
  * Activate the [[http://www.clearcenter.com/support/documentation/user_guide/software_repository|software repository app]] to make it easy to track 
  * Activate the clearos-updates-testing repo to get latest code, with revamped webserver in 6.6 Beta 1 & Beta 2
  * Launch software updates (about 40 updates happen)


====== Check current version of PHP ======
<code>
cd /var/www/html
wget --output-document=tiki-check.php https://svn.code.sf.net/p/tikiwiki/code/trunk/tiki-check.php
</code>
visit example.org/tiki-check.php to check PHP version. You will be at 5.3.x

====== Adding Remi's key ======
<code>
cd /root
wget http://rpms.famillecollet.com/RPM-GPG-KEY-remi
rpm --import RPM-GPG-KEY-remi
</code>

====== Getting RPMs from Remi ======
<code>
cd /root
wget http://dl.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
wget http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
rpm -Uvh remi-release-6*.rpm epel-release-6*.rpm
</code>

====== Updating to PHP 5.5 ======
<code>
yum --enablerepo=remi-php55,remi,clearos-core update php*
</code>

You will see a lot of warnings. At this point, PHP 5.5 is installed but you need to reboot server. Just restarting Apache is not enough.

====== Reboot ======
<code>
reboot
</code>

====== Check Web PHP ======
visit example.org/tiki-check.php to confirm you are now at PHP 5.5.x

====== Check CLI ======
<code>
# php -v
PHP 5.5.14 (cli) (built: Jul 16 2014 11:10:19)
Copyright (c) 1997-2014 The PHP Group
Zend Engine v2.5.0, Copyright (c) 1998-2014 Zend Technologies
</code>

====== Get OPcache ======
<code>
yum install --enablerepo=remi,remi-php55 install php-opcache
service httpd restart
</code>

====== Todo ======
  * Make sure .htaccess works (same issue as with PHP 5.3) -> http://tracker.clearfoundation.com/view.php?id=1661

====== Related links  ======
  * http://blog.famillecollet.com/pages/Config-en
  * http://blog.famillecollet.com/post/2014/06/27/PHP-5.4.30-and-5.5.14
  * http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,17/func,view/id,61167/
  * http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,17/func,view/id,62573/
  * http://tracker.clearfoundation.com/view.php?id=1864
  * https://forums.zarafa.com/showthread.php?10683-Is-it-possible-to-run-Zarafa-without-the-PHP-mapi-extension
{{keywords>clearos, clearos content, php, clearos6, howto, maintainer_dloper}}
