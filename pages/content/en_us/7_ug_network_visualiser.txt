===== Network Visualiser =====
The Network Traffic tool is used to view network utilization on your ClearOS system.  This can be useful for:

  * Locating virus infected desktops sending out spam, viruses, etc
  * Pinpointing individuals abusing the network, e.g. downloading movies
  * Checking for unusual network activity

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:

<navigation>Network|Bandwidth and QoS|Network Visualiser</navigation>

===== Configuration =====
==== Update Interval ====
Use the **update interval** to change the frequency for collecting network data.

==== Interface ====
Select the network interface to monitor.

==== Display ====
You can view either the **total bandwidth** over the time interval, or the **total network transfer**.

==== Report Format ====
Three report types are available
  * Tabular
  * Tabular (detailed)
  * Graphical

The tabular formats are useful for determining specific clients and the applications that may be consuming a lot of bandwidth on your network.  The graphical report summarizes data coming and going from specific devices on your network.  The resulting list is sorted by top bandwidth usage and the top 10 highest consumers of bandwidth are displayed.
=== Sample Table Report ===
{{:omedia:network_traffic.png|ClearOS Network Traffic Monitor}} 

=== Sample Graphical Report ===
{{:omedia:ss-network-visualiser-graph.png|}}

===== Links =====
  * [[:content:en_us:6_bandwidth_manager|Bandwidth Manager]]
  * [[:content:en_us:6_network_report|Network Report]]
{{keywords>clearos, clearos content, Network Visualiser, app-network-visualiser, clearos7, userguide, categoryreports, subcategoryperformanceandresources, maintainer_bchambers}}
