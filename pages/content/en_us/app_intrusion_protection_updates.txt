{{ :userguides:intrusion_protection_updates.svg?80}}
<WRAP clear></WRAP>

===== IDS Signatures =====
Intrusion Protection takes an active role at the edge of your network. It detects attempts to gain access to your system by known exploits and then pro-actively firewalls your server from the perpetrator.  Having additional attack vector signatures in additional to continual updates is essential in deploying a security perimeter for your network.

The Intrusion Protection Signatures and Updates app provides over 12,000 additional signatures and continual weekly updates.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/cloud/intrusion_protection_updates|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_intrusion_protection_updates|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_intrusion_protection_updates|here]].
==== Additional Notes ====
