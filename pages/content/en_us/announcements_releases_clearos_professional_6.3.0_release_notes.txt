===== ClearOS Professional 6.3.0 Final Release Notes =====
**Released: June 27, 2012**

ClearOS Professional 6.3.0 has arrived!  Along with the usual round of enhancements, this release introduces new apps focused on the mail server stack.  Anchored on the Zarafa for ClearOS Professional solution, you can now implement an on-premise or private cloud mail server with Outlook/Exchange support.  This release includes the following new apps: [[http://www.clearcenter.com/Software/zarafa-collaboration-platform.html|Zarafa Professional for ClearOS]], Mail Antivirus, Mail Antispam,  [[http://www.clearcenter.com/Services/clearsdn-antispam-updates-8.html|Antispam Updates]], Greylisting, [[http://www.clearcenter.com/Software/antimalware-premium.html|Mail Antimalware Premium powered by Kaspersky]], [[http://www.clearcenter.com/Software/antimalware-premium.html|Gateway Antimalware Premium powered by Kaspersky]] and more.

===== What's New in ClearOS Professional 6.3.0 =====
Along with updating the base system built on source code from Red Hat Enterprise Linux ((Trademark is the property of the respective owner.  ClearFoundation is not affiliated with Red Hat.)) 6.3, this release includes the following major changes.  

  * Zarafa Community for ClearOS
  * [[http://www.clearcenter.com/Software/zarafa-collaboration-platform.html|Zarafa Professional for ClearOS]]
  * Basic POP/IMAP Server (beta)
  * [[http://www.clearcenter.com/support/documentation/marketplace/smtp_server|SMTP Server]]
  * [[http://www.clearcenter.com/support/documentation/marketplace/greylisting|Greylisting]]
  * [[http://www.clearcenter.com/Software/antimalware-premium.html|Mail Antimalware Premium powered by Kaspersky]]
  * [[http://www.clearcenter.com/support/documentation/marketplace/mail_antivirus|Mail Antivirus]]
  * [[http://www.clearcenter.com/support/documentation/marketplace/mail_antispam|Mail Antispam]]
  * [[http://www.clearcenter.com/Services/clearsdn-antispam-updates-8.html|Antispam Updates]]
  * [[http://www.clearcenter.com/Software/antimalware-premium.html|Gateway Antimalware Premium powered by Kaspersky]]

===== Mail Stack Configuration =====
The base mail domain is used by a number of apps: SMTP Server, IMAP, Mail Filter, etc.  Each app saves this information in its own configuration file, but we really only want to see the domain in one place in the user interface.  That's where the **Mail Settings** app comes in.  This might not be the way we'll do it in the final release, but that's how it is done right now:

  * Go to <navigation>System|Account Manager|Account Manager</navigation> to make sure the accounts system (users/groups) is running.

  * Go to <navigation>System|Settings|Mail Settings</navigation> to set the mail domain.

  * Go to <navigation>System|Accounts|Users</navigation> to create a few users.  Please make sure the **SMTP Server User** and **Zarafa Account** are enabled for the user.  Feel free to fill out the contact information too - this information is shown in Zarafa's address book! 

  * Go to <navigation>System|Accounts|Groups</navigation> to create a group or two.  You will see a new option when adding/editing a group: **Distribution List**.  If you leave this option enabled, the group becomes a mail distribution list (e.g. sales@example.com will go to all the members of the sales group).
   
  * Go to <navigation>Server|Messaging and Collaboration|Zarafa Professional for ClearOS</navigation> and start all the servers (if not running).

That's it.  You should be able login to the Zarafa web client with your user account credentials:

  * Go to https://w.x.y.z/webaccess to access Zarafa's web client

===== Download =====
To be released.

===== Known Issues =====
  * The last step in installer may take a couple of minutes to complete

===== ClearOS 5.2 Differences =====
For ClearOS 6, the barrier for having an app available in the web interface was raised.  If the quality or features in ClearOS 5.2 were not up to the standards of a modern small business server/gateway, the feature was either upgraded or deprecated.

==== Zarafa for ClearOS ====
The default groupware and mail solution is now based on [[http://www.zarafa.com|Zarafa]] technology.

==== Horde/Kolab ====
Horde/Kolab has been deprecated and is no longer supported.  This policy may change in a future ClearOS 6 release.

==== Reports ====
The reporting engine in ClearOS 6 is being completely overhauled.  An administrator does not want to sift through 5 different reports to see what a particular user was doing on the network.  Instead, a unified reporting tool is under development.  The reports in version 5.x have been deprecated.

==== Advanced Firewall ====
The Advanced Firewall app was marked as deprecated in version 5.2.  It is no longer available.  The [[http://www.clearcenter.com/support/documentation/marketplace/custom_firewall|Custom Firewall]] can be used as an alternative.

==== Administrators ====
The Administrators app does not fit into the group-based policy engine in ClearOS 6.  The underlying mechanism that existed in ClearOS 5 has been ported to version 6, but it requires command line configuration. 

==== IPsec ====
The [[http://www.clearcenter.com/Services/clearsdn-dynamic-vpn-6.html|Dynamic VPN]] app provides advanced IPsec support in ClearOS Professional.  The unmanaged IPsec tool has been unmaintained for a few years and was dropped in version 6.  It's open source, so if someone wants to revive unmanaged IPsec, go right ahead.

==== Mail Archive ====
The [[http://www.clearcenter.com/support/documentation/clearos_enterprise_5.2/user_guide/mail_archive|Mail Archive]] module in 5.x has not been converted over to the new 6.x framework.  This app will be re-introduced into the ClearOS Marketplace in the future.  The Zarafa Professional Edition has [[http://www.zarafa.com/content/zarafa-archiver|Mail Archiving]] included for up to 20 users.  For additional mail archive 'seats', please contact Zarafa sales.

{{keywords>clearos, clearos content, announcements, releases, clearos6.3, final, previousfinal, maintainer_dloper}}

