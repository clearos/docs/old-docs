===== Amavisd-New =====
amavisd-new is a high-performance interface between mailer (MTA) and content
checkers:

  * ClamAV
  * Kaspersky
  * SpamAssassin 

===== Links =====
  * [[http://www.ijs.si/software/amavisd/|amavisd-new]]
{{keywords>clearos, clearos content, AppName, app_name, version7, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
