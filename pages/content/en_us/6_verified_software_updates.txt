===== Verified Software Updates =====
Like all operating systems, regular bug and security fixes are provided to ClearOS systems.  As part of a subscription to ClearOS, these updates go through an additional quality assurance screening and verified by the ClearCenter technical team.

===== Backporting and Security Scanners =====
Many of the core software packages from ClearOS are derived from source code from an prominent Linux vendor.  One of their policies is to maintain a high level of stability for all their Linux releases.  This policy is different from some other Linux distributions where the focus is on the releasing a solution with all the latest and greatest features (for example, Fedora Linux).

For various reasons, ClearOS has also adopted the **stability first** policy.

==== Backporting - How It Works ====
So how does the stability first policy impact the software development cycle in ClearOS?  Here is a scenario for the fictional Widget software:

  * September 2010 - ClearOS 5.2 released with Widget 2.0.0
  * November 2010 - Widget 2.1.0 released
  * December 2010 - Widget 2.1.1 released - a simple but important security update in this release

For operating systems shipped with the Widget 2.1.0, it is simply a matter of upgrading the software from the Widget software company.  With the **stability first** policy in place, the software engineers did not want to add all the extra features found in Widget 2.1.x, but simply wanted a fix for the security issue.  The simple security fix from version 2.1.1 was //backported// to the 2.0.0 version and released as 2.0.1.  With this backporting complete, only a minor change to the ClearOS system was required.

==== Security Scanning Limitations ====
Backporting is an important tool for keeping an operating system stable and secure. However, you may run across security scanning tools that report security issues with the versions of software running on a ClearOS system. For example, we receive the following types of messages on a regular basis. 

<note>XYZ was identified with an outdated version of OpenSSH according to the banner presented upon connection. They (the security experts) have determined this exploit to be high on the vulnerability matrix and suggest we receive an update from our vendor.</note>

{{keywords>clearos, clearos content, Verified Software Updates, clearos6, userguide, categorysystem, subcategoryoperatingsystem, maintainer_dloper}}