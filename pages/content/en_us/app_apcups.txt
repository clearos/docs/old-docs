{{ :userguides:apcups.svg?80}}
<WRAP clear></WRAP>

===== APC Battery Backup Manager =====
PC Battery Backup/UPS provides status information , reporting and administrative actions for managing supported APC UPS models.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/system/backup|here]]
===== Documentation =====
==== Version 6 ====
This app is not available for ClearOS 6.
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_apcups|here]].
==== Additional Notes ====
