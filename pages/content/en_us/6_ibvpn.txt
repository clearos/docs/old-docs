===== ibVPN =====
If you are outside the United States, you know the frustration of getting locked out of content providers like Hulu, Netflix, Pandora and others.  With ClearOS and the ibVPN app, you can now invisibly and securely access these sites. 

Install ibVPN on your ClearOS gateway and you can selectively route devices on your local network via your favorite country: US, Canada, UK, and many more.  For example, you may want to route your Apple TV system through the US while leaving other systems on your network as is.

<note warning>Though the ibVPN app is a free app, [[http://www.ibvpn.com/billing/aff.php?aff=1088|a subscription from ibVPN]] is required.  If you follow the link provided when you subscribe, we'll receive some referral fees -- that would be much appreciated!</note>

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Network|VPN|ibVPN</navigation>

===== Configuration =====
The first time you visit the ibVPN app, you will be prompted for your VPN credentials.  If you do not have an account, please follow  [[http://www.ibvpn.com/billing/aff.php?aff=1088|this link]] ((Full disclosure: the link includes an affiliate ID. The ibVPN app is free, but we receive a percentage of the ibVPN subscription.)) to sign up.  Once the VPN credentials are entered, you only need to do two more steps:

  * Select the country VPN server (US, UK, Canada, etc)
  * Add the devices on your network that should use the ibVPN service

===== Links =====
{{keywords>clearos, clearos content, ibVPN, app-ibvpn, clearos6, userguide, categorynetwork, subcategoryvpn, maintainer_dloper}}
