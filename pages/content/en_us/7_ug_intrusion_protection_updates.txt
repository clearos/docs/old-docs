===== IDS Signatures =====
{{:omedia:ids_compare.png |IPS Comparison}} Intrusion Protection Service, or IPS, takes an active security role at the edge of your network. The service detects attempts to gain access to your network from outsiders using known exploits and pro-actively firewalls (blocks) your server from the perpetrator's source Internet Address.

The **IDS Signatures** app works in unison with the [[intrusion_detection|IDS]] and [[intrusion_prevention|IPS]] apps by providing over 13,000 additional signatures.  In addition, the service constantly updates your server to keep pace with newly discovered exploits from security firms, software vendors and researchers.

<note tip>Did you know? When you install and run ClearOS Community or Professional Edition on your network, only a small fraction of total available signatures are included by default.  Furthermore, the rule-set is static, leaving your network exposed to countless attack vectors - both old and new.</note>

===== Intrusion Protection Updates and ClearCenter =====
{{:omedia:clearsdn-icon-xxs.png }} The ClearCenter [[http://www.clearcenter.com/Services/clearsdn-intrusion-protection-4.html|IDS Signatures]] update service is strongly recommended for deploying an effective intrusion protection system.  These signatures are compiled from third party organizations as well as internal engineering resources from ClearCenter.  We keep tabs on the latest available updates and fine tune the system so you can focus on more important things.  

The [[:content:en_us:7_ug_intrusion_protection_updates|IDS Signatures]] app:

  * Provides 13,000+ additional signatures (compared to the base 1,150 signatures)
  * Weekly updates to keep up with the latest threats

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Cloud|Updates|IDS Signatures</navigation>

===== Configuration ======
==== ClearCenter Rule Set ====
{{7_ug_ids_updates_summary.png}}

You can enable and disable the **IDS Signatures** update service from your ClearOS system.  A report on recent updates is also provided.

==== Rule Sets =====
{{7_ug_ids_updates.png}}

There are two different types of rules for the intrusion detection system.  The **Security** rules detect issues related to overall system security, while **Policy** rules detect issues related to your organization's Internet usage policies.  For example, the //chat// policy rules will detect instant messaging traffic that goes through your ClearOS system.

<note warning>Intrusion detection system does require some computing horsepower. There is no point enabling rules for services which are not exposed to the internet. E.g if you only use IMAP for picking up e-mails, there is no point in enabling the POP3 rules.</note>

===== IDS Logging =====
IDS logs can be viewed in log viewer in the snort/syslog files and also in the messages log by filtering for "snort". For further investigation you will need the SID number for the detection rule from the logs. The SID is the middle number from the group looking like [1:2101867:2], so in this case is 2101867 (rev 2).

<note>The IDS sits outside the firewall, so, even if a port is blocked or not open in the firewall, you will still see log messages for every match of your selected rules</note>

===== Security ID - SID =====
A security ID (SID) is referenced in various parts of the intrusion detection and prevention systems.  These IDs reference individual signatures and come from various sources including [[http://www.sourcefire.com/|SourceFire]] and [[http://www.emergingthreats.net/|Emerging Threats]].  The following table provides information on the most common signatures detected by the [[content:en_us:7_ug_intrusion_prevention|Intrusion Protection]] System:

^SID^Description^
|3000001|SSH brute force attack|
|3000002|FTP brute force attack|
|3000003|POP3 brute force attack|
|3100001|Scan detected via telnet attempt|

If you would like more information for a particular ID, use the [[http://www.snort.org/search/|SourceFire Snort ID Search]] for some of the signatures available in ClearOS.  If you are familiar with the command line, you can also find information on signatures be searching the files in /etc/snort.d/rules/clearcenter.

{{keywords>clearos, clearos content, Intrusion Protection Updates, app-intrusion-protection-updates, clearos7, userguide, categorycloud, subcategoryupdates, maintainer_bchambers}}
