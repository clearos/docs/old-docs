{{ :userguides:ftp_plugin_core.svg?80}}
<WRAP clear></WRAP>

===== FTP Plugin =====
Account manager plugin for the FTP service.  This app is required for the master node only in a master/slave environment.  The app allows an administrator to define user access to FTP services that may be available on one or more servers.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/system/ftp_plugin_core|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_ftp_plugin_core|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_ftp_plugin_core|here]].
==== Additional Notes ====
