===== RAID =====
The **RAID** feature is used to manage software and hardware RAID partitions.  Only some hardware RAID cards are currently supported.

===== Installation =====
This feature is part of the core system and installed by default.

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>System|Hardware|RAID</navigation>
{{keywords>clearos, clearos content, AppName, app_name, clearos5, userguide, categorysystem, subcategoryhardware, maintainer_dloper}}
