{{ :userguides:dropbox.svg?80}}
<WRAP clear></WRAP>

===== Dropbox Sync =====
Dropbox is a cloud-based file storage and synchronization service. Use this app to synchronize files to a folder located in your home directory which can then be accessed by any device associated to the same Dropbox account (laptop, smartphone, tablet etc.).

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/cloud/dropbox|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_dropbox|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_dropbox|here]].
==== Additional Notes ====
