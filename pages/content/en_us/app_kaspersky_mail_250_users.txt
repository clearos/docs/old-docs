{{ :userguides:kaspersky_mail_250_users.svg?80}}
<WRAP clear></WRAP>

===== Premium Mail Antimalware by Kaspersky =====
Additional 250 users for Premium Mail Antimalware - Powered by Kaspersky Labs.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/server/kaspersky_mail_250_users|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_kaspersky_mail_250_users|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_kaspersky_mail_250_users|here]].
==== Additional Notes ====
