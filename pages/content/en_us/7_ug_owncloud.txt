===== ownCloud for Home =====

ownCloud for Home provides file sync and share solution that is easy to use an consumer-grade products, but is integrated right into ClearOS. ClearOS and ownCloud allow you to deliver file sharing services that conform to your data security and compliance policies. Your employees will love ownCloud’s clean, professional user interfaces; you’ll enjoy peace of mind, knowing that your data is under IT control at all times.

This document provides instructions for the ClearOS integration. Additional, more detailed, ownCloud documentation including users and administrators manuals can be found [[http://doc.owncloud.org|here]].

===== Installation =====

If your system does not have this app available, you can install it via the [[:content:en_us:6_marketplace|Marketplace]].

ownCloud for Home is only available in ClearOS 7 Home Edition.  ClearOS Business and Community Editions have access to ownCloud for Business.

===== Menu =====

You can find this feature in the menu system at the following location:

Server > File > ownCloud for Home

===== Configuration =====

==== Webconfig Configuration ====

A limited number of ownCloud configuration parameters can be configured via Webconfig - just enough to handle the bootstrap/auto-configuration and allow you to login and get started.

The screenshot below shows a typical 'first visit' to ownCloud's Webconfig page after installation.

{{:userguides:clearos_7.x:oc_home_post_install.png}}

=== Admin Account ===

This is an auto-generated, reserved account to manage ownCloud.  The username will always be 'admin' unless you decide to get 'under the hood' and change this using SQL updates or ownCloud' occ utility.

=== Admin Default Password ===

To improve default security, the admin's default password is randomly created when you first install ownCloud.  Use this password to login.

If you want to continue to use this password, remember it or save it to your favourite password keeper.  After you login to the admin account for the first time, this password will no longer be displayed in Webconfig. If you forget the admin password and you did not change it, you can always get it using the following CLI command:

  cat /etc/clearos/owncloud.conf | grep adminpass

<note>If you have uninstalled and reinstalled ownCloud, the original password may not be in the file listed above, but in a backup file - /etc/clearos/owncloud.conf.rpmsave</note>

=== Auto Configure Trusted Domains ===

ownCloud enhanced security by requiring you to set which domains or IP's you can access ownCloud.  For convenience, set this field to allow ownCloud access from any hostname or IP you use.  It will automatically update with any network or Web Server (eg. alias) change you make.

=== Trusted Domains ===
Your list of trusted domains that users can log into. Specifying trusted domains prevents host header poisoning.

==== ownCloud Configuration ====

The ownCloud app interface is provided by the webserver running on ClearOS, and can be accessed directly via the link found on the app page. Once you have installed the app, the LDAP configuration and backend is tied in with the webconfig user interface. To get yourself up and running create or edit an existing user

Go to System > Accounts > Users, create or edit an existing user and enable 'ownCloud' access from the dropdown menu.
Access is granted on a per user basis and permits the configuration of per-user quotas

You can then navigate with your browser to https://yourdomain.com/owncloud and login with your users credentials. These credentials can also be used for your mobile apps, desktop etc to obtain access to your private cloud!. The web interface allows you to share files to specific local users on the ClearOS system, to local groups defined within the webconfig, or to external users with email addresses. These shares can be limited to specific time periods, with or without passwords!

The web interface provides your user with various links to the mobile or desktop apps that are available for Apple, Android, Windows etc. For external access to your cloud please make sure you webserver can be reached remotely and you have opened TCP ports 80(HTTP) or 443(HTTPS)

For those that like to get to grips with the configuration files, you can find the ownCloud configuration at:

  /usr/share/owncloud/config/config.php

===== Administration =====

ownCloud is integrated into the ClearOS LDAP system and so primarily user configuration should be carried out through the ClearOS webconfig interface. There is also a special 'administrator' user setup on install who is able to extent or otherwise configure ownCloud through the ownCloud interface. This special user exists on within ownCloud and is setup with the username admin on first install.  Feel free to change the password after first logging into the ownCloud portal.  You can do this by clicking on 'admin' in ownCloud's portal at the top right hand corner of the page, then clicking on "Personal".


===== 3rd-Party Apps =====

ownCloud comes with built in support for file sync, WebDAV access, external mount points, contacts, calendar, file revisions, deleted files, PDF viewer, photo and video viewer and basic document editing. Additional 3rd party apps are available and can be installed by using the ownCloud admin interface. Login with the 'admin' user, select 'admin' menu on the top right hand side, then select the '+Apps' button at the bottom left hand side.

Apps include:- bookmarks, encryption, external sites, tasks, news, notes and many more available from http://apps.owncloud.com


===== Storage location =====

The initial storage location is within the standard ClearOS data structure located at:

  /var/clearos/owncloud/data

but could easily be moved elsewhere to a drive or mount point with more storage space for example. 

If the default location is not suitable you can directly modify the path at /usr/share/owncloud/config/config.php - but first remember to copy the directory structure including the hidden '.ocdata' file to your new location. It is also possible to use the Storage Manager app to 'mound --bind' your data to a central storage position. In a future version of the app this will hopefully be automated through the webconfig.

<note>The storage location is deliberately separate from the main ownCloud folder so that the app can be completely removed and reinstalled without losing any data. The storage location can be easily migrated from one location to another or between server if required (assuming they both authenticate to the same LDAP server, e.g. in a master/slave scenario).</note>

<note>It is also possible to add additional data your users mount points by mounting 'external' data storage. This could be a local NAS sever, Samba share, Flexshare, FTP server or even another Dropbox or Google Drive account. This can be configured through the ownCloud interface admin. Please note that the 'apache' web server user must be given read access to the folder to access the files.</note>

===== Known Issues =====

Please note that the ClearOS LDAP configuration is used by ownCloud to authenticate, but ownCloud does not modify the LDAP database in any way. Therefore the Quota settings whilst visible in the ownCloud interface cannot be administered here for LDAP users.

If you have an existing ownCloud custom installation, please consider backing it up and removing it prior to installing this app. The ownCloud RPM is specifically built to be compatible with ClearOS and the app will install and auto configure ownCloud to use the data in the standard ClearOS places, and will also create a database within ClearOS System MySQL. This will likely trample over any existing setup and, therefore, it is recommend you remove it prior to installing. The cloud data can then be migrated afterwards to /var/clearos/owncloud/data

==== Links ====
  * [[http://owncloud.com|Owncloud's Website]]


{{keywords>clearos, clearos content, owncloud, owncloud clearos, dropbox, dropbox replacement, userguide, app-owncloud, clearos7, categoryserver, subcategoryfile, maintainer_bchambers}}
