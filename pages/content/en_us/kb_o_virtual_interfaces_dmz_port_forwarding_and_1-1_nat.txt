===== Differences Between Virtual Interfaces, DMZ, Port Forwarding, and 1-1 NAT =====
This guide will discuss the differences in routing, NAT, and IP assignments in ClearOS for Virtual Interfaces, DMZ, Port Forwarding, and 1:1 NAT.

People coming from Sonicwall and other similar firewalls are often confused at the simplicity of ClearOS. In Sonicwall, for example, you would need to set up an alias address, create and incoming accept rule, and then an appropriate forwarding rule to create a 1 to 1 NAT port. This is not the case in ClearOS. Most actions can be accomplished in one setting.

If you want traffic originating from the internet to be accepted by the ClearOS box then you will define that in the 'Incoming Firewall' rules. You will NOT use any other module to accomplish this task.

If you want traffic originating from the internet to to be accepted and forwarded from the IP address of the ClearOS box to an internal port on an internal host then you will use the "Port Forwarding" module. You will NOT use any other module to accomplish this task. Meaning, if you set up port 80 on the incoming firewall rule AND you set up port 80 on the port forwarding, then ClearOS will honor one or the other...but not both. This results in your traffic being accepted at the ClearOS box and NOT forwarded.

If you want traffic originating from the internet to to be accepted and forwarded from an address that is DIFFERENT than the IP address of the ClearOS box to an internal port on an internal host then you will use the "1 to 1 NAT" module. You will NOT use any other module to accomplish this task. Meaning, if you set up an alias address and then set port 80 on the incoming firewall rule AND you set up port 80 on the port forwarding, then ClearOS will honor one or the other of these rules...but not all three. This results in your traffic being accepted at the ClearOS box and NOT forwarded.

Simply put, if you want port 80 traffic from an alias address to be allowed into the ClearOS server and port forwarded to an internal host then you will simply use the 1:1 NAT module and only the 1:1 NAT module to accomplish this task. All other rules are redundant and will likely cause conflict. This is because ClearOS is designed to accomplish all the required tasks necessary for completing the task.

This guide will explain in detail some of the concepts and will help you achieve a simple and effective deployment.

===== Virtual Interfaces =====
Virtual interfaces tie an additional IP address to ClearOS. These are used ONLY for ports destined for the ClearOS server directly. If you want ports to go from a virtual address to an internal device behind the firewall you will need to use the 1-to-1 NAT module as explained below.

The virtual address will bind itself to the network card and provision itself with the same MAC address as illustrated here:

<code>
eth0      Link encap:Ethernet  HWaddr 00:0C:29:15:7A:D1  
          inet addr:192.168.61.193  Bcast:192.168.61.255  Mask:255.255.255.0
          inet6 addr: fe80::20c:29ff:fe15:7ad1/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:12638 errors:0 dropped:0 overruns:0 frame:0
          TX packets:8137 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:11019915 (10.5 MiB)  TX bytes:2982790 (2.8 MiB)

eth0:0    Link encap:Ethernet  HWaddr 00:0C:29:15:7A:D1  
          inet addr:192.168.61.194  Bcast:192.168.61.255  Mask:255.255.255.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1

</code>

{{:documentation:clearos_guides:virtual_ip.png?550|}}

=== ARP considerations ===
The ARP used for virtual addresses and for the 1-to-1 NAT module is the same as the interface to which they are virtually bound.

<code>
MacPro:~ user$ arp -a|grep 192.168.61
? (192.168.61.193) at 0:c:29:15:7a:d1 on vmnet8 ifscope [ethernet]
? (192.168.61.194) at 0:c:29:15:7a:d1 on vmnet8 ifscope [ethernet]
? (192.168.61.255) at (incomplete) on vmnet8 ifscope [ethernet]
</code>

===== Port Forwarding =====
ClearOS uses port forwarding to forward specified ports from all IP address of the external interface which are either real or virtual to the address and port(s) specified in the module. 

{{:documentation:clearos_guides:port-forward.png?550|}}

This is accomplished with the following rule listed in **'iptables -L'**:

<code>
Chain FORWARD (policy DROP)
target     prot opt source               destination         
ACCEPT     tcp  --  anywhere             192.168.1.30        tcp dpt:pop3 
</code>

And on the NAT table listed as **'iptables -L -t nat'**:

<code>
Chain PREROUTING (policy ACCEPT)
target     prot opt source               destination         
DNAT       tcp  --  anywhere             system.domain.lan   tcp dpt:pop3 to:192.168.1.30:110 

Chain POSTROUTING (policy ACCEPT)
target     prot opt source               destination         
SNAT       tcp  --  192.168.1.0/24       192.168.1.30        tcp dpt:pop3 to:192.168.1.4 
</code>

It's important to note that, as mentioned, ALL external addresses are forwarded under this rule. This is the default behavior and what it gives you is a simple way to provide high availability to a service in MultiWAN. For example, if you had an internal mail server on port 25, a forward rule would forward 25 for all external, Multiwan interfaces. Using the Dynamic DNS to follow the surviving interfaces, this is a robust solution and makes your service survive outages from ISPs.

But what if you DON'T want this behavior. If that is the case, you will need to forward ports manually in the custom firewall rule and forego using this tool for this port. For example, if you had 2 ISPs and need to have two different internal servers have the port 80 forwarded to them, you can do the following. In this example, let us say that your first ISP is eth0 and your second is eth1. First, make sure that you define a "Source Based Route" and assign the web server's internal address to use the proper external interface (eg. eth0). Next, create an custom firewall rule. If your internal web server address was 192.168.1.10, your rules might look like this:

  iptables -t nat -I PREROUTING -i eth0 -p tcp --dport 80 -j DNAT --to 192.168.1.10:80
  iptables -I FORWARD -i eth0 -p tcp --dport 80 -d 192.168.1.10 -j ACCEPT
  

===== DMZ =====
TBD

===== 1-to-1 NAT =====
The 1-to-1 NAT module accomplishes a number of tasks in one easy setting. First it binds an IP address to the external interface using the same method as used for the virtual interface. As such, do not use a 1-to-1 NAT rule on a pre-existing virtual interface. Instead, remove the virtual and provision the 1-to-1 NAT entry. The binding of the address will start with ethX:200 and subsequent numbering (ie. eth0:201, eth0:202 and so forth). In this example you can see the virtual interface eth0:0 and the virtual interface used by the 1-to-1 NAT module as eth0:200:

<code>
eth0      Link encap:Ethernet  HWaddr 00:0C:29:15:7A:D1  
          inet addr:192.168.61.193  Bcast:192.168.61.255  Mask:255.255.255.0
          inet6 addr: fe80::20c:29ff:fe15:7ad1/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:12745 errors:0 dropped:0 overruns:0 frame:0
          TX packets:8202 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:11043978 (10.5 MiB)  TX bytes:2999391 (2.8 MiB)

eth0:0    Link encap:Ethernet  HWaddr 00:0C:29:15:7A:D1  
          inet addr:192.168.61.194  Bcast:192.168.61.255  Mask:255.255.255.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1

eth0:200  Link encap:Ethernet  HWaddr 00:0C:29:15:7A:D1  
          inet addr:192.168.61.195  Bcast:192.168.61.255  Mask:255.255.255.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
</code>

{{:documentation:clearos_guides:1-1nat.png?550|}}

The next thing that the 1-to-1 NAT module will do is to create an incoming firewall rule to allow the incoming data on that address. This is located in the forward chain as illustrated here using **'iptables -L'**:

<code>
Chain FORWARD (policy DROP)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             192.168.1.20        
</code>

Next, the 1-to-1 NAT module will set up the NAT rule to support the address translation from the external to the internal address. This is locate in the NAT table as illustrated here using **'iptables -L -t nat'**:

<code>
Chain PREROUTING (policy ACCEPT)
target     prot opt source               destination         
DNAT       all  --  anywhere             192.168.61.195      to:192.168.1.20 

Chain POSTROUTING (policy ACCEPT)
target     prot opt source               destination         
SNAT       all  --  192.168.1.20         anywhere            to:192.168.61.195 
SNAT       all  --  192.168.1.0/24       192.168.1.20        to:192.168.1.4 </code>
{{keywords>clearos, clearos content, kb, ClearOS Guides Virtual Interfaces DMZ Port Forwording and 1-1 NAT, app_firewall, networkcategory, maintainer_dloper}}
