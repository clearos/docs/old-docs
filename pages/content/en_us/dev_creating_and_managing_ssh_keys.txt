===== Creating and Managing SSH Keys =====
Though many parts of the ClearFoundation infrastructure are protected by  usernames and passwords, SSH keys are used for security sensitive areas. Your own systems can be set up to favor a key-based paradigm.  In particular, all software maintainers, ClearGLASS users, tightened ClearOS systems, contributors and developers require key-based access for the following systems:

  * [[:content:en_us:dev_packaging_building_an_rpm_-_build_system|Build System]]
  * Package Management System
  * [[:content:en_us:dev_source_code_management_system|Source Code Management System]]
    * CVS
    * SVN
  * [[https://clear.glass|ClearGLASS]]

If using SSH keys is new to you, then this document will guide you through the process of creating and managing SSH keys.  If you are familiar with SSH keys but need to know how to manage multiple keys, you too will find this document to be helpful.  

===== Generating an SSH Key =====
You can either use your own SSH key, or you can generate a separate key for accessing key-based systems.  We strongly suggest the latter.  To generate a key, run the following:

  ssh-keygen -t rsa -f ~/.ssh/id_rsa.clearfoundation -C "ClearFoundation"

===== Creating the SSH Configuration =====
To make sure SSH is aware of this key, add it to the ~/.ssh/config file.  If this file does not exist, create it:

  touch ~/.ssh/config
  chmod 600 ~/.ssh/config

Now edit the //config// file with your favorite text editor and add the following:

  Host *.clearfoundation.com
    User username
    IdentityFile ~/.ssh/id_rsa.clearfoundation
    ConnectTimeout 20
    Protocol 2
    ForwardAgent yes

Don't forget to change //username// to your ClearFoundation username and the IdentityFile filename if you specified something different. 

==== Managing Multiple Keys ====
If you are using multiple keys, add another //Host// configuration block in your SSH config file.  For example, the developers from ClearCenter use a different SSH key for accessing ClearCenter systems.  A config file for this scenario looks like:

  Host *.clearfoundation.com
    User username
    IdentityFile ~/.ssh/id_rsa.clearfoundation
    ConnectTimeout 20
    Protocol 2
    ForwardAgent yes
  
  Host *.clearcenter.com
    User username
    IdentityFile ~/.ssh/id_rsa.clearcenter
    ConnectTimeout 20
    Protocol 2
    ForwardAgent yes
  
  Host *
    IdentityFile ~/.ssh/id_rsa.my_personal_key
    IdentityFile ~/.ssh/id_rsa.clearfoundation
    IdentityFile ~/.ssh/id_rsa.clearcenter

===== Configuring the Target System =====
If you would like to test your SSH setup, find another Linux system with shell access.  You can use a ClearOS system of course, just make sure you have [[:content:en_us:kb_howtos_adding_shell_access_for_a_user|shell access enabled]] for your login.  

  * Login to your target system with SSH
  * Create the .ssh directory if it does not exist, and tighten up the file permissions:

  mkdir ~/.ssh
  chmod 700 ~/.ssh

  * Create the .ssh/authorized_keys file if it does not exist.  Again, tighten up the file permissions: 

  touch ~/.ssh/authorized_keys
  chmod 600 ~/.ssh/authorized_keys

  * Append the contents of your public key ( ~/.ssh/id_rsa.clearfoundation.pub) to the authorized_keys file.  The key looks something like (though it will be much longer):

  ssh-rsa ABUNCHOFSEEMLINGLYRANDOMCHARACTERSTHATISREALLYLONG== ClearFoundation 

And that's it.  Logout of your target system then try to login again.  You should get prompted for a password, but it won't be your login password -- it will instead be your SSH key password for id_rsa.clearfoundation.  On most Linux systems, this password is cached using an SSH key agent (see next section).  This means that you will only have to provide the password once per local login session.

===== Integrating with an SSH Key Agent ====
If you are running a Linux system, you probably already have an SSH key agent running.  This lets you type in your SSH key's password once and it will be remembered for the rest of your login session.  The behavior of the key agent differs from one Linux distribution to another.  If you run into issues, you may want to run the following command from a shell: 

  ssh-add ~/.ssh/id_rsa.clearfoundation

You can add multiple keys if necessary.

===== Sending Your Public Key =====
If you are setting up a developer account with us, please send an e-mail to developer@clearfoundation.com to arrange the transfer of your public key. 

===== Links =====
  * [[http://www.puddingonline.com/~dave/publications/SSH-with-Keys-HOWTO/document/html/SSH-with-Keys-HOWTO.html|SSH Key Howto]]
  * [[:content:en_us:dev_source_code_management_system|Source Code Management System - SVN and CVS]]
  * [[:content:en_us:dev_packaging_building_an_rpm_-_build_system|Build System]]
{{keywords>clearos, clearos content, dev, maintainer_dloper}}
