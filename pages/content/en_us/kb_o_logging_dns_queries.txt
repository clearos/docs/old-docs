===== Logging DNS Queries =====
ClearOS, by default, sends a log of all the DNS queries that hit the local caching server to the system log service. However, there is not log file set up for these messages. A simple change can register these queries to a log file but caution should be used as this log can grow very big on an active network. That being said, this can be a very, VERY effective tool for monitoring traffic for two reasons.

  * DNS queries will show you not only requests for websites but also for other services!
  * You can force/redirect port 53 traffic to your local cache server so that even if someone overrides the DNS on their workstation, you will see their queries here!

===== Setup =====
You will need to modify the /etc/rsyslog.conf file and add the following line near the bottom of the file:

  daemon.debug /var/log/daemon.log

<note>This logging facility is for more than just dnsmasq, simple filtration through will show only the DNS queries. This will be discusses later</note>

<note warning>This method is very chatty and produces large log files. Be sure you have sufficient space for the task if you use this method for more than just casual monitoring. I've also not tested log rotations yet so be careful.</note>

Once you have made the change, restart the rsyslog service. A simple 'reload' of the service will NOT work.

  service rsyslog restart

===== Viewing the data =====
Now you will see the DNS queries flow into this file. To make sense of it, you will likely want to view it with search tools. You can also view it live. Here are some examples:

  * Watch the whole thing:

  tail -f /var/log/daemon.log

  * Watch for just dnsmasq results:

  tail -f /var/log/daemon.log | grep dnsmasq

  * Watch for just dnsmasq results and queries only:

  tail -f /var/log/daemon.log | grep dnsmasq | grep "query\[A\]"

  * Watch for just dnsmasq results and queries only and watch queries from a specific IP address (192.168.1.101 in this example):

  tail -f /var/log/daemon.log | grep dnsmasq | grep "query\[A\]" | grep 192.168.1.101

===== Removal =====
Since this file is chatty, you may want to remove the logging of this data. To do this merely remove the line in /etc/rsyslog.conf which was added at the start or comment it so that you can refer to it later:

  #daemon.debug /var/log/daemon.log

Be sure to restart the rsyslog service:

  service rsyslog restart
{{keywords>clearos, clearos content, kb, howtos, maintainer_dloper, maintainerreview_x, keywordfix}}