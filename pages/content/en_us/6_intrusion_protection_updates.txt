===== Intrusion Protection Updates =====
{{:omedia:ids_compare.png |IPS Comparison}} Intrusion Protection Service, or IPS, takes an active security role at the edge of your network. The service detects attempts to gain access to your network from outsiders using known exploits and pro-actively firewalls (blocks) your server from the perpetrator's source Internet Address.

The **Intrusion Protection Updates** app works in unison with the [[intrusion_detection|IDS]] and [[intrusion_prevention|IPS]] apps by providing over 12,000 additional signatures.  In addition, the service constantly updates your server to keep pace with newly discovered exploits from security firms, software vendors and researchers.

<note tip>Did you know? When you install and run ClearOS Community or Professional Edition on your network, only a small fraction of total available signatures are included by default.  Furthermore, the rule-set is static, leaving your network exposed to countless attack vectors - both old and new.</note>

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Gateway|Intrusion Protection|Intrusion Protection Updates</navigation>

===== Configuration ======
You can enable and disable the **Intrusion Protection Updates** service from your ClearOS system.  A report on recent updates is also provided.
{{keywords>clearos, clearos content, Intrusion Protection Updates, app-intrusion-protection-updates, clearos6, userguide, categorygateway, subcategoryintrusionprotection, maintainer_dloper}}
