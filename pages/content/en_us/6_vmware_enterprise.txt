===== ClearOS on VMWare Enterprise =====
Now that you have [[:content:en_us:6_e_downloading|downloaded]] your ClearOS VMware Enterprise image, you can begin the install.  The VMware Enterprise images can be used on the following VMware products:

  * VMware vSphere Hypervisor (was ESXi Free Version)
  * VMware ESX
  * VMware ESXi

===== Next Steps =====
Once you have booted the system, you can configure and verify your network with the [[:content:en_us:6_network_console|Network Console]].
{{keywords>clearos, clearos content, clearos6, userguide, categoryinstallation, subcategoryinstaller, subsubcategoryvirtualmachines, maintainer_dloper}}
