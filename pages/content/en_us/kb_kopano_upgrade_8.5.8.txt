===== Kopano Upgrade to 8.5.8 =====
Kopano version 8.5.8 provides an important fix for two critical CVE's (CVE-2018-8950 and CVE-2018-8951).

Unfortunately, Kopano's recommend upgrade process to patch these vunerabilities makes it difficult or too risky to automatically deploy these packages via the regular ClearOS update process.

You can read more about the Kopano upgrade [[https://kopano.com/releases/kopanocore-8-5-7/?mc_cid=055d2566fc&mc_eid=179b39ae79|here]].

====== Kopano on ClearOS ======

If you are running Kopano on ClearOS 7, it is likely version 8.4.5.  You will need to upgrade to Kopano 8.5.8 (2018-04-20).

===== Stop incoming mail =====
Stop any incoming mail...close your firewall, port 25 from Webconfig.  New mail coming in will usually attempt re-delivery so you won't lose any messages.

===== Stop Kopano Services =====
systemctl stop kopano-server.service
systemctl stop kopano-gateway.service

===== Backup the Database =====

If you don't have an up to date backup of the database, now is a good time to start.

Get the Kopano database password:

    [root@system]# cat /var/clearos/system_database/kopano

    [root@system]# /usr/clearos/sandbox/usr/bin/mysqldump -ukopano -p"xxx" kopano > /tmp/kopano.dmp

Where xxx is the password retrieved from the first step.

===== Perform the Upgrade =====

    ENABLE_BETA=True yum upgrade "*kopano*" "lib*" "python*"

If you use z-push also do:

    ENABLE_BETA=True yum upgrade zarafa-z-push

<note imporant>The "ENABLE_BETA" flag allows you to access the repository where the 8.5.8 packages reside.  These packages are not considered to be of 'beta' quality.  They have been released by Kopano for production use and verified with their community/customers and ClearCenter's own internal QA team.</note>

===== Check Database =====

<note>If you have previously upgrade from Zarafa, your database and username may be 'zarafa' and not 'kopano' as used by convention below.</note>

Login to the system database using the "xxx" password obtained above.

    /usr/clearos/sandbox/usr/bin/mysql -ukopano -p"xxx" kopano


Run:

    mysql> SELECT MAX(id) from names;

If this returns a value of 31485 or higher, there are too many entries and the database needs to be cleaned.

Run:

    mysql> select namestring, count(*) as c from names group by guid,nameid,namestring having c>=2;

If this returns any row(s), the database is inconsistent and needs to be cleaned.

If either of the two cases indicate areas for concern, run from the command line:

    kopano-dbadm np-stat
    kopano-dbadm k-1216

<note>The clean-up can take a while depending on your database size and hardware configuration.</note>


If you have any issues, check Kopano's [[https://kb.kopano.io/display/WIKI/K-1216|troubleshooting guide]].

===== Run ClearOS Install and Upgrade Script =====

Run the following script to fix systemctl unit files for Kopano:

        /usr/clearos/apps/kopano/deploy/install
        /usr/clearos/apps/kopano/deploy/upgrade

===== Start up Services =====

Run:

    kopano-condrestart 


===== Open Your Firewall =====

Re-Open Port 25 on your firewall to allow new mail to come in.


====== Zarafa on ClearOS ======

===== ClearOS 6 =====
Unfortunately, if you are running Zarafa on ClearOS 6, you have no alternative to patch these vulnerabilities other than upgrading to ClearOS 7 and Kopano.

Follow the upgrading 6 to 7 knowledge base article [[https://www.clearos.com/resources/documentation/clearos/content:en_us:kb_o_upgrading_from_clearos_6.x_to_clearos_7.x|here]].

===== ClearOS 7 =====

If you on ClearOS 7, an upgrade to Kopano will work. Follow the Zarafa to Kopano upgrade documentation [[https://www.clearos.com/resources/documentation/clearos/content:en_us:7_kb_zarafa_to_kopano_upgrade|here]].

<note>On completed upgrade, follow the steps at the start of this knowledgbase article to upgrade to 8.5.8.</note>
====== Related links ======
 * https://www.kopano.com

{{keywords>clearos, kb, howtos, tiki, skunkworks, maintainer_dloper, maintainerreview_dloper, wikisuite}}
