===== Setup eGroupware =====
[[http://www.egroupware.org/|www.egroupware.org]]

[[http://www.stylite.de/egroupware_demo_login|eGroupware demo login]]

===== Before you start =====
Necessary and configured ClearOS modules:
  * LDAP
  * Mail (incl. IMAP/POP server module, SMTP server module)
  * MySQL database server module
  * Web server module (incl. PHP module)

Required information during installation:
  * [Root password]
  * [LDAP Bind Password]
  * [MySQL root password]

===== Installing Packages =====
<note>Please login to the console or via 'ssh root@[clearosIP]' as you will need root rights</note>

eGroupware is a PHP application but needs Pear (a framework and distribution system for reusable PHP components: more info [[http://pear.php.net/|here]])

<code>
yum install php-pear.noarch php-gd.i386 php-xml.i386
apachectl restart
pear upgrade-all
pear install Auth_SASL
pear install XML_Feed_Parser
apachectl restart
</code>

===== eGroupware environment =====
<note>Please login to the console or via 'ssh root@[clearosIP]' as you will need root rights</note>

<note tip>This instruction is based on the default eGroupWare installation path, you can change this yourself (%%http://[FQDN]/egroupware%%)</note>

Let's download and unpack the eGroupware packages onto the web server.
<code>
cd /var/www/html
wget http://sourceforge.net/projects/egroupware/files/egroupware/eGroupware-1.6.002/eGroupware-1.6.002.tar.bz2/download
wget http://sourceforge.net/projects/egroupware/files/egroupware/eGroupware-1.6.002/eGroupware-egw-pear-1.6.002.tar.bz2/download
tar xvf eGroupware-1.6.002.tar.bz2
tar xvf eGroupware-egw-pear-1.6.002.tar.bz2
rm -fv eGroupware-1.6.002.tar.bz2 eGroupware-egw-pear-1.6.002.tar.bz2
</code>
<note important>This how-to is based on version 1.6.002 but can easily be adjusted to another version by replacing the version number</note>

In /etc/PHP.ini change the following settings:
^ Old setting                ^ New setting               ^
| memory_limit = 16M         | memory_limit = 96M        |
| upload_max_filesize = 2M   | upload_max_filesize = 8M  |
| ;mbstring.func_overload = x| mbstring.func_overload = 7|

and for the changes to take effect
<code>
apachectl restart
</code>

<note>The memory_limit is required, upload_max_filesize is the php limit and has impact on attachments (composing mail), the mbstring.func_overload is not required but it gives a warning when not set during installation.</note>

===== Installing eGroupware =====
And now for the fun part.....
fire up your browser to start the eGroupware setup: %%http://[FQDN]/egroupware/setup%%
<note tip>Make sure your firewall allows access!</note>

Click <button>Run installation tests</button>
a set of tests is now run, if the PHP.ini settings have been set as stated above, then you should see no 'red crosses' (errors that must be resolved) and only a few 'yellow thunderstrikes' (warnings regarding not installed database connectors -> important: the MySQL connector must pass the test)

Click <button>Continue to the Header Admin</button>
The header file is the actual configuration file that is needed to initialize eGroupware.

You can leave the settings default, but must specify (and note) passwords for the following accounts:
  * Header Username: admin / Header Password: [specify]
  * DB User: egroupware / DB Password: [specify]
  * Configuration User: admin / Configuration Password: [specify]

!Do NOT Click: <button>Add new database instance (eGW domain)</button> because you do not have root access to MySQL

Because the header.inc.php cannot be created due to file permission restrictions you need to click <button>Download</button> and upload the file in /var/www/html/egroupware (or click <button>View</button> and copy the contents in the console screen (VI))

In your (ssh) console run the following to protect your newly created header file and create two directories you will be needing later on
<code>
chmod 640 /var/www/html/egroupware/header.inc.php
chown :apache /var/www/html/egroupware/header.inc.php
mkdir -p /var/lib/egroupware/default/files /var/lib/egroupware/default/backup
chown -R apache /var/lib/egroupware/default/
</code>

in your browser click <button>Continue</button>

you should now be able to login to the 'Setup/Config Admin Login' screen using your freshly created 'configuration user': admin

==== Step 1 - Simple Application Management ====
Note that there is a message that the database is not working. This is because setup is trying to create a database with the newly created 'DB user' who does not have these rights.

Just fill in your [MySQL root password] and click <button>Create Database</button> and if you don't see any errors click <button>Re-Check my installation</button>

if you get the message: "Your database is working, but you dont have any applications installed", click <button>Install</button> (to install all applications), and if you don't see any errors click <button>Re-Check my installation</button>. 

==== Step 2 - Configuration ====
Click <button>Edit Current Configuration</button>
  * Path information, Virtual filesystem -> [keep default]
  * Host information -> fill in your own server settings
  * Standard mailserver settings (used for Mail authentication too) -> fill in your own server settings
  * Authentication / Accounts ->
    * Select which type of authentication you are using: choose [LDAP]
    * Select where you want to store/retrieve user accounts: [LDAP]
    * -> [keep rest default]
  * If using LDAP -> fill in your LDAP settings

Click <button>Save</button>

<note tip>as an example please find below my settings:
  * LDAP host: 127.0.0.1
  * LDAP accounts: ou=Users,ou=Accounts,dc=xceed,dc=no-ip,dc=org
  * LDAP search filter..: [empty]
  * LDAP groups context: ou=Groups,ou=Accounts,dc=xceed,dc=no-ip,dc=org
  * LDAP rootdn: cn=manager,cn=internal,dc=xceed,dc=no-ip,dc=org
  * LDAP root password: [this is the [LDAP Bind Password] as stated in the ClearOS web interface (Directory -> Setup -> Domain and LDAP -> LDAP information)
  * LDAP encryption type: MD5
  * Do you want to manage homedirectory and loginshell attributes?: No
  * LDAP Default homedirectory prefix (e.g. /home for /home/username): /home
  * LDAP Default shell (e.g. /bin/bash): /bin/bash
</note>

==== Step 3 - Admin Account ====
Click <button>Create Admin Account</button> (The account used for administrating all user accounts and settings.
  * Give admin access to all installed apps -> [empty]
  * Create demo accountsCreate demo account]
Click <button>Save</button>

Thats it!

Now click <button>Back to user login</button> (left side frame) and log in with your newly created admin account.....

Click <button>Admin icon</button> (first icon next to the logo) and select 'User Accounts'

Click <button>Edit</button> behind you user account name.

In the 'Edit user account' screen check the information and select the applications that you want to use (with this account).

Click <button>Save</button>
You can now log in with your own account.

----

{{keywords>clearos, clearos content, egroupware, skunkworks, clearos6, categoryserver, howto, maintainer_dloper}}
