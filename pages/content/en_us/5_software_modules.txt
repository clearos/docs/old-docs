===== Software Modules =====
Optional software modules can be installed on your ClearOS system.  This modular design lets you select all the features that you need -- no more, no less.  Whether you want a finely tuned firewall/gateway or an all-in-one system, you can install the modules to match your needs.

===== Installation =====
This feature is part of the core system and installed by default.

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>ClearCenter|Software|Software Modules</navigation>

===== Video Tutorial =====
{{youtube>large:CeZL4-IKTkg|ClearOS Enterprise Software Modules Video Tutorial}}

===== Finding a Module =====
Software modules are offered from [[http://www.clearcenter.com|ClearCenter's Service Delivery Network]] (ClearSDN) via webconfig.  To view a list of available modules, click on <navigation>ClearCenter|Software|Software Modules</navigation>.  A sample screenshot is shown below.

{{:omedia:ss_list_modules.png?350}}

<note>The system must be [[:content:en_us:5_system_registration|registered with ClearCenter]] to access this feature.</note>

===== Installing a Module =====
Select the module you wish to install, and hit <button>Go</button>.  Installing a module may take some time, depending on the size of the package, dependencies, your connection speed.  Please be patient!

Once complete, you will see an additional navigation link under the appropriate heading.  For example, if you were installing **[[:content:en_us:5_dmz|DMZ]] and [[:content:en_us:5_1_to_1_nat|1:1 NAT]]** firewall module, have a look under the //Network// heading. 

Here is an example of a system that needs a lot of updates. In this case, the user was running a Release Candidate version of ClearOS.

{{:omedia:ss_software-modules-bug-update.png?350}}
===== Advanced Users =====
The following instructions should only be used by advanced users familiar with ClearOS command line interface (CLI).

==== Package Listing ====
A complete listing of all packages in the yum repository can be found by using the following command:

  yum list

By piping the results of the search into a grep filter, you can easily find what you are looking for.  For example, if you wanted to find packages relating to the Postfix SMTP mail server, you could issue the following command:

  yum list | grep postfix

The response would include all packages containing the search string 'postfix'.

==== Package Installation ====
Some users will prefer to use yum via command line to install and manage packages.  The following example would install the [[:content:en_us:5_protocol_filter|Protocol Filter]] module:

  yum install app-protocol-filter
 
{{keywords>clearos, clearos content, Software Modules, clearos5, userguide, categoryinstallation, subcategorypostinstall, maintainer_dloper}}
