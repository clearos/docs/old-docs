===== Packages Modified for ClearOS 5.x =====
The following packages have been modified.

  * [[dev_packaging_clearos_5.x_apr-util|aprutil]]
  * [[dev_packaging_clearos_5.x_chkfontpath|chkfontpath]]
  * [[dev_packaging_clearos_5.x_cups|cups]]
  * [[dev_packaging_clearos_5.x_cyrus-imapd|cyrus-imapd]]
  * [[dev_packaging_clearos_5.x_dnsmasq|dnsmasq]]
  * [[dev_packaging_clearos_5.x_httpd|httpd]]
  * [[dev_packaging_clearos_5.x_initscripts|initscripts]]
  * [[dev_packaging_clearos_5.x_iptables|iptables]]
  * iscsi-initiator-utils
  * [[dev_packaging_clearos_5.x_imagemagick|imagemagick]]
  * [[dev_packaging_clearos_5.x_kernel|kernel]]
  * [[dev_packaging_clearos_5.x_kudzu|kudzu]]
  * [[dev_packaging_clearos_5.x_libc-client|libc-client]]
  * [[dev_packaging_clearos_5.x_mdadm|mdadm]]
  * [[dev_packaging_clearos_5.x_openldap|openldap]]
  * [[dev_packaging_clearos_5.x_postfix|postfix]]
  * [[dev_packaging_clearos_5.x_ppp|ppp]]
  * [[dev_packaging_clearos_5.x_rpm|rpm]]
  * [[dev_packaging_clearos_5.x_rp-pppoe|rp-pppoe]]
  * [[dev_packaging_clearos_5.x_samba|samba]]
  * [[dev_packaging_clearos_5.x_spamassassin|spamassassin]]
  * [[dev_packaging_clearos_5.x_squid|squid]]
  * [[dev_packaging_clearos_5.x_sudo|sudo]]
  * [[dev_packaging_clearos_5.x_yum|yum]]
  * [[dev_packaging_clearos_5.x_yum-utils|yum-utils]]

The following packages have been modified to remove upstream branding //only//.

  * [[dev_packaging_clearos_5.x_filesystem|filesystem]]
  * [[dev_packaging_clearos_5.x_mkinitrd|mkinitrd]]
  * [[dev_packaging_clearos_5.x_ntp|ntp]]
 
===== Packages Rebuilt for ClearOS (LDAP) =====
The following packages have been rebuilt without changing the source code in Red Hat Enterprise Linux source RPMs.  A rebuild was necessary to pull in a more recent OpenLDAP library.

  * autofs
  * cyrus-sasl 
  * freeradius
  * freeradius2
  * GConf2
  * gnupg
  * gnupg2
  * libuser
  * mod_authz_ldap
  * mod_perl
  * nfs-utils
  * nfs-utils-lib
  * nss_ldap
  * php
  * pwlib
  * python-ldap
  * subversion

===== Additional Packages for ClearOS =====
The following packages are not part of the source code in Red Hat Enterprise Linux.

  * adzapper
  * amavisd-new
  * awstats
  * [[dev_packaging_clearos_5.x_altermime|altermime]]
  * bacula
  * clamav
  * dansguardian-av
  * dansguardian-phraselists
  * dhcping
  * eziod
  * firewall
  * fuzzyocr
  * gallery
  * geoip
  * geolitecity
  * gifsicle
  * gocr
  * horde
  * horde-imp
  * horde-ingo
  * horde-kronolith
  * horde-mnemo
  * horde-nag
  * horde-turba
  * hping2
  * imapsync
  * jhead
  * jnettop
  * kmod-network-bypass
  * kolabconf
  * kolabd
  * kolab-filter
  * kolab-freebusy
  * l7-filter-userspace
  * l7-protocols
  * lha
  * libmcrypt
  * libnetfilter_conntrack
  * libnetfilter_queue
  * libnfnetlink
  * log2mysql
  * lzo
  * mailzu
  * mhash
  * [[dev_packaging_clearos_5.x_mpt-status|mpt-status]]
  * ocrad
  * openvpn
  * openvpn-auth-ldap
  * philesight
  * php-kolab
  * phpmyadmin
  * phpsysinfo
  * postgrey
  * pptpd
  * proftpd
  * python-elementtree
  * ruby-bdb
  * ruby-cairo
  * [[dev_packaging_clearos_5.x_snort|snort]]
  * suva
  * suvlets
  * swift
  * system-mysql
  * tconsole
  * text-console
  * [[dev_packaging_clearos_5.x_tnef|tnef]]
  * tw_cli
  * webconfig-bacula
  * webconfig-httpd
  * webconfig-pear
  * webconfig-pear-geoip
  * webconfig-pecl-fileinfo
  * webconfig-pecl-memcache
  * webconfig-php
{{keywords>clearos, clearos content, clearos5, dev, packaging, modifiedpackages, maintainer_dloper}}
