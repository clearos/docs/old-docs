===== Azure Classic =====
To add your Azure Classic cloud, you'll need to supply the Subscription ID plus a certificate file. The certificate file is required by Azure Classic to authenticate your account and can be created with OpenSSL on MacOS and Linux. On Windows, you can use IIS, or install  OpenSSL for Windows.

Run the following openssl command to create the certificate file:

<code>
user@user:~$ openssl req -x509 -nodes -days 730 -newkey rsa:2048 -subj "/C=GR/ST=Attiki/L=Athens/O=Dis/CN=www.ClearGLASS" -keyout azure.pem -out azure.pem

Generating a 2048 bit RSA private key
....++++++
....++++++
writing new private key to 'azure.pem'
-----
</code>

This creates azure.pem, a 2048 bit RSA self signed pem certificate that will be valid for 2 years.

Generate a .cer file out of it with the following command

<code>
user@user:~$ openssl x509 -inform pem -in azure.pem -outform der -out azure.cer
</code>

You will need to upload the azure.cer file on Azure and the azure.pem file on ClearGLASS.

Login to your Azure account on Azure Classic -  [[https://manage.windowsazure.com]] - and visit the **SETTINGS** section on the bottom left. Copy the **SUBSCRIPTION ID** (eg 807fff8a-86ec-4314-84fa-cfea350485e5), then select **MANAGEMENT CERTIFICATES**, and on the bottom of the page **UPLOAD**. A popup window opens, where you can select the .cer file and upload it to Azure. Make sure you select the correct Subscription if you have more than one

[image]

Once the file is uploaded, you should see it on the listing of your certificates.

[image]

Now to add your cloud to ClearGLASS, select **Add cloud** and choose **Azure**. Paste the Subscription ID and on the CERTIFICATE fields paste the .pem file you generated above.

[image]

By clicking Add, ClearGLASS will try to authenticate with Azure, and if the credentials are correct the cloud will be added and you should see a list of your virtual machines on the Machines section

[image]

==== Related Articles ====
  * [[content:en_us:cg_setting-up-clearglass|Quick Start Guide]]
  * [[content:en_us:cg_monitoring-on-linux|Setting up monitoring on Linux]]
  * [[content:en_us:cg_manage-infrastructure|Managing Infrastructure]]
  * [[content:en_us:cg_monitoring-on-windows|Setting up monitoring on Windows]]