===== Knowledge Base =====
The ClearOS Knowledge Base contains free and paid articles dealing with best practices, implementation guides and outlines, real-world deployment considerations, troubleshooting techniques and tools, and support beyond. 

It contains Howto documents designed to implement features. It also contains initiatives and helpful documentation for developers including prototyping, skunkworks, and works in progress.

  * [[:index:kb|Full Knowledge Base]]

Frequently used Knowledge Base sections:

  * [[:knowledgebase:faq:|Frequently asked questions (FAQ)]]
  * [[:knowledgebase:bestpractices|Best Practices, Whitepapers and Implementation Guides]]
  * [[:knowledgebase:troubleshooting:|Troubleshooting and error correction]]
  * [[:knowledgebase:skunkworks|Prototyping, unsupported apps, and tricks]]
  * [[:knowledgebase:hardware:|Hardware Compatibility Lists]]
===== Help =====
==== Navigation ====

[[:start|ClearOS Documentation]]