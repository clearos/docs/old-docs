===== ClearOS 6 User Guide =====
/**
 * This guide is dynamically updated. the following tags are used in the following sections:
 * userguide clearos6 categoryinstallation subcategorypreparation
 * userguide clearos6 categoryinstallation subcategoryinstaller subsubcategoryfullinstalliso
 * userguide clearos6 categoryinstallation subcategoryinstaller subsubcategoryvirtualmachines
 * userguide clearos6 categoryinstallation subcategoryinstaller subsubcategorycloud
 * userguide clearos6 categoryinstallation subcategoryfirstboot
 * userguide clearos6 categoryinstallation subcategorymarketplace
 * userguide clearos6 categoryinstallation subcategoryinstallationappendix
 * userguide clearos6 categoryserver subcategorydatabase
 * userguide clearos6 categoryserver subcategorydirectory
 * userguide clearos6 categoryserver subcategoryfileandprint
 * userguide clearos6 categoryserver subcategorymail
 * userguide clearos6 categoryserver subcategorymessagingandcollaboration
 * userguide clearos6 categoryserver subcategoryweb
 * userguide clearos6 categorynetwork subcategorybandwidthandqos
 * userguide clearos6 categorynetwork subcategoryfirewall
 * userguide clearos6 categorynetwork subcategoryinfrastructure
 * userguide clearos6 categorynetwork subcategoryvpn
 * userguide clearos6 categorynetwork subcategorysettings
 * userguide clearos6 categorygateway subcategoryantimalware
 * userguide clearos6 categorygateway subcategorycontentfilterandproxy
 * userguide clearos6 categorygateway subcategoryintrusionprotection
 * userguide clearos6 categorygateway subcategoryprotocolfilter
 * userguide clearos6 categorysystem subcategoryaccounts
 * userguide clearos6 categorysystem subcategoryaccountmanager
 * userguide clearos6 categorysystem subcategorybackup
 * userguide clearos6 categorysystem subcategorydeveloper
 * userguide clearos6 categorysystem subcategoryoperatingsystem
 * userguide clearos6 categorysystem subcategoryresources
 * userguide clearos6 categorysystem subcategorysecurity
 * userguide clearos6 categorysystem subcategorystorage
 * userguide clearos6 categorysystem subcategorysettings
 * userguide clearos6 categoryreports subcategorygateway
 * userguide clearos6 categoryreports subcategorynetwork
 * userguide clearos6 categoryreports subcategorysystem
 */
This guide will help you install and configure ClearOS 6.

===== Installation =====
==== Preparation ====
  * [[:content:en_us:6_a_what_is_clearos|What is ClearOS?]]
  * [[:content:en_us:6_b_system_requirements|System Requirements]]
  * [[:content:en_us:6_c_raid_support|RAID Support Overview]]
  * [[:content:en_us:6_d_compatibility|Compatibility]]
  * [[:content:en_us:6_e_downloading|Downloading ClearOS]]
==== Installer ====
=== Full Install - ISO ===
  * [[:content:en_us:6_b_installation_wizard|Installation Wizard]]
  * [[:content:en_us:6_boot_loader|Manual GRUB install of Bootloader]]
  * [[:content:en_us:6_c_configuring_partitions_and_raid|Configuring Partitions and RAID]]
  * [[:content:en_us:6_d_raid_0_-_striping|RAID 0 - Striping]]
  * [[:content:en_us:6_d_raid_1_-_mirroring|RAID 1 - Mirroring]]
  * [[:content:en_us:6_d_raid_5_-_redundancy|RAID 5 - Striping with Parity]]
  * [[:content:en_us:6_d_raid_6_-_extra_redundancy|RAID 6 - Striping with two parity drives]]
  * [[:content:en_us:6_deleting_partitions|Deleting Partitions]]
  * [[:content:en_us:6_e_installer_troubleshooting|Installer Troubleshooting]]
=== Virtual Machines ===
  * [[:content:en_us:6_virtualbox|ClearOS with Virtualbox]]
  * [[:content:en_us:6_virtualpc|ClearOS on Virtual PC]]
  * [[:content:en_us:6_vmware_basic|ClearOS on VMWare Basic]]
  * [[:content:en_us:6_vmware_enterprise|ClearOS on VMWare Enterprise]]
=== Cloud ===
  * [[:content:en_us:6_amazon_ec2|Amazon EC2]]
  * [[:content:en_us:6_linode|Linode]]
==== First Boot ====
  * [[:content:en_us:6_first_boot_wizard|First Boot Configuration Wizard]]
  * [[:content:en_us:6_network_console|Network Console]]
  * [[:content:en_us:6_webconfig|Webconfig]]
==== Marketplace ====
  * [[:content:en_us:6_marketplace|Marketplace]]
  * [[:index:all6apps|List of ClearOS 6 Apps]]
==== Installation Appendix ====
  * [[:content:en_us:6_command_line|Connecting to the Secure Shell]]
  * [[:content:en_us:6_network_notation|Network Notation]]

===== Server =====
==== Backup ====
  * [[:content:en_us:6_backuppc|BackupPC]]
  * [[:content:en_us:6_bmbackup|BareMetal Backup]]
==== Database ====
  * [[:content:en_us:6_mysql_server|MySQL Server]]
==== Directory ====
  * [[:content:en_us:6_active_directory_connector|Active Directory Connector]]
  * [[:content:en_us:6_directory_server|Directory Server]]
==== File and Print ====
  * [[:content:en_us:6_advanced_print_server|Advanced Print Server]]
  * [[:content:en_us:6_antimalware_file_scan|Antimalware File Scan]]
  * [[:content:en_us:6_dropbox|Dropbox]]
  * [[:content:en_us:6_flexshare|Flexshare]]
  * [[:content:en_us:6_ftp|FTP Server]]
  * [[:content:en_us:6_owncloud|ownCloud]]
  * [[:content:en_us:6_owncloud_business|OwnCloud Business]]
  * [[:content:en_us:6_photo_organizer|Photo Organizer]]
  * [[:content:en_us:6_plex_media_server|Plex Media Server]]
  * [[:content:en_us:6_windows_networking|Windows Networking]]
==== Mail ====
  * [[:content:en_us:6_antispam_updates|Antispam Updates]]
  * [[:content:en_us:6_greylisting|Greylisting]]
  * [[:content:en_us:6_imap_and_pop_server|IMAP and POP Server]]
  * [[:content:en_us:6_mail_antimalware_premium_powered_by_kaspersky|Mail Antimalware Premium powered by Kaspersky]]
  * [[:content:en_us:6_mail_antispam|Mail Antispam]]
  * [[:content:en_us:6_mail_antivirus|Mail Antimalware]]
  * [[:content:en_us:6_mail_archive|Mail Archive]]
  * [[:content:en_us:6_mail_retrieval|Mail Retrieval]]
  * [[:content:en_us:6_smtp_server|SMTP Server]]
==== Messaging and Collaboration ====
  * [[:content:en_us:6_google_apps_synchronization|Google Apps Synchronization]]
  * [[:content:en_us:6_zarafa_community_for_clearos|Zarafa Community for ClearOS]]
  * [[:content:en_us:6_zarafa_professional_for_clearos|Zarafa Professional for ClearOS]]
  * [[:content:en_us:6_zarafa_small_business_for_clearos|Zarafa Small Business for ClearOS]]
==== Web ====
  * [[:content:en_us:6_web_server|Web Server]]

===== Network =====
==== Bandwidth and QoS ====
  * [[:content:en_us:6_bandwidth_manager|Bandwidth Manager]]
  * [[:content:en_us:6_qos|QoS]]
  * [[:content:en_us:6_remote_bandwidth_monitor|Remote Bandwidth Monitor]]
==== Firewall ====
  * [[:content:en_us:6_1_to_1_nat|1-to-1 NAT]]
  * [[:content:en_us:6_custom_firewall|Custom Firewall]]
  * [[:content:en_us:6_dmz_firewall|DMZ Firewall]]
  * [[:content:en_us:6_egress_firewall|Egress Firewall]]
  * [[:content:en_us:6_firewall_groups|Firewall Groups]]
  * [[:content:en_us:6_incoming_firewall|Incoming Firewall]]
  * [[:content:en_us:6_port_forwarding|Port Forwarding]]
==== Infrastructure ====
  * [[:content:en_us:6_dhcp_server|DHCP Server]]
  * [[:content:en_us:6_dns_server|DNS Server]]
  * [[:content:en_us:6_network_map|Network Map]]
  * [[:content:en_us:6_ntp_server|NTP Server]]
  * [[:content:en_us:6_radius_server|RADIUS server]]
  * [[:content:en_us:6_ssh_server|SSH Server]]
==== VPN ====
  * [[:content:en_us:6_dynamic_vpn|Dynamic VPN]]
  * [[:content:en_us:6_ibvpn|ibVPN]]
  * [[:content:en_us:6_openvpn|OpenVPN]]
  * [[:content:en_us:6_pptp_server|PPTP Server]]
  * [[:content:en_us:6_static_ipsec_vpn|Static IPSec VPN]]
==== Settings ====
  * [[:content:en_us:6_dynamic_dns|Dynamic DNS]]
  * [[:content:en_us:6_ether_wake|Ether Wake]]
  * [[:content:en_us:6_ip_settings|IP Settings]]
  * [[:content:en_us:6_multi-wan|Multi-WAN]]

===== Gateway =====
==== Anti-Malware ====
  * [[:content:en_us:6_antimalware_updates|Antimalware Updates]]
  * [[:content:en_us:6_antiphishing|Antiphishing]]
  * [[:content:en_us:6_gateway_antimalware_premium_powered_by_kaspersky|Gateway Antimalware Premium Powered by Kaspersky]]
  * [[:content:en_us:6_gateway_antiphishing|Gateway Antiphishing]]
  * [[:content:en_us:6_gateway_antivirus|Gateway Antivirus]]
==== Content Filter and Proxy ====
  * [[:content:en_us:6_content_filter|Content Filter]]
  * [[:content:en_us:6_content_filter_updates|Content Filter Updates]]
  * [[:content:en_us:6_web_access_control|Web Access Control]]
  * [[:content:en_us:6_web_proxy|Web Proxy]]
==== Intrusion Protection ====
  * [[:content:en_us:6_intrusion_detection|Intrusion Detection]]
  * [[:content:en_us:6_intrusion_prevention|Intrusion Prevention]]
  * [[:content:en_us:6_intrusion_protection_updates|Intrusion Protection Updates]]
==== Protocol Filter ====

===== System =====
==== Accounts ====
  * [[:content:en_us:6_administrators|Administrators]]
  * [[:content:en_us:6_groups|Groups]]
  * [[:content:en_us:6_users|Users]]
==== Account Manager ====
  * [[:content:en_us:6_account_import|Account Import]]
  * [[:content:en_us:6_account_manager|Account Manager]]
  * [[:content:en_us:6_account_synchronization|Account Synchronization]]
  * [[:content:en_us:6_password_policies|Password Policies]]
==== Backup ====
  * [[:content:en_us:6_configuration_backup|Configuration Backup]]
==== Developer ====
  * [[:content:en_us:6_developer_tools|Developer Tools]]
  * [[:content:en_us:6_mobile_demo|Mobile Demo]]
==== Operating System ====
  * [[:content:en_us:6_amazon_ec2_support|Amazon EC2 Support]]
  * [[:content:en_us:6_services_manager|Services Manager]]
  * [[:content:en_us:6_software_repository|Software Repository]]
  * [[:content:en_us:6_software_updates|Software Updates]]
  * [[:content:en_us:6_system_registration|System Registration]]
  * [[:content:en_us:6_verified_software_updates|Verified Software Updates]]
==== Resources ====
  * [[:content:en_us:6_log_viewer|Log Viewer]]
  * [[:content:en_us:6_process_viewer|Process Viewer]]
  * [[:content:en_us:6_remote_system_monitor|Remote System Monitor]]
==== Security ====
  * [[:content:en_us:6_certificate_manager|Certificate Manager]]
  * [[:content:en_us:6_remote_security_audit|Remote Security Audit]]
==== Storage ====
  * [[:content:en_us:6_disk_usage|Disk Usage]]
  * [[:content:en_us:6_encrypted_file_system|Encrypted File System]]
  * [[:content:en_us:6_raid_manager|RAID Manager]]
  * [[:content:en_us:6_smart_monitor|Smart Monitor]]
  * [[:content:en_us:6_storage_manager|Storage Manager]]
==== Settings ====
  * [[:content:en_us:6_date_and_time|Date and Time]]
  * [[:content:en_us:6_mail_notification|Mail Notification]]
  * [[:content:en_us:6_mail_settings|Mail Settings]]

===== Reports =====
==== Gateway ====
  * [[:content:en_us:6_filter_and_proxy_report|Filter and Proxy Report]]
==== Network ====
  * [[:content:en_us:6_bandwidth_viewer|Bandwidth Viewer]]
  * [[:content:en_us:6_network_detail_report|Network Detail Report]]
  * [[:content:en_us:6_network_report|Network Report]]
  * [[:content:en_us:6_network_visualiser|Network Visualiser]]
==== System ====
  * [[:content:en_us:6_resource_report|Resource Report]]

===== Cloud =====
==== Services ====
  * [[:content:en_us:6_remote_server_backup|Remote Server Backup]]

===== My Account =====
  * [[:content:en_us:6_user_certificates|User Certificates]]
  * [[:content:en_us:6_user_profile|User Profile]]

===== Spotlight =====
  * [[:content:en_us:6_dashboard|Dashboard]]
  * [[:content:en_us:6_marketplace|Marketplace]]

