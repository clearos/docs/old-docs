===== Troubleshooting Guides =====
/**
 * This guide is dynamically updated. the following tags are used in the following sections:
 * troubleshooting installation
 */
This section deals with issues that you may experience under ClearOS. Fixing common problems and other useful information. This section is comprised of several past troubleshooting mechanisms and should be comprehensive.

===== Installation - Recovery - Support =====
  * [[:content:en_us:kb_troubleshooting_a_crashing_server|Troubleshooting a Crashing Server]]
  * [[:content:en_us:kb_troubleshooting_getting_nics_and_hardware_detected_in_clearos|Getting NICs and Hardware Detected in ClearOS]]
  * [[:content:en_us:kb_troubleshooting_installation_fails_asking_to_insert_the_cd_or_other_media|Installation Fails Asking to 'Insert the CD or other media']]

===== Settings =====
  * [[:content:en_us:kb_troubleshooting_checking_your_disk_health_with_smartmon|Checking Your Disk Health with SmartMon]]

===== Marketplace =====

===== Server =====
  * [[:content:en_us:kb_troubleshooting_a_crashing_server|Troubleshooting a Crashing Server]]
  * [[:content:en_us:kb_troubleshooting_alock_package_is_unstable|alock Package is Unstable]]

==== Windows Networking (Samba) ====

==== Zarafa ====

===== Gateway =====

==== Proxy and Content Filter ====
  * [[:content:en_us:kb_troubleshooting_checking_up_on_squid|Checking up on Squid]]

===== Network =====
  * [[:content:en_us:kb_troubleshooting_connectivity|Advanced Connectivity Troubleshooting]]

===== Cloud / Service Delivery Network (ClearSDN) =====

===== 3rd party =====

===== Help =====
==== Links ====
  * [[http://www.clearcenter.com/support/documentation/clearos_guides/start|Old ClearCenter Guides]]
  * [[http://w3w.clearfoundation.com/docs/|Old ClearFoundation Guides]]
  * [[http://www.clearos.com/clearfoundation/social/community-forums|ClearOS Forums]]
  * [[http://tracker.clearfoundation.com|ClearFoundation Bug Tracker]]
===== Troubleshooting Guides =====
Troubleshooting guides will help you fix known issues with ClearOS or work through problems that may occur for situations outside of ClearOS. These guides help you find the line in the sand between where an issue is caused by your ISP, your network, your hardware, your third party software, or whether the problem is with ClearOS and is a bug

==== Cloud Troubleshooting ====

==== Server Troubleshooting ====
  * [[:content:en_us:kb_howtos_forgot_mysql_password|Forgot MYSQL Password]]

==== Network Troubleshooting ====

==== Gateway Troubleshooting ====

==== Settings Troubleshooting ====

===== All Troubleshooting =====
  * [[:content:en_us:kb_adding_hash_values_as_certificates|Adding Hash Values as Certificates]]
  * [[:content:en_us:kb_howtos_forgot_mysql_password|Forgot MYSQL Password]]
  * [[:content:en_us:kb_troubleshooting_security_audit_failures|Security Audit Failures]]

