===== ClearVM Help Center (formerly WitsBits) =====
Welcome to the ClearVM Help Center

===== HYPERVISORS (HOSTS) AND MANAGEMENT SYSTEM =====

  * [[:content:en_us:vm_Hardware Requirements|Hardware Requirements]]
  * [[:content:en_us:vm_Enable Virtual Technology|Enable Virtual Technology]]
  * [[:content:en_us:vm_Network Attached Storage NAS|Network Attached Storage (NAS)]]
  * [[:content:en_us:vm_Enabling Failover|Enabling Failover]]
  * [[:content:en_us:vm_Using Server Scripts|Using Server Scripts]]
  * [[:content:en_us:vm_Using Dell Remote Access Card DRAC|Using Dell Remote Access Card (DRAC)]]
  * [[:content:en_us:vm_Download Witsbits LIVEvisor from Command-line|Download Witsbits LIVEvisor from Command-line]]
  * [[:content:en_us:vm_Best practices Host Networking|Best practices Host Networking]]
  * [[:content:en_us:vm_Known Issues|Known Issues]]
  * [[:content:en_us:vm_ssh_access|Setting up Console and SSH Access]]

===== VIRTUAL ENVIRONMENT =====

  * [[:content:en_us:vm_Running Windows on Witsbits|Running Windows on Witsbits]]
  * [[:content:en_us:vm_Boot a VM from an ISO Image|Boot a VM from an ISO Image]]
  * [[:content:en_us:vm_Adding Custom VM Templates|Adding Custom VM Templates]]
  * [[:content:en_us:vm_Importing VMs from other Hypervisors|Importing VMs from other Hypervisors]]
  * [[:content:en_us:vm_Taking a Snapshot of a VM|Taking a Snapshot of a VM]]
  * [[:content:en_us:vm_Compatible VNC Clients|Compatible VNC Clients]]

===== QUESTIONS & ANSWERS =====
==== Can I eject the LIVEvisor USB/CD once I’ve booted? ====

Yes you can, it’s not needed anymore. But please make sure to initialize at least one hard drive before you try to reboot the server or it won’t come up again (a bootloader is installed on the hard drive during the drive initialization, which is required for the server to boot).

You can initialize the hard drive before or after you remove the USB/CD, but make sure to do it before you do a reboot to avoid having to insert the USB/CD again.

  * Sign in to the Web GUI
  * Select your server and open the Storage tab
  * Now click on the power icon (the tooltip says “Initialize”) next to the drive you want to initialize.

Once it has completed you don’t need the ISO/CD/USB anymore.

==== Can I download the LIVEvisor ISO from the command line? ====

Yes, you can use for example wget or cURL to [[:content:en_us:vm_download_witsbits_livevisor_from_command-line|download the LIVEvisor ISO]].

===== Knowledge Base =====
The ClearOS Knowledge Base contains free and paid articles dealing with best practices, implementation guides and outlines, real-world deployment considerations, troubleshooting techniques and tools, and support beyond. 

It contains Howto documents designed to implement features. It also contains initiatives and helpful documentation for developers including prototyping, skunkworks, and works in progress.

  * [[:index:kb|Full Knowledge Base]]

Frequently used Knowledge Base sections:

  * [[:knowledgebase:faq:|Frequently asked questions (FAQ)]]
  * [[:knowledgebase:bestpractices|Best Practices, Whitepapers and Implementation Guides]]
  * [[:knowledgebase:troubleshooting:|Troubleshooting and error correction]]
  * [[:knowledgebase:skunkworks|Prototyping, unsupported apps, and tricks]]
  * [[:knowledgebase:hardware:|Hardware Compatibility Lists]]
==== I can not seem to be able to log in to the LIVEvisor. What is the login / password? ====

You should probably not log in to the LIVEvisor at all. All management is be done by signing in to the [[https://witsbits.com/app/#/login|centralized management system]] – hosts and guests will be available for management there. That said, if you want to look around inside the LIVEvisor that’s fine just sign in with user:root pass:witsbits (it only works from the server console).

==== What hypervisor are you using? ====

The LIVEvisor virtual machine monitor is [[http://www.linux-kvm.org/|KVM]] (Kernel-based Virtual Machine).

==== What is the file format for disk image files used by Witsbits LIVEvisor? ====

The virtual machine container format is [[http://en.wikipedia.org/wiki/Qcow|qcow2]] – the native format for KVM/QEMU.

==== If i manually build VM on a cow2 image, would I be able to use it with Witsbits? ====

Yes, you can bring it into Witsbits management later. [[:content:en_us:vm_importing_vms_from_other_hypervisors|Read more about imports]].

==== How can I change the RAM, CPU and HDD configuration on an existing VM? ====

Turn off the VM and click **Start** in the GUI. Then select the new configuration and click **Power On**.

==== Why is my VM briefly unavailable during snapshot? ====

To prevent any risk of data corruption or inconsistency in the snapshot we tell the hypervisor to pause the source VM until the snapshot process has completed. This leads to the source VM briefly being unavailable when a snapshot is taken.

==== The LIVEvisor doesn’t boot, why? ====

Some older BIOSes has a bug in their APCI handling which in some cases prevent your server from booting the LIVEvisor. The symptom appears at kernel boot similar to

  ata1 failed to identify I/O error err_mask=0x4

or

  /bin/sh: can't access tty; job control turned off
  (initramfs)

First make sure you have the latest LIVEvisor. Then start your server and hit Tab on your keyboard when the boot screen appears. Now, add

<code> noapic</code>

(add a space between the old parameters and noapic) to the end of the command-line and press Enter to continue the boot process.

==== Why can’t my DELL boot Witsbits ISO on USB? ====

Check your BIOS and set accordingly:

  USB Flash Drive emulation type: Hard Drive

==== Why do I only get 10GB of hard drive space for my VM? ====

Many of the pre-configured VMs are configured with a root filesystem partion of 10GB. To utilize the capacity you selected when you launched the VM you may create a 2nd (3rd, 4th etc.) partition from inside your guest operating system.

To partition Linux guest operating systems, type: “fdisk /dev/vda”
To partition Windows guest operating systems, use [[http://technet.microsoft.com/en-us/library/cc754936.aspx|Disk Manager]]
Note: the first partition is used by the operating system.

==== Why the software virtualization warning? ====

In order to maintain good performance using Witsbits, software virtualization should not be used. Some BIOS has virtualization set to disabled by default. How to enable hardware virtualization in BIOS:

  * Close down any running virtual machine(s) on the server
  * Reboot the server
  * During BIOS post press the stated on-screen key(s) to enter BIOS. Refer to manual if needed
  * Boot up as before with Witsbits media
  * In the WebApp, select the server and verify **CPU Virtualization** is **Intel® VT** or **AMD-V™**

==== How do my servers connect to my Witsbits account? ====

The LIVEvisor ISO is imprinted with an identifier unique to your account at the time of download. When a server is started with your LIVEvisor ISO it will automatically connect it with your account. Thus, you should never share your LIVEvisor media or ISO with anyone else (it’s not dangerous, but their servers will pop up in your account).

==== What information is stored on Witsbits’ servers? ====

Witsbits Director collects and stores only information necessary to be able to provide management capabilities to your physical and virtual infrastructure. The collected data includes, hardware utilization, hardware configuration, hardware health, hardware status, virtual machine utilization, virtual machine configuration, virtual machine health, virtual machine status and software versions.

By using the VM backup function you will upload a snapshot of your virtual machine container to Witsbits’ Servers. The Virtual Machine Container is the virtual hard drive of your virtual machine and holds all and any data you have stored while operating the virtual machine.


===== Comments =====
**ANDROSoft* on February 26, 2014 at 6:35 pm said:
Enlarge the partition to use all space:

  fdisk -u /dev/vda.

p to print the partition table, take note of the number, start, end, type of vda1.

Delete it: d
Recreate it: n
with same number (1), start and type but with a bigger end (taking care not to overlap with other partitions). Try to align things on a megabyte boundary that is for end, make it a multiple of 2048 minus 1.

Then w to write and q to quit.

The partition table will have been modified but the kernel will not be able to take that into account as some partitions are mounted.

So you’ll need to reboot. The system should boot just fine.

Once rebooted, resize the filesystem so it spreads to the extent of the enlarged partition:

  resize2fs /dev/vda1

Which for ext4 will work just fine even on a live FS.

orginal solution:
[[http://unix.stackexchange.com/questions/67095/how-can-i-expand-ext4-partition-size-on-debian|http://unix.stackexchange.com/questions/67095/how-can-i-expand-ext4-partition-size-on-debian]]

{{keywords>clearvm, kb, menu, howto, maintainer_dloper}}