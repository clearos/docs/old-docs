===== ClearCenter CVE Database =====
/**
 * This guide is dynamically updated. the following tags are used in the following sections:
 * announcments cve cveYYYY
 * (Where YYYY is the year of the CVE)
 */
This index contains statements from ClearCenter which address common CVE alerts that are related to ClearOS and services installed on ClearOS

===== CVEs by Year =====
==== 2018 ====
  * [[:content:en_us:announcements_cve_cve-2018-0732|CVE-2018-0732]]
  * [[:content:en_us:announcements_cve_cve-2018-0737|CVE-2018-0737]]
  * [[:content:en_us:announcements_cve_cve-2018-0739|CVE-2018-0739]]
  * [[:content:en_us:announcements_cve_cve-2018-1283|CVE-2018-1283]]
  * [[:content:en_us:announcements_cve_cve-2018-1301|CVE-2018-1301]]
  * [[:content:en_us:announcements_cve_cve-2018-1303|CVE-2018-1303]]
  * [[:content:en_us:announcements_cve_cve-2018-1312|CVE-2018-1312]]

==== 2017 ====
  * [[:content:en_us:announcements_cve_cve-2017-3167|CVE-2017-3167]]
  * [[:content:en_us:announcements_cve_cve-2017-3169|CVE-2017-3169]]
  * [[:content:en_us:announcements_cve_cve-2017-3735|CVE-2017-3735]]
  * [[:content:en_us:announcements_cve_cve-2017-3736|CVE-2017-3736]]
  * [[:content:en_us:announcements_cve_cve-2017-3737|CVE-2017-3737]]
  * [[:content:en_us:announcements_cve_cve-2017-3738|CVE-2017-3738]]
  * [[:content:en_us:announcements_cve_cve-2017-7679|CVE-2017-7679]]
  * [[:content:en_us:announcements_cve_cve-2017-5715|CVE-2017-5715]]
  * [[:content:en_us:announcements_cve_cve-2017-5753|CVE-2017-5753]]
  * [[:content:en_us:announcements_cve_cve-2017-5754|CVE-2017-5754]]
  * [[:content:en_us:announcements_cve_cve-2017-9788|CVE 2017-9788]]
  * [[:content:en_us:announcements_cve_cve-2017-9798|CVE-2017-9798]]
  * [[:content:en_us:announcements_cve_cve-2017-13704|CVE 2017-13704]]
  * [[:content:en_us:announcements_cve_cve-2017-14491|CVE 2017-14491]]
  * [[:content:en_us:announcements_cve_cve-2017-14492|CVE 2017-14492]]
  * [[:content:en_us:announcements_cve_cve-2017-14493|CVE 2017-14493]]
  * [[:content:en_us:announcements_cve_cve-2017-14494|CVE 2017-14494]]
  * [[:content:en_us:announcements_cve_cve-2017-14495|CVE 2017-14495]]
  * [[:content:en_us:announcements_cve_cve-2017-14496|CVE 2017-14496]]
  * [[:content:en_us:announcements_cve_cve-2017-15710|CVE-2017-15710]]
  * [[:content:en_us:announcements_cve_cve-2017-15715|CVE 2017-15715]]

==== 2016 ====
  * [[:content:en_us:announcements_cve_cve-2016-0736|CVE-2016-0736]]
  * [[:content:en_us:announcements_cve_cve-2016-2118|CVE 2016-2118]]
  * [[:content:en_us:announcements_cve_cve-2016-2161|CVE-2016-2161]]
  * [[:content:en_us:announcements_cve_cve-2016-2183|CVE-2016-2183]]
  * [[:content:en_us:announcements_cve_cve-2016-5387|CVE-2016-5387]]
  * [[:content:en_us:announcements_cve_cve-2016-8743|CVE-2016-8743]]
  * [[:content:en_us:announcements_cve_cve-2016-4975|CVE-2016-4975]]

==== 2015 ====
  * [[:content:en_us:announcements_cve_cve-2015-3183|CVE-2015-3183]]
  * [[:content:en_us:announcements_cve_cve-2015-3185|CVE-2015-3185]]

==== 2014 ====
  * [[:content:en_us:announcements_cve_cve-2014-0098|CVE-2014-0098]]
  * [[:content:en_us:announcements_cve_cve-2014-0117|CVE-2014-0117]]
  * [[:content:en_us:announcements_cve_cve-2014-0118|CVE-2014-0118]]
  * [[:content:en_us:announcements_cve_cve-2014-0160|CVE 2014-0160]]
  * [[:content:en_us:announcements_cve_cve-2014-0226|CVE 2014-0226]]
  * [[:content:en_us:announcements_cve_cve-2014-0231|CVE-2014-0231]]
  * [[:content:en_us:announcements_cve_cve-2014-0224|CVE 2014-1224]]
  * [[:content:en_us:announcements_cve_cve-2014-2532|CVE 2014-2532]]
  * [[:content:en_us:announcements_cve_cve-2014-2653|CVE 2014-2653]]
  * [[:content:en_us:announcements_cve_cve-2014-3581|CVE-2014-3581]]
  * [[:content:en_us:announcements_cve_cve-2014-8109|CVE-2014-8109]]

==== 2013 ====
  * [[:content:en_us:announcements_cve_cve-2013-1862|CVE 2013-1862]]
  * [[:content:en_us:announcements_cve_cve-2013-2566|CVE 2013-2566]]
  * [[:content:en_us:announcements_cve_cve-2013-4352|CVE-2013-4352]]
  * [[:content:en_us:announcements_cve_cve-2013-5704|CVE-2013-5704]]
  * [[:content:en_us:announcements_cve_cve-2013-6438|CVE-2013-6438]]

==== 2012 ====
  * [[:content:en_us:announcements_cve_cve-2012-0031|CVE 2012-0031]]
  * [[:content:en_us:announcements_cve_cve-2012-0053|CVE 2012-0053]]
  * [[:content:en_us:announcements_cve_cve-2012-0814|CVE 2012-0814]]
  * [[:content:en_us:announcements_cve_cve-2012-0883|CVE 2012-0883]]
  * [[:content:en_us:announcements_cve_cve-2012-2687|CVE 2012-2687]]
  * [[:content:en_us:announcements_cve_cve-2012-3499|CVE 2012-3499]]
  * [[:content:en_us:announcements_cve_cve-2012-4558|CVE 2012-4558]]

==== 2011 ====
  * [[:content:en_us:announcements_cve_cve-2011-0419|CVE 2011-0419]]
  * [[:content:en_us:announcements_cve_cve-2011-1928|CVE 2011-1928]]
  * [[:content:en_us:announcements_cve_cve-2011-3192|CVE 2011-3192]]
  * [[:content:en_us:announcements_cve_cve-2011-3348|CVE 2011-3348]]
  * [[:content:en_us:announcements_cve_cve-2011-3607|CVE 2011-3607]]
  * [[:content:en_us:announcements_cve_cve-2011-4415|CVE 2011-4415]]
  * [[:content:en_us:announcements_cve_cve-2011-5000|CVE 2011-5000]]

==== 2010 ====
  * [[:content:en_us:announcements_cve_cve-2010-0386|CVE 2010-0386]]
  * [[:content:en_us:announcements_cve_cve-2010-0425|CVE 2010-0425]]
  * [[:content:en_us:announcements_cve_cve-2010-0434|CVE 2010-0434]]
  * [[:content:en_us:announcements_cve_cve-2010-1452|CVE 2010-1452]]
  * [[:content:en_us:announcements_cve_cve-2010-1623|CVE 2010-1623]]
  * [[:content:en_us:announcements_cve_cve-2010-4478|CVE 2010-4478]]
  * [[:content:en_us:announcements_cve_cve-2010-4755|CVE 2010-4755]]
  * [[:content:en_us:announcements_cve_cve-2010-5107|CVE 2010-5107]]

==== 2009 ====
  * [[:content:en_us:announcements_cve_cve-2009-1195|CVE 2009-1195]]
  * [[:content:en_us:announcements_cve_cve-2009-2412|CVE 2009-2412]]
  * [[:content:en_us:announcements_cve_cve-2009-2957|CVE 2009-2957]]
  * [[:content:en_us:announcements_cve_cve-2009-2958|CVE 2009-2958]]

==== 2008 ====
  * [[:content:en_us:announcements_cve_cve-2008-0005|CVE 2008-0005]]
  * [[:content:en_us:announcements_cve_cve-2008-0455|CVE 2008-0455]]
  * [[:content:en_us:announcements_cve_cve-2008-0456|CVE 2008-0456]]
  * [[:content:en_us:announcements_cve_cve-2008-1483|CVE 2008-1483]]
  * [[:content:en_us:announcements_cve_cve-2008-3259|CVE 2008-3259]]
  * [[:content:en_us:announcements_cve_cve-2008-5161|CVE 2008-5161]]

==== 2007 ====
  * [[:content:en_us:announcements_cve_cve-2007-1863|CVE 2007-1863]]
  * [[:content:en_us:announcements_cve_cve-2007-2243|CVE 2007-2243]]
  * [[:content:en_us:announcements_cve_cve-2007-3303|CVE 2007-3303]]
  * [[:content:en_us:announcements_cve_cve-2007-3304|CVE 2007-3304]]
  * [[:content:en_us:announcements_cve_cve-2007-4465|CVE 2007-4465]]
  * [[:content:en_us:announcements_cve_cve-2007-4752|CVE 2007-4752]]
  * [[:content:en_us:announcements_cve_cve-2007-5000|CVE 2007-5000]]
  * [[:content:en_us:announcements_cve_cve-2007-6388|CVE 2007-6388]]
  * [[:content:en_us:announcements_cve_cve-2007-6420|CVE 2007-6420]]
  * [[:content:en_us:announcements_cve_cve-2007-6421|CVE 2007-6421]]
  * [[:content:en_us:announcements_cve_cve-2007-6422|CVE 2007-6422]]

==== 2006 ====
  * [[:content:en_us:announcements_cve_cve-2006-4110|CVE 2006-4110]]
  * [[:content:en_us:announcements_cve_cve-2006-4924|CVE 2006-4924]]
  * [[:content:en_us:announcements_cve_cve-2006-5051|CVE 2006-5051]]
  * [[:content:en_us:announcements_cve_cve-2006-5052|CVE 2006-5052]]
  * [[:content:en_us:announcements_cve_cve-2006-5752|CVE 2006-5752]]
  * [[:content:en_us:announcements_cve_cve-2006-5794|CVE 2006-5794]]

==== 2005 ====
  * [[:content:en_us:announcements_cve_cve-2005-2969|CVE 2005-2969]]

==== 2004 ====
  * [[:content:en_us:announcements_cve_cve-2004-0230|CVE 2004-0230]]
  * [[:content:en_us:announcements_cve_cve-2004-2320|CVE 2004-2320]]

==== 2003 ====
  * [[:content:en_us:announcements_cve_cve-2003-1567|CVE 2003-1567]]

==== 2002 ====
  * [[:content:en_us:announcements_cve_cve-2002-0510|CVE 2002-0510]]

==== 2001 ====

==== 2000 ====

==== 1999 ====
  * [[:content:en_us:announcements_cve_cve-1999-0519|CVE 1999-0519]]
  * [[:content:en_us:announcements_cve_cve-1999-0524|CVE 1999-0524]]

===== Non-CVE issues =====
Here is a list of security audit providers which may report additional, non-CVE issues. Refer to the following for additional defenses and answers to some common assessments:
  * [[:knowledgebase:securitymetrics|Security Metrics]]
  * [[:knowledgebase:rapid7|Rapid 7]]



===== Notice of Copyright for CVE Descriptions =====
The following notice exists for all CVE Descriptions which are quoted from the CVE Database:

==== Terms of Use ====
LICENSE
The MITRE Corporation (MITRE) hereby grants you a non-exclusive, royalty-free license to use Common Vulnerabilities and Exposures (CVE®) for research, development, and commercial purposes. Any copy you make for such purposes is authorized provided that you reproduce MITRE’s copyright designation and this license in any such copy.

DISCLAIMERS
ALL DOCUMENTS AND THE INFORMATION CONTAINED THEREIN ARE PROVIDED ON AN AS IS BASIS AND THE CONTRIBUTOR, THE ORGANIZATION HE/SHE REPRESENTS OR IS SPONSORED BY (IF ANY), THE MITRE CORPORATION, ITS BOARD OF TRUSTEES, OFFICERS, AGENTS, AND EMPLOYEES, DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTY THAT THE USE OF THE INFORMATION THEREIN WILL NOT INFRINGE ANY RIGHTS OR ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.

===== Notices =====
This section of support documentation that deals with vulnerabilities, CVEs, and named security audit providers is not to be construed as a complete and accurate representation of all known vulnerabilities to ClearOS, ClearCenter repository contents, community applications, and/or other closed and open source vulnerabilities. Rather the purpose is to provide information, help, and technical responses to active support and vulnerability challenges as they arise. If the information is incorrect or incomplete, please use the ClearCenter Support method that is associated with your license to request updates to particular issues and they will be modified here in response.

[[:content:en_us:announcements_cve_cve-template|.]]

{{keywords>clearos, clearos content, cve, menu, index, maintainer_dloper}}

