=== Summary: ===

<ul> "TAG_LOG_PKT <br>

</ul>


=== Impact: ===

<ul>Confidentiality Impact: COMPLETE Integrity Impact: COMPLETE Availability Impact: COMPLETE <br>

</ul>


=== Detailed Information: ===

<ul>Multiple stack-based buffer overflows in the pr_netio_telnet_gets function in netio.c in ProFTPD before 1.3.3c allow remote attackers to execute arbitrary code via vectors involving a TELNET IAC escape character to a (1) FTP or (2) FTPS server. <br>

</ul>


=== Affected Systems: ===

<ul>proftpd proftpd 1.3.2 <br>

proftpd proftpd 1.3.3 <br>

proftpd proftpd 1.3.2 d <br>

proftpd proftpd 1.3.2 b <br>

proftpd proftpd 1.3.2 rc3 <br>

proftpd proftpd 1.3.2 e <br>

proftpd proftpd 1.3.2 rc4 <br>

proftpd proftpd 1.3.3 rc3 <br>

proftpd proftpd 1.3.2 c <br>

proftpd proftpd 1.3.3 rc4 <br>

proftpd proftpd 1.3.3 b <br>

proftpd proftpd 1.3.3 rc1 <br>

proftpd proftpd 1.3.3 a <br>

proftpd proftpd 1.3.2 a <br>

proftpd proftpd 1.3.3 rc2 <br>

</ul>


=== Attack Scenarios: ===

<ul>No data available <br>

</ul>


=== False Positives: ===

<ul>None known <br>

</ul>


=== False Negatives: ===

<ul>None known <br>

</ul>


=== Corrective Action: ===

<ul>Upgrade to the latest non-affected version <br>

Apply vendor-provided patches <br>

</ul>


=== Contributors: ===

<ul>No data available <br>

</ul>


=== Additional References: ===

* [[http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-1999-0660|1999-0660]]

* [[http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1535|2009-1535]]
###ms09-020
<nowiki>
http://www.microsoft.com/technet/security/bulletin/ms09-020.mspx
</nowiki>

* [[http://docs.idsresearch.org/http_ids_evasions.pdf]]

* [[http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2001-0260|2001-0260]]

* [[http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2005-0560|2005-0560]]
###ms05-021
<nowiki>
http://www.microsoft.com/technet/security/bulletin/ms05-021.mspx
</nowiki>

* [[http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-3867|2010-3867]]

* [[http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-3972|2010-3972]]

* [[http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-4221|2010-4221]]

* [[http://www.microsoft.com/technet/security/bulletin/MS11-004.mspx|MS11-004]]

* [[http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2001-0554|2001-0554]]

* [[http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2002-0639|2002-0639]]

* [[http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2002-0640|2002-0640]]
###ms04-011
<nowiki>
http://technet.microsoft.com/en-us/security/bulletin/ms04-011
</nowiki>

* [[http://www.cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2004-0120|2004-0120]]