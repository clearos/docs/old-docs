===== App Development in ClearOS =====
This is the place to find specifications, technical notes and other notes on **ClearOS Apps**, including those under development.

===== Creating Apps =====
With the release of ClearOS 6.0, a new PHP-based application developer framework was launched.  

  * **[[:content:en_us:dev_framework_start|Get Started with the ClearOS App Framework!]]**

===== Maintaining Existing Apps =====
The following apps have been implemented in ClearOS. 

  * [[:content:en_us:dev_apps_base|Base System]]
  * [[:content:en_us:dev_apps_directory_manager|Directory Manager]]
  * [[:content:en_us:dev_apps_multi-wan|Multi-WAN]]
  * [[:content:en_us:dev_apps_password_policies|Password Policies]]
  * [[:content:en_us:dev_apps_qos|QoS]]
  * [[:content:en_us:dev_apps_radius|RADIUS]]
  * [[:content:en_us:dev_apps_samba|Samba]]
  * [[:content:en_us:dev_apps_web_proxy|Web Proxy]]

===== Proposed Apps =====
The following features are under development.

  * [[:content:en_us:dev_apps_ldap_dns|LDAP DNS]]
  * [[:content:en_us:dev_apps_samba4|Samba 4]]
  * [[:content:en_us:dev_apps_using_ad_for_openldap_user_authentication|Using AD for OpenLDAP User Authentication]]
  * [[:content:en_us:dev_apps_using_heartbeat_to_provide_fenced_clearbox|Using Heartbeat to provide fenced ClearBOX]]
{{keywords>clearos, clearos content, dev, apps, maintainer_dloper}}
